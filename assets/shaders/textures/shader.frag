#version 450 core

layout(set = 0, binding = 0) uniform sampler2D Texture2dDiffuse;

layout(location = 0) smooth in vec2 texCoord;

layout(location = 0) out vec4 color;
// "layout" is mandatory in Vulkan
// "location" is used to fit fragment shader output with the so called "attachment"

void main()
{
    color = texture(Texture2dDiffuse, texCoord);
}