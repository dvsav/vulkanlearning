#version 450 core

layout(push_constant) uniform ModelViewProjection {
    mat4 value;
} mvp;

layout(location = 0) in vec3 pos;
layout(location = 1) in vec2 uv;

layout(location = 0) smooth out vec2 texCoord;

void main()
{
    texCoord = uv;
    gl_Position = mvp.value * vec4(pos, 1.0);
}