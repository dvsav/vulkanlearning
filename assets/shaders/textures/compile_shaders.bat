Rem @echo off

Rem The advantage of glslc is that it uses the same parameter format as
Rem well-known compilers like GCC and Clang and includes some extra
Rem functionality like includes. Both of them are already included in the
Rem Vulkan SDK, so you don't need to download anything extra.
Rem (https://vulkan-tutorial.com/Drawing_a_triangle/Graphics_pipeline_basics/Shader_modules)
set GLCC=glslc.exe

%GLCC% shader.vert -o vert.spv
%GLCC% shader.frag -o frag.spv

pause
