#version 450 core

layout(location = 0) in vec3 pos;
layout(location = 1) in vec4 color;

layout(location = 0) smooth out vec4 fragColor;

void main()
{
    fragColor = color;
    gl_Position = vec4(pos, 1.0);
}