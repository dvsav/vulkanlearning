#version 450 core

layout(location = 0) smooth in vec4 fragColor;

layout(location = 0) out vec4 color;
// "layout" is mandatory in Vulkan
// "location" is used to fit fragment shader output with the so called "attachment"

void main()
{
    color = fragColor;
}