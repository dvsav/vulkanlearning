#version 450 core

layout(location = 0) in vec3 vs_color;
// "layout" is mandatory in Vulkan
// "location" is used to fit inputs and outputs of the shaders together

layout(location = 0) out vec4 color;
// "layout" is mandatory in Vulkan
// "location" is used to fit fragment shader output with the so called "attachment"

void main()
{
    color = vec4(vs_color, 1.0);
}