#version 450 core

vec3 positions[3] = vec3[](
    vec3(0.0, -0.4, 0.0),
    vec3(0.4, 0.4, 0.0),
    vec3(-0.4, 0.4, 0.0)
);

vec3 colors[3] = vec3[](
    vec3(1.0, 0.0, 0.0),
    vec3(0.0, 1.0, 0.0),
    vec3(0.0, 0.0, 1.0)
);

layout(location = 0) out vec3 vs_color;
// "layout" is mandatory in Vulkan
// "location" is used to fit inputs and outputs of the shaders together

void main()
{
    gl_Position = vec4(positions[gl_VertexIndex], 1.0);
    vs_color = colors[gl_VertexIndex];
}