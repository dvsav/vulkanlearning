#version 450 core

layout(push_constant) uniform ModelViewProjection {
    mat4 mvp; // model-view-projection
    mat4 mv;  // model-view
} MVP;

layout(location = 0) in vec3 pos;

layout(location = 0) smooth out float viewZ;

void main()
{
    viewZ = (MVP.mv * vec4(pos, 1.0)).z;
    gl_Position = MVP.mvp * vec4(pos, 1.0);
}