#version 450 core

layout(binding = 0) uniform Subpass {
    int value;
} subpass;

layout(input_attachment_index = 0, binding = 0) uniform subpassInput inputThickness;

layout(location = 0) smooth in float viewZ;

layout(location = 0) out float outputThickness;
// "layout" is mandatory in Vulkan
// "location" is used to fit fragment shader output with the so called "attachment"

void main()
{
    if (subpass == 1)      // rendering front faces
        outputThickness = viewZ;
    else if (subpass == 2) // rendering back faces
        outputThickness = viewZ - subpassLoad(inputDepth).r;
    else                   // bad subpass number
        outputThickness = 0.0f;
}