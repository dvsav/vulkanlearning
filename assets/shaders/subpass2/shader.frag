#version 450 core

layout(input_attachment_index = 0, binding = 0) uniform subpassInput inputColor; // Color output from Subpass 1
layout(input_attachment_index = 1, binding = 1) uniform subpassInput inputDepth; // Depth output from Subpass 1

layout(location = 0) out vec4 color;

void main()
{
    int xHalf = 1280 / 2;
    if (gl_FragCoord.x > xHalf)
    {
        float depth = subpassLoad(inputDepth).r;
        float gray = 1.0 - depth;
        color = vec4(gray, gray, gray, 1.0);
    }
    else
        color = subpassLoad(inputColor).rgba;
}