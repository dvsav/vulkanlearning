#version 450 core

// binding index is referenced in the Vulkan's descriptor layout object
layout(binding = 0) uniform ModelViewProjection {
    mat4 value;
} mvp;

layout(location = 0) in vec3 pos;
layout(location = 1) in vec4 color;

layout(location = 0) smooth out vec4 fragColor;

void main()
{
    fragColor = color;
    gl_Position = mvp.value * vec4(pos, 1.0);
}