/*
Copyright (c) 2020 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

#include <type_traits>
#include <cmath>
#include <array>
#include <iostream>
#include <iomanip>

#include <xmmintrin.h>

namespace vmath
{
    /*
    A vector
    Template parameters:
    T - type of vector coordinates
    Dimension - vector space dimension (number of coordinates in vector)
    */
    template<typename T, int Dimension>
    class VectorBase
    {
        #pragma region Typedefs

        public:
            using value_type = T;

        private:
            using inner_container = std::array<T, Dimension>;

        #pragma endregion

        #pragma region Fields

        public:
            static constexpr size_t components_number = Dimension;

        private:
#ifdef _ENABLE_EXTENDED_ALIGNED_STORAGE
            alignas(16) inner_container elems;
#else
            inner_container elems;
#endif

        #pragma endregion

        #pragma region Constructors

        public:
            VectorBase() noexcept
                : elems()
            {}

            explicit VectorBase(T value) noexcept
                : elems()
            {
                std::fill(elems.begin(), elems.end(), value);
            }

            // copy constructor
            VectorBase(const VectorBase& vec) noexcept
                : elems(vec.elems)
            {}

            // copy constructor
            template<typename U>
            explicit VectorBase(const VectorBase<U, Dimension>& vec) noexcept
                : elems()
            {
                std::copy(vec.cbegin(), vec.cend(), elems.begin());
            }

            VectorBase(std::initializer_list<T> lst) noexcept
                : elems()
            {
                std::copy(lst.begin(), lst.end(), elems.begin());
            }

            // move constructor
            VectorBase(VectorBase&& vec) noexcept
                : elems(std::move(vec.elems))
            {}

        #pragma endregion

        #pragma region Properties

        public:
            T& operator[](int i) noexcept
            {
                return elems[i];
            }

            const T& operator[](int i) const noexcept
            {
                return elems[i];
            }

            T& at(int i) noexcept
            {
                return elems.at(i);
            }

            const T& at(int i) const noexcept
            {
                return elems.at(i);
            }

            T* data() noexcept
            {
                return elems.data();
            }

            const T* data() const noexcept
            {
                return elems.data();
            }

            constexpr static int size()
            {
                return Dimension;
            }

            template<int SIZE>
            VectorBase& slice() noexcept { return *reinterpret_cast<VectorBase<T, SIZE>*>(this); }

            template<int SIZE>
            const VectorBase& slice() const noexcept
            {
                return *reinterpret_cast<VectorBase<T, SIZE>*>(const_cast<VectorBase<T, Dimension>*>(this));
            }

        #pragma endregion

        #pragma region Methods

        public:
            const VectorBase& operator=(const VectorBase& rhs) noexcept
            {
                std::copy(rhs.cbegin(), rhs.cend(), elems.begin());
                return *this;
            }

            const VectorBase& operator=(VectorBase&& rhs) noexcept
            {
                elems = std::move(rhs.elems);
                return *this;
            }

            template<typename U>
            const VectorBase& assign(const VectorBase<U, Dimension>& rhs) noexcept
            {
                std::copy(rhs.cbegin(), rhs.cend(), elems.begin());
                return *this;
            }

            typename inner_container::iterator begin() noexcept
            {
                return elems.begin();
            }

            typename inner_container::iterator end() noexcept
            {
                return elems.end();
            }

            typename inner_container::const_iterator begin() const noexcept
            {
                return elems.cbegin();
            }

            typename inner_container::const_iterator end() const noexcept
            {
                return elems.cend();
            }

            typename inner_container::const_iterator cbegin() const noexcept
            {
                return elems.cbegin();
            }

            typename inner_container::const_iterator cend() const noexcept
            {
                return elems.cend();
            }

        #pragma endregion
    };

    /*
    A vector
    Template parameters:
    T - type of vector coordinates
    Dimension - vector space dimension (number of coordinates in vector)
    */
    template<typename T, int Dimension>
    class Vector : public VectorBase<T, Dimension>
    {
    public:
        using VectorBase<T, Dimension>::VectorBase;

        template<typename U>
        const Vector& assign(const VectorBase<U, Dimension>& rhs) noexcept
        {
            return static_cast<Vector>(VectorBase<T, Dimension>::assign(rhs));
        }
    };

    /*
    A 2-dimensional vector
    Template parameters:
    T - type of vector coordinates
    */
    template<typename T>
    class Vector<T, 2> : public VectorBase<T, 2>
    {
    public:
        using VectorBase<T, 2>::VectorBase;

        Vector() noexcept {}

        Vector(T x, T y)
        {
            X() = x;
            Y() = y;
        }

        template<typename U>
        const Vector& assign(const VectorBase<U, 2>& rhs) noexcept
        {
            return static_cast<Vector>(VectorBase<T, 2>::assign(rhs));
        }

    public:
        T& X() noexcept { return this->at(0); }
        const T& X() const noexcept { return this->at(0); }

        T& Y() noexcept { return this->at(1); }
        const T& Y() const noexcept { return this->at(1); }
    };

    /*
    A 3-dimensional vector
    Template parameters:
    T - type of vector coordinates
    */
    template<typename T>
    class Vector<T, 3> : public VectorBase<T, 3>
    {
    public:
        using VectorBase<T, 3>::VectorBase;

        Vector() noexcept {}

        Vector(T x, T y, T z) noexcept
        {
            X() = x;
            Y() = y;
            Z() = z;
        }

        template<typename U>
        const Vector& assign(const VectorBase<U, 3>& rhs) noexcept
        {
            return static_cast<Vector>(VectorBase<T, 3>::assign(rhs));
        }

    public:
        T& X() noexcept { return this->at(0); }
        const T& X() const noexcept { return this->at(0); }

        T& Y() noexcept { return this->at(1); }
        const T& Y() const noexcept { return this->at(1); }

        T& Z() noexcept { return this->at(2); }
        const T& Z() const noexcept { return this->at(2); }

        Vector<T, 2>& vec2() noexcept { return *reinterpret_cast<Vector<T, 2>*>(this); }
        const Vector<T, 2>& vec2() const noexcept { return *reinterpret_cast<Vector<T, 2>*>(const_cast<Vector<T, 3>*>(this)); }
    };

    /*
    A 4-dimensional vector
    Template parameters:
    T - type of vector coordinates
    */
    template<typename T>
    class Vector<T, 4> : public VectorBase<T, 4>
    {
    public:
        using VectorBase<T, 4>::VectorBase;

        Vector() noexcept {}

        Vector(T x, T y, T z, T w) noexcept
        {
            X() = x;
            Y() = y;
            Z() = z;
            W() = w;
        }

        template<typename U>
        const Vector& assign(const VectorBase<U, 4>& rhs) noexcept
        {
            return static_cast<Vector>(VectorBase<T, 4>::assign(rhs));
        }

    public:
        T& X() noexcept { return this->at(0); }
        const T& X() const noexcept { return this->at(0); }

        T& Y() noexcept { return this->at(1); }
        const T& Y() const noexcept { return this->at(1); }

        T& Z() noexcept { return this->at(2); }
        const T& Z() const noexcept { return this->at(2); }

        T& W() noexcept { return this->at(3); }
        const T& W() const noexcept { return this->at(3); }

        Vector<T, 3>& vec3() noexcept { return *reinterpret_cast<Vector<T, 3>*>(this); }
        const Vector<T, 3>& vec3() const noexcept { return *reinterpret_cast<Vector<T, 3>*>(const_cast<Vector<T, 4>*>(this)); }
    };

    template<typename T>
    Vector<T, 2> make_Vector(T x, T y) noexcept
    {
        return Vector<T, 2>(x, y);
    }

    template<typename T>
    Vector<T, 3> make_Vector(T x, T y, T z) noexcept
    {
        return Vector<T, 3>(x, y, z);
    }

    template<typename T>
    Vector<T, 4> make_Vector(T x, T y, T z, T w) noexcept
    {
        return Vector<T, 4>(x, y, z, w);
    }

    // Outputs vector to a stream in user-friendly style (as a vertical column)
    template <typename T, int Dimension>
    std::ostream& print(
        std::ostream& os,
        const VectorBase<T, Dimension>& vec) noexcept
    {
        for (const auto& el : vec)
            os << el << std::endl;
        return os;
    }

    template<typename T, int Dimension>
    std::ostream& operator<<(
        std::ostream& os,
        const VectorBase<T, Dimension>& vec) noexcept
    {
        auto width = os.width();
        os << std::setw(1) << "{ " << std::setw(width) << std::left << vec[0];
        os << std::right;
        for (int i = 1; i < vec.size(); i++)
            os << ", " << std::setw(width) << vec[i];
        os << " }";
        return os;
    }

    template<typename T, int Dimension>
    std::istream& operator>>(
        std::istream& is,
        VectorBase<T, Dimension>& vec) noexcept
    {
        char ch;
        if (is >> ch && ch == '{')
        {
            is >> vec[0];
            for (int i = 1; i < vec.size(); i++)
            {
                if (!(is >> ch && ch == ',' && is >> vec[i]))
                {
                    is.clear(std::ios_base::failbit);
                    return is;
                }
            }

            if (!(is >> ch && ch == '}'))
                is.clear(std::ios_base::failbit);
        }
        else
            is.clear(std::ios_base::failbit);

        return is;
    }

    template<typename T, typename V, int Dimension>
    const Vector<decltype(std::declval<T>() + std::declval<V>()), Dimension> operator+(
        const Vector<T, Dimension>& lhs,
        const Vector<V, Dimension>& rhs) noexcept
    {
        Vector<decltype(std::declval<T>() + std::declval<V>()), Dimension> vec;
        for (int i = 0; i < Dimension; i++)
            vec[i] = lhs[i] + rhs[i];
        return vec;
    }

    template<typename T, typename V, int Dimension>
    const Vector<decltype(std::declval<T>() - std::declval<V>()), Dimension> operator-(
        const Vector<T, Dimension>& lhs,
        const Vector<V, Dimension>& rhs) noexcept
    {
        Vector<decltype(std::declval<T>() - std::declval<V>()), Dimension> vec;
        for (int i = 0; i < Dimension; i++)
            vec[i] = lhs[i] - rhs[i];
        return vec;
    }

    template<typename T, typename V, int Dimension>
    const Vector<decltype(std::declval<T>() * std::declval<V>()), Dimension> operator*(
        const Vector<T, Dimension>& lhs,
        const Vector<V, Dimension>& rhs) noexcept
    {
        Vector<decltype(std::declval<T>() * std::declval<V>()), Dimension> vec;
        for (int i = 0; i < Dimension; i++)
            vec[i] = lhs[i] * rhs[i];
        return vec;
    }

    template<typename T, typename V, int Dimension>
    const Vector<decltype(std::declval<T>() / std::declval<V>()), Dimension> operator/(
        const Vector<T, Dimension>& lhs,
        const Vector<V, Dimension>& rhs) noexcept
    {
        Vector<decltype(std::declval<T>() / std::declval<V>()), Dimension> vec;
        for (int i = 0; i < Dimension; i++)
            vec[i] = lhs[i] / rhs[i];
        return vec;
    }

    template<typename T, typename V, int Dimension>
    Vector<T, Dimension>& operator+=(
        Vector<T, Dimension>& lhs,
        const Vector<V, Dimension>& rhs) noexcept
    {
        for (int i = 0; i < Dimension; i++)
            lhs[i] += rhs[i];
        return lhs;
    }

    template<typename T, typename V, int Dimension>
    Vector<T, Dimension>& operator-=(
        Vector<T, Dimension>& lhs,
        const Vector<V, Dimension>& rhs) noexcept
    {
        for (int i = 0; i < Dimension; i++)
            lhs[i] -= rhs[i];
        return lhs;
    }

    template<typename T, typename V, int Dimension>
    Vector<T, Dimension>& operator*=(
        Vector<T, Dimension>& lhs,
        const Vector<V, Dimension>& rhs) noexcept
    {
        for (int i = 0; i < Dimension; i++)
            lhs[i] *= rhs[i];
        return lhs;
    }

    template<typename T, typename V, int Dimension>
    Vector<T, Dimension>& operator/=(
        Vector<T, Dimension>& lhs,
        const Vector<V, Dimension>& rhs) noexcept
    {
        for (int i = 0; i < Dimension; i++)
            lhs[i] /= rhs[i];
        return lhs;
    }

    template<typename T, typename V, int Dimension>
    Vector<T, Dimension>& operator*=(
        Vector<T, Dimension>& lhs,
        const V rhs) noexcept
    {
        for (int i = 0; i < Dimension; i++)
            lhs[i] *= rhs;
        return lhs;
    }

    template<typename T, typename V, int Dimension>
    Vector<T, Dimension>& operator/=(
        Vector<T, Dimension>& lhs,
        const V rhs) noexcept
    {
        for (int i = 0; i < Dimension; i++)
            lhs[i] /= rhs;
        return lhs;
    }

    template<typename T, typename V, int Dimension>
    const Vector<decltype(std::declval<T>()*std::declval<V>()), Dimension> operator*(
        const T lhs,
        const Vector<V, Dimension>& rhs) noexcept
    {
        Vector<decltype(std::declval<T>()*std::declval<V>()), Dimension> vec;
        for (int i = 0; i < Dimension; i++)
            vec[i] = lhs * rhs[i];
        return vec;
    }

    template<typename T, typename V, int Dimension>
    const Vector<decltype(std::declval<T>()*std::declval<V>()), Dimension> operator*(
        const Vector<T, Dimension>& rhs,
        const V lhs) noexcept
    {
        Vector<decltype(std::declval<T>()*std::declval<V>()), Dimension> vec;
        for (int i = 0; i < Dimension; i++)
            vec[i] = rhs[i] * lhs;
        return vec;
    }

    template<typename T, typename V, int Dimension>
    const Vector<decltype(std::declval<T>()/std::declval<V>()), Dimension> operator/(
        const Vector<T, Dimension>& rhs,
        const V lhs) noexcept
    {
        Vector<decltype(std::declval<T>()/std::declval<V>()), Dimension> vec;
        for (int i = 0; i < Dimension; i++)
            vec[i] = rhs[i] / lhs;
        return vec;
    }

    // unary 'negate' operator
    template<typename T, int Dimension>
    const Vector<T, Dimension> operator-(const Vector<T, Dimension>& val) noexcept
    {
        Vector<T, Dimension> vec;
        for (int i = 0; i < Dimension; i++)
            vec[i] = -val[i];
        return vec;
    }

    // Calculates the cross product of two vectors.
    // Note that the direction of cross product depends on the handedness (right-handed or left-handed) of the coordinate system in use:
    // if the the coordinate system is right-handed the direction of cross product is defined by the right hand rule (clockwise);
    // if the the coordinate system is left-handed the direction of cross product is defined by the left hand rule (counterclockwise);
    template<typename T>
    const Vector<T, 3> cross_product(
        const Vector<T, 3>& lhs,
        const Vector<T, 3>& rhs) noexcept
    {
        return Vector<T, 3>
        {
            lhs.Y() * rhs.Z() - rhs.Y() * lhs.Z(),
           -lhs.X() * rhs.Z() + rhs.X() * lhs.Z(),
            lhs.X() * rhs.Y() - rhs.X() * lhs.Y()
        };
    }

    template<typename T, int Dimension>
    T dot_product(
        const Vector<T, Dimension>& lhs,
        const Vector<T, Dimension>& rhs) noexcept
    {
        T sum = 0;
        for (int i = 0; i < Dimension; i++)
            sum = sum + lhs[i] * rhs[i];
        return sum;
    }

    template<typename T, int Dimension>
    T length(const Vector<T, Dimension>& vec) noexcept
    {
        return sqrt(dot_product(vec, vec));
    }

    template<typename T, int Dimension>
    Vector<T, Dimension> normalize(const Vector<T, Dimension>& vec) noexcept
    {
        return vec / length(vec);
    }

    template<typename T>
    Vector<T, 4> eliminateW(const Vector <T, 4>& vec) noexcept
    {
        Vector <T, 4> newVec(vec);
        newVec[3] = T(0);
        return newVec;
    }

    template<typename T>
    Vector<T, 4> eliminateW(const Vector <T, 4>&& vec) noexcept
    {
        vec[3] = T(0);
        return std::move(vec);
    }

// Below are functions utilizing Intel's Streaming SIMD Extensions (SSE)
#ifdef USE_SSE

    inline const Vector<float, 4> operator+(
        const Vector<float, 4>& lhs,
        const Vector<float, 4>& rhs) noexcept
    {
        auto a = _mm_load_ps(lhs.data());
        auto b = _mm_load_ps(rhs.data());

        auto c = _mm_add_ps(a, b);

        __declspec(align(16)) Vector<float, 4> vec;
        _mm_store_ps(vec.data(), c);

        return vec;
    }

    inline const Vector<float, 4> operator-(
        const Vector<float, 4>& lhs,
        const Vector<float, 4>& rhs) noexcept
    {
        auto a = _mm_load_ps(lhs.data());
        auto b = _mm_load_ps(rhs.data());

        auto c = _mm_sub_ps(a, b);

        __declspec(align(16)) Vector<float, 4> vec;
        _mm_store_ps(vec.data(), c);

        return vec;
    }

    inline const Vector<float, 4> operator*(
        const Vector<float, 4>& lhs,
        const Vector<float, 4>& rhs) noexcept
    {
        auto a = _mm_load_ps(lhs.data());
        auto b = _mm_load_ps(rhs.data());

        auto c = _mm_mul_ps(a, b);

        __declspec(align(16)) Vector<float, 4> vec;
        _mm_store_ps(vec.data(), c);

        return vec;
    }

    inline const Vector<float, 4> operator/(
        const Vector<float, 4>& lhs,
        const Vector<float, 4>& rhs) noexcept
    {
        auto a = _mm_load_ps(lhs.data());
        auto b = _mm_load_ps(rhs.data());

        auto c = _mm_div_ps(a, b);

        __declspec(align(16)) Vector<float, 4> vec;
        _mm_store_ps(vec.data(), c);

        return vec;
    }

#endif
}