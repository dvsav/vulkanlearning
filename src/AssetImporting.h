#pragma once

#include "ConditionalCompilation.h"

#include <vector>
#include <tuple>

// Assimp
#pragma comment(lib, "assimp/assimp-vc141-mt.lib")
#include "assimp/Importer.hpp"
#include "assimp/scene.h"
#include "assimp/postprocess.h"

// Engine
#include "Vertex.h"
#include "Mesh.h"

namespace engine
{
    template <typename TVertex, typename TIndex = size_t>
    inline MeshInfo<TVertex, TIndex> import_mesh(
        const std::string& filename,
        const vec3& scale);

    template<>
    inline MeshInfo<VertexPT, uint16_t> import_mesh<VertexPT, uint16_t>(
        const std::string& filename,
        const vec3& scale)
    {
        // Create an instance of the Importer class
        Assimp::Importer importer;

        // And have it read the given file with some example postprocessing
        // Usually - if speed is not the most important aspect for you - you'll
        // probably to request more postprocessing than we do in this example.
        const aiScene* scene = importer.ReadFile(
            filename,
            aiProcess_MakeLeftHanded |
            aiProcess_Triangulate |
            aiProcess_JoinIdenticalVertices |
            aiProcess_FlipUVs |
            aiProcess_SortByPType);

        // If the import failed
        if (!scene)
            throw std::runtime_error("engine::import_model: " + filename);

        // Now we can access the file's contents.
        unsigned int iMesh = 0;
        {
            auto& mesh = *scene->mMeshes[iMesh];

            std::vector<VertexPT> vertices;
            vertices.reserve(mesh.mNumVertices);
            for (unsigned int iVertex = 0; iVertex < mesh.mNumVertices; iVertex++)
            {
                vertices.push_back(
                    VertexPT
                    {
                        vec3{ mesh.mVertices[iVertex].x, mesh.mVertices[iVertex].y, mesh.mVertices[iVertex].z } * scale,
                        vec2{ mesh.mTextureCoords[0][iVertex].x, mesh.mTextureCoords[0][iVertex].y },
                    });
            }

            std::vector<uint16_t> indices;
            indices.reserve(mesh.mNumFaces * 3);
            for (unsigned int iFace = 0; iFace < mesh.mNumFaces; iFace++)
            {
                indices.insert(
                    indices.end(),
                    &mesh.mFaces[iFace].mIndices[0],
                    &mesh.mFaces[iFace].mIndices[mesh.mFaces[iFace].mNumIndices]);
            }

            return MeshInfo
            {
                /*Vertices*/ vertices,
                /*Indices*/ indices,
                /*Topology*/ PrimitiveTopologyEnum::TriangleList
            };
        }
    }

    template<>
    inline MeshInfo<VertexPNT, uint16_t> import_mesh<VertexPNT, uint16_t>(
        const std::string& filename,
        const vec3& scale)
    {
        // Create an instance of the Importer class
        Assimp::Importer importer;

        // And have it read the given file with some example postprocessing
        // Usually - if speed is not the most important aspect for you - you'll
        // probably to request more postprocessing than we do in this example.
        const aiScene* scene = importer.ReadFile(
            filename,
            aiProcess_MakeLeftHanded |
            aiProcess_Triangulate |
            aiProcess_JoinIdenticalVertices |
            aiProcess_GenNormals |
            aiProcess_FlipUVs |
            aiProcess_SortByPType);

        // If the import failed
        if (!scene)
            throw std::runtime_error("engine::import_model: " + filename);

        // Now we can access the file's contents.
        unsigned int iMesh = 0;
        {
            auto& mesh = *scene->mMeshes[iMesh];

            std::vector<VertexPNT> vertices;
            vertices.reserve(mesh.mNumVertices);

            if (scale.X() == scale.Y() && scale.X() == scale.Z())
            {
                for (unsigned int iVertex = 0; iVertex < mesh.mNumVertices; iVertex++)
                {
                    vertices.push_back(
                        VertexPNT
                        {
                            vec3{ mesh.mVertices[iVertex].x, mesh.mVertices[iVertex].y, mesh.mVertices[iVertex].z } *scale,
                            vec3{ mesh.mNormals[iVertex].x, mesh.mNormals[iVertex].y, mesh.mNormals[iVertex].z },
                            vec2{ mesh.mTextureCoords[0][iVertex].x, mesh.mTextureCoords[0][iVertex].y }
                        });
                }
            }
            else
            {
                for (unsigned int iVertex = 0; iVertex < mesh.mNumVertices; iVertex++)
                {
                    vertices.push_back(
                        VertexPNT
                        {
                            vec3{ mesh.mVertices[iVertex].x, mesh.mVertices[iVertex].y, mesh.mVertices[iVertex].z } *scale,
                            normalize(vec3{ mesh.mNormals[iVertex].x, mesh.mNormals[iVertex].y, mesh.mNormals[iVertex].z } / scale),
                            vec2{ mesh.mTextureCoords[0][iVertex].x, mesh.mTextureCoords[0][iVertex].y }
                        });
                }
            }

            std::vector<uint16_t> indices;
            indices.reserve(mesh.mNumFaces * 3);
            for (unsigned int iFace = 0; iFace < mesh.mNumFaces; iFace++)
            {
                indices.insert(
                    indices.end(),
                    &mesh.mFaces[iFace].mIndices[0],
                    &mesh.mFaces[iFace].mIndices[mesh.mFaces[iFace].mNumIndices]);
            }

            return MeshInfo
            {
                /*Vertices*/ vertices,
                /*Indices*/ indices,
                /*Topology*/ PrimitiveTopologyEnum::TriangleList
            };
        }
    }

    template<>
    inline MeshInfo<VertexPNTT, uint16_t> import_mesh<VertexPNTT, uint16_t>(
        const std::string& filename,
        const vec3& scale)
    {
        // Create an instance of the Importer class
        Assimp::Importer importer;

        // And have it read the given file with some example postprocessing
        // Usually - if speed is not the most important aspect for you - you'll
        // probably to request more postprocessing than we do in this example.
        const aiScene* scene = importer.ReadFile(
            filename,
            aiProcess_MakeLeftHanded |
            aiProcess_CalcTangentSpace |
            aiProcess_Triangulate |
            aiProcess_JoinIdenticalVertices |
            aiProcess_GenNormals |
            aiProcess_FlipUVs |
            aiProcess_SortByPType);

        // If the import failed
        if (!scene)
            throw std::runtime_error("engine::import_model: " + filename);

        // Now we can access the file's contents.
        unsigned int iMesh = 0;
        {
            auto& mesh = *scene->mMeshes[iMesh];

            std::vector<VertexPNTT> vertices;
            vertices.reserve(mesh.mNumVertices);

            if (scale.X() == scale.Y() && scale.X() == scale.Z())
            {
                for (unsigned int iVertex = 0; iVertex < mesh.mNumVertices; iVertex++)
                {
                    vertices.push_back(
                        VertexPNTT
                        {
                            vec3{ mesh.mVertices[iVertex].x, mesh.mVertices[iVertex].y, mesh.mVertices[iVertex].z } * scale,
                            vec3{ mesh.mNormals[iVertex].x, mesh.mNormals[iVertex].y, mesh.mNormals[iVertex].z },
                            vec2{ mesh.mTextureCoords[0][iVertex].x, mesh.mTextureCoords[0][iVertex].y },
                            vec4{ mesh.mTangents[iVertex].x, mesh.mTangents[iVertex].y, mesh.mTangents[iVertex].z, -1.0f }
                        });
                }
            }
            else
            {
                for (unsigned int iVertex = 0; iVertex < mesh.mNumVertices; iVertex++)
                {
                    vertices.push_back(
                        VertexPNTT
                        {
                            vec3{ mesh.mVertices[iVertex].x, mesh.mVertices[iVertex].y, mesh.mVertices[iVertex].z } *scale,
                            normalize(vec3{ mesh.mNormals[iVertex].x, mesh.mNormals[iVertex].y, mesh.mNormals[iVertex].z } / scale),
                            vec2{ mesh.mTextureCoords[0][iVertex].x, mesh.mTextureCoords[0][iVertex].y },
                            vec4{ mesh.mTangents[iVertex].x, mesh.mTangents[iVertex].y, mesh.mTangents[iVertex].z, -1.0f }
                        });
                }
            }

            std::vector<uint16_t> indices;
            indices.reserve(mesh.mNumFaces * 3);
            for (unsigned int iFace = 0; iFace < mesh.mNumFaces; iFace++)
            {
                indices.insert(
                    indices.end(),
                    &mesh.mFaces[iFace].mIndices[0],
                    &mesh.mFaces[iFace].mIndices[mesh.mFaces[iFace].mNumIndices]);
            }

            return MeshInfo
            {
                /*Vertices*/ vertices,
                /*Indices*/ indices,
                /*Topology*/ PrimitiveTopologyEnum::TriangleList
            };
        }
    }
}