/*
Copyright (c) 2020 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

#include <map>
#include <string>
#include <tuple>
#include <windows.h>

#include "winapi_error.h"
#include "Event.h"

namespace win
{
    // Base class for a window
    class Window
    {
        #pragma region Typedefs

        public:
            using MOUSE_EVENT_DELEGATE = util::Event<std::tuple<Window*, int, int, WPARAM>>;
            using KEYBOARD_EVENT_DELEGATE = util::Event<std::tuple<Window*, WPARAM>>;
            using SIZECHANGED_EVENT_DELEGATE = util::Event<std::tuple<Window*, int, int>>;
            using PAINT_EVENT_DELEGATE = util::Event<std::tuple<Window*, HDC>>;

        #pragma endregion

        #pragma region Fields

        private:
            static WNDCLASSEX s_WndClassEx;                     // WinAPI window class structure
            static std::map<HWND, Window*> s_RegisteredWindows; // collection of existing windows

            HWND m_hWnd; // window descriptor
            HDC m_hDC;   // window client area device context descriptor

            MOUSE_EVENT_DELEGATE m_MouseMoveEvent;
            MOUSE_EVENT_DELEGATE m_MouseLeftButtonDownEvent;
            MOUSE_EVENT_DELEGATE m_MouseLeftButtonUpEvent;
            KEYBOARD_EVENT_DELEGATE m_KeyDownEvent;
            KEYBOARD_EVENT_DELEGATE m_KeyUpEvent;
            PAINT_EVENT_DELEGATE m_PaintEvent;
            SIZECHANGED_EVENT_DELEGATE m_SizeChangedEvent;

        #pragma endregion

        #pragma region Constructors

        public:
            /*
            Constructor
            parameters:
            title - window title
            width - window width
            height - window height
            */
            Window(const std::string& title,
                   int width,
                   int height);

        #pragma endregion

        #pragma region Destructor

        public:
            virtual ~Window(); // a class assumed to serve as a base class for other classes must have a virtual destructor

        #pragma endregion

        #pragma region Deleted Functions

        private:
            Window(const Window&) = delete;
            Window& operator=(const Window&) = delete;

        #pragma endregion

        #pragma region Properties

        public:
            // Gets window handle.
            HWND getWindowHandle() const
            {
                return m_hWnd;
            }

            // Gets the device context handle for the window's client area.
            HDC getDeviceContext() const
            {
                return m_hDC;
            }

            // Gets client area rectangle.
            RECT getClientRect() const
            {
                RECT rect;
                ::GetClientRect(m_hWnd, &rect);
                return rect;
            }

            long getClientWidth() const
            {
                auto clientRect = getClientRect();
                return clientRect.right - clientRect.left;
            }

            long getClientHeight() const
            {
                auto clientRect = getClientRect();
                return clientRect.bottom - clientRect.top;
            }

            // Sets client area size.
            void setClientSize(int width, int height);

            // Gets window rectangle.
            RECT getWindowRect() const
            {
                RECT rect;
                ::GetWindowRect(m_hWnd, &rect);
                return rect;
            }

            // Sets window size.
            void setWindowSize(int width, int height);

            // Sets the position of window's top left corner.
            void setPosition(int left, int top);

            // Gets window title.
            const std::string getTitle() const
            {
                TCHAR buffer[256];
                ::GetWindowText(m_hWnd, buffer, 255);
                return std::string(buffer);
            }

            // Sets window title.
            void setTitle(const std::string& value)
            {
                ::SetWindowText(m_hWnd, value.c_str());
            }

        #pragma endregion

        #pragma region Events

        public:
            MOUSE_EVENT_DELEGATE& MouseMoveEvent() { return m_MouseMoveEvent; }
            MOUSE_EVENT_DELEGATE& MouseLeftButtonDownEvent() { return m_MouseLeftButtonDownEvent; }
            MOUSE_EVENT_DELEGATE& MouseLeftButtonUpEvent() { return m_MouseLeftButtonUpEvent; }
            KEYBOARD_EVENT_DELEGATE& KeyDownEvent() { return m_KeyDownEvent; }
            KEYBOARD_EVENT_DELEGATE& KeyUpEvent() { return m_KeyUpEvent; }
            PAINT_EVENT_DELEGATE& PaintEvent() { return m_PaintEvent; }
            SIZECHANGED_EVENT_DELEGATE& SizeChangedEvent() { return m_SizeChangedEvent; }

        #pragma endregion

        #pragma region Methods

        public:
            // Shows the window on the screen.
            void Show()
            {
                // show window and fill its client area with background brush
                ShowWindow(
                    m_hWnd,         // window handle
                    SW_SHOWNORMAL); // show options

                // redraw window
                if (UpdateWindow(m_hWnd) == 0)
                    throw make_winapi_error("Window::Show() -> UpdateWindow()");
            }

            // Closes the window.
            void Close()
            {
                auto result = ::DestroyWindow(m_hWnd);
                if (result == 0)
                {
                    throw make_winapi_error("Window::Close() -> DestroyWindow()");
                }
            }

            // Minimizes the window.
            void Minimize()
            {
                auto result = ::CloseWindow(m_hWnd);
                if (result == 0)
                {
                    throw make_winapi_error("Window::Minimize() -> CloseWindow()");
                }
            }

            // Sets cursor position relative to the window's client area.
            void setCursorPosition(int xClient, int yClient)
            {
                POINT pt;
                pt.x = xClient;
                pt.y = yClient;
                ClientToScreen(m_hWnd, &pt);
                SetCursorPos(pt.x, pt.y);
            }

        private:
            static void InitWndClass();

            static LRESULT CALLBACK WndProc(
                HWND hWnd,
                UINT message,
                WPARAM wParam,
                LPARAM lParam);

            LRESULT WindowProcedure(
                UINT message,
                WPARAM wParam,
                LPARAM lParam);

        protected:
            virtual void OnDestroy()
            {}

            virtual void OnPaint(HDC dc)
            {
                m_PaintEvent(std::make_tuple(this, dc));
            };

            virtual void OnMouseMove(int x, int y, WPARAM virtualKeyCode)
            {
                m_MouseMoveEvent(std::make_tuple(this, x, y, virtualKeyCode));
            };

            virtual void OnMouseLeftButtonDown(int x, int y, WPARAM virtualKeyCode)
            {
                m_MouseLeftButtonDownEvent(std::make_tuple(this, x, y, virtualKeyCode));
            };

            virtual void OnMouseLeftButtonUp(int x, int y, WPARAM virtualKeyCode)
            {
                m_MouseLeftButtonUpEvent(std::make_tuple(this, x, y, virtualKeyCode));
            };

            virtual void OnKeyDown(WPARAM virtualKeyCode)
            {
                m_KeyDownEvent(std::make_tuple(this, virtualKeyCode));
            };

            virtual void OnKeyUp(WPARAM virtualKeyCode)
            {
                m_KeyUpEvent(std::make_tuple(this, virtualKeyCode));
            };

            virtual void OnCommand(int childControlId)
            {};

            virtual void OnSizeChanged(int width, int height)
            {
                m_SizeChangedEvent(std::make_tuple(this, width, height));
            };

            virtual void OnTimer(UINT_PTR timerIdentifier)
            {};

        #pragma endregion
    };

    inline void getClientSize(
        const Window& window,
        int& w, int& h)
    {
        auto client_rect = window.getClientRect();
        w = client_rect.right - client_rect.left;
        h = client_rect.bottom - client_rect.top;
    }
}