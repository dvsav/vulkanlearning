#pragma once

#include "ConditionalCompilation.h"

// STL
#include <vector>

// Engine
#include "Mesh.h"

// VkEngine
#include "GpuBuffer.h"

namespace vulkan
{
    template <typename TVertex, typename TIndex = size_t>
    class VkMesh final : public engine::Mesh<TVertex, TIndex>
    {
        #pragma region Typedefs

        public:
            using vertex_type = TVertex;
            using index_type = TIndex;

        #pragma endregion

        #pragma region Fields

        private:
            GpuBuffer m_VertexBuffer;
            GpuBuffer m_IndexBuffer;
            bool m_IsDestroyed;

        #pragma endregion

        #pragma region Constructors

        public:
            VkMesh(
                const VkContext& vkContext,
                PrimitiveTopologyEnum primitiveTopology,
                size_t vertexCount,
                size_t indexCount) :

                engine::Mesh<vertex_type, index_type>(
                    primitiveTopology,
                    vertexCount,
                    indexCount),

                m_VertexBuffer(
                    vkContext,
                    /*bufferSize*/ vertexCount * sizeof(vertex_type),
                    /*usage*/ vk::BufferUsageFlagBits::eVertexBuffer),

                m_IndexBuffer(
                    vkContext,
                    /*bufferSize*/ indexCount * sizeof(index_type),
                    /*usage*/ vk::BufferUsageFlagBits::eIndexBuffer),

                m_IsDestroyed(false)
            {}

            VkMesh(
                const VkContext& vkContext,
                PrimitiveTopologyEnum primitiveTopology,
                const std::vector<vertex_type>& vertices,
                const std::vector<index_type>& indices) :

                engine::Mesh<vertex_type, index_type>(
                    primitiveTopology,
                    vertices.size(),
                    indices.size()),

                m_VertexBuffer(
                    vkContext,
                    /*bufferSize*/ vertices.size() * sizeof(vertex_type),
                    /*usage*/ vk::BufferUsageFlagBits::eVertexBuffer),

                m_IndexBuffer(
                    vkContext,
                    /*bufferSize*/ indices.size() * sizeof(index_type),
                    /*usage*/ vk::BufferUsageFlagBits::eIndexBuffer),

                m_IsDestroyed(false)
            {
                setVertices(
                    vertices.data(),
                    0,
                    vertices.size());

                setIndices(
                    indices.data(),
                    0,
                    indices.size());
            }

            VkMesh(VkMesh&& rvalue) noexcept :
                engine::Mesh<vertex_type, index_type>(std::move(rvalue)),
                m_VertexBuffer(std::move(rvalue.m_VertexBuffer)),
                m_IndexBuffer(std::move(rvalue.m_IndexBuffer)),
                m_IsDestroyed(rvalue.m_IsDestroyed)
            {
                rvalue.m_IsDestroyed = true;
            }

        #pragma endregion

        #pragma region Destructor

        public:
            void Destroy()
            {
                if (m_IsDestroyed)
                    return;
                else
                    m_IsDestroyed = true;

                m_VertexBuffer.Destroy();
                m_IndexBuffer.Destroy();
            }

            ~VkMesh()
            {
                Destroy();
            }

        #pragma endregion

        #pragma region Properties

        public:
            const vertex_type getVertex(size_t nVertex) const override
            {
                vertex_type vert{};
                const_cast<GpuBuffer&>(m_VertexBuffer).getData(
                    /*destination */ &vert,
                    /*offset*/ nVertex * sizeof(vertex_type),
                    /*size*/ sizeof(vertex_type));
                return vert;
            }

            void setVertex(size_t nVertex, const vertex_type& vertex) override
            {
                m_VertexBuffer.setData(
                    /*destination */ &vertex,
                    /*offset*/ nVertex * sizeof(vertex_type),
                    /*size*/ sizeof(vertex_type));
            }

            void setVertices(const vertex_type* vertices, size_t nFirstVertex, size_t nVertices) override
            {
                m_VertexBuffer.setData(
                    /*destination */ vertices,
                    /*offset*/ nFirstVertex * sizeof(vertex_type),
                    /*size*/ nVertices * sizeof(vertex_type));
            }

            index_type getIndex(size_t nIndex) const override
            {
                index_type ind{};
                const_cast<GpuBuffer&>(m_IndexBuffer).getData(
                    /*destination */ &ind,
                    /*offset*/ nIndex * sizeof(index_type),
                    /*size*/ sizeof(index_type));
                return ind;
            }

            void setIndex(size_t nIndex, index_type index) override
            {
                m_IndexBuffer.setData(
                    /*destination */ &index,
                    /*offset*/ nIndex * sizeof(index_type),
                    /*size*/ sizeof(index_type));
            }

            void setIndices(const index_type* indices, size_t nFirstIndex, size_t nIndices) override
            {
                m_IndexBuffer.setData(
                    /*destination */ indices,
                    /*offset*/ nFirstIndex * sizeof(index_type),
                    /*size*/ nIndices * sizeof(index_type));
            }

        public:
            const GpuBuffer& VertexBuffer() const { return m_VertexBuffer; }
            const GpuBuffer& IndexBuffer() const { return m_IndexBuffer; }

            vk::PrimitiveTopology VkPrimitiveTopology() const
            {
                auto topology = Mesh::PrimitiveTopology();

                switch (topology)
                {
                case PrimitiveTopologyEnum::TriangleList:
                    return vk::PrimitiveTopology::eTriangleList;

                case PrimitiveTopologyEnum::TriangleStrip:
                    return vk::PrimitiveTopology::eTriangleStrip;

                case PrimitiveTopologyEnum::TriangleFan:
                    return vk::PrimitiveTopology::eTriangleFan;

                default:
                    throw std::runtime_error(FUNCTION_INFO);
                    break;
                }
            }

        #pragma endregion
    };
}