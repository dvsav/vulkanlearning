#pragma once

#include "ConditionalCompilation.h"

// STL
#include <vector>

// Utility
#include "utility.h"
#include "VkUtility.h"
#include "VkContext.h"

// VkEngine
#include "HostVisibleBuffer.h"

namespace vulkan
{
    // Represents a buffer object that stores its data in memory that is visible to the host CPU.
    // Such kind of buffer filts well to the data that need to be changed frequently by the host CPU.
    class UniformBuffer : public HostVisibleBuffer
    {
        #pragma region Constructors

        public:
            UniformBuffer(
                const VkContext& vkContext,
                size_t bufferSize) :
                HostVisibleBuffer(
                    vkContext,
                    bufferSize,
                    /*usage*/ vk::BufferUsageFlagBits::eUniformBuffer)
            {}

        #pragma endregion
    };

    template<typename T>
    class UniformValue final : public UniformBuffer
    {
        #pragma region Typedefs

        public:
            using value_type = T;

        #pragma endregion

        #pragma region Constructors

        public:
            UniformValue(
                const VkContext& vkContext) :
                UniformBuffer(vkContext, sizeof(value_type))
            {}

        #pragma endregion

        #pragma region Methods

        public:
            void set(const value_type& value)
            {
                UniformBuffer::set(
                    0, // offset,
                    sizeof(value_type), // size
                    &value); // srcData
            }

        #pragma endregion
    };
}