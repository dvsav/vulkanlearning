#include "Program.h"

// This creates the so called defaultDispatchLoaderDynamic
// a facility which loads Vulkan extension functions
// (finds and stores their addresses)
VULKAN_HPP_DEFAULT_DISPATCH_LOADER_DYNAMIC_STORAGE

namespace vulkan
{
    std::vector<const char*> VkGraphicsContext::DEVICE_EXTENSIONS
    {
        // Not all graphics cards are capable of presenting images directly to a screen for various reasons,
        // for example because they are designed for servers and don't have any display outputs. Secondly,
        // since image presentation is heavily tied into the window system and the surfaces associated with
        // windows, it is not actually part of the Vulkan core. You have to enable the VK_KHR_swapchain device
        // extension after querying for its support.
        // Source: https://vulkan-tutorial.com/en/Drawing_a_triangle/Presentation/Swap_chain
        // This extension adds functions to create, destroy and otherwise manage swapchains.
        VK_KHR_SWAPCHAIN_EXTENSION_NAME,

        // Among other API fixes, adds support for passing negative viewport heights.
        // https://www.saschawillems.de/blog/2019/03/29/flipping-the-vulkan-viewport/
        //VK_KHR_MAINTENANCE1_EXTENSION_NAME
    };
}
