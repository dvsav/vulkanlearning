/*
Copyright (c) 2020 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

// Suppress "C4834: discarding return value of function with 'nodiscard' attribute"
#pragma warning(disable : 4834)

// Put here any macro definitions for conditional compilation.
// You must include this file to any hearder or source files using conditional compilation.

#define VULKAN
#define USE_SSE

#ifdef USE_SSE
    #define _ENABLE_EXTENDED_ALIGNED_STORAGE
#endif

//#define _DEBUG
#define USE_REQUIRES

// This is to resolve the conflict between max() macro defined in <windows.h>
// and STL functions having the same name - max.
#define NOMINMAX

#ifdef _DEBUG
    #define USE_REQUIRES
    #define ENABLE_DEBUG_LAYERS
    #define PRINT_INFORMATION
#endif

#ifdef VULKAN
    #define INVERT_Y_AXIS
    #define VMATH_ZERO_TO_ONE_DEPTH
    #define USE_DYNAMIC_STATES
#endif

#define ENABLE_GEOMETRY_SHADER
#define ENABLE_ANISOTROPY

#undef UNICODE
#undef _UNICODE
