/*
Copyright (c) 2020 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#include "ConditionalCompilation.h"

#include <stdexcept>
#include <cassert>
#include <windowsx.h>
#include "winapi_error.h"
#include "Window.h"
#include "WinUtility.h"
#include "Requires.h"

namespace win
{
    #pragma region Static Fields

    std::map<HWND, Window*> Window::s_RegisteredWindows;

    WNDCLASSEX Window::s_WndClassEx;

    #pragma endregion

    #pragma region Constructors

    Window::Window(const std::string& title,
                   int width,
                   int height) :
        m_hWnd(NULL),
        m_hDC(NULL),
        m_MouseMoveEvent(),
        m_MouseLeftButtonDownEvent(),
        m_MouseLeftButtonUpEvent(),
        m_KeyDownEvent(),
        m_KeyUpEvent(),
        m_PaintEvent(),
        m_SizeChangedEvent()
    {
        util::Requires::ArgumentPositive(width, "width", FUNCTION_INFO);
        util::Requires::ArgumentPositive(height, "height", FUNCTION_INFO);

        InitWndClass();

        m_hWnd
            = CreateWindow(
                s_WndClassEx.lpszClassName,   // window class name
                title.c_str(),                // window title
                WS_OVERLAPPEDWINDOW,          // window type
                CW_USEDEFAULT, CW_USEDEFAULT, // starting window location in pixels (x, y)
                width,                        // window width in pixels
                height,                       // window height in pixels
                NULL,                         // parent window descriptor
                NULL,                         // menu descriptor
                GetModuleHandle(NULL),        // application descriptor
                NULL);                        // pointer to a value passed along with WM_CREATE message through ((CREATESTRUCT*)lParam)->lpCreateParams

        if (m_hWnd == NULL)
            throw make_winapi_error("win::Window::Window() -> CreateWindow()");

        // Get device context for the window's client area.
        m_hDC = GetDC(m_hWnd);

        if(m_hDC == NULL)
            throw make_winapi_error("win::Window::Window() -> GetDC()");

        // Place window descriptor to the registered windows collection.
        s_RegisteredWindows[m_hWnd] = this;
    }

    #pragma endregion

    #pragma region Destructor

    Window::~Window()
    {
        CloseWindow(m_hWnd);
        DestroyWindow(m_hWnd);
    }

    #pragma endregion

    #pragma region Properties

    void Window::setClientSize(int width, int height)
    {
        util::Requires::ArgumentPositive(width, "width", FUNCTION_INFO);
        util::Requires::ArgumentPositive(height, "height", FUNCTION_INFO);

        RECT rc = { 0, 0, width, height };

        // Calculates the required size of the window rectangle, based on the desired client-rectangle size.
        AdjustWindowRect(
            &rc,                 // in - client rectangle, out - window rectangle
            WS_OVERLAPPEDWINDOW, // window style
            FALSE);              // whether the window has menu

        setWindowSize(
            rc.right - rc.left,
            rc.bottom - rc.top);
    }

    void Window::setWindowSize(int width, int height)
    {
        util::Requires::ArgumentPositive(width, "width", FUNCTION_INFO);
        util::Requires::ArgumentPositive(height, "height", FUNCTION_INFO);

        RECT windowRect = getWindowRect();

        SetWindowPos(
            m_hWnd,
            HWND_TOP,
            windowRect.left,
            windowRect.top,
            width,
            height,
            SWP_ASYNCWINDOWPOS);
    }

    void Window::setPosition(int left, int top)
    {
        util::Requires::ArgumentNotNegative(left, "left", FUNCTION_INFO);
        util::Requires::ArgumentNotNegative(top, "top", FUNCTION_INFO);

        RECT windowRect = getWindowRect();

        SetWindowPos(
            m_hWnd,
            HWND_TOP,
            left,
            top,
            windowRect.right - windowRect.left,
            windowRect.bottom - windowRect.top,
            SWP_ASYNCWINDOWPOS);
    }

    #pragma endregion

    #pragma region Methods

    void Window::InitWndClass()
    {
        // flag showing whether the window class is initialized
        static bool s_Initialized = false;

        if (!s_Initialized)
        {
            s_Initialized = true;

            HINSTANCE hInst = GetModuleHandle(NULL);

            s_WndClassEx.cbSize = sizeof(WNDCLASSEX);                                 // structure size
            s_WndClassEx.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;                  // window style
            s_WndClassEx.lpfnWndProc = Window::WndProc;                               // pointer to window procedure WndProc
            s_WndClassEx.cbClsExtra = 0;                                              // shared memory
            s_WndClassEx.cbWndExtra = 0;                                              // number of additional bytes
            s_WndClassEx.hInstance = hInst;                                           // application handle
            s_WndClassEx.hIcon = LoadIcon(hInst, IDI_APPLICATION);                    // icon descriptor
            s_WndClassEx.hCursor = LoadCursor(NULL, IDC_ARROW);                       // cursor descriptor
            s_WndClassEx.hbrBackground = reinterpret_cast<HBRUSH>(COLOR_MENU + 1);    // background brush descriptor
            s_WndClassEx.lpszMenuName = NULL;                                         // menu name
            s_WndClassEx.lpszClassName = "win::Window";                               // window class name
            s_WndClassEx.hIconSm = LoadIcon(hInst, IDI_APPLICATION);                  // small icon descriptor

            // register window class
            if (!RegisterClassEx(&s_WndClassEx))
                throw make_winapi_error("win::Window::InitWndClass() -> RegisterClassEx()");
        }
    }

    LRESULT CALLBACK Window::WndProc(
        HWND hWnd,
        UINT message,
        WPARAM wParam,
        LPARAM lParam)
    {
        Window* pWindow = NULL;
        try
        {
            pWindow = s_RegisteredWindows.at(hWnd);
        }
        catch (std::out_of_range&)
        {
            return DefWindowProc(hWnd, message, wParam, lParam);
        }

        return pWindow->WindowProcedure(message, wParam, lParam);
    }

    LRESULT Window::WindowProcedure(
        UINT message,
        WPARAM wParam,
        LPARAM lParam)
    {
        switch (message)
        {
            case WM_PAINT:
            {
                PAINTSTRUCT paintStruct;
                auto hDC = BeginPaint(m_hWnd, &paintStruct);
                OnPaint(hDC);
                EndPaint(m_hWnd, &paintStruct);
                break;
            }
            case WM_MOUSEMOVE:
            {
                int xPos = GET_X_LPARAM(lParam);
                int yPos = GET_Y_LPARAM(lParam);
                OnMouseMove(xPos, yPos, wParam);
                break;
            }
            case WM_LBUTTONDOWN:
            {
                int xPos = GET_X_LPARAM(lParam);
                int yPos = GET_Y_LPARAM(lParam);
                OnMouseLeftButtonDown(xPos, yPos, wParam);
                break;
            }
            case WM_LBUTTONUP:
            {
                int xPos = GET_X_LPARAM(lParam);
                int yPos = GET_Y_LPARAM(lParam);
                OnMouseLeftButtonUp(xPos, yPos, wParam);
                break;
            }
            case WM_TIMER:
            {
                OnTimer(wParam);
                break;
            }
            case WM_KEYDOWN:
            {
                OnKeyDown(wParam);
                break;
            }
            case WM_KEYUP:
            {
                OnKeyUp(wParam);
                break;
            }
            case WM_COMMAND:
            {
                OnCommand(LOWORD(wParam));
                break;
            }
            case WM_SIZE:
            {
                int height = HIWORD(lParam); // get window height
                int width = LOWORD(lParam);  // get window width
                OnSizeChanged(width, height);
                break;
            }
            case WM_DESTROY:
            {
                // remove window descriptor from the registered windows collection
                auto elems_erased = s_RegisteredWindows.erase(m_hWnd);
                assert(elems_erased == 1);

                // Release the device context of the window
                auto result = ReleaseDC(m_hWnd, m_hDC);
                assert(result == 1);

                OnDestroy();

                // if this was the last existing window, quit
                if(s_RegisteredWindows.size() == 0)
                    PostQuitMessage(0);

                break;
            }
            default:
            {
                return DefWindowProc(m_hWnd, message, wParam, lParam);
            }
        }

        return 0;
    }

    #pragma endregion
}