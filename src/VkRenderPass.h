#pragma once

#include "ConditionalCompilation.h"

#include "VkHeaders.h"

namespace vulkan
{
    // Before we can finish creating the pipeline, we need to tell Vulkan about the framebuffer attachments
    // that will be used while rendering. We need to specify how many color and depth buffers there will be,
    // how many samples to use for each of them and how their contents should be handled throughout the
    // rendering operations. All of this information is wrapped in a render pass object.
    // https://vulkan-tutorial.com/en/Drawing_a_triangle/Graphics_pipeline_basics/Render_passes
    // In Vulkan, a render pass represents (or describes) a set of framebuffer attachments (images) required
    // for drawing operations and a collection of subpasses that drawing operations will be ordered into.
    // Each of these drawing operations may read from some input attachments and render data into some other
    // (color, depth, stencil) attachments. A render pass also describes the dependencies between these
    // attachments: in one subpass we perform rendering into the texture, but in another this texture will
    // be used as a source of data (that is, it will be sampled from).
    // https://software.intel.com/content/www/us/en/develop/articles/api-without-secrets-introduction-to-vulkan-part-3.html
    // A render pass describes the scope of a rendering operation by specifying the collection of attachments,
    // subpasses, and dependencies used during the rendering operation. A render pass consists of at least one
    // subpass. The communication of this information to the driver allows the driver to know what to expect
    // when rendering begins and to set up the hardware optimally for the rendering operation.
    // https://vulkan.lunarg.com/doc/view/1.2.170.0/linux/tutorial/html/10-init_render_pass.html
    inline vk::RenderPass CreateRenderPassOneSubpass(
        const vk::Device& logicalDevice,
        vk::Format colorImageFormat,
        vk::Format depthImageFormat)
    {
        // --- ATTACHMENTS ---

        // Position in this array is important, because AttachmentReferences refer to it
        std::array<vk::AttachmentDescription, 2> attachments
        {
            // 0 - Color attachment of the render pass
            vk::AttachmentDescription
            {
                .format = colorImageFormat,               // Format to use for attachment
                .samples = vk::SampleCountFlagBits::e1,   // Number of samples to write for multisampling (we are not using any multisampling here so we just use one sample)
                .loadOp = vk::AttachmentLoadOp::eClear,   // What to do with the attached framebuffer before rendering
                .storeOp = vk::AttachmentStoreOp::eStore, // What to do with the attached framebuffer after rendering

                // When an attachment has a depth format (and potentially also a stencil component) load and store ops refer
                // only to the depth component. If a stencil is present, stencil values are treated the way stencil load and
                // store ops describe. For color attachments, stencil ops are not relevant.
                //.stencilLoadOp = vk::AttachmentLoadOp::eDontCare,   // What to do with the stencil buffer before rendering
                //.stencilStoreOp = vk::AttachmentStoreOp::eDontCare, // What to do with the stencil buffer after rendering

                // Framebuffer data will be stored as an image, but images can be given different data layouts
                // to give optimal use for certain operations.
                // Layout is an internal memory arrangement of an image. Image data may be organized in such a way
                // that neighboring "image pixels" are also neighbors in memory, which can increase cache hits
                // (faster memory reading) when image is used as a source of data (that is, during texture sampling).
                // But caching is not necessary when the image is used as a target for drawing operations, and the
                // memory for that image may be organized in a totally different way. Image may have linear layout
                // (which gives the CPU ability to read or populate image's memory contents) or optimal layout
                // (which is optimized for performance but is also hardware/vendor dependent). So some hardware may
                // have special memory organization for some types of operations; other hardware may be operations-
                // agnostic. Some of the memory layouts may be better suited for some intended image "usages."
                // Or from the other side, some usages may require specific memory layouts. There is also a general
                // layout that is compatible with all types of operations. But from the performance point of view,
                // it is always best to set the layout appropriate for an intended image usage and it is application's
                // responsibility to inform the driver about transitions.
                .initialLayout = vk::ImageLayout::eUndefined,  // INITIAL layout informs the hardware about the layout the application "provides" the given attachment with.
                .finalLayout = vk::ImageLayout::ePresentSrcKHR // The final layout is the layout the given attachment will be transitioned into (automatically) AT THE END of a render pass.
            },

            // 1 - Depth attachment of the render pass
            vk::AttachmentDescription
            {
                .format = depthImageFormat,                  // Format to use for attachment
                .samples = vk::SampleCountFlagBits::e1,      // Number of samples to write for multisampling (we are not using any multisampling here so we just use one sample)
                .loadOp = vk::AttachmentLoadOp::eClear,      // What to do with the attached framebuffer before rendering
                .storeOp = vk::AttachmentStoreOp::eDontCare, // What to do with the attached framebuffer after rendering

                // When an attachment has a depth format (and potentially also a stencil component) load and store ops refer
                // only to the depth component. If a stencil is present, stencil values are treated the way stencil load and
                // store ops describe.
                .stencilLoadOp = vk::AttachmentLoadOp::eDontCare,
                .stencilStoreOp = vk::AttachmentStoreOp::eDontCare,

                .initialLayout = vk::ImageLayout::eUndefined,                   // INITIAL layout informs the hardware about the layout the application "provides" the given attachment with.
                .finalLayout = vk::ImageLayout::eDepthStencilAttachmentOptimal  // The final layout is the layout the given attachment will be transitioned into (automatically) AT THE END of a render pass.
            }
        };

        // --- REFERENCES ---
        // Every subpass references one or more of the attachments.
        // These references are themselves AttachmentReference structs.
        // There is a separation between a whole render pass and its subpasses because each subpass may use
        // multiple attachments in a different way, that is, in one subpass we are rendering into one color
        // attachment but in the next subpass we are reading from this attachment. In this way, we can prepare
        // a list of all attachments used in the whole render pass, and at the same time we can specify how
        // each attachment will be used in each subpass. And as each subpass may use a given attachment in
        // its own way, we must also specify each image�s layout for each subpass.
        // So before we can specify a description of all subpasses(an array with elements of type
        // VkSubpassDescription) we must create references for each attachment used in each subpass.
        // And this is what the color_attachment_references variable was created for.

        vk::AttachmentReference colorAttachmentRef
        {
            // specifies which attachment to reference by its index in the attachment descriptions array
            .attachment = 0,

            // Requested (required) layout the attachment will use DURING a given subpass.
            // The hardware will perform an automatic transition into a provided layout just
            // before a given subpass.
            .layout = vk::ImageLayout::eColorAttachmentOptimal
        };

        vk::AttachmentReference depthAttachmentRef
        {
            // specifies which attachment to reference by its index in the attachment descriptions array
            .attachment = 1,

            // Requested (required) layout the attachment will use DURING a given subpass.
            // The hardware will perform an automatic transition into a provided layout just
            // before a given subpass.
            .layout = vk::ImageLayout::eDepthStencilAttachmentOptimal
        };

        // --- SUBPASSES ---

        // Information about a particular subpass the render pass is using.
        // A subpass consists of drawing operations that use (more or less) the same attachments.
        // Each of these drawing operations may read from some input attachments and render data
        // into some other (color, depth, stencil) attachments.
        // Each subpass uses its own unique pipeline (the one currently bound to the pipelineBindPoint).
        vk::SubpassDescription subpass
        {
            // Type of pipeline in which this subpass will be used (graphics
            // or compute). Our example, of course, uses a graphics pipeline.
            .pipelineBindPoint = vk::PipelineBindPoint::eGraphics,

            // Array describing (pointing to) attachments which will be used
            // as color render targets (that image will be rendered into).
            // The index of the attachment in this array is directly referenced from the
            // fragment shader with the layout(location = 0) out vec4 outColor directive.
            .colorAttachmentCount = 1,
            .pColorAttachments = &colorAttachmentRef,

            // Unlike color attachments, a subpass can only use a single depth (+stencil) attachment.
            .pDepthStencilAttachment = &depthAttachmentRef
        };

        // Need to determine when layout transitions occur using subpass dependencies.
        // An execution dependency is a guarantee that for two sets of operations, the 1st set must happen-before the 2nd set.
        // A memory dependency is an execution dependency which includes availability and visibility operations such that:
        //     The first set of operations happens - before the availability operation.
        //     The availability operation happens - before the visibility operation.
        //     The visibility operation happens - before the second set of operations.
        // If a synchronization command includes a source stage mask, its first synchronization scope only includes execution
        // of the pipeline stages specified in that mask, and its first access scope only includes memory access performed by
        // pipeline stages specified in that mask. If a synchronization command includes a destination stage mask, its second
        // synchronization scope only includes execution of the pipeline stages specified in that mask, and its second access
        // scope only includes memory access performed by pipeline stages specified in that mask.
        // https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSubpassDependency.html
        // https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#synchronization-dependencies-scopes
        // When multiple subpasses are in use, the driver needs to be told the relationship between them. A subpass can depend
        // on operations which were submitted outside the current render pass, or be the source on which later rendering depends.
        // Most commonly, the need is to ensure that the fragment shader from an earlier subpass has completed rendering (to the
        // current tile, on a tiler) before the next subpass starts to try to read that data.
        // https://developer.samsung.com/galaxy-gamedev/resources/articles/renderpasses.html
        std::array<vk::SubpassDependency, 2> subpassDependencies
        {
            // External --> Subpass 0
            vk::SubpassDependency
            {
                .srcSubpass = VK_SUBPASS_EXTERNAL, // The subpass index of the 1st subpass in the dependency. Subpass index (VK_SUBPASS_EXTERNAL = special value meaning outside of renderpass).
                .dstSubpass = 0, // The subpass index of the 2nd subpass in the dependency

                // The synchronization scope of the 1st set of commands. What pipeline stage must have completed for the dependency
                .srcStageMask = vk::PipelineStageFlagBits::eBottomOfPipe, // eBottomOfPipe specifies all operations performed by all commands supported on the queue it is used with
                // The synchronization scope of the 2nd set of commands. What pipeline stage is waiting on the dependency
                .dstStageMask = vk::PipelineStageFlagBits::eColorAttachmentOutput | vk::PipelineStageFlagBits::eEarlyFragmentTests,

                // Memory access scope of the 1st set of commands
                .srcAccessMask = vk::AccessFlagBits::eNoneKHR,
                // Memory access scope of the 2nd set of commands
                .dstAccessMask = vk::AccessFlagBits::eColorAttachmentWrite | vk::AccessFlagBits::eDepthStencilAttachmentWrite
            },
            // Subpass 0 --> External
            vk::SubpassDependency
            {
                .srcSubpass = 0,  // The subpass index of the 1st subpass in the dependency
                .dstSubpass = VK_SUBPASS_EXTERNAL, // The subpass index of the 2nd subpass in the dependency. Subpass index (VK_SUBPASS_EXTERNAL = special value meaning outside of renderp

                 // The synchronization scope of the 1st set of commands. What pipeline stage must have completed for the dependency
                .srcStageMask = vk::PipelineStageFlagBits::eColorAttachmentOutput | vk::PipelineStageFlagBits::eEarlyFragmentTests,
                // The synchronization scope of the 2nd set of commands. What pipeline stage is waiting on the dependency
                .dstStageMask = vk::PipelineStageFlagBits::eTopOfPipe, // eTopOfPipe specifies all operations performed by all commands supported on the queue it is used with

                // Memory access scope of the 1st set of commands
                .srcAccessMask = vk::AccessFlagBits::eColorAttachmentWrite | vk::AccessFlagBits::eDepthStencilAttachmentWrite,
                // Memory access scope of the 2nd set of commands
                .dstAccessMask = vk::AccessFlagBits::eNoneKHR,
            },
        };

        // --- RENDER PASS ---

        vk::RenderPassCreateInfo renderPassCreateInfo
        {
            .attachmentCount = attachments.size(), // Number of all different attachments (elements in pAttachments array) used during whole render pass
            .pAttachments = attachments.data(),    // Array specifying all attachments used in a render pass
            .subpassCount = 1,      // Number of subpasses a render pass consists of
            .pSubpasses = &subpass, // Array with descriptions of all subpasses
            .dependencyCount = subpassDependencies.size(),
            .pDependencies = subpassDependencies.data()
        };

        return logicalDevice.createRenderPass(renderPassCreateInfo);
    }

    inline vk::RenderPass CreateRenderPassMultipass(
        const vk::Device& logicalDevice,
        vk::Format swapchainImageFormat,
        vk::Format colorImageFormat,
        vk::Format depthImageFormat)
    {
        // --- ATTACHMENTS ---

        // Position in this array is important, because AttachmentReferences refer to it
        std::array<vk::AttachmentDescription, 3> attachments
        {
            // 0 - Swapchain color attachment (Output of Subpass 2)
            vk::AttachmentDescription
            {
                .format = swapchainImageFormat,           // Format to use for attachment (here we are rendering directly into a swap chain so we need to take its format)
                .samples = vk::SampleCountFlagBits::e1,   // Number of samples to write for multisampling (we are not using any multisampling here so we just use one sample)
                .loadOp = vk::AttachmentLoadOp::eClear,   // What to do with the attached framebuffer before rendering
                .storeOp = vk::AttachmentStoreOp::eStore, // What to do with the attached framebuffer after rendering
                .initialLayout = vk::ImageLayout::eUndefined,  // INITIAL layout informs the hardware about the layout the application "provides" the given attachment with.
                .finalLayout = vk::ImageLayout::ePresentSrcKHR // The final layout is the layout the given attachment will be transitioned into (automatically) AT THE END of a render pass.
            },
            // 1 - Color Attachment (Output of Subpass 1, Input to Subpass 2)
            vk::AttachmentDescription
            {
                .format = colorImageFormat,                // Format to use for attachment (here we are rendering directly into a swap chain so we need to take its format)
                .samples = vk::SampleCountFlagBits::e1,    // Number of samples to write for multisampling (we are not using any multisampling here so we just use one sample)
                .loadOp = vk::AttachmentLoadOp::eClear,    // What to do with the attached framebuffer before rendering
                .storeOp = vk::AttachmentStoreOp::eDontCare,   // What to do with the attached framebuffer after rendering (after the whole renderpass has finished)
                .initialLayout = vk::ImageLayout::eUndefined,  // INITIAL layout informs the hardware about the layout the application "provides" the given attachment with.
                .finalLayout = vk::ImageLayout::eColorAttachmentOptimal // The final layout is the layout the given attachment will be transitioned into (automatically) AT THE END of a render pass.
            },
            // 2 - Depth attachment (Output of Subpass 1, Input to Subpass 2)
            vk::AttachmentDescription
            {
                .format = depthImageFormat,                  // Format to use for attachment (here we are rendering directly into a swap chain so we need to take its format)
                .samples = vk::SampleCountFlagBits::e1,      // Number of samples to write for multisampling (we are not using any multisampling here so we just use one sample)
                .loadOp = vk::AttachmentLoadOp::eClear,      // What to do with the attached framebuffer before rendering
                .storeOp = vk::AttachmentStoreOp::eDontCare, // What to do with the attached framebuffer after rendering

                // When an attachment has a depth format (and potentially also a stencil component) load and store ops refer
                // only to the depth component. If a stencil is present, stencil values are treated the way stencil load and
                // store ops describe.
                .stencilLoadOp = vk::AttachmentLoadOp::eDontCare,
                .stencilStoreOp = vk::AttachmentStoreOp::eDontCare,

                .initialLayout = vk::ImageLayout::eUndefined,                   // INITIAL layout informs the hardware about the layout the application "provides" the given attachment with.
                .finalLayout = vk::ImageLayout::eDepthStencilAttachmentOptimal  // The final layout is the layout the given attachment will be transitioned into (automatically) AT THE END of a render pass.
            }
        };

        // --- REFERENCES ---

        // Subpass 1
        vk::AttachmentReference colorAttachmentRef
        {
            // specifies which attachment to reference by its index in the attachment descriptions array
            .attachment = 1,

            // Requested (required) layout the attachment will use DURING a given subpass.
            // The hardware will perform an automatic transition into a provided layout just
            // before a given subpass.
            .layout = vk::ImageLayout::eColorAttachmentOptimal
        };

        // Subpass 1
        vk::AttachmentReference depthAttachmentRef
        {
            // specifies which attachment to reference by its index in the attachment descriptions array
            .attachment = 2,

            // Requested (required) layout the attachment will use DURING a given subpass.
            // The hardware will perform an automatic transition into a provided layout just
            // before a given subpass.
            .layout = vk::ImageLayout::eDepthStencilAttachmentOptimal
        };

        // Subpass 2
        vk::AttachmentReference swapchainColorAttachmentRef
        {
            // specifies which attachment to reference by its index in the attachment descriptions array
            .attachment = 0,

            // Requested (required) layout the attachment will use DURING a given subpass.
            // The hardware will perform an automatic transition into a provided layout just
            // before a given subpass.
            .layout = vk::ImageLayout::eColorAttachmentOptimal
        };

        std::array<vk::AttachmentReference, 2> subpass2InputAttachmentRefs
        {
            // color
            vk::AttachmentReference
            {
                // specifies which attachment to reference by its index in the attachment descriptions array
                .attachment = 1,

                // Requested (required) layout the attachment will use DURING a given subpass.
                // The hardware will perform an automatic transition into a provided layout just
                // before a given subpass.
                .layout = vk::ImageLayout::eShaderReadOnlyOptimal
            },
            // depth
            vk::AttachmentReference
            {
                // specifies which attachment to reference by its index in the attachment descriptions array
                .attachment = 2,

                // Requested (required) layout the attachment will use DURING a given subpass.
                // The hardware will perform an automatic transition into a provided layout just
                // before a given subpass.
                .layout = vk::ImageLayout::eShaderReadOnlyOptimal
            }
        };

        // --- SUBPASSES ---
        // Information about a particular subpass the render pass is using.
        // A subpass consists of drawing operations that use (more or less) the same attachments.
        // Each of these drawing operations may read from some input attachments and render data
        // into some other (color, depth, stencil) attachments.
        // Each subpass uses its own unique pipeline (the one currently bound to the pipelineBindPoint).
        std::array<vk::SubpassDescription, 2> subpasses
        {
            // SUBPASS 1
            vk::SubpassDescription
            {
                // Type of pipeline in which this subpass will be used (graphics
                // or compute). Our example, of course, uses a graphics pipeline.
                .pipelineBindPoint = vk::PipelineBindPoint::eGraphics,

                // Array describing (pointing to) attachments which will be used
                // as color render targets (that image will be rendered into).
                // The index of the attachment in this array is directly referenced from the
                // fragment shader with the layout(location = 0) out vec4 outColor directive.
                .colorAttachmentCount = 1,
                .pColorAttachments = &colorAttachmentRef,

                // Unlike color attachments, a subpass can only use a single depth (+stencil) attachment.
                .pDepthStencilAttachment = &depthAttachmentRef
            },
            // SUBPASS 2
            vk::SubpassDescription
            {
                // Type of pipeline in which this subpass will be used (graphics
                // or compute). Our example, of course, uses a graphics pipeline.
                .pipelineBindPoint = vk::PipelineBindPoint::eGraphics,

                .inputAttachmentCount = subpass2InputAttachmentRefs.size(),
                .pInputAttachments = subpass2InputAttachmentRefs.data(),

                // Array describing (pointing to) attachments which will be used
                // as color render targets (that image will be rendered into).
                // The index of the attachment in this array is directly referenced from the
                // fragment shader with the layout(location = 0) out vec4 outColor directive.
                .colorAttachmentCount = 1,
                .pColorAttachments = &swapchainColorAttachmentRef,
            }
        };

        // Need to determine when layout transitions occur using subpass dependencies.
        // An execution dependency is a guarantee that for two sets of operations, the 1st set must happen-before the 2nd set.
        // A memory dependency is an execution dependency which includes availability and visibility operations such that:
        //     The first set of operations happens - before the availability operation.
        //     The availability operation happens - before the visibility operation.
        //     The visibility operation happens - before the second set of operations.
        // If a synchronization command includes a source stage mask, its first synchronization scope only includes execution
        // of the pipeline stages specified in that mask, and its first access scope only includes memory access performed by
        // pipeline stages specified in that mask. If a synchronization command includes a destination stage mask, its second
        // synchronization scope only includes execution of the pipeline stages specified in that mask, and its second access
        // scope only includes memory access performed by pipeline stages specified in that mask.
        // https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSubpassDependency.html
        // https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#synchronization-dependencies-scopes
        // When multiple subpasses are in use, the driver needs to be told the relationship between them. A subpass can depend
        // on operations which were submitted outside the current render pass, or be the source on which later rendering depends.
        // Most commonly, the need is to ensure that the fragment shader from an earlier subpass has completed rendering (to the
        // current tile, on a tiler) before the next subpass starts to try to read that data.
        // https://developer.samsung.com/galaxy-gamedev/resources/articles/renderpasses.html
        std::array<vk::SubpassDependency, 3> subpassDependencies
        {
            // External --> Subpass 1
            vk::SubpassDependency
            {
                .srcSubpass = VK_SUBPASS_EXTERNAL, // The subpass index of the 1st subpass in the dependency. Subpass index (VK_SUBPASS_EXTERNAL = special value meaning outside of renderpass).
                .dstSubpass = 0, // The subpass index of the 2nd subpass in the dependency

                // The synchronization scope of the 1st set of commands. What pipeline stage must have completed for the dependency
                .srcStageMask = vk::PipelineStageFlagBits::eBottomOfPipe,
                // The synchronization scope of the 2nd set of commands. What pipeline stage is waiting on the dependency
                .dstStageMask = vk::PipelineStageFlagBits::eColorAttachmentOutput,

                // Memory access scope of the 1st set of commands
                .srcAccessMask = vk::AccessFlagBits::eMemoryRead,
                // Memory access scope of the 2nd set of commands
                .dstAccessMask = vk::AccessFlagBits::eColorAttachmentWrite
            },
            // Subpass 1 --> Subpass 2
            vk::SubpassDependency
            {
                .srcSubpass = 0, // The subpass index of the 1st subpass in the dependency.
                .dstSubpass = 1, // The subpass index of the 2nd subpass in the dependency

                // The synchronization scope of the 1st set of commands. What pipeline stage must have completed for the dependency
                .srcStageMask = vk::PipelineStageFlagBits::eColorAttachmentOutput,
                // The synchronization scope of the 2nd set of commands. What pipeline stage is waiting on the dependency
                .dstStageMask = vk::PipelineStageFlagBits::eFragmentShader,

                // Memory access scope of the 1st set of commands
                .srcAccessMask = vk::AccessFlagBits::eColorAttachmentWrite,
                // Memory access scope of the 2nd set of commands
                .dstAccessMask = vk::AccessFlagBits::eShaderRead
            },
            // Subpass 2 --> External
            vk::SubpassDependency
            {
                .srcSubpass = 1,  // The subpass index of the 1st subpass in the dependency
                .dstSubpass = VK_SUBPASS_EXTERNAL, // The subpass index of the 2nd subpass in the dependency. Subpass index (VK_SUBPASS_EXTERNAL = special value meaning outside of renderp

                // The synchronization scope of the 1st set of commands. What pipeline stage must have completed for the dependency
                .srcStageMask = vk::PipelineStageFlagBits::eColorAttachmentOutput,
                // The synchronization scope of the 2nd set of commands. What pipeline stage is waiting on the dependency
                .dstStageMask = vk::PipelineStageFlagBits::eBottomOfPipe,

                // Memory access scope of the 1st set of commands
                .srcAccessMask = vk::AccessFlagBits::eColorAttachmentWrite,
                // Memory access scope of the 2nd set of commands (The presentation engine only reads from the image)
                .dstAccessMask = vk::AccessFlagBits::eMemoryRead
            },
        };

        // --- RENDER PASS ---

        vk::RenderPassCreateInfo renderPassCreateInfo
        {
            .attachmentCount = attachments.size(), // Number of all different attachments (elements in pAttachments array) used during whole render pass
            .pAttachments = attachments.data(),    // Array specifying all attachments used in a render pass
            .subpassCount = subpasses.size(),      // Number of subpasses a render pass consists of
            .pSubpasses = subpasses.data(),        // Array with descriptions of all subpasses
            .dependencyCount = subpassDependencies.size(),
            .pDependencies = subpassDependencies.data()
        };

        return logicalDevice.createRenderPass(renderPassCreateInfo);
    }

    /*
    Provides more safe renderpass running in RAII fashion.
    Example:
    {
        RunRenderPass runRp(commandBuffer, renderPassBeginInfo, vk::SubpassContents::eInline); // here begin renderpass
        // record some commands to commandBuffer
    } // here the renderpass is ended automatically
    */
    class RunRenderPass final
    {
    private:
        const vk::CommandBuffer& m_CommandBuffer;

    private:
        RunRenderPass(const RunRenderPass&) = delete;
        RunRenderPass& operator=(const RunRenderPass&) = delete;

    public:
        /// <param name="commandBuffer"></param>
        /// <param name="renderPassBegin"></param>
        /// <param name="contents">
        /// VK_SUBPASS_CONTENTS_INLINE
        ///     The render pass commands will be embedded in the primary command buffer itself and no
        ///     secondary command buffers will be executed.
        /// VK_SUBPASS_CONTENTS_SECONDARY_COMMAND_BUFFERS
        ///     The render pass commands will be executed from secondary command buffers that will be
        ///     called from the primary command buffer.
        /// </param>
        RunRenderPass(
            const vk::CommandBuffer& commandBuffer,
            const vk::RenderPassBeginInfo& renderPassBegin,
            vk::SubpassContents contents) :
            m_CommandBuffer(commandBuffer)
        {
            commandBuffer.beginRenderPass(renderPassBegin, contents);
        }

        ~RunRenderPass()
        {
            m_CommandBuffer.endRenderPass();
        }
    };
}