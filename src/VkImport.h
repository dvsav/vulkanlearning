#pragma once

#include "ConditionalCompilation.h"

#include <string>

// Engine
#include "AssetImporting.h"

// VkEngine
#include "VkMesh.h"

namespace vulkan
{
    template <typename TVertex, typename TIndex>
    inline VkMesh<TVertex, TIndex> vkImportMesh(
        const VkContext& vkContext,
        const std::string& filename,
        const engine::vec3& scale)
    {
        auto meshInfo = engine::import_mesh<TVertex, TIndex>(filename, scale);
        return VkMesh(
            /*vkContext*/ vkContext,
            /*primitiveTopology*/ meshInfo.Topology,
            /*vertices*/ meshInfo.Vertices,
            /*indices*/ meshInfo.Indices);
    }
}