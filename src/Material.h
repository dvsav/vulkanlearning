#pragma once

#include "ConditionalCompilation.h"

// STL
#include <memory>

// VkEngine
#include "VkTexture.h"

namespace vulkan
{
    struct Material
    {
        std::shared_ptr<Texture> DiffuseColor;
        std::shared_ptr<Texture> EmissiveColor;
        std::shared_ptr<Texture> NormalMap;
        std::shared_ptr<Texture> HeightMap;

        void Destroy()
        {
            DiffuseColor.reset();
            EmissiveColor.reset();
            NormalMap.reset();
            HeightMap.reset();
        }
    };
}