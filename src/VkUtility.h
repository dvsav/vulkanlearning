/*
Copyright (c) 2020 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

// Vulkan
#include "VkHeaders.h"
#include "VkException.h"

// Utility
#include "Requires.h"

// VkEngine


namespace vulkan
{
    inline std::vector<vk::ExtensionProperties>& GetInstanceExtensions()
    {
        static std::vector<vk::ExtensionProperties> properties
            = vk::enumerateInstanceExtensionProperties();
        return properties;
    }

    inline bool IsInstanceExtensionSupported(
        const char* extensionName)
    {
        auto& supported_extensions = GetInstanceExtensions();

        return std::find_if(
            supported_extensions.cbegin(),
            supported_extensions.cend(),
            [extensionName](const vk::ExtensionProperties& prop)
                { return std::strcmp(prop.extensionName, extensionName) == 0; })
        != supported_extensions.cend();
    }

    inline std::vector<vk::LayerProperties>& GetInstanceLayers()
    {
        static std::vector<vk::LayerProperties> layers
            = vk::enumerateInstanceLayerProperties();
        return layers;
    }

    inline bool IsLayerSupported(
        const char* layerName)
    {
        auto& supported_layers = GetInstanceLayers();

        return std::find_if(
            supported_layers.cbegin(),
            supported_layers.cend(),
            [layerName](const vk::LayerProperties& prop)
                { return std::strcmp(prop.layerName, layerName) == 0; })
            != supported_layers.cend();
    }

    inline bool IsDeviceExtensionSupported(
        const vk::PhysicalDevice& physicalDevice,
        const char* extensionName)
    {
        auto extensions = physicalDevice.enumerateDeviceExtensionProperties();

        return std::find_if(
            extensions.cbegin(),
            extensions.cend(),
            [extensionName](const vk::ExtensionProperties& prop)
                { return std::strcmp(prop.extensionName, extensionName) == 0; })
            != extensions.cend();
    }

    inline void PrintPhysicalDevices(
        const vk::Instance& instance)
    {
        std::cout << "=== Physical Devices ===" << std::endl;
        for (const auto& pdev : instance.enumeratePhysicalDevices())
        {
            auto properties = pdev.getProperties();

            std::cout << properties.deviceName;
            switch (properties.deviceType)
            {
            case vk::PhysicalDeviceType::eDiscreteGpu:
                std::cout << " (discrete GPU)";
                break;

            case vk::PhysicalDeviceType::eIntegratedGpu:
                std::cout << " (integrated GPU)";
                break;
            }
            std::cout << std::endl;
        }
    }

    // A queue is like a CPU thread but in GPU.
    // Queues have different types (Graphics, Presentation, Compute, Transfer...)
    // Queue of a certain type can execute only certain kinds of operations.
    // A queue family just describes a set of queues with identical properties.
    // There's only a limited number of available queues of a certain type supported by the GPU.
    // https://stackoverflow.com/questions/55272626/what-is-actually-a-queue-family-in-vulkan
    inline void PrintQueueFamilies(
        const vk::PhysicalDevice& physicalDevice)
    {
        std::cout << "=== Queue Families ===" << std::endl;
        int i = 0;
        for (const auto& queueFamily : physicalDevice.getQueueFamilyProperties())
        {
            std::cout << "Family index: " << i++
            << " Queue count: " << queueFamily.queueCount << " {";

            if (queueFamily.queueFlags & vk::QueueFlagBits::eGraphics)
                std::cout << " Graphics";
            if (queueFamily.queueFlags & vk::QueueFlagBits::eCompute)
                std::cout << " Compute";
            if (queueFamily.queueFlags & vk::QueueFlagBits::eTransfer)
                std::cout << " Transfer";
            if (queueFamily.queueFlags & vk::QueueFlagBits::eProtected)
                std::cout << " Protected";
            if (queueFamily.queueFlags & vk::QueueFlagBits::eSparseBinding)
                std::cout << " SparseBinding";

            std::cout << " }" << std::endl;
        }
    }

    // Prints information about all memory types present in a specified physicalDevice.
    // Each memory type has its own set of properties (device local, host visible, etc.).
    // Each memory type belongs to some memory heap. There can be many memory heaps in
    // the physicalDevice each of which can be either device-local or not device-local
    // and have different size (in bytes).
    inline void PrintMemoryInfo(
        const vk::PhysicalDevice& physicalDevice)
    {
        auto memoryProperties = physicalDevice.getMemoryProperties();

        for (uint32_t i = 0; i < memoryProperties.memoryTypeCount; i++)
        {
            auto heapIndex = memoryProperties.memoryTypes[i].heapIndex;
            std::cout
                << "memory type index = " << i
                << " heap = " << heapIndex
                << " size = " << memoryProperties.memoryHeaps[heapIndex].size
                << " heapFlags = "
                << (memoryProperties.memoryHeaps[heapIndex].flags & vk::MemoryHeapFlagBits::eDeviceLocal ? "" : "not ") << "DEVICE_LOCAL"
                << " propertyFlags ="
                << (memoryProperties.memoryTypes[i].propertyFlags & vk::MemoryPropertyFlagBits::eDeviceLocal  ? " DEVICE_LOCAL"  : "")
                << (memoryProperties.memoryTypes[i].propertyFlags & vk::MemoryPropertyFlagBits::eHostVisible  ? " HOST_VISIBLE"  : "")
                << (memoryProperties.memoryTypes[i].propertyFlags & vk::MemoryPropertyFlagBits::eHostCoherent ? " HOST_COHERENT" : "")
                << (memoryProperties.memoryTypes[i].propertyFlags & vk::MemoryPropertyFlagBits::eHostCached   ? " HOST_CACHED"   : "")
                << std::endl;
        }
    }

    // Looks for the right memory index that fits the requirements.
    // Graphics cards can offer different types of memory to allocate from. Each type
    // of memory varies in terms of allowed operations and performance characteristics.
    // We need to combine the requirements of the buffer and our own application requirements
    // to find the right type of memory to use.
    inline uint32_t FindMemoryTypeIndex(
        const vk::PhysicalDevice& physicalDevice,
        const vk::MemoryRequirements& bufferMemoryRequirements,
        vk::MemoryPropertyFlags appMemoryRequirements)
    {
        // Get properties of physical device memory
        auto memoryProperties = physicalDevice.getMemoryProperties();

        auto allowedTypes = bufferMemoryRequirements.memoryTypeBits;
        for (uint32_t i = 0; i < memoryProperties.memoryTypeCount; i++)
        {
            if (// Index of memory type must match corresponding bit in allowedTypes
                (allowedTypes & (1 << i)) &&
                // Desired property bit flags must be part of memory type's property flags
                ((memoryProperties.memoryTypes[i].propertyFlags & appMemoryRequirements) == appMemoryRequirements))
            {
                return i;
            }
        }
        throw VkException(FUNCTION_INFO);
    }

    // Looks among specified candidates for am image format
    // that supports specified tiling and usage.
    inline vk::Format FindSupportedFormat(
        const vk::PhysicalDevice& physicalDevice,
        const std::vector<vk::Format>& candidates,
        vk::ImageTiling tiling,
        vk::FormatFeatureFlags features)
    {
        for (vk::Format format : candidates)
        {
            auto props = physicalDevice.getFormatProperties(format);

            switch (tiling)
            {
            case vk::ImageTiling::eOptimal:
                if ((props.optimalTilingFeatures & features) == features)
                    return format;
                break;

            case vk::ImageTiling::eLinear:
                if ((props.linearTilingFeatures & features) == features)
                    return format;
                break;
            }
        }
        throw VkException(FUNCTION_INFO);
    }

    inline vk::Format FindDepthFormat(
        const vk::PhysicalDevice& physicalDevice)
    {
        return FindSupportedFormat(
            physicalDevice,
            { vk::Format::eD32Sfloat, vk::Format::eD32SfloatS8Uint, vk::Format::eD24UnormS8Uint },
            vk::ImageTiling::eOptimal,
            vk::FormatFeatureFlagBits::eDepthStencilAttachment);
    }

    inline vk::Format FindColorBufferFormat(
        const vk::PhysicalDevice& physicalDevice)
    {
        return FindSupportedFormat(
            physicalDevice,
            { vk::Format::eR8G8B8A8Unorm },
            vk::ImageTiling::eOptimal,
            vk::FormatFeatureFlagBits::eColorAttachment);
    }

    // ImageView to an Image is the same as LogicalDevice to PhysicalDevice.
    // To use any VkImage, including those in the swap chain, in the render
    // pipeline we have to create a VkImageView object. An image view is quite
    // literally a view into an image. It describes how to access the image and
    // which part of the image to access, for example if it should be treated as
    // a 2D texture depth texture without any mipmapping levels.
    // Source: https://vulkan-tutorial.com/en/Drawing_a_triangle/Presentation/Image_views
    inline vk::ImageView CreateImageView(
        const vk::Device& logicalDevice,
        vk::Image image,
        vk::Format format,
        vk::ImageAspectFlags aspectFlags)
    {
        vk::ImageViewCreateInfo viewCreateInfo
        {
            .image = image,
            .viewType = vk::ImageViewType::e2D, // Type of image (1D, 2D, 3D, Cube map, etc)
            .format = format,
            // The components field allows remapping of rgba components to other rgba values the
            // color channels around. For example, you can map all of the channels to the red channel
            // for a monochrome texture. You can also map constant values of 0 and 1 to a channel.
            .components = vk::ComponentMapping
            {
                .r = vk::ComponentSwizzle::eIdentity,
                .g = vk::ComponentSwizzle::eIdentity,
                .b = vk::ComponentSwizzle::eIdentity,
                .a = vk::ComponentSwizzle::eIdentity,
            },
            // The subresourceRange field describes what the image's purpose is and which part of the image should be accessed.
            .subresourceRange = vk::ImageSubresourceRange
            {
                .aspectMask = aspectFlags, // Which aspect of image to view (e.g. COLOR_BIT for viewing color)
                .baseMipLevel = 0,         // Start mipmap level to view from
                .levelCount = 1,           // Number of mipmap levels to view

                // For cube and cube array image views, the layers of the image view starting
                // at baseArrayLayer correspond to faces in the order +X, -X, +Y, -Y, +Z, -Z.
                .baseArrayLayer = 0,       // Start array layer to view from
                .layerCount = 1            // Number of array levels to view
            }
        };
        return logicalDevice.createImageView(viewCreateInfo);
    }

    inline vk::CommandBuffer BeginCommandBuffer(
        const vk::Device& logicalDevice,
        const vk::CommandPool& commandPool)
    {
        vk::CommandBufferAllocateInfo cbAllocInfo
        {
            .commandPool = commandPool,
            // Primary buffer is the one that you submit directly to
            // queue and can't be called by other command buffers
            .level = vk::CommandBufferLevel::ePrimary,
            .commandBufferCount = 1
        };
        auto commandBuffers = logicalDevice.allocateCommandBuffers(cbAllocInfo);
        auto& commandBuffer = commandBuffers[0];

        vk::CommandBufferBeginInfo commandBufferBeginInfo
        {
            // We're only going to use the command buffer once and wait with returning
            // from the function until the commands have finished executing
            .flags = vk::CommandBufferUsageFlagBits::eOneTimeSubmit
        };

        commandBuffer.begin(commandBufferBeginInfo);

        return commandBuffer;
    }

    inline void EndAndSubmitCommandBuffer(
        const vk::Device& logicalDevice,
        const vk::Queue& commandQueue,
        const vk::CommandPool& commandPool,
        vk::CommandBuffer& commandBuffer)
    {
        commandBuffer.end();

        // Submit command buffer to the command queue
        commandQueue.submit(
            vk::SubmitInfo
            {
                .commandBufferCount = 1,
                .pCommandBuffers = &commandBuffer
            });
        // Wait until the commands submitted to the command queue have been executed
        commandQueue.waitIdle();

        // Once the commands are submitted
        logicalDevice.freeCommandBuffers(commandPool, commandBuffer);
    }

    /*
    Provides more safe temporary command buffer begin-end loop in RAII fashion.
    Example:
    {
        RunTemporaryCommandBuffer runCmdBuf(logicalDevice, commandQueue, commandPool); // here we create command buffer
        // record commands to the command buffer
    } // here the buffer is submitted and destroyed automatically
    */
    class RunTemporaryCommandBuffer final
    {
    private:
        vk::CommandBuffer m_CommandBuffer;
        const vk::Device& m_LogicalDevice;
        const vk::Queue& m_CommandQueue;
        const vk::CommandPool& m_CommandPool;

    private:
        RunTemporaryCommandBuffer(const RunTemporaryCommandBuffer&) = delete;
        RunTemporaryCommandBuffer& operator=(const RunTemporaryCommandBuffer&) = delete;

    public:
        RunTemporaryCommandBuffer(
            const vk::Device& logicalDevice,
            const vk::Queue& commandQueue,
            const vk::CommandPool& commandPool) :
            m_CommandBuffer(BeginCommandBuffer(logicalDevice, commandPool)),
            m_LogicalDevice(logicalDevice),
            m_CommandQueue(commandQueue),
            m_CommandPool(commandPool)
        {}

        ~RunTemporaryCommandBuffer()
        {
            EndAndSubmitCommandBuffer(
                m_LogicalDevice,
                m_CommandQueue,
                m_CommandPool,
                m_CommandBuffer);
        }

        const vk::CommandBuffer& getCommandBuffer() const { return m_CommandBuffer; }
    };
}
