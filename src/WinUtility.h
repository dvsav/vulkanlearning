/*
Copyright (c) 2020 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

#include <string>
#include <Windows.h>

namespace win
{
    std::string GetLastErrorMessage();

    inline void ShowMessage(LPCSTR caption, const LPCSTR text)
    {
        ::MessageBox(NULL, text, caption, NULL);
    }

    /*
    Returns true if the specified key is pressed.
    Otherwise returns false.
    Parameters:
    virtualKey - virtual-key code.
    */
    inline bool IsKeyPressed(int virtualKey)
    {
        return (GetAsyncKeyState(virtualKey) & (1 << ((sizeof(decltype(GetAsyncKeyState(virtualKey))) * 8 - 1)))) != 0;
    }

    inline void RunMessageLoop()
    {
        MSG msg {0};
        while (msg.message != WM_QUIT)
        {
            if (GetMessage(&msg, NULL, 0, 0))
            {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
            }
        }
    }
}

