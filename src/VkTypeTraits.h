#pragma once

#include "ConditionalCompilation.h"
#include "EngineTypedefs.h"
#include "VkHeaders.h"

using namespace engine;

namespace vulkan
{
    template <typename T>
    struct type_traits { };

    template <>
    struct type_traits<vec2>
    {
        using value_type = vec2;
        static constexpr vk::Format format = vk::Format::eR32G32Sfloat;
    };

    template <>
    struct type_traits<vec3>
    {
        using value_type = vec3;
        static constexpr vk::Format format = vk::Format::eR32G32B32Sfloat;
    };

    template <>
    struct type_traits<color_rgba32f>
    {
        using value_type = color_rgba32f;
        static constexpr vk::Format format = vk::Format::eR32G32B32A32Sfloat;
    };

    template <>
    struct type_traits<uint16_t>
    {
        using value_type = uint16_t;
        static constexpr vk::IndexType index_type = vk::IndexType::eUint16;
    };

    template <>
    struct type_traits<uint32_t>
    {
        using value_type = uint32_t;
        static constexpr vk::IndexType index_type = vk::IndexType::eUint32;
    };
}