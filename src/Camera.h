/*
Copyright (c) 2020 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

#include "EngineTypedefs.h"
#include "WorldPositionAndOrientation.h"
#include "utility.h"

namespace engine
{
    // Abstract camera class
    class Camera
    {
        #pragma region Fields

        private:
            WorldPositionAndOrientation m_WorldPositionAndOrientation;
            bool m_WorldPositionAndOrientationInvalidated;
            bool m_CameraToScreenInvalidated;
            float m_NearPlane;
            float m_FarPlane;
            mat4 m_WorldToCameraMatrix;
            mat4 m_WorldToScreenMatrix;

        protected:
            mat4 m_CameraToScreenMatrix;

        #pragma endregion

        #pragma region Constructors

        public:
            Camera() :
                m_WorldPositionAndOrientation(),
                m_WorldPositionAndOrientationInvalidated(true),
                m_CameraToScreenInvalidated(true),
                m_NearPlane(1.0f),
                m_FarPlane(100.0f),
                m_WorldToCameraMatrix(),
                m_WorldToScreenMatrix(),
                m_CameraToScreenMatrix()
            { }

        #pragma endregion

        #pragma region Destructor

        public:
            virtual ~Camera() { }

        #pragma endregion

        #pragma region Properties

        protected:
            virtual void CalcCameraToScreenMatrix() { m_CameraToScreenInvalidated = true; };

        public:
            WorldPositionAndOrientation& PositionAndOrientation()
            {
                m_CameraToScreenInvalidated = m_WorldPositionAndOrientationInvalidated = true;
                return m_WorldPositionAndOrientation;
            }
            const WorldPositionAndOrientation& PositionAndOrientation() const { return m_WorldPositionAndOrientation; }

            float getNearPlane() const { return m_NearPlane; }
            float getFarPlane() const { return m_FarPlane; }

            void setNearPlane(float value) { m_NearPlane = value; CalcCameraToScreenMatrix(); }
            void setFarPlane(float value) { m_FarPlane = value; CalcCameraToScreenMatrix(); }

            const mat4& getWorldToCameraMatrix() const
            {
                if (m_WorldPositionAndOrientationInvalidated)
                {
                    const_cast<mat4&>(m_WorldToCameraMatrix) = vmath::make_WorldToCameraMatrix<float>(
                        m_WorldPositionAndOrientation.Position().vec3(),
                        m_WorldPositionAndOrientation.getForwardDirection().vec3(),
                        m_WorldPositionAndOrientation.getUpDirection().vec3());

                    const_cast<bool&>(m_WorldPositionAndOrientationInvalidated) = false;
                }
                return m_WorldToCameraMatrix;
            }

            const mat4& getCameraToScreenMatrix() const { return m_CameraToScreenMatrix; }

            const mat4& getWorldToScreenMatrix() const
            {
                if (m_CameraToScreenInvalidated)
                {
                    const_cast<mat4&>(m_WorldToScreenMatrix) = getCameraToScreenMatrix() * getWorldToCameraMatrix();
                    const_cast<bool&>(m_CameraToScreenInvalidated) = false;
                }
                return m_WorldToScreenMatrix;
            }

        #pragma endregion
    };

    // Perspective projection camera
    class PerspectiveCamera final : public Camera
    {
        #pragma region Fields

        private:
            float m_ViewAngle;
            float m_AspectRatio;

        #pragma endregion

        #pragma region Constructors

        public:
            PerspectiveCamera() :
                m_ViewAngle(M_PI / 3.0f),
                m_AspectRatio(16.0f / 9.0f)
            {
                CalcCameraToScreenMatrix();
            }

            PerspectiveCamera(
                float viewAngle,
                float aaspectRatio) :
                m_ViewAngle(viewAngle),
                m_AspectRatio(aaspectRatio)
            {
                CalcCameraToScreenMatrix();
            }

        #pragma endregion

        #pragma region Properties

        public:
            float getViewAngle() const { return m_ViewAngle; }
            float getAspectRatio() const { return m_AspectRatio; }

            void setViewAngle(float value) { m_ViewAngle = value; CalcCameraToScreenMatrix(); }
            void setAspectRatio(float value) { m_AspectRatio = value; CalcCameraToScreenMatrix(); }

        #pragma endregion

        #pragma region Methods

        protected:
            void CalcCameraToScreenMatrix() override
            {
                Camera::CalcCameraToScreenMatrix();
                m_CameraToScreenMatrix = vmath::make_PerspectiveProjectionMatrix(
                    m_ViewAngle,
#ifdef INVERT_Y_AXIS
                    -m_AspectRatio,
#else
                    m_AspectRatio,
#endif
                    getNearPlane(),
                    getFarPlane());
            }

        #pragma endregion
    };

    // Orthographic projection camera
    class OrthographicCamera final : public Camera
    {
        #pragma region Fields

        private:
            float m_Width;
            float m_Height;

        #pragma endregion

        #pragma region Constructors

        public:
            OrthographicCamera() :
                m_Width(1.0f),
                m_Height(1.0f)
            {
                CalcCameraToScreenMatrix();
            }

        #pragma endregion

        #pragma region Properties

        public:
            float getWidth() const { return m_Width; }
            float getHeight() const { return m_Height; }

            void setWidth(float value) { m_Width = value; CalcCameraToScreenMatrix(); }
            void setHeight(float value) { m_Height = value; CalcCameraToScreenMatrix(); }

        #pragma endregion

        #pragma region Methods

        private:
            void CalcCameraToScreenMatrix() override
            {
                Camera::CalcCameraToScreenMatrix();
                m_CameraToScreenMatrix = vmath::make_OrthographicProjectionMatrix(
                    /*width*/ m_Width,
#ifdef INVERT_Y_AXIS
                    /*height*/ -m_Height,
#else
                    /*height*/ m_Height,
#endif
                    /*zNear*/ getNearPlane(),
                    /*zFar*/ getFarPlane());
            }

        #pragma endregion
    };
}
