/*
Copyright (c) 2020 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

#include <type_traits>
#include <utility>

#include "Vector.h"

namespace vmath
{
    /*
    A matrix. Elements are stored in column-major format.
    Template parameters:
    T - type of matrix elements
    Rows - number of rows
    Cols - number of columns
    */
    template <typename T, int Rows, int Cols>
    class Matrix
    {
        #pragma region Typedefs

        public:
            using column_vector = Vector<T, Rows>;

        private:
            using inner_container = std::array<column_vector, Cols>;

        #pragma endregion

        #pragma region Fields

        private:
            std::array<column_vector, Cols> cols;

        #pragma endregion

        #pragma region Constructors

        public:
            Matrix() noexcept :
                cols()
            {}

            explicit Matrix(T value) noexcept
                : cols()
            {
                column_vector vec(value);
                std::fill(cols.begin(), cols.end(), vec);
            }

            // copy constructor
            Matrix(const Matrix& mat) noexcept
                : cols(mat.cols)
            {}

            // copy constructor
            template<typename U>
            explicit Matrix(const Matrix<U, Rows, Cols>& rhs) noexcept
                : cols()
            {
                std::copy(rhs.cbegin(), rhs.cend(), cols.begin());
            }

            // move constructor
            Matrix(Matrix&& mat) noexcept
                : cols(std::move(mat.cols))
            {}

            Matrix(std::initializer_list<column_vector> lst) noexcept
                : cols()
            {
                std::copy(lst.begin(), lst.end(), cols.begin());
            }

        #pragma endregion

        #pragma region Properties

        public:
            T& at(int row, int col) noexcept
            {
                return cols[col][row];
            }

            const T& at(int row, int col) const noexcept
            {
                return cols[col][row];
            }

            column_vector& at(int col) noexcept
            {
                return cols.at(col);
            }

            const column_vector& at(int col) const noexcept
            {
                return cols.at(col);
            }

            column_vector& operator[](int col) noexcept
            {
                return cols[col];
            }

            const column_vector& operator[](int col) const noexcept
            {
                return cols[col];
            }

            T* data() noexcept
            {
                return cols[0].data();
            }

            const T* data() const noexcept
            {
                return cols[0].data();
            }

            static constexpr int rows()
            {
                return Rows;
            }

            static constexpr int columns()
            {
                return Cols;
            }

        #pragma endregion

        #pragma region Methods

        public:
            const Matrix& operator=(const Matrix& rhs) noexcept
            {
                std::copy(rhs.cbegin(), rhs.cend(), cols.begin());
                return *this;
            }

            const Matrix& operator=(Matrix&& rhs) noexcept
            {
                cols = std::move(rhs.cols);
                return *this;
            }

            template<typename U>
            const Matrix& assign(const Matrix<U, Rows, Cols>& rhs) noexcept
            {
                std::copy(rhs.cbegin(), rhs.cend(), cols.begin());
                return *this;
            }

            typename inner_container::iterator begin() noexcept
            {
                return cols.begin();
            }

            typename inner_container::iterator end() noexcept
            {
                return cols.end();
            }

            typename inner_container::const_iterator begin() const noexcept
            {
                return cols.cbegin();
            }

            typename inner_container::const_iterator end() const noexcept
            {
                return cols.cend();
            }

            typename inner_container::const_iterator cbegin() const noexcept
            {
                return cols.cbegin();
            }

            typename inner_container::const_iterator cend() const noexcept
            {
                return cols.cend();
            }

        #pragma endregion
    };

    // Outputs the matrix to a stream in a user-familiar style (row by row).
    template <typename T, int Rows, int Cols>
    std::ostream& print(
        std::ostream& os,
        const Matrix<T, Rows, Cols>& mat)
    {
        auto width = os.width();

        for (int row = 0; row < Rows; row++)
        {
            for (int col = 0; col < Cols; col++)
                os << std::setw(width) << mat.at(row, col) << ' ';

            os << std::endl;
        }

        return os;
    }

    /*
    Example of an input cycle:
    vmath::Matrix<int, 2, 2> mat(0);
    while (!(std::cin >> mat))
    {
        std::cin.clear();
        skip_to_newline();
        std::cout << "Bad input. Try again." << std::endl;
    }
    */
    template <typename T, int Rows, int Cols>
    std::ostream& operator<<(
        std::ostream& os,
        const Matrix<T, Rows, Cols>& mat)
    {
        auto width = os.width();
        os << std::setw(1) << "{" << std::endl;

        for (const auto& col : mat)
            os << std::setw(4) << "" << std::setw(width) << col << std::endl;

        os << "}";
        return os;
    }

    template <typename T, int Rows, int Cols>
    std::istream& operator>>(
        std::istream& is,
        Matrix<T, Rows, Cols>& mat)
    {
        char ch;
        if (is >> ch && ch == '{')
        {
            for (auto& col : mat)
            {
                if (!(is >> col))
                {
                    is.clear(std::ios_base::failbit);
                    return is;
                }
            }

            if (!(is >> ch && ch == '}'))
                is.clear(std::ios_base::failbit);
        }
        else
            is.clear(std::ios_base::failbit);

        return is;
    }

    // Transposes the matrix.
    template<typename T, int Dimension>
    void Transpose(Matrix<T, Dimension, Dimension>& mat) noexcept
    {
        T temp;
        for (int col = 0; col < Dimension - 1; col++)
        {
            for (int row = col + 1; row < Dimension; row++)
            {
                temp = mat[row][col];
                mat[row][col] = mat[col][row];
                mat[col][row] = temp;
            }
        }
    }

    // Creates a transposed matrix out of a specifed matrix.
    template <typename T, int Rows, int Cols>
    const Matrix<T, Cols, Rows> make_TransposedMatrix(const Matrix<T, Rows, Cols>& mat) noexcept
    {
        Matrix<T, Cols, Rows> result;
        for (int col = 0; col < Cols; col++)
        {
            for (int row = 0; row < Rows; row++)
                result[col][row] = mat[row][col];
        }
        return result;
    }

    // Creates a transposed matrix out of a specifed matrix.
    template <typename T, int Rows, int Cols>
    Matrix<T, Cols, Rows> make_TransposedMatrix(Matrix<T, Rows, Cols>&& mat) noexcept
    {
        for (int col = 0; col < Cols; col++)
        {
            for (int row = 0; row < Rows; row++)
                std::swap(mat[col][row], mat[row][col]);
        }
        return std::move(mat);
    }

    // Creates an identity matrix.
    template<typename T, int Dimension>
    const Matrix<T, Dimension, Dimension>& make_IdentityMatrix() noexcept
    {
        static Matrix<T, Dimension, Dimension> mat(0);
        if (mat[0][0] != T(1))
        {
            for (int i = 0; i < Dimension; i++)
                mat[i][i] = T(1);
        }
        return mat;
    }

    // Creates an identity 4x4 matrix.
    template<typename T>
    const Matrix<T, 4, 4>& make_IdentityMatrix4x4() noexcept
    {
        static Matrix<T, 4, 4> mat
        {
            { T(1), T(0), T(0), T(0) },
            { T(0), T(1), T(0), T(0) },
            { T(0), T(0), T(1), T(0) },
            { T(0), T(0), T(0), T(1) }
        };
        return mat;
    }

    /*
    Creates a 4-dimensional perspective projection matrix in left-handed camera and screen spaces.
    == Assumptions ==
    X-axis goes right both in camera space and in screen space
    Y-axis goes up both in camera space and in screen space
    Z-axis goes from the viewer deep into the screen
    == Parameters ==
    [viewAngle] - opening angle of the truncated pyramid in radians
    [aspectRatio] - screen width / height
    [zNear] - z-coordinate of the near to the viewer plane of the truncated pyramid. Positive number.
    [zFar] - z-coordinate of the far from the viewer plane of the truncated pyramid. Positive number.
    */
    template<typename T>
    const Matrix<T, 4, 4> make_PerspectiveProjectionMatrix(
        T viewAngle,
        T aspectRatio,
        T zNear,
        T zFar) noexcept
    {
        T S = 1.0f / std::tanf(viewAngle / 2.0f);

#ifdef VMATH_ZERO_TO_ONE_DEPTH
        return Matrix<T, 4, 4>
        {
            { S, T(0), T(0), T(0) },
            { T(0), S * aspectRatio, T(0), T(0) },
            { T(0), T(0), zFar / (zFar - zNear) , T(1) },
            { T(0), T(0), -zNear * zFar / (zFar - zNear) , T(0) }
        };
#else
        return Matrix<T, 4, 4>
        {
            { S, T(0), T(0), T(0) },
            { T(0), S*aspectRatio, T(0), T(0) },
            { T(0), T(0), (zFar + zNear) / (zFar - zNear) , T(1) },
            { T(0), T(0), -2 * zNear*zFar / (zFar - zNear) , T(0) }
        };
#endif
    }

    template<>
    inline const Matrix<double, 4, 4> make_PerspectiveProjectionMatrix(
        double viewAngle,
        double aspectRatio,
        double zNear,
        double zFar) noexcept
    {
        double S = 1.0 / std::tan(viewAngle / 2.0);
#ifdef VMATH_ZERO_TO_ONE_DEPTH
        return Matrix<double, 4, 4>
        {
            { S, 0.0, 0.0, 0.0 },
            { 0.0, S * aspectRatio, 0.0, 0.0 },
            { 0.0, 0.0, zFar / (zFar - zNear) , 0.0 },
            { 0.0, 0.0, -zNear * zFar / (zFar - zNear) , 0.0 }
        };
#else
        return Matrix<double, 4, 4>
        {
            { S, 0.0, 0.0, 0.0 },
            { 0.0, S*aspectRatio, 0.0, 0.0 },
            { 0.0, 0.0, (zFar + zNear) / (zFar - zNear) , 0.0 },
            { 0.0, 0.0, -2.0 * zNear*zFar / (zFar - zNear) , 0.0 }
        };
#endif
    }

    /*
    Creates a 4-dimensional perspective projection matrix in right-handed camera and screen spaces.
    == Assumptions ==
    X-axis goes right both in camera space and in screen space
    Y-axis goes up both in camera space and in screen space
    Z-axis goes out of the screen towards the viewer
    == Parameters ==
    [viewAngle] - opening angle of the truncated pyramid in radians
    [aspectRatio] - screen width / height
    [zNear] - z-coordinate of the near to the viewer plane of the truncated pyramid. Negative number.
    [zFar] - z-coordinate of the far from the viewer plane of the truncated pyramid. Negative number.
    */
    template<typename T>
    const Matrix<T, 4, 4> make_PerspectiveProjectionMatrixRightHanded(
        T viewAngle,
        T aspectRatio,
        T zNear,
        T zFar) noexcept
    {
        T S = 1.0f / std::tanf(viewAngle / 2.0f);
#ifdef VMATH_ZERO_TO_ONE_DEPTH
        return Matrix<T, 4, 4>
        {
            { -S, T(0), T(0), T(0) },
            { T(0), -S * aspectRatio, T(0), T(0) },
            { T(0), T(0), zFar / (zFar - zNear) , T(1) },
            { T(0), T(0), -zNear * zFar / (zFar - zNear) , T(0) }
        };
#else
        return Matrix<T, 4, 4>
        {
            { -S, T(0), T(0), T(0) },
            { T(0), -S*aspectRatio, T(0), T(0) },
            { T(0), T(0), (zFar + zNear) / (zFar - zNear) , T(1) },
            { T(0), T(0), -2 * zNear*zFar / (zFar - zNear) , T(0) }
        };
#endif
    }

    template<>
    inline const Matrix<double, 4, 4> make_PerspectiveProjectionMatrixRightHanded(
        double viewAngle,
        double aspectRatio,
        double zNear,
        double zFar) noexcept
    {
        double S = 1.0 / std::tan(viewAngle / 2.0);
#ifdef VMATH_ZERO_TO_ONE_DEPTH
        return Matrix<double, 4, 4>
        {
            { -S, 0.0, 0.0, 0.0 },
            { 0.0, -S * aspectRatio, 0.0, 0.0 },
            { 0.0, 0.0, zFar / (zFar - zNear) , 0.0 },
            { 0.0, 0.0, -zNear * zFar / (zFar - zNear) , 0.0 }
        };
#else
        return Matrix<double, 4, 4>
        {
            { -S, 0.0, 0.0, 0.0 },
            { 0.0, -S*aspectRatio, 0.0, 0.0 },
            { 0.0, 0.0, (zFar + zNear) / (zFar - zNear) , 0.0 },
            { 0.0, 0.0, -2.0 * zNear*zFar / (zFar - zNear) , 0.0 }
        };
#endif
    }

    /*
    Creates an orthographic projection matrix
    Parameters:
    left, right, bottom, top, zNear, zFar - bounds of the space
    */
    template<typename T>
    const Matrix<T, 4, 4> make_OrthographicProjectionMatrix(
        T width, T height,
        T zNear, T zFar) noexcept
    {
#ifdef VMATH_ZERO_TO_ONE_DEPTH
        return Matrix<T, 4, 4>
        {
            { 2.0f / width, T(0), T(0), T(0) },
            { T(0), 2.0f / height, T(0), T(0) },
            { T(0), T(0), 1.0f / (zFar - zNear), T(0) },
            { T(0), T(0), zNear / (zNear - zFar), T(1) }
        };
#else
        return Matrix<T, 4, 4>
        {
            { 2.0f / width, T(0), T(0), T(0) },
            { T(0), 2.0f / height, T(0), T(0) },
            { T(0), T(0), 2.0f / (zFar - zNear), T(0) },
            { T(0), T(0), (zNear + zFar) / (zNear - zFar), T(1) }
        };
#endif
    }

    /*
    Creates a 4-dimensional translation matrix.
    Parameters:
    [translation_vec] - translation vector
    */
    template<typename T>
    const Matrix<T, 4, 4> make_TranslationMatrix(
        const Vector<T, 3>& translation_vec) noexcept
    {
        return Matrix<T, 4, 4>
        {
            { T(1), T(0), T(0), T(0) },
            { T(0), T(1), T(0), T(0) },
            { T(0), T(0), T(1), T(0) },
            { translation_vec.X(), translation_vec.Y(), translation_vec.Z(), T(1) }
        };
    }

    /*
    Creates a 3-dimensional rotation matrix.
    Rotation is clockwise, when viewed in the direction specified by the rotation axis vector.
    Left-handed coordinate systems is assumed.
    Parameters:
    [rotation_axis_vec] - specifies rotation axis direction (assumed to be normalized i. e. of unit length)
    [angle] - rotation angle in radians
    */
    template<typename T>
    const Matrix<T, 3, 3> make_RotationMatrix3x3(
        const Vector<T, 3>& rotation_axis_vec,
        T angle) noexcept
    {
        const T x = rotation_axis_vec.X();
        const T y = rotation_axis_vec.Y();
        const T z = rotation_axis_vec.Z();

        const T C = cos(angle);
        const T S = -sin(angle);
        const T iC = 1 - C;
        const T iS = 1 - S;

        return Matrix<T, 3, 3>
        {
            { x*x + (1 - x*x)*C, iC*x*y + z*S, iC*x*z - y*S },
            { iC*x*y - z*S, y*y + (1 - y*y)*C, iC*y*z + x*S },
            { iC*x*z + y*S, iC*y*z - x*S, z*z + (1 - z*z)*C }
        };
    }

    /*
    Creates a 4-dimensional rotation matrix.
    Rotation is clockwise, when viewed in the direction specified by the rotation axis vector.
    Left-handed coordinate systems is assumed.
    Parameters:
    [rotation_axis_vec] - specifies rotation axis direction (assumed to be normalized i. e. of unit length)
    [angle] - rotation angle in radians
    */
    template<typename T>
    const Matrix<T, 4, 4> make_RotationMatrix4x4(
        const Vector<T, 3>& rotation_axis_vec,
        T angle) noexcept
    {
        const T x = rotation_axis_vec.X();
        const T y = rotation_axis_vec.Y();
        const T z = rotation_axis_vec.Z();

        const T C = cos(angle);
        const T S = -sin(angle);
        const T iC = 1 - C;
        const T iS = 1 - S;

        return Matrix<T, 4, 4>
        {
            { x*x + (1 - x*x)*C, iC*x*y + z*S, iC*x*z - y*S, T(0) },
            { iC*x*y - z*S, y*y + (1 - y*y)*C, iC*y*z + x*S, T(0) },
            { iC*x*z + y*S, iC*y*z - x*S, z*z + (1 - z*z)*C, T(0) },
            { T(0), T(0), T(0), T(1) }
        };
    }

    /*
    Creates a 4-dimensional rotation matrix.
    Rotation is clockwise around X-axis
    Left-handed coordinate systems is assumed.
    Parameters:
    [angle] - rotation angle in radians
    */
    template<typename T>
    const Matrix<T, 4, 4> make_XRotationMatrix4x4(T angle) noexcept
    {
        T C = cos(-angle);
        T S = sin(-angle);

        return Matrix<T, 4, 4>
        {
            { T(1),  T(0), T(0), T(0) },
            { T(0),  C,    S,    T(0) },
            { T(0), -S,    C,    T(0) },
            { T(0),  T(0), T(0), T(1) }
        };
    }

    /*
    Creates a 4-dimensional rotation matrix.
    Rotation is clockwise around Y-axis
    Left-handed coordinate systems is assumed.
    Parameters:
    [angle] - rotation angle in radians
    */
    template<typename T>
    const Matrix<T, 4, 4> make_YRotationMatrix4x4(T angle) noexcept
    {
        T C = cos(-angle);
        T S = sin(-angle);

        return Matrix<T, 4, 4>
        {
            { C,    T(0), -S,   T(0) },
            { T(0), T(1), T(0), T(0) },
            { S,    T(0), C,    T(0) },
            { T(0), T(0), T(0), T(1) }
        };
    }

    /*
    Creates a 4-dimensional rotation matrix.
    Rotation is clockwise around Z-axis
    Left-handed coordinate systems is assumed.
    Parameters:
    [angle] - rotation angle in radians
    */
    template<typename T>
    const Matrix<T, 4, 4> make_ZRotationMatrix4x4(T angle) noexcept
    {
        T C = cos(-angle);
        T S = sin(-angle);

        return Matrix<T, 4, 4>
        {
            {  C,   S,    T(0), T(0) },
            { -S,   C,    T(0), T(0) },
            { T(0), T(0), T(1), T(0) },
            { T(0), T(0), T(0), T(1) }
        };
    }

    /*
    Creates a 3-dimensional rotation matrix.
    Rotation is clockwise around X-axis
    Left-handed coordinate systems is assumed.
    Parameters:
    [angle] - rotation angle in radians
    */
    template<typename T>
    const Matrix<T, 3, 3> make_XRotationMatrix3x3(T angle) noexcept
    {
        T C = cos(-angle);
        T S = sin(-angle);

        return Matrix<T, 3, 3>
        {
            { T(1), T(0), T(0) },
            { T(0),  C,   S    },
            { T(0), -S,   C    }
        };
    }

    /*
    Creates a 3-dimensional rotation matrix.
    Rotation is clockwise around Y-axis
    Left-handed coordinate systems is assumed.
    Parameters:
    [angle] - rotation angle in radians
    */
    template<typename T>
    const Matrix<T, 3, 3> make_YRotationMatrix3x3(T angle) noexcept
    {
        T C = cos(-angle);
        T S = sin(-angle);

        return Matrix<T, 3, 3>
        {
            { C,    T(0), -S },
            { T(0), T(1), T(0) },
            { S,    T(0), C }
        };
    }

    /*
    Creates a 3-dimensional rotation matrix.
    Rotation is clockwise around Z-axis
    Left-handed coordinate systems is assumed.
    Parameters:
    [angle] - rotation angle in radians
    */
    template<typename T>
    const Matrix<T, 3, 3> make_ZRotationMatrix3x3(T angle) noexcept
    {
        T C = cos(-angle);
        T S = sin(-angle);

        return Matrix<T, 3, 3>
        {
            {  C,   S,    T(0) },
            { -S,   C,    T(0) },
            { T(0), T(0), T(1) }
        };
    }

    /*
    Creates a 4-dimensional anisotropic scale matrix.
    Parameters:
    [scale_vec] - vector containing scale factors along X, Y and Z axes
    */
    template<typename T>
    const Matrix<T, 4, 4> make_ScaleMatrix(const Vector<T, 3>& scale_vec) noexcept
    {
        return Matrix<T, 4, 4>
        {
            { scale_vec.X(), T(0), T(0), T(0) },
            { T(0), scale_vec.Y(), T(0), T(0) },
            { T(0), T(0), scale_vec.Z(), T(0) },
            { T(0), T(0), T(0), T(1) }
        };
    }

    /*
    Creates a 4-dimensional isotropic scale matrix.
    Parameters:
    [scale] - scale factor along X, Y and Z axes
    */
    template<typename T>
    const Matrix<T, 4, 4> make_ScaleMatrix(const T scale) noexcept
    {
        return Matrix<T, 4, 4>
        {
            { scale, T(0), T(0), T(0) },
            { T(0), scale, T(0), T(0) },
            { T(0), T(0), scale, T(0) },
            { T(0), T(0), T(0), T(1) }
        };
    }

    /*
    Creates a 3-dimensional reflection matrix. 
    The reflection plane goes through the origin of coordinates.
    Parameters:
    [normal] - normal vector of the reflection plane.
    */
    template<typename T>
    const Matrix<T, 3, 3> make_ReflectionMatrix3x3(const Vector<T, 3> normal) noexcept
    {
        return Matrix<T, 3, 3>
        {
            { T(1) - T(2) * normal.X() * normal.X(), -T(2) * normal.X() * normal.Y(), -T(2) * normal.X() * normal.Z() },
            { -T(2) * normal.X() * normal.Y(), T(1) - T(2) * normal.Y() * normal.Y(), -T(2) * normal.Y() * normal.Z() },
            { -T(2) * normal.X() * normal.Z(), -T(2) * normal.Y() * normal.Z(), T(1) - T(2) * normal.Z() * normal.Z() }
        };
    }

    /*
    Creates a 4-dimensional reflection matrix.
    The reflection plane does not necessarily go through the origin of coordinates.
    Parameters:
    [normal] - normal vector of the reflection plane.
    [distance] - the distance of the reflection plane from the origin of coordinates.
    */
    template<typename T>
    const Matrix<T, 4, 4> make_ReflectionMatrix4x4(const Vector<T, 3> normal, T distance) noexcept
    {
        return Matrix<T, 4, 4>
        {
            { T(1) - T(2) * normal.X() * normal.X(), -T(2) * normal.X() * normal.Y(), -T(2) * normal.X() * normal.Z(), T(0) },
            { -T(2) * normal.X() * normal.Y(), T(1) - T(2) * normal.Y() * normal.Y(), -T(2) * normal.Y() * normal.Z(), T(0) },
            { -T(2) * normal.X() * normal.Z(), -T(2) * normal.Y() * normal.Z(), T(1) - T(2) * normal.Z() * normal.Z(), T(0) },
            { -T(2) * normal.X() * distance, -T(2) * normal.Y() * distance, -T(2) * normal.Z() * distance, T(1) }
        };
    }

    /*
    Creates a world-to-camera matrix which transforms world coordinates into camera coordinates.
    Left-handed coordinate systems is assumed.
    == Parameters ==
    [camPosition] - camera position vector in world space
    [camViewDirection] - camera view direction (z-axis direction) in world space
    [camUpDirection] - camera up direction (y-axis direction) in world space
    == Assumptions ==
    [camViewDirection] is normalized (i. e. it is of unit length).
    [camUpDirection] is normalized (i. e. it is of unit length).
    [camUpDirection] is not necessarily the same as the camera's y-axis direction, but it must not be the same as camViewDirection.
    Handedness of the camera space is the same as that of the world space.
    */
    template<typename T>
    const Matrix<T, 4, 4> make_WorldToCameraMatrix(
        const Vector<T, 3>& camPosition,
        const Vector<T, 3>& camViewDirection,
        const Vector<T, 3>& camUpDirection) noexcept
    {
        auto s = cross_product(camUpDirection, camViewDirection); // X = [Y x Z] = [Up x View]
        auto u = cross_product(camViewDirection, s); // Y = [Z x X] = [View x S]

        Matrix<T, 4, 4> viewMat
        {
            { s.X(), u.X(), camViewDirection.X(), T(0) },
            { s.Y(), u.Y(), camViewDirection.Y(), T(0) },
            { s.Z(), u.Z(), camViewDirection.Z(), T(0) },
            { T(0), T(0),  T(0), T(1) }
        };

        //return viewMat * make_TranslationMatrix(-camPosition);
        viewMat.at(3) = viewMat * Vector<T, 4>{ -camPosition.X(), -camPosition.Y(), -camPosition.Z(), 1.0f };
        return viewMat;
    }

    /*
    Creates a model-to-world matrix which transforms model coordinates into world coordinates.
    Left-handed coordinate systems is assumed.
    == Parameters ==
    [position] - position vector of the model in world space
    [forwardDirection] - forward direction of the model (z-axis direction) in world space
    [upDirection] - up direction of the model (y-axis direction) in world space
    == Assumptions ==
    [forwardDirection] is normalized (i. e. it is of unit length).
    [upDirection] is normalized (i. e. it is of unit length).
    [upDirection] is not necessarily the same as model's y-axis direction, but it shouldn't be the same as forwardDirection.
    Handedness of the model space is the same as that of the world space.
    */
    template<typename T>
    const Matrix<T, 4, 4> make_ModelToWorldMatrix(
        const Vector<T, 3>& position,
        const Vector<T, 3>& forwardDirection,
        const Vector<T, 3>& upDirection) noexcept
    {
        auto s = cross_product(upDirection, forwardDirection); // X = [Y x Z] = [Up x Forward]
        auto u = cross_product(forwardDirection, s); // Y = [Z x X] = [Forward x S]

        return Matrix<T, 4, 4>
        {
            { s.X(), s.Y(), s.Z(), T(0) },
            { u.X(), u.Y(), u.Z(), T(0) },
            { forwardDirection.X(), forwardDirection.Y(), forwardDirection.Z(), T(0) },
            { position.X(), position.Y(), position.Z(), T(1) }
        };
    }

    /*
    Eliminates translation from the intial matrix [m].
    This function is needed for example to make a matrix for the
    model-to-world transformation of normals of a surface (initial matrix in that case
    is the model-to-world transformation matrix).
    */
    template<typename T>
    const Matrix<T, 4, 4> eliminateTranslation(const Matrix<T, 4, 4>& m) noexcept
    {
        Matrix<T, 4, 4> newMat(m);
        newMat.at(0, 3) = T(0);
        newMat.at(1, 3) = T(0);
        newMat.at(2, 3) = T(0);
        return newMat;
    }

    /*
    Eliminates translation from the intial matrix [m].
    This function is needed for example to make a matrix for the
    model-to-world transformation of normals of a surface (initial matrix in that case
    is the model-to-world transformation matrix).
    */
    template<typename T>
    Matrix<T, 4, 4> eliminateTranslation(Matrix<T, 4, 4>&& m) noexcept
    {
        m.at(0, 3) = T(0);
        m.at(1, 3) = T(0);
        m.at(2, 3) = T(0);
        return std::move(m);
    }

    // Returns determinant of a matrix
    template<typename T>
    T Determinant(const Matrix<T, 3, 3>& m) noexcept
    {
        return
            m[0][0] * m[1][1] * m[2][2] + m[0][1] * m[1][2] * m[2][0] + m[0][2] * m[1][0] * m[2][1] -
            m[2][0] * m[1][1] * m[0][2] - m[2][1] * m[1][2] * m[0][0] - m[2][2] * m[1][0] * m[0][1];
    }

    // Returns determinant of a matrix
    template<typename T>
    T Determinant(const Matrix<T, 4, 4>& m) noexcept
    {
        return
            m[0][3] * m[1][2] * m[2][1] * m[3][0] - m[0][2] * m[1][3] * m[2][1] * m[3][0] - m[0][3] * m[1][1] * m[2][2] * m[3][0] + m[0][1] * m[1][3] * m[2][2] * m[3][0] +
            m[0][2] * m[1][1] * m[2][3] * m[3][0] - m[0][1] * m[1][2] * m[2][3] * m[3][0] - m[0][3] * m[1][2] * m[2][0] * m[3][1] + m[0][2] * m[1][3] * m[2][0] * m[3][1] +
            m[0][3] * m[1][0] * m[2][2] * m[3][1] - m[0][0] * m[1][3] * m[2][2] * m[3][1] - m[0][2] * m[1][0] * m[2][3] * m[3][1] + m[0][0] * m[1][2] * m[2][3] * m[3][1] +
            m[0][3] * m[1][1] * m[2][0] * m[3][2] - m[0][1] * m[1][3] * m[2][0] * m[3][2] - m[0][3] * m[1][0] * m[2][1] * m[3][2] + m[0][0] * m[1][3] * m[2][1] * m[3][2] +
            m[0][1] * m[1][0] * m[2][3] * m[3][2] - m[0][0] * m[1][1] * m[2][3] * m[3][2] - m[0][2] * m[1][1] * m[2][0] * m[3][3] + m[0][1] * m[1][2] * m[2][0] * m[3][3] +
            m[0][2] * m[1][0] * m[2][1] * m[3][3] - m[0][0] * m[1][2] * m[2][1] * m[3][3] - m[0][1] * m[1][0] * m[2][2] * m[3][3] + m[0][0] * m[1][1] * m[2][2] * m[3][3];
    }

    // Returns the inverse matrix
    template<typename T>
    const Matrix<T, 4, 4> make_InverseMatrix(const Matrix<T, 4, 4>& m) noexcept
    {
        T determinant = Determinant(m);
        if (determinant == 0)
            throw std::runtime_error("Matrix must have nonzero determinant");

        return Matrix<T, 4, 4>
        {
            {
                (m[1][2] * m[2][3] * m[3][1] - m[1][3] * m[2][2] * m[3][1] + m[1][3] * m[2][1] * m[3][2] - m[1][1] * m[2][3] * m[3][2] - m[1][2] * m[2][1] * m[3][3] + m[1][1] * m[2][2] * m[3][3]) / determinant,
                (m[0][3] * m[2][2] * m[3][1] - m[0][2] * m[2][3] * m[3][1] - m[0][3] * m[2][1] * m[3][2] + m[0][1] * m[2][3] * m[3][2] + m[0][2] * m[2][1] * m[3][3] - m[0][1] * m[2][2] * m[3][3]) / determinant,
                (m[0][2] * m[1][3] * m[3][1] - m[0][3] * m[1][2] * m[3][1] + m[0][3] * m[1][1] * m[3][2] - m[0][1] * m[1][3] * m[3][2] - m[0][2] * m[1][1] * m[3][3] + m[0][1] * m[1][2] * m[3][3]) / determinant,
                (m[0][3] * m[1][2] * m[2][1] - m[0][2] * m[1][3] * m[2][1] - m[0][3] * m[1][1] * m[2][2] + m[0][1] * m[1][3] * m[2][2] + m[0][2] * m[1][1] * m[2][3] - m[0][1] * m[1][2] * m[2][3]) / determinant
            },
            {
                (m[1][3] * m[2][2] * m[3][0] - m[1][2] * m[2][3] * m[3][0] - m[1][3] * m[2][0] * m[3][2] + m[1][0] * m[2][3] * m[3][2] + m[1][2] * m[2][0] * m[3][3] - m[1][0] * m[2][2] * m[3][3]) / determinant,
                (m[0][2] * m[2][3] * m[3][0] - m[0][3] * m[2][2] * m[3][0] + m[0][3] * m[2][0] * m[3][2] - m[0][0] * m[2][3] * m[3][2] - m[0][2] * m[2][0] * m[3][3] + m[0][0] * m[2][2] * m[3][3]) / determinant,
                (m[0][3] * m[1][2] * m[3][0] - m[0][2] * m[1][3] * m[3][0] - m[0][3] * m[1][0] * m[3][2] + m[0][0] * m[1][3] * m[3][2] + m[0][2] * m[1][0] * m[3][3] - m[0][0] * m[1][2] * m[3][3]) / determinant,
                (m[0][2] * m[1][3] * m[2][0] - m[0][3] * m[1][2] * m[2][0] + m[0][3] * m[1][0] * m[2][2] - m[0][0] * m[1][3] * m[2][2] - m[0][2] * m[1][0] * m[2][3] + m[0][0] * m[1][2] * m[2][3]) / determinant
            },
            {
                (m[1][1] * m[2][3] * m[3][0] - m[1][3] * m[2][1] * m[3][0] + m[1][3] * m[2][0] * m[3][1] - m[1][0] * m[2][3] * m[3][1] - m[1][1] * m[2][0] * m[3][3] + m[1][0] * m[2][1] * m[3][3]) / determinant,
                (m[0][3] * m[2][1] * m[3][0] - m[0][1] * m[2][3] * m[3][0] - m[0][3] * m[2][0] * m[3][1] + m[0][0] * m[2][3] * m[3][1] + m[0][1] * m[2][0] * m[3][3] - m[0][0] * m[2][1] * m[3][3]) / determinant,
                (m[0][1] * m[1][3] * m[3][0] - m[0][3] * m[1][1] * m[3][0] + m[0][3] * m[1][0] * m[3][1] - m[0][0] * m[1][3] * m[3][1] - m[0][1] * m[1][0] * m[3][3] + m[0][0] * m[1][1] * m[3][3]) / determinant,
                (m[0][3] * m[1][1] * m[2][0] - m[0][1] * m[1][3] * m[2][0] - m[0][3] * m[1][0] * m[2][1] + m[0][0] * m[1][3] * m[2][1] + m[0][1] * m[1][0] * m[2][3] - m[0][0] * m[1][1] * m[2][3]) / determinant
            },
            {
                (m[1][2] * m[2][1] * m[3][0] - m[1][1] * m[2][2] * m[3][0] - m[1][2] * m[2][0] * m[3][1] + m[1][0] * m[2][2] * m[3][1] + m[1][1] * m[2][0] * m[3][2] - m[1][0] * m[2][1] * m[3][2]) / determinant,
                (m[0][1] * m[2][2] * m[3][0] - m[0][2] * m[2][1] * m[3][0] + m[0][2] * m[2][0] * m[3][1] - m[0][0] * m[2][2] * m[3][1] - m[0][1] * m[2][0] * m[3][2] + m[0][0] * m[2][1] * m[3][2]) / determinant,
                (m[0][2] * m[1][1] * m[3][0] - m[0][1] * m[1][2] * m[3][0] - m[0][2] * m[1][0] * m[3][1] + m[0][0] * m[1][2] * m[3][1] + m[0][1] * m[1][0] * m[3][2] - m[0][0] * m[1][1] * m[3][2]) / determinant,
                (m[0][1] * m[1][2] * m[2][0] - m[0][2] * m[1][1] * m[2][0] + m[0][2] * m[1][0] * m[2][1] - m[0][0] * m[1][2] * m[2][1] - m[0][1] * m[1][0] * m[2][2] + m[0][0] * m[1][1] * m[2][2]) / determinant
            }
        };
    }

    /*
    Returns the product of a matrix and a vector.
    The vector must have as many elements as the number of matrix's columns.
    */
    template<typename T, int Rows, int Cols>
    const Vector<T, Rows> operator*(
        const Matrix<T, Rows, Cols>& mat,
        const Vector<T, Cols>& vec) noexcept
    {
        Vector<T, Rows> product(0);

        for (int col = 0; col < Cols; col++)
            product += (vec[col] * mat[col]);

        return product;
    }

    /*
    Returns the product of two matrices.
    Number of columns of the 1st matrix must be the same as number of rows of the 2nd matrix.
    Template parameters:
    T - matrix element type
    FirstRows - 1st matrix rows number
    SecondCols - 2nd matrix columns number
    Common - 1st matrix columns number as well as 2nd matrix rows number
    */
    template<typename T, int FirstRows, int Common, int SecondCols>
    const Matrix<T, FirstRows, SecondCols> operator*(
        const Matrix<T, FirstRows, Common>& lhs,
        const Matrix<T, Common, SecondCols>& rhs) noexcept
    {
        Matrix<T, FirstRows, SecondCols> product;

        for (int col = 0; col < SecondCols; col++)
            product[col] = lhs * rhs[col];

        return product;
    }

// Below are functions utilizing Intel's Streaming SIMD Extensions (SSE).
#ifdef USE_SSE

// Due to a bug in VS2017 optimization causes access violation at memory address 0xFFFFFFFF (misaligned data)
// https://developercommunity.visualstudio.com/content/problem/174967/read-access-violation-when-moving-16-byte-aligned.html
#pragma optimize( "", off )

#define SHUFFLE_PARAM(x, y, z, w) ((x) | ((y) << 2) | ((z) << 4) | ((w) << 6))
#define _mm_replicate_x_ps(v) _mm_shuffle_ps((v), (v), SHUFFLE_PARAM(0, 0, 0, 0))
#define _mm_replicate_y_ps(v) _mm_shuffle_ps((v), (v), SHUFFLE_PARAM(1, 1, 1, 1))
#define _mm_replicate_z_ps(v) _mm_shuffle_ps((v), (v), SHUFFLE_PARAM(2, 2, 2, 2))
#define _mm_replicate_w_ps(v) _mm_shuffle_ps((v), (v), SHUFFLE_PARAM(3, 3, 3, 3))

    inline const Vector<float, 4> operator*(
        const Matrix<float, 4, 4>& mat,
        const Vector<float, 4>& vec) noexcept
    {
        auto mcol0 = _mm_load_ps(mat.at(0).data());
        auto mcol1 = _mm_load_ps(mat.at(1).data());
        auto mcol2 = _mm_load_ps(mat.at(2).data());
        auto mcol3 = _mm_load_ps(mat.at(3).data());
        auto v = _mm_load_ps(vec.data());

        auto xxxx = _mm_replicate_x_ps(v);
        auto yyyy = _mm_replicate_y_ps(v);
        auto zzzz = _mm_replicate_z_ps(v);
        auto wwww = _mm_replicate_w_ps(v);

        auto xmcol0 = _mm_mul_ps(xxxx, mcol0);
        auto ymcol1 = _mm_mul_ps(yyyy, mcol1);
        auto zmcol2 = _mm_mul_ps(zzzz, mcol2);
        auto wmcol3 = _mm_mul_ps(wwww, mcol3);

        auto result = _mm_add_ps(xmcol0, ymcol1);
        result = _mm_add_ps(result, zmcol2);
        result = _mm_add_ps(result, wmcol3);

        __declspec(align(16)) Vector<float, 4> retVal;
        _mm_store_ps(retVal.data(), result);
        return retVal;
    }
#pragma optimize( "", on )

#endif
}