/*
Copyright (c) 2020 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

#include <iostream>
#include <limits>
#include <string>
#include <set>

#include "utility.h"

#include "Window.h"
#include "WinUtility.h"
#include "HighResolutionTimer.h"

#include "Camera.h"

namespace engine
{
    class ProgramBase
    {
        #pragma region Fields

        private:
            win::Window m_Window;
            PerspectiveCamera m_Camera;
            std::set<WPARAM> m_VirtualKeyCodes;
            double currentTimeSeconds;
            double timeSinceLastFrameSeconds;
            float cameraSpeed = 10.0; // camera speed in units per second

        #pragma endregion

        #pragma region Constructors

        public:
            ProgramBase() :
                m_Window("Test Window", 1280, 720),
                m_Camera(),
                m_VirtualKeyCodes(),
                currentTimeSeconds(0.0),
                timeSinceLastFrameSeconds(0.0)
            {
                // TODO: INIT VULKAN
                InitEventHandlers();

                auto clientRect = m_Window.getClientRect();
                // TODO: SET APPROPRIATE VIEWPORT
            }

        #pragma endregion

        #pragma region Destructor

        public:
            virtual ~ProgramBase() { }

        #pragma endregion

        #pragma region Deleted Functions

        private:
            ProgramBase(const ProgramBase&) = delete;
            ProgramBase& operator=(const ProgramBase&) = delete;

        #pragma endregion

        #pragma region Properties

        public:
            win::Window& getWindow() { return m_Window; }

            engine::PerspectiveCamera& getCamera() { return m_Camera; }

            // Gets time in seconds passed since the start of the program.
            const double CurrentTime() const { return currentTimeSeconds; }

            const float& CameraSpeed() const { return cameraSpeed; }
            float& CameraSpeed() { return cameraSpeed; }

            // Gets time in seconds passed since the last call to UpdateTime().
            const double TimeSinceLastFrame() const { return timeSinceLastFrameSeconds; }

            const std::set<WPARAM>& PressedKeys() const { return m_VirtualKeyCodes; }

        #pragma endregion

        #pragma region Methods

        public:
            void Main()
            {
                m_Window.Show();

                MSG msg{ 0 };
                while (msg.message != WM_QUIT)
                {
                    if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
                    {
                        //TranslateMessage(&msg);
                        DispatchMessage(&msg);
                    }
                    else
                        UpdateTimeAndRender();
                }
            }

        private:
            void InitEventHandlers()
            {
                m_Window.PaintEvent() += fastdelegate::MakeDelegate(this, &ProgramBase::OnPaint);
                m_Window.MouseMoveEvent() += fastdelegate::MakeDelegate(this, &ProgramBase::OnMouseMove);
                m_Window.KeyDownEvent() += fastdelegate::MakeDelegate(this, &ProgramBase::OnKeyDown);
                m_Window.KeyUpEvent() += fastdelegate::MakeDelegate(this, &ProgramBase::OnKeyUp);
                m_Window.SizeChangedEvent() += fastdelegate::MakeDelegate(this, &ProgramBase::OnSizeChanged);
            }

            void HandleKeys()
            {
                bool setup_camera = false;
                float speed = cameraSpeed * (m_VirtualKeyCodes.find(VK_SHIFT) != m_VirtualKeyCodes.end() ? 0.5f : 1.0f);

                if (m_VirtualKeyCodes.find('A') != m_VirtualKeyCodes.end() || m_VirtualKeyCodes.find(VK_LEFT) != m_VirtualKeyCodes.end())
                {
                    m_Camera.PositionAndOrientation().MoveRightLeft(static_cast<float>(-speed * TimeSinceLastFrame()));
                    setup_camera = true;
                }
                else if (m_VirtualKeyCodes.find('D') != m_VirtualKeyCodes.end() || m_VirtualKeyCodes.find(VK_RIGHT) != m_VirtualKeyCodes.end())
                {
                    m_Camera.PositionAndOrientation().MoveRightLeft(static_cast<float>(speed * TimeSinceLastFrame()));
                    setup_camera = true;
                }

                if (m_VirtualKeyCodes.find('W') != m_VirtualKeyCodes.end() || m_VirtualKeyCodes.find(VK_UP) != m_VirtualKeyCodes.end())
                {
                    m_Camera.PositionAndOrientation().MoveForwardBackward(static_cast<float>(speed * TimeSinceLastFrame()));
                    setup_camera = true;
                }
                else if (m_VirtualKeyCodes.find('S') != m_VirtualKeyCodes.end() || m_VirtualKeyCodes.find(VK_DOWN) != m_VirtualKeyCodes.end())
                {
                    m_Camera.PositionAndOrientation().MoveForwardBackward(static_cast<float>(-speed * TimeSinceLastFrame()));
                    setup_camera = true;
                }

                if (m_VirtualKeyCodes.find(VK_PRIOR) != m_VirtualKeyCodes.end()) // PgUp
                {
                    m_Camera.PositionAndOrientation().MoveUpDownWorld(static_cast<float>(speed * TimeSinceLastFrame()));
                    setup_camera = true;
                }
                else if (m_VirtualKeyCodes.find(VK_NEXT) != m_VirtualKeyCodes.end()) // PgDown
                {
                    m_Camera.PositionAndOrientation().MoveUpDownWorld(static_cast<float>(-speed * TimeSinceLastFrame()));
                    setup_camera = true;
                }

                if (setup_camera)
                    setupCamera(m_Camera);
            }

            void UpdateTimeAndRender()
            {
                static uint64_t timeStart = win::HighResolutionTimer::getTicks();
                static uint64_t lastFrameTime = 0;
                
                uint64_t currentTime = win::HighResolutionTimer::getTicks() - timeStart;

                currentTimeSeconds = currentTime / static_cast<double>(win::HighResolutionTimer::getTicksPerSecond());
                timeSinceLastFrameSeconds = (currentTime - lastFrameTime) / static_cast<double>(win::HighResolutionTimer::getTicksPerSecond());

                UpdateTime();
                HandleKeys();

                lastFrameTime = currentTime;

                // RENDER
                OnRender();

                // >>> Frame rate estimation >>>
                static int frames = 0;
                static uint64_t start_ticks = 0;

                ++frames;
                uint64_t delta_ticks = currentTime - start_ticks;
                if (delta_ticks > win::HighResolutionTimer::getTicksPerSecond())
                {
                    auto seconds = delta_ticks / static_cast<double>(win::HighResolutionTimer::getTicksPerSecond());
                    std::cout << std::fixed << std::setprecision(1) << frames / seconds << std::endl;

                    start_ticks = currentTime;
                    frames = 0;

                    EachSecond(seconds);
                }
                // <<< Frame rate estimation <<<
            }

            void OnMouseMove(std::tuple<win::Window*, int, int, WPARAM> eventArgs);

            void OnKeyDown(std::tuple<win::Window*, WPARAM> eventArgs);

            void OnKeyUp(std::tuple<win::Window*, WPARAM> eventArgs)
            {
                m_VirtualKeyCodes.erase(std::get<1>(eventArgs));
            }

            void OnPaint(std::tuple<win::Window*, HDC> eventArgs)
            {
                UpdateTimeAndRender();
            }

            void OnSizeChanged(std::tuple<win::Window*, int, int> eventArgs)
            {
                auto width = std::get<1>(eventArgs);
                auto height = std::get<2>(eventArgs);

                if (width > 0 && height > 0)
                {
                    // In Vulkan you have to recreate a
                    // lot of stuff when the window gets resized
                    OnSizeChangedOverride(width, height);
                    
                    // Update camera's aspect ratio
                    m_Camera.setAspectRatio(width / static_cast<float>(height));
                }
            }

        protected:
            // Updates the state of the program with time.
            virtual void UpdateTime() = 0;

            virtual void setupCamera(const Camera& cam) = 0;

            virtual void OnRender() = 0;

            virtual void EachSecond(double seconds)
            {
                // Saving screenshots
            }

            virtual void OnSizeChangedOverride(int width, int height) = 0;

        #pragma endregion
    };
}
