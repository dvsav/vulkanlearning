#pragma once

#include "ConditionalCompilation.h"

// Utility
#include "utility.h"
#include "VkUtility.h"
#include "VkContext.h"

// VkEngine
#include "HostVisibleBuffer.h"

namespace vulkan
{
    // Represents a buffer object that stores its data in local GPU memory.
    // Such kind of buffer fits well for data that don't need to be changed very frequently by the host CPU,
    // because reading-writing those data by the CPU involves moving data between conventional memory and GPU memory
    // and this is not very efficient.
    class GpuBuffer : public VkMemoryBuffer
    {
        #pragma region Constructors

        public:
            GpuBuffer(
                const VkContext& vkContext,
                size_t bufferSize,
                vk::BufferUsageFlags usage) :
                VkMemoryBuffer(
                    vkContext,
                    bufferSize,
                    // Buffer can be a data transfer destination (from a staging buffer)
                    /*usage*/ vk::BufferUsageFlagBits::eTransferDst | usage,
                    // CPU-visible and not needing flushing
                    /*appMemoryRequirements*/ vk::MemoryPropertyFlagBits::eDeviceLocal)
            {}

        #pragma endregion

        #pragma region Methods

        public:
            /// <summary>
            /// Copies data from host memory to GPU memory.
            /// This operation consumes time and memory resources, so
            /// it's recommended that you use it rarely.
            /// </summary>
            /// <param name="source">Pointer to the source buffer in host memory</param>
            /// <param name="offset">Offset to the GPU buffer at which data will be copied</param>
            /// <param name="size">Size of data being copied in bytes</param>
            void setData(
                _In_ _Notnull_ const void* source,
                size_t offset,
                size_t size)
            {
                util::Requires::That(
                    source != nullptr &&
                    size <= Size() &&
                    offset >= 0 &&
                    offset + size <= Size(),
                    FUNCTION_INFO);

                // *****************************************************
                // ***             CREATE STAGING BUFFER             ***
                // *****************************************************
                auto stagingBuffer = CreateStagingBuffer_Src(Context(), size);

                stagingBuffer.set(
                    /*offset*/ 0,
                    /*size*/ size,
                    /*srcData*/ source);

                // *****************************************************
                // *** COPY DATA FROM STAGING BUFFER TO LOCAL BUFFER ***
                // *****************************************************
                // Data are copied from the host memory to the GPU memory
                // through the execution of a copy command that must be
                // placed in a command buffer.
                CopyBuffer(
                    Context(),
                    /*srcBuffer*/ stagingBuffer.Buffer(),
                    /*dstBuffer*/ Buffer(),
                    /*srcOffset*/ 0,
                    /*dstOffset*/ offset,
                    /*size*/ size);

                // Once we have copied data from the host memory to
                // the GPU memory, we no longer need staging buffer.
                stagingBuffer.Destroy();
            }

            /// <summary>
            /// Copies data from host memory to GPU memory.
            /// This operation consumes time and memory resources, so
            /// it's recommended that you use it rarely.
            /// </summary>
            /// <param name="source">Pointer to the source buffer in host memory</param>
            void setData(
                _In_ _Notnull_ const void* source)
            {
                setData(source, 0, Size());
            }

            /// <summary>
            /// Copies data from GPU memory to host memory.
            /// This operation consumes time and memory resources, so
            /// it's recommended that you use it rarely.
            /// </summary>
            /// <param name="destination">Pointer to the destination buffer in host memory</param>
            /// <param name="offset">Offset to the GPU buffer at which data will be taken</param>
            /// <param name="size">Size of data being copied in bytes</param>
            void* getData(
                _Out_ _Notnull_ void* destination,
                size_t offset,
                size_t size)
            {
                util::Requires::That(
                    destination != nullptr &&
                    size <= Size() &&
                    offset >= 0 &&
                    offset + size <= Size(),
                    FUNCTION_INFO);

                // *****************************************************
                // ***             CREATE STAGING BUFFER             ***
                // *****************************************************
                auto stagingBuffer = CreateStagingBuffer_Dst(Context(), size);

                // *****************************************************
                // *** COPY DATA FROM LOCAL BUFFER TO STAGING BUFFER ***
                // *****************************************************
                // Data are copied from the GPU memory to the host memory
                // through the execution of a copy command that must be
                // placed in a command buffer.
                CopyBuffer(
                    Context(),
                    /*srcBuffer*/ Buffer(),
                    /*dstBuffer*/ stagingBuffer.Buffer(),
                    /*srcOffset*/ offset,
                    /*dstOffset*/ 0,
                    /*size*/ size);

                // MAP MEMORY -> COPY DATA -> UNMAP MEMORY
                std::memcpy(
                    destination, // destination
                    HostVisibleBuffer::Mapping(stagingBuffer).MappedPointer(), // source
                    Size()); // size

                // Once we have copied data from the host memory to
                // the GPU memory, we no longer need staging buffer.
                stagingBuffer.Destroy();

                return destination;
            }

        #pragma endregion
    };
}