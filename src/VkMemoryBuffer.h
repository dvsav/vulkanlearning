#pragma once

#include "ConditionalCompilation.h"

// Utility
#include "utility.h"
#include "VkUtility.h"
#include "VkContext.h"

namespace vulkan
{
    // A class that binds together two Vulkan notions: "Buffer" and "Memory".
    class VkMemoryBuffer
    {
        #pragma region Fields

        private:
            const VkContext m_VkContext;
            const size_t m_BufferSize;
            
            vk::Buffer m_LocalBuffer;
            vk::DeviceMemory m_LocalMemory;

            bool m_IsDestroyed;

        #pragma endregion

        #pragma region Constructors

        public:
            /// <summary>
            /// Constructor
            /// </summary>
            /// <param name="vkContext"></param>
            /// <param name="bufferSize"></param>
            /// <param name="usage">Indicates for which purposes the data in the buffer is going to be used</param>
            /// <param name="appMemoryRequirements">Our own requirements to memory</param>
            VkMemoryBuffer(
                const VkContext& vkContext,
                size_t bufferSize,
                vk::BufferUsageFlags usage,
                vk::MemoryPropertyFlags appMemoryRequirements) :
                m_VkContext(vkContext),
                m_BufferSize(bufferSize),
                m_LocalBuffer(),
                m_LocalMemory(),
                m_IsDestroyed(false)
            {
                // CREATE BUFFER
                m_LocalBuffer = m_VkContext.LogicalDevice.createBuffer(
                    vk::BufferCreateInfo
                    {
                        .size = bufferSize,
                        // Indicates for which purposes the data in the buffer is going to be used
                        .usage = usage,
                        // Buffers can be owned by a specific queue family or be shared between multiple at the same time
                        .sharingMode = vk::SharingMode::eExclusive
                    });

                // ALLOCATE MEMORY FOR BUFFER
                // Our physical device can have multiple memory types on board.
                // But our buffer is only compatible with some types of our physical device's memory.
                // The VkMemoryRequirements struct has three fields:
                //     size: The size of the required amount of memory in bytes, may differ from bufferInfo.size.
                //     alignment: The offset in bytes where the buffer begins in the allocated region of memory, depends on bufferInfo.usage and bufferInfo.flags.
                //     memoryTypeBits: Bit field of the memory types that are suitable for the buffer.
                auto memoryRequirements = m_VkContext.LogicalDevice.getBufferMemoryRequirements(m_LocalBuffer);
                m_LocalMemory = m_VkContext.LogicalDevice.allocateMemory(
                    vk::MemoryAllocateInfo
                    {
                        .allocationSize = memoryRequirements.size,

                        // Index of memory type on Physical Device that has required bit flags
                        .memoryTypeIndex = FindMemoryTypeIndex(
                            m_VkContext.PhysicalDevice,
                            memoryRequirements,
                            appMemoryRequirements)
                    });

                // BIND MEMORY TO BUFFER
                m_VkContext.LogicalDevice.bindBufferMemory(m_LocalBuffer, m_LocalMemory,
                    0); // The offset within the region of memory
            }

            VkMemoryBuffer(VkMemoryBuffer&& rvalue) noexcept :
                m_VkContext(rvalue.m_VkContext),
                m_BufferSize(rvalue.m_BufferSize),
                m_LocalBuffer(rvalue.m_LocalBuffer),
                m_LocalMemory(rvalue.m_LocalMemory),
                m_IsDestroyed(rvalue.m_IsDestroyed)
            {
                rvalue.m_IsDestroyed = true;
            }

        #pragma endregion

        #pragma region Deleted Functions

        private:
            VkMemoryBuffer(const VkMemoryBuffer&) = delete;
            VkMemoryBuffer& operator=(const VkMemoryBuffer&) = delete;

        #pragma endregion

        #pragma region Destructor

        public:
            void Destroy()
            {
                if (m_IsDestroyed)
                    return;

                m_IsDestroyed = true;

                m_VkContext.LogicalDevice.destroyBuffer(m_LocalBuffer);
                m_VkContext.LogicalDevice.freeMemory(m_LocalMemory);
            }

            virtual ~VkMemoryBuffer()
            {
                Destroy();
            }

        #pragma endregion

        #pragma region Properties

        protected:
            const VkContext& Context() { return m_VkContext; }

        public:
            size_t Size() const { return m_BufferSize; }
            const vk::Buffer& Buffer() const { return m_LocalBuffer; }
            constexpr vk::DescriptorType DescriptorType() const { return vk::DescriptorType::eUniformBuffer; }
            const vk::DeviceMemory& LocalMemory() const { return m_LocalMemory; }

        #pragma endregion
    };

    /// <summary>
    /// Copies data from one memory type to another using
    /// temporary created command buffer.
    /// </summary>
    inline void CopyBuffer(
        const VkContext& vkContext,
        const vk::Buffer& source,
        const vk::Buffer& destination,
        size_t srcOffset,
        size_t dstOffset,
        size_t size)
    {
        // Data is copied from one memory type to another
        // through the execution of a copy command that must be
        // placed in a command buffer.

        vk::BufferCopy copyRegion
        {
            .srcOffset = srcOffset,
            .dstOffset = dstOffset,
            .size = size
        };

        RunTemporaryCommandBuffer(
            vkContext.LogicalDevice,
            vkContext.GraphicsQueue,
            vkContext.GraphicsCommandPool)
            .getCommandBuffer().copyBuffer(
                source, // src
                destination, // dst
                1, // region count
                &copyRegion); // regions
    }
}