/*
Copyright (c) 2020 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

#include <stdexcept>

namespace vulkan
{
    class VkException : public std::exception
    {
        public:
            using std::exception::exception;

            explicit VkException(const std::string& message)
                : exception(message.c_str())
            {}
    };
}