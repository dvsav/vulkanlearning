#pragma once

#include "ConditionalCompilation.h"
#include "VkHeaders.h"

namespace vulkan
{
    struct VkContext
    {
        const vk::PhysicalDevice& PhysicalDevice;
        const vk::Device& LogicalDevice;
        const vk::CommandPool& GraphicsCommandPool;
        const vk::Queue& GraphicsQueue;
    };
}