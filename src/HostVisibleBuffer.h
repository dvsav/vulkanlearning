#pragma once

#include "ConditionalCompilation.h"

// Utility
#include "VkMemoryBuffer.h"

namespace vulkan
{
    class HostVisibleBuffer : public VkMemoryBuffer
    {
        #pragma region Nested Types

        public:
            /*
            Provides more safe buffer mapping in RAII fashion.
            Example:
            {
                HostVisibleBuffer::Mapping mapping(buffer); // here we map the buffer
                // do some work with the mapped pointer
            } // here the buffer is unmapped automatically
            */
            class Mapping final
            {
            private:
                HostVisibleBuffer& m_Buffer;
                void* m_MappedPointer;

            private:
                Mapping(const Mapping&) = delete;
                Mapping& operator=(const Mapping&) = delete;

            public:
                Mapping(
                    HostVisibleBuffer& buffer) :
                    m_Buffer(buffer),
                    m_MappedPointer(buffer.Map())
                {}

                Mapping(
                    HostVisibleBuffer& buffer,
                    size_t offset,
                    size_t size) :
                    m_Buffer(buffer),
                    m_MappedPointer(buffer.Map(offset, size))
                {}

                ~Mapping()
                {
                    m_Buffer.Unmap();
                }

                void* MappedPointer() const { return m_MappedPointer; }
            };

        #pragma endregion

        #pragma region Constructors

        public:
            HostVisibleBuffer(
                const VkContext& vkContext,
                size_t bufferSize,
                vk::BufferUsageFlags usage) :
                VkMemoryBuffer(
                    vkContext,
                    bufferSize,
                    /*usage*/ usage,
                    // CPU-visible and not needing flushing
                    /*appMemoryRequirements*/ vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent)
            {}

        #pragma endregion

        #pragma region Methods

        public:
            // Copies data from memory to buffer
            void set(size_t offset, size_t size, const void* srcData)
            {
                // MAP MEMORY -> COPY DATA -> UNMAP MEMORY
                std::memcpy(
                    Mapping(*this, offset, size).MappedPointer(), // destination
                    srcData, // source
                    size);   // size
            }

            // Copies data from memory to buffer
            void set(const void* srcData)
            {
                set(0, Size(), srcData);
            }

            void* Map(size_t offset, size_t size)
            {
                void* data;
                Context().LogicalDevice.mapMemory(
                    LocalMemory(),
                    offset, // offset
                    size,   // size
                    vk::MemoryMapFlags{}, // flags
                    &data);
                return data;
            }

            void* Map()
            {
                return Map(0, Size());
            }

            void Unmap()
            {
                Context().LogicalDevice.unmapMemory(LocalMemory());
            }

        #pragma endregion
    };

    // Staging buffer is a temporary buffer that is created in the host-visible memory
    // to HOLD the data that we are going to later copy TO the GPU local memory.
    inline HostVisibleBuffer CreateStagingBuffer_Src(
        const VkContext& vkContext,
        size_t bufferSize)
    {
        return HostVisibleBuffer(
            vkContext,
            bufferSize,
            /*usage*/ vk::BufferUsageFlagBits::eTransferSrc);
    }

    // Staging buffer is a temporary buffer that is created in the host-visible memory
    // to ACCEPT the data that we are going to later copy FROM the GPU local memory.
    inline HostVisibleBuffer CreateStagingBuffer_Dst(
        const VkContext& vkContext,
        size_t bufferSize)
    {
        return HostVisibleBuffer(
            vkContext,
            bufferSize,
            /*usage*/ vk::BufferUsageFlagBits::eTransferDst);
    }
}