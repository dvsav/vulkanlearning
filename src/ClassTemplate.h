#pragma once

namespace mynamespace
{
    class MyClass
    {
        #pragma region Nested Types

        public:

        #pragma endregion

        #pragma region Fields

        private:

        #pragma endregion

        #pragma region Constructors

        public:

        #pragma endregion

        #pragma region Destructor

        public:

        #pragma endregion

        #pragma region Deleted Functions

        private:

        #pragma endregion

        #pragma region Properties

        public:

        #pragma endregion

        #pragma region Methods

        public:

        #pragma endregion
    };
}