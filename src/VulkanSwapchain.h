#pragma once

#include "ConditionalCompilation.h"

// STL
#include <vector>

// Utility
#include "utility.h"
#include "VkUtility.h"

// VkEngine
#include "VkGraphicsContext.h"
#include "VkImage2d.h"
#include "BufferAttachment.h"
#include "VkRenderPass.h"

namespace vulkan
{
    class VulkanSwapchainBase
    {
        #pragma region Fields

        protected:
            const VkGraphicsContext& m_VkGraphicsContext;

            // If we want to display something we can create a set of buffers to which we can render. These buffers
            // along with their properties are called a swap chain. A swap chain can contain  many images. To display
            // any of them we don't "swap" them � as the name suggests � but we present them, which means that we
            // give them back to a presentation engine. (Remark: There will be situations where we will have to destroy this
            // swap chain and recreate it. In the middle of a working application).
            // https://software.intel.com/content/www/us/en/develop/articles/api-without-secrets-introduction-to-vulkan-part-2.html
            vk::Format m_SwapChainImageFormat;
            vk::Extent2D m_SwapChainExtent;
            vk::SwapchainKHR m_Swapchain;

            std::vector<vk::ImageView> m_SwapchainImages; // color buffers to present on the screen (color attachment in the render pass)
            std::vector<VkImage2d> m_DepthBuffers;        // depth buffers (depth attachment in the render pass)

            vk::RenderPass m_RenderPass;
            std::vector<vk::Framebuffer> m_Framebuffers; // references to the attachments above (swapchain images as color attachments and depth buffer as depth attachment)

            bool m_IsDestroyed;

        #pragma endregion

        #pragma region Constructors

        protected:
            VulkanSwapchainBase(
                const VkGraphicsContext& vulkanResources,
                uint32_t width,
                uint32_t height) :
                
                m_VkGraphicsContext(vulkanResources),
                m_SwapChainImageFormat(),
                m_SwapChainExtent(),
                m_Swapchain(CreateSwapchain(width, height)),

                m_SwapchainImages(CreateImageViews()),
                m_DepthBuffers(),

                m_RenderPass(),
                m_Framebuffers(),

                m_IsDestroyed(false)
            {}

        public:
            VulkanSwapchainBase(VulkanSwapchainBase&& rvalue) noexcept :
                m_VkGraphicsContext(rvalue.m_VkGraphicsContext),
                m_SwapChainImageFormat(rvalue.m_SwapChainImageFormat),
                m_SwapChainExtent(rvalue.m_SwapChainExtent),
                m_Swapchain(rvalue.m_Swapchain),
                m_SwapchainImages(std::move(rvalue.m_SwapchainImages)),
                m_DepthBuffers(std::move(rvalue.m_DepthBuffers)),
                m_RenderPass(rvalue.m_RenderPass),
                m_Framebuffers(std::move(rvalue.m_Framebuffers)),
                m_IsDestroyed(rvalue.m_IsDestroyed)
            {
                rvalue.m_IsDestroyed = true;
            }

            VulkanSwapchainBase& operator=(VulkanSwapchainBase&& rvalue) noexcept
            {
                util::Requires::That(&m_VkGraphicsContext == &rvalue.m_VkGraphicsContext, FUNCTION_INFO);
                m_SwapChainImageFormat = rvalue.m_SwapChainImageFormat;
                m_SwapChainExtent = rvalue.m_SwapChainExtent;
                m_Swapchain = rvalue.m_Swapchain;
                m_SwapchainImages = std::move(rvalue.m_SwapchainImages);
                m_DepthBuffers = std::move(rvalue.m_DepthBuffers);
                m_RenderPass = rvalue.m_RenderPass;
                m_Framebuffers = std::move(rvalue.m_Framebuffers);
                m_IsDestroyed = rvalue.m_IsDestroyed;
                rvalue.m_IsDestroyed = true;
                return *this;
            }

        #pragma endregion

        #pragma region Deleted Functions

        private:
            VulkanSwapchainBase(const VulkanSwapchainBase&) = delete;
            VulkanSwapchainBase& operator=(const VulkanSwapchainBase&) = delete;

        #pragma endregion

        #pragma region Destructor

        public:
            virtual void Destroy()
            {
                if (m_IsDestroyed)
                    return;
                m_IsDestroyed = true;

                for (auto& framebuffer : m_Framebuffers)
                    m_VkGraphicsContext.LogicalDevice().destroyFramebuffer(framebuffer);

                m_VkGraphicsContext.LogicalDevice().destroyRenderPass(m_RenderPass);

                m_DepthBuffers.clear();

                for (auto& imageView : m_SwapchainImages)
                    m_VkGraphicsContext.LogicalDevice().destroyImageView(imageView);

                m_VkGraphicsContext.LogicalDevice().destroySwapchainKHR(m_Swapchain);
            }

            virtual ~VulkanSwapchainBase() { Destroy(); }

        #pragma endregion

        #pragma region Properties

        public:
            size_t NumberOfImages() const { return m_SwapchainImages.size(); }
            vk::Format SwapChainImageFormat() const { return m_SwapChainImageFormat; }
            const vk::Extent2D& SwapChainExtent() const { return m_SwapChainExtent; }
            const vk::SwapchainKHR& Swapchain() const { return m_Swapchain; }
            const std::vector<vk::ImageView>& SwapchainImages() const { return m_SwapchainImages; }
            const std::vector<VkImage2d>& DepthBuffers() const { return m_DepthBuffers; }
            const vk::RenderPass& RenderPass() const { return m_RenderPass; }
            const std::vector<vk::Framebuffer>& Framebuffers() const { return m_Framebuffers; }

        #pragma endregion

        #pragma region Methods

        private:
            // Vulkan does not have the concept of a "default framebuffer", hence it requires
            // an infrastructure that will own the buffers we will render to before we visualize
            // them on the screen. This infrastructure is known as the swap chain and must be
            // created explicitly in Vulkan. The swap chain is essentially a queue of images that
            // are waiting to be presented to the screen... The general purpose of the swap chain
            // is to synchronize the presentation of images with the refresh rate of the screen.
            // Source: https://vulkan-tutorial.com/en/Drawing_a_triangle/Presentation/Swap_chain
            vk::SwapchainKHR CreateSwapchain(
                uint32_t width,
                uint32_t height)
            {
                // Note: Swapchain must be compatible with our window surface.

                // 1. Choose the best surface format
                auto surfaceFormats = m_VkGraphicsContext.PhysicalDevice().getSurfaceFormatsKHR(m_VkGraphicsContext.Surface());
                if (surfaceFormats.empty())
                    throw VkException(FUNCTION_INFO);
                vk::SurfaceFormatKHR surfaceFormat = surfaceFormats[0];
                if (surfaceFormats.size() == 1 && surfaceFormats[0].format == vk::Format::eUndefined)
                {   // This means that all formats are available
                    surfaceFormat =
                    {
                        // Format specifies the bit layout of a color value in memory
                        // (eR8G8B8A8Unorm: 32-bit unsigned normalized format that has
                        // an 8-bit R component in byte 0, an 8-bit G component in byte 1,
                        // an 8-bit B component in byte 2, and an 8-bit A component in byte 3).
                        // Unsigned normalized format maps [0...255] integer to [0.0 ... 1.0] float.
                        vk::Format::eR8G8B8A8Unorm,

                        // Color space specifies the mapping between the RGB component values
                        // and the human-perceived color (SRGB color space is commonly used in computer displays)
                        vk::ColorSpaceKHR::eSrgbNonlinear
                    };
                }
                else
                {   // Not all formats are available
                    auto searchFmt = std::find_if(
                        surfaceFormats.cbegin(),
                        surfaceFormats.cend(),
                        [](const vk::SurfaceFormatKHR& fmt)
                        {
                            return (fmt.format == vk::Format::eR8G8B8A8Unorm
                                || fmt.format == vk::Format::eB8G8R8A8Unorm)
                                && fmt.colorSpace == vk::ColorSpaceKHR::eSrgbNonlinear;
                        });
                    if (searchFmt != surfaceFormats.cend())
                        surfaceFormat = *searchFmt;
                }

                // 2. Choose the best presentation mode
                // Double buffering was introduced to prevent the visibility of drawing operations: one image
                // was displayed and the second was used to render into. During presentation, the contents
                // of the second image were copied into the first image (earlier) or (later) the images were
                // swapped (remember SwapBuffers() function used in OpenGL applications?) which means that
                // their pointers were exchanged.
                // Tearing was another issue with displaying images, so the ability to wait for the
                // vertical blank signal was introduced if we wanted to avoid it. But waiting introduced
                // another problem: input lag. So double buffering was changed into triple buffering in
                // which we were drawing into two back buffers interchangeably and during v-sync the most
                // recent one was used for presentation.
                // https://software.intel.com/content/www/us/en/develop/articles/api-without-secrets-introduction-to-vulkan-part-2.html
                auto presentModes = m_VkGraphicsContext.PhysicalDevice().getSurfacePresentModesKHR(m_VkGraphicsContext.Surface());
                if (presentModes.empty())
                    throw VkException(FUNCTION_INFO);
                vk::PresentModeKHR presentMode = vk::PresentModeKHR::eFifo; // FIFO presentation mode (no tearing, possible input lag) is the only one that is guaranteed to be available
                auto searchPmd = std::find(
                    presentModes.cbegin(),
                    presentModes.cend(),
                    vk::PresentModeKHR::eMailbox); // Another good option is MAILBOX: no tearing and input lag is minimized. Makes sense only if there are at least 3 images in the swap chain.
                if (searchPmd != presentModes.cend())
                    presentMode = *searchPmd;

                // Acquired capabilities contain important information about ranges (limits) that
                // are supported by the swap chain, that is, minimal and maximal number of images,
                // minimal and maximal dimensions of images, or supported transforms (some platforms
                // may require transformations applied to images before these images may be presented).
                // https://software.intel.com/content/www/us/en/develop/articles/api-without-secrets-introduction-to-vulkan-part-2.html
                auto surfaceCapabilities = m_VkGraphicsContext.PhysicalDevice().getSurfaceCapabilitiesKHR(m_VkGraphicsContext.Surface());

                // 3. Choose the best swap chain image resolution
                // If currentExtent.(width|height) == uint32_t::max this means that window
                // subsystem allows us to set any width and height for our swapchain within
                // [minImageExtent...maxImageExtent] boundaries
                vk::Extent2D swapExtent =
                    surfaceCapabilities.currentExtent.width != (std::numeric_limits<uint32_t>::max)() ?
                    surfaceCapabilities.currentExtent : vk::Extent2D{ width, height };
                // Surface defines max and min size, so make sure within boundaries by clamping value
                swapExtent.width = util::constraint(swapExtent.width, surfaceCapabilities.minImageExtent.width, surfaceCapabilities.maxImageExtent.width);
                swapExtent.height = util::constraint(swapExtent.width, surfaceCapabilities.minImageExtent.height, surfaceCapabilities.maxImageExtent.height);

                // 4. Create swap chain createinfo
                vk::SwapchainCreateInfoKHR createInfo
                {
                    .surface = m_VkGraphicsContext.Surface(),

                    // An application may request more images. If it wants to use multiple images at
                    // once it may do so, for example, when encoding a video stream where every fourth
                    // image is a key frame and the application needs it to prepare the remaining three
                    // frames. Such usage will determine the number of images that will be automatically
                    // created in a swap chain: how many images the application requires at once for
                    // processing and how many images the presentation engine requires to function properly.
                    // https://software.intel.com/content/www/us/en/develop/articles/api-without-secrets-introduction-to-vulkan-part-2.html
                    .minImageCount = surfaceCapabilities.maxImageCount > 0 ? // maxImageCount = 0 means that there's no maximum
                        (std::min)(surfaceCapabilities.minImageCount + 1, surfaceCapabilities.maxImageCount) :
                        surfaceCapabilities.minImageCount + 1,

                    .imageFormat = surfaceFormat.format,
                    .imageColorSpace = surfaceFormat.colorSpace,
                    .imageExtent = swapExtent,
                    .imageArrayLayers = 1, // specifies the amount of layers each image consists of. This is always 1 unless you are developing a stereoscopic 3D application.

                    // Usage flags define how a given image may be used in Vulkan. If we want an image to be
                    // sampled (used inside shaders) it must be created with "sampled" usage. If the image
                    // should be used as a depth render target, it must be created with "depth and stencil" usage.
                    // An image without proper usage "enabled" cannot be used for a given purpose or the results
                    // of such operations will be undefined.
                    // For a swap chain we want to render(in most cases) into the image(use it as a render target),
                    // so we must specify �color attachment� usage with VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT enum.
                    // In Vulkan this usage is always available for swap chains, so we can always set it without
                    // any additional checking. But for any other usage we must ensure it is supported � we can do
                    // this through a "supportedUsageFlags" member of surface capabilities structure.
                    // https://software.intel.com/content/www/us/en/develop/articles/api-without-secrets-introduction-to-vulkan-part-2.html
                    .imageUsage = vk::ImageUsageFlagBits::eColorAttachment | vk::ImageUsageFlagBits::eTransferDst, // just color, no depth; "transfer destination" usage is required for image clear operation

                    .preTransform = surfaceCapabilities.currentTransform, // we can specify that a certain transform should be applied to images in the swap chain if it is supported
                    .compositeAlpha = vk::CompositeAlphaFlagBitsKHR::eOpaque, // no alpha blending (specifies if the alpha channel should be used for blending with other windows in the window system)
                    .presentMode = presentMode,
                    .clipped = VK_TRUE // If set to VK_TRUE then that means that we don't care about the color of pixels that are obscured, for example because another window is in front of them.
                };

                // 5. If graphics and presentation families are different, then swapchain must let images be shared between queue families
                uint32_t queuFamilyIndices[]
                {
                    m_VkGraphicsContext.GraphicsQueueFamilyIndex(),
                    m_VkGraphicsContext.PresentationQueueFamilyIndex()
                };
                if (m_VkGraphicsContext.GraphicsQueueFamilyIndex() != m_VkGraphicsContext.PresentationQueueFamilyIndex())
                {
                    // If we want to reference images from many different queue families at a time we can do so.
                    // In this case we must provide "concurrent" sharing mode. But this (probably) requires us to
                    // manage image data coherency by ourselves, that is, we must synchronize different queues in
                    // such a way that data in the images is proper and no hazards occur - some queues are reading
                    // data from images, but other queues haven't finished writing to them yet.
                    // We may not specify these queue families and just tell Vulkan that only one queue family
                    // (queues from one family) will be referencing image at a time. This doesn't mean other queues
                    // can't reference these images. It just means they can't do it all at once, at the same time.
                    // So if we want to reference images from one family and then from another we must specifically
                    // tell Vulkan: "My image was used inside this queue family, but from now on another family,
                    // this one, will be referencing it." Such a transition is done using image memory barrier.
                    // https://software.intel.com/content/www/us/en/develop/articles/api-without-secrets-introduction-to-vulkan-part-2.html

                    // Images can be used across multiple queue families without explicit ownership transfers.
                    createInfo.imageSharingMode = vk::SharingMode::eConcurrent;
                    // Concurrent mode requires you to specify in advance between which queue families ownership will be shared.
                    createInfo.queueFamilyIndexCount = sizeof(queuFamilyIndices) / sizeof(queuFamilyIndices[0]);
                    createInfo.pQueueFamilyIndices = queuFamilyIndices;
                }
                else
                {
                    // An image is owned by one queue family at a time and ownership must be explicitly transferred
                    // before using it in another queue family. This option offers the best performance.
                    createInfo.imageSharingMode = vk::SharingMode::eExclusive;
                    createInfo.queueFamilyIndexCount = 0;
                    createInfo.pQueueFamilyIndices = nullptr;
                }

                // 6. With Vulkan it's possible that your swap chain becomes invalid or unoptimized while your application
                // is running, for example because the window was resized. In that case the swap chain actually needs to be
                // recreated from scratch and a reference to the old one must be specified in this field.
                createInfo.oldSwapchain = vk::SwapchainKHR(); // default vk::SwapchainKHR() plays a role of nullptr

                // 7. Create swapchain
                m_SwapChainImageFormat = createInfo.imageFormat;
                m_SwapChainExtent = swapExtent;
                return m_VkGraphicsContext.LogicalDevice().createSwapchainKHR(createInfo);
            }

            std::vector<vk::ImageView> CreateImageViews()
            {
                std::vector<vk::ImageView> imageViews;
                for (auto image : m_VkGraphicsContext.LogicalDevice().getSwapchainImagesKHR(m_Swapchain))
                {
                    imageViews.push_back(
                        {
                            CreateImageView(
                                m_VkGraphicsContext.LogicalDevice(),
                                image,
                                m_SwapChainImageFormat,
                                vk::ImageAspectFlagBits::eColor)
                        });
                }
                return imageViews;
            }

        #pragma endregion
    };

    class VulkanSwapchain : public VulkanSwapchainBase
    {
        #pragma region Constructors

        public:
            VulkanSwapchain(
                const VkGraphicsContext& vulkanResources,
                uint32_t width,
                uint32_t height) :
                VulkanSwapchainBase(
                    vulkanResources,
                    width,
                    height)
            {
                m_DepthBuffers = CreateDepthBuffers();
                m_RenderPass = CreateRenderPass();
                m_Framebuffers = CreateFramebuffers();
            }

            VulkanSwapchain(VulkanSwapchain&& rvalue) noexcept :
                VulkanSwapchainBase(std::move(rvalue))
            {}

            VulkanSwapchain& operator=(VulkanSwapchain&& rvalue) noexcept
            {
                *static_cast<VulkanSwapchainBase*>(this) = std::move(rvalue);
                return *this;
            }

        #pragma endregion

        #pragma region Methods

        private:
            std::vector<VkImage2d> CreateDepthBuffers()
            {
                std::vector<VkImage2d> depthBuffers;
                depthBuffers.reserve(NumberOfImages());
                for (size_t i = 0; i < NumberOfImages(); i++)
                {
                    depthBuffers.push_back(CreateDepthBuffer(
                        m_VkGraphicsContext.PhysicalDevice(),
                        m_VkGraphicsContext.LogicalDevice(),
                        m_SwapChainExtent));
                }
                return depthBuffers;
            }

            vk::RenderPass CreateRenderPass()
            {
                return CreateRenderPassOneSubpass(
                    m_VkGraphicsContext.LogicalDevice(),
                    m_SwapChainImageFormat,
                    m_DepthBuffers[0].ImageFormat());
            }

            // Framebuffers represent a collection of memory attachments that are used by a render pass instance.
            // Examples of these memory attachments include the color image buffers and depth buffer.
            // A framebuffer provides the attachments that a render pass needs while rendering.
            // For example there might be two images in a swapchain (color buffers) and just a single depth buffer,
            // so there will be two framebuffer objects each referencing one of the color buffers (image view object)
            // and that single depth buffer.
            // https://vulkan.lunarg.com/doc/view/1.2.170.0/linux/tutorial/html/12-init_frame_buffers.html
            std::vector<vk::Framebuffer> CreateFramebuffers()
            {
                std::vector<vk::Framebuffer> framebuffers;
                framebuffers.reserve(NumberOfImages());
                for (size_t i = 0; i < NumberOfImages(); i++)
                {
                    std::array<vk::ImageView, 2> attachments
                    {
                        m_SwapchainImages[i],     // 0 (position is important - it should match that of the render pass in vk::RenderPassCreateInfo::pAttachments)
                        m_DepthBuffers[i].ImageView() // 1 (position is important - it should match that of the render pass in vk::RenderPassCreateInfo::pAttachments)
                    };

                    vk::FramebufferCreateInfo framebufferCreateInfo
                    {
                        // Render Pass with which the framebuffer needs to be compatible. You can only use a framebuffer
                        // with the render passes that it is compatible with, which roughly means that they use the same
                        // number and type of attachments.
                        // https://vulkan-tutorial.com/Drawing_a_triangle/Drawing/Framebuffers
                        .renderPass = m_RenderPass,

                        .attachmentCount = attachments.size(),
                        .pAttachments = attachments.data(), // List of attachments (1:1 with Render Pass)
                        .width = SwapChainExtent().width,   // Framebuffer width
                        .height = SwapChainExtent().height, // Framebuffer height
                        .layers = 1                         // Framebuffer layers
                    };

                    framebuffers.push_back(
                        m_VkGraphicsContext.LogicalDevice().createFramebuffer(framebufferCreateInfo));
                }
                return framebuffers;
            }

        #pragma endregion
    };

    class VulkanSwapchainPostprocessing : public VulkanSwapchainBase
    {
        #pragma region Fields

        private:
            std::vector<VkImage2d> m_ColorBuffers; // Output of the 1st subpass, Input to the 2nd subpass

        #pragma endregion

        #pragma region Constructors

        public:
            VulkanSwapchainPostprocessing(
                const VkGraphicsContext& vulkanResources,
                uint32_t width,
                uint32_t height) :

                VulkanSwapchainBase(
                    vulkanResources,
                    width,
                    height),

                m_ColorBuffers(
                    CreateColorBuffers())
            {
                m_DepthBuffers = CreateDepthBuffers();
                m_RenderPass = CreateRenderPass();
                m_Framebuffers = CreateFramebuffers();
            }

            VulkanSwapchainPostprocessing(VulkanSwapchainPostprocessing&& rvalue) noexcept :
                VulkanSwapchainBase(std::move(rvalue)),
                m_ColorBuffers(std::move(rvalue.m_ColorBuffers))
            {}

            VulkanSwapchainPostprocessing& operator=(VulkanSwapchainPostprocessing&& rvalue) noexcept
            {
                m_ColorBuffers = std::move(rvalue.m_ColorBuffers);
                *static_cast<VulkanSwapchainBase*>(this) = std::move(rvalue);
                return *this;
            }

        #pragma endregion

        #pragma region Destructor

        public:
            void Destroy() override
            {
                if (m_IsDestroyed)
                    return;

                m_ColorBuffers.clear();
                VulkanSwapchainBase::Destroy();
            }

            virtual ~VulkanSwapchainPostprocessing() { Destroy(); }

        #pragma endregion

        #pragma region Properties

        public:
            /// <summary>
            /// Output of the 1st subpass, Input to the 2nd subpass
            /// </summary>
            const std::vector<VkImage2d>& ColorBuffers() const { return m_ColorBuffers; }

        #pragma endregion

        #pragma region Methods

        private:
            std::vector<VkImage2d> CreateDepthBuffers()
            {
                std::vector<VkImage2d> depthBuffers;
                depthBuffers.reserve(NumberOfImages());
                for (size_t i = 0; i < NumberOfImages(); i++)
                {
                    depthBuffers.push_back(CreateDepthBuffer(
                        m_VkGraphicsContext.PhysicalDevice(),
                        m_VkGraphicsContext.LogicalDevice(),
                        m_SwapChainExtent,
                        vk::ImageUsageFlagBits::eInputAttachment));
                }
                return depthBuffers;
            }

            std::vector<VkImage2d> CreateColorBuffers()
            {
                std::vector<VkImage2d> colorBuffers;
                colorBuffers.reserve(NumberOfImages());
                for (size_t i = 0; i < NumberOfImages(); i++)
                {
                    colorBuffers.push_back(CreateColorBuffer(
                        m_VkGraphicsContext.PhysicalDevice(),
                        m_VkGraphicsContext.LogicalDevice(),
                        SwapChainExtent(),
                        vk::ImageUsageFlagBits::eInputAttachment));
                }
                return colorBuffers;
            }

            vk::RenderPass CreateRenderPass()
            {
                return CreateRenderPassMultipass(
                    m_VkGraphicsContext.LogicalDevice(),
                    SwapChainImageFormat(),
                    m_ColorBuffers[0].ImageFormat(),
                    DepthBuffers()[0].ImageFormat());
            }

            // Framebuffers represent a collection of memory attachments that are used by a render pass instance.
            // Examples of these memory attachments include the color image buffers and depth buffer.
            // A framebuffer provides the attachments that a render pass needs while rendering.
            // For example there might be two images in a swapchain (color buffers) and just a single depth buffer,
            // so there will be two framebuffer objects each referencing one of the color buffers (image view object)
            // and that single depth buffer.
            // https://vulkan.lunarg.com/doc/view/1.2.170.0/linux/tutorial/html/12-init_frame_buffers.html
            std::vector<vk::Framebuffer> CreateFramebuffers()
            {
                std::vector<vk::Framebuffer> framebuffers;
                framebuffers.reserve(NumberOfImages());
                for (size_t i = 0; i < NumberOfImages(); i++)
                {
                    std::array<vk::ImageView, 3> attachments
                    {
                        SwapchainImages()[i],          // 0 (position is important - it should match that of the render pass)
                        m_ColorBuffers[i].ImageView(), // 1 (position is important - it should match that of the render pass)
                        DepthBuffers()[i].ImageView()  // 2 (position is important - it should match that of the render pass)
                    };

                    vk::FramebufferCreateInfo framebufferCreateInfo
                    {
                        // Render Pass with which the framebuffer needs to be compatible. You can only use a framebuffer
                        // with the render passes that it is compatible with, which roughly means that they use the same
                        // number and type of attachments.
                        // https://vulkan-tutorial.com/Drawing_a_triangle/Drawing/Framebuffers
                        .renderPass = RenderPass(),

                        .attachmentCount = attachments.size(),
                        .pAttachments = attachments.data(), // List of attachments (1:1 with Render Pass)
                        .width = SwapChainExtent().width,   // Framebuffer width
                        .height = SwapChainExtent().height, // Framebuffer height
                        .layers = 1                         // Framebuffer layers
                    };

                    framebuffers.push_back(
                        m_VkGraphicsContext.LogicalDevice().createFramebuffer(framebufferCreateInfo));
                }
                return framebuffers;
            }

        #pragma endregion
    };
}