#pragma once

#include "ConditionalCompilation.h"

// STL
#include <memory>

// Engine
#include "Model.h"

// VkEngine
#include "VkMesh.h"
#include "Material.h"

namespace vulkan
{
    template <typename TVertex, typename TIndex>
    class VkModel : public engine::Model< VkMesh<TVertex, TIndex> >
    {
        #pragma region Typedefs

        public:
            using mesh_type = typename engine::Model< VkMesh<TVertex, TIndex> >::mesh_type;

        #pragma endregion

        #pragma region Fields

        private:
            Material m_Material;

        #pragma endregion

        #pragma region Constructors

        public:
            VkModel(
                const std::shared_ptr<mesh_type>& mesh,
                const Material& material) :
                engine::Model<mesh_type>(mesh),
                m_Material(material)
            {}

            // Move constructor.
            VkModel(VkModel&& rvalue) noexcept :
                engine::Model<mesh_type>(std::move(rvalue)),
                m_Material(std::move(rvalue.m_Material))
            {}

        #pragma endregion

        #pragma region Destructor

        public:
            void Destroy() override
            {
                if (engine::Model<mesh_type>::m_IsDestroyed)
                    return;

                m_Material.Destroy();
                engine::Model<mesh_type>::Destroy();

                engine::Model<mesh_type>::m_IsDestroyed = true;
            }

        #pragma endregion

        #pragma region Properties

        public:
            Material& material() { return m_Material; }
            const Material& material() const { return m_Material; }

        #pragma endregion
    };
}