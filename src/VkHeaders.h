/*
Copyright (c) 2020 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#define VK_USE_PLATFORM_WIN32_KHR

// This is to enable C++20 designated initializers (https://github.com/KhronosGroup/Vulkan-Hpp)
#define VULKAN_HPP_NO_STRUCT_CONSTRUCTORS

// This is to use defaultDispatchLoaderDynamic
// in Vulkan functions by default
#define VULKAN_HPP_DISPATCH_LOADER_DYNAMIC 1

#include "vulkan/vulkan.hpp"

#include "vulkan/vk_sdk_platform.h"

#ifdef _DEBUG
    #define ENABLE_DEBUG_LAYERS
#endif
