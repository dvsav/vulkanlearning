#pragma once

#include "ConditionalCompilation.h"

// STL
#include <memory>

// Engine
#include "Mesh.h"
#include "WorldPositionAndOrientation.h"

namespace engine
{
    /// <summary>
    /// Class representing a 3d-model, encapsulating mesh,
    /// its world position & orientation and material.
    /// </summary>
    /// <typeparam name="TMesh">Mesh type</typeparam>
    template <typename TMesh>
    class Model
    {
        #pragma region Nested Types

        public:
            using mesh_type = TMesh;

        #pragma endregion

        #pragma region Fields

        private:
            std::shared_ptr<mesh_type> m_Mesh;
            WorldPositionAndOrientation m_Wpo;

        protected:
            bool m_IsDestroyed;

        #pragma endregion

        #pragma region Constructors

        public:
            Model(const std::shared_ptr<mesh_type>& mesh) :
                m_Mesh(mesh),
                m_Wpo(),
                m_IsDestroyed(false)
            {}

            // Move constructor.
            Model(Model&& rvalue) noexcept :
                m_Mesh(std::move(rvalue.m_Mesh)),
                m_Wpo(std::move(rvalue.m_Wpo)),
                m_IsDestroyed(rvalue.m_IsDestroyed)
            {
                rvalue.m_IsDestroyed = true;
            }

        #pragma endregion

        #pragma region Destructor

        public:
            virtual void Destroy()
            {
                if (m_IsDestroyed)
                    return;

                m_IsDestroyed = true;

                m_Mesh.reset();
            }

            virtual ~Model()
            {
                Destroy();
            }

        #pragma endregion

        #pragma region Properties

        public:
            WorldPositionAndOrientation& Wpo() { return m_Wpo; }
            const WorldPositionAndOrientation& Wpo() const { return m_Wpo; }

            const std::shared_ptr<mesh_type>& MeshPtr() const { return m_Mesh; }
            const mesh_type& Mesh() const { return *m_Mesh; }
            mesh_type& Mesh() { return *m_Mesh; }

        #pragma endregion
    };
}