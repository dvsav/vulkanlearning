/*
Copyright (c) 2020 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

#include <stdexcept>
#include "WinUtility.h"

namespace win
{
    class winapi_error : public std::exception
    {
    public:
        using std::exception::exception;

        winapi_error()
            : exception(GetLastErrorMessage().c_str())
        {}

        explicit winapi_error(const std::string& message)
            : exception(message.c_str())
        {}
    };

    inline winapi_error make_winapi_error(const std::string& message)
    {
        return winapi_error(message + "\n" + GetLastErrorMessage());
    }
}
