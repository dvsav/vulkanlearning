/*
Copyright (c) 2020 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

#include "utility.h"

#include "Matrix.h"
#include "Color.h"

namespace engine
{
    using vec2 = vmath::Vector<float, 2>;
    using vec3 = vmath::Vector<float, 3>;
    using vec4 = vmath::Vector<float, 4>;

    using mat2 = vmath::Matrix<float, 2, 2>;
    using mat3 = vmath::Matrix<float, 3, 3>;
    using mat4 = vmath::Matrix<float, 4, 4>;

    using color_rgb32f = Color_RGB<float>;
    using color_rgba32f = Color_RGBA<float>;

    using color_rgb = Color_RGB<unsigned char>;
    using color_rgba = Color_RGBA<unsigned char>;

    template <typename VEC>
    struct vector_traits
    {
        using vec_type = VEC;
        static constexpr int components_number = sizeof(vec_type) / sizeof(util::value_type<vec_type>::t);
    };

    template <>
    struct vector_traits<vec2>
    {
        using vec_type = vec2;
        static constexpr int components_number = vec_type::components_number;
    };

    template <>
    struct vector_traits<vec3>
    {
        using vec_type = vec3;
        static constexpr int components_number = vec_type::components_number;
    };

    template <>
    struct vector_traits<vec4>
    {
        using vec_type = vec4;
        static constexpr int components_number = vec_type::components_number;
    };
}
