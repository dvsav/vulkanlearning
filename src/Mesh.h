#pragma once

#include "ConditionalCompilation.h"

namespace engine
{
    /// <summary>
    /// How vertices are assembled into primitives.
    /// </summary>
    enum class PrimitiveTopologyEnum
    {
        TriangleList,
        TriangleStrip,
        TriangleFan
    };

    template <typename TVertex, typename TIndex = size_t>
    struct MeshInfo
    {
        std::vector<TVertex> Vertices;
        std::vector<TIndex> Indices;
        PrimitiveTopologyEnum Topology;
    };

    /// <summary>
    /// Abstract class representing a mesh: a collection of vertices
    /// coupled with certain indices (we assume indexed drawing anyway)
    /// and certain primitive topology (i.e. how these indexed vertices
    /// are assembled into primitives).
    /// </summary>
    /// <typeparam name="TVertex">Vertex type</typeparam>
    template <typename TVertex, typename TIndex = size_t>
    class Mesh
    {
        #pragma region Typedefs

        public:
            using vertex_type = TVertex;
            using index_type = TIndex;

        #pragma endregion

        #pragma region Fields

        private:
            PrimitiveTopologyEnum m_PrimitiveTopology;
            size_t m_VertexCount;
            size_t m_IndexCount;

        #pragma endregion

        #pragma region Constructors

        protected:
            Mesh(
                PrimitiveTopologyEnum primitiveTopology,
                size_t vertexCount,
                size_t indexCount) :
                m_PrimitiveTopology(primitiveTopology),
                m_VertexCount(vertexCount),
                m_IndexCount(indexCount)
            {
                util::Requires::That(
                    vertexCount > 0 && indexCount > 0,
                    FUNCTION_INFO);
            }

        #pragma endregion

        #pragma region Destructor

        public:
            virtual ~Mesh() {}

        #pragma endregion

        #pragma region Properties

        public:
            PrimitiveTopologyEnum PrimitiveTopology() const { return m_PrimitiveTopology; }

            size_t VertexCount() const { return m_VertexCount; }
            size_t IndexCount() const { return m_IndexCount; }

            constexpr size_t VertexStride() const { return sizeof(vertex_type); }
            constexpr size_t IndexStride() const { return sizeof(index_type); }

            virtual const vertex_type getVertex(size_t nVertex) const = 0;
            virtual void setVertex(size_t nVertex, const vertex_type& vertex) = 0;
            virtual void setVertices(const vertex_type* vertices, size_t nFirstVertex, size_t nVertices) = 0;

            virtual index_type getIndex(size_t nIndex) const = 0;
            virtual void setIndex(size_t nIndex, index_type index) = 0;
            virtual void setIndices(const index_type* indices, size_t nFirstIndex, size_t nVertices) = 0;

        #pragma endregion
    };
}