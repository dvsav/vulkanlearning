/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

#include <type_traits>

#include "EngineTypedefs.h"

namespace engine
{
    struct VertexP
    {
        alignas(16) vec3 Position;

        VertexP() : Position() {}

        VertexP(const vec3& position) :
            Position(position)
        {}

        VertexP(vec3&& position) noexcept :
            Position(std::move(position))
        {}
    };

    struct VertexPC : public VertexP
    {
        alignas(16) color_rgba32f Color;

        VertexPC() : Color() {}

        template<typename V>
        VertexPC(
            V&& position,
            const color_rgba32f& color) :
            VertexP(std::forward<V>(position)),
            Color(color)
        {}
    };

    struct VertexPN : public VertexP
    {
        alignas(16) vec3 Normal;

        VertexPN() : Normal() {}

        template<typename V1, typename V2>
        VertexPN(
            V1&& position,
            V2&& normal) :
            VertexP(std::forward<V1>(position)),
            Normal(std::forward<V2>(normal))
        {}
    };

    struct VertexPT : public VertexP
    {
        alignas(8) vec2 TextureCoords;

        VertexPT() : TextureCoords() {}

        template<typename V1, typename V2>
        VertexPT(
            V1&& position,
            V2&& tex_coord) :
            VertexP(std::forward<V1>(position)),
            TextureCoords(std::forward<V2>(tex_coord))
        {}
    };

    struct VertexPNT : public VertexPN
    {
        alignas(8) vec2 TextureCoords;

        VertexPNT() : TextureCoords() {}

        template<typename V1, typename V2, typename V3>
        VertexPNT(
            V1&& position,
            V2&& normal,
            V3&& tex_coord) :
            VertexPN(
                std::forward<V1>(position),
                std::forward<V2>(normal)),
            TextureCoords(std::forward<V3>(tex_coord))
        {}
    };

    struct VertexPNTT : public VertexPNT
    {
        // The 4th component of Tangent represents the
        // handedness of the tangent coordinate system
        // and can be either +1 (right-handed) or -1 (left-handed)
        alignas(16) vec4 Tangent;

        VertexPNTT() : Tangent() {}

        template<typename V1, typename V2, typename V3, typename V4>
        VertexPNTT(
            V1&& position,
            V2&& normal,
            V3&& tex_coord,
            V4&& tangent) :
            VertexPNT(
                std::forward<V1>(position),
                std::forward<V2>(normal),
                std::forward<V3>(tex_coord)),
            Tangent(std::forward<V4>(tangent))
        {}
    };

    // Vertex used in particle systems.
    struct VertexPVT : public VertexP
    {
        alignas(16) vec3 Velocity; // is written on GPU side by transform feedback
        alignas(16) vec3 InitialVelocity;
        float StartTime;

        VertexPVT() : Velocity(), InitialVelocity(), StartTime() {}

        template<typename V>
        VertexPVT(
            V&& position,
            const vec3& velocity,
            float startTime) :
            VertexP(std::forward<V>(position)),
            Velocity(velocity),
            InitialVelocity(velocity),
            StartTime(startTime)
        {}
    };
}