/*
Copyright (c) 2020 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#include "ConditionalCompilation.h"

#include <iostream>
#include "File.h"
#include "Program.h"

int main(int argc, char** argv)
{
#ifdef _DEBUG
    std::cout << argv[0] << std::endl;
    std::cout << util::file::getCurrentDirectory() << std::endl;
    std::cout << "--- DEBUG VERSION ---" << std::endl;
#else
    std::cout << "--- RELEASE VERSION ---" << std::endl;
#endif

    int exit_code = EXIT_SUCCESS;
    try
    {
        Program prog;
        prog.Main();
    }
    catch (std::exception ex)
    {
        std::cout << ex.what() << std::endl;
        exit_code = EXIT_FAILURE;
    }

    std::cout << "Press [Enter] to exit ...";
    std::cin.get();

    return exit_code;
}