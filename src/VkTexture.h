#pragma once

#include "ConditionalCompilation.h"

// Utility
#include "Image.h"

// VkEngine
#include "HostVisibleBuffer.h"
#include "VkImage2d.h"

namespace vulkan
{
    class Texture
    {
        #pragma region Fields

        private:
            VkImage2d m_Image;

            bool m_IsDestroyed;

        #pragma endregion

        #pragma region Constructors

        public:
            Texture(
                const VkContext& vkContext,
                const util::Image& image) :

                m_Image(
                    vkContext.PhysicalDevice,
                    vkContext.LogicalDevice,
                    image.width(),
                    image.height(),
                    /*format*/ vk::Format::eR8G8B8A8Unorm,
                    /*tiling*/ vk::ImageTiling::eOptimal,
                    /*useFlags*/ vk::ImageUsageFlagBits::eTransferDst | vk::ImageUsageFlagBits::eSampled,
                    /*propFlags*/ vk::MemoryPropertyFlagBits::eDeviceLocal,
                    /*aspectFlags*/ vk::ImageAspectFlagBits::eColor),
                m_IsDestroyed(false)
            {
                auto imageStagingBuffer = CreateStagingBuffer_Src(vkContext, image.size());
                imageStagingBuffer.set(image.data());

                TransitionImageLayout(
                    vkContext,
                    m_Image.Image(),
                    /*oldLayout*/ vk::ImageLayout::eUndefined,
                    /*newLayout*/ vk::ImageLayout::eTransferDstOptimal);

                CopyBufferToImage(
                    vkContext,
                    /*source*/ imageStagingBuffer.Buffer(),
                    /*destination*/ m_Image.Image(),
                    /*srcOffset*/ 0,
                    /*dstOffset*/ vk::Offset3D {0, 0, 0},
                    m_Image.width(),
                    m_Image.height());

                TransitionImageLayout(
                    vkContext,
                    m_Image.Image(),
                    /*oldLayout*/ vk::ImageLayout::eTransferDstOptimal,
                    /*newLayout*/ vk::ImageLayout::eShaderReadOnlyOptimal);
            }

            Texture(Texture&& rvalue) noexcept :
                m_Image(std::move(rvalue.m_Image)),
                m_IsDestroyed(rvalue.m_IsDestroyed)
            {
                rvalue.m_IsDestroyed = true;
            }

        #pragma endregion

        #pragma region Destructor

        public:
            void Destroy()
            {
                if (m_IsDestroyed)
                    return;
                m_IsDestroyed = true;

                m_Image.Destroy();
            }

            ~Texture() { Destroy(); }

        #pragma endregion

        #pragma region Deleted Functions

        private:
            Texture(const Texture&) = delete;
            Texture& operator=(const Texture&) = delete;

        #pragma endregion

        #pragma region Properties

        public:
            const VkImage2d& Image() const { return m_Image; }

        #pragma endregion

        #pragma region Methods

        public:

        #pragma endregion
    };
}