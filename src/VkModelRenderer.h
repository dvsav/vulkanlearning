/*
Copyright (c) 2020 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

// STL
#include <cstring>
#include <vector>
#include <array>
#include <iostream>
#include <algorithm>
#include <limits>
#include <optional>
#include <string>

// Utility
#include "File.h"
#include "utility.h"
#include "VkUtility.h"

// Engine
#include "ProgramBase.h"
#include "Vertex.h"
#include "WorldPositionAndOrientation.h"
#include "Model.h"
#include "Camera.h"

// VkEngine
#include "VkGraphicsContext.h"
#include "VulkanSwapchain.h"
#include "VkGraphicsPipeline.h"
#include "VkRenderLoop.h"
#include "VkContext.h"
#include "VkMesh.h"
#include "UniformBuffer.h"
#include "DynamicUniformBuffer.h"
#include "VkTexture.h"
#include "VkImport.h"
#include "VkModel.h"

namespace vulkan
{
    class VkModelRenderer
    {
        #pragma region Typedefs

        protected:
            using vertex_type = VertexPT;
            using index_type = uint16_t;
            using model_type = VkModel<vertex_type, index_type>;

        #pragma endregion

        #pragma region Fields

        protected:
            // how many textures we can load in our program
            static constexpr size_t MAX_TEXTURES = 10;
            static constexpr size_t MAX_MODELS = 5;

        protected:
            const VkGraphicsContext& m_VkGraphicsContext;
            const GraphicsPipeline& m_GraphicsPipeline;

            //vk::DescriptorPool m_UniformDescriptorPool;
            //std::vector< std::vector<vk::DescriptorSet> > m_UniformDescriptorSets; // [Number of swapchain images][MAX_MODELS] mamtrix of uniform descriptor sets

            vk::Sampler m_TextureSampler;
            vk::DescriptorPool m_SamplerDescriptorPool;
            std::vector<vk::DescriptorSet> m_TextureSamplerDescriptorSets;

            std::vector< const model_type* > m_Models;

            bool m_IsDestroyed;

        #pragma endregion

        #pragma region Constructors

        public:
            VkModelRenderer(
                const VkGraphicsContext& vkGraphicsContext,
                const GraphicsPipeline& graphicsPipeline) :
                m_VkGraphicsContext(vkGraphicsContext),
                m_GraphicsPipeline(graphicsPipeline),
                //m_UniformDescriptorPool(CreateUniformDescriptorPool()),
                //m_UniformDescriptorSets(CreateUniformDescriptorSets()),
                m_TextureSampler(CreateTextureSampler()),
                m_SamplerDescriptorPool(CreateSamplerDescriptorPool()),
                m_TextureSamplerDescriptorSets(),
                m_Models(),
                m_IsDestroyed(false)
            {}

        #pragma endregion

        #pragma region Destructor

        public:
            virtual void Destroy()
            {
                if (m_IsDestroyed)
                    return;
                m_IsDestroyed = true;

                m_VkGraphicsContext.LogicalDevice().destroyDescriptorPool(m_SamplerDescriptorPool);
                m_VkGraphicsContext.LogicalDevice().destroySampler(m_TextureSampler);

                //m_VkGraphicsContext.LogicalDevice().destroyDescriptorPool(m_UniformDescriptorPool);
            }

            virtual ~VkModelRenderer() { Destroy(); }

        #pragma endregion

        #pragma region Deleted Functions

        private:
            VkModelRenderer(const VkModelRenderer&) = delete;
            VkModelRenderer& operator=(const VkModelRenderer&) = delete;

        #pragma endregion

        #pragma region Methods

        private:
            // Sampler describes how a texture is sampled inside shader.
            // Determines mipmap filtering, clamping etc.
            vk::Sampler CreateTextureSampler()
            {
                return m_VkGraphicsContext.LogicalDevice().createSampler(
                    vk::SamplerCreateInfo
                    {
                        .magFilter = vk::Filter::eLinear, // How to render when texture pixel occupies many screen pixels
                        .minFilter = vk::Filter::eLinear, // How to render when texture pixel occupies less than one screen pixel
                        .mipmapMode = vk::SamplerMipmapMode::eLinear,    // Mipmap interpolation mode
                        .addressModeU = vk::SamplerAddressMode::eRepeat, // How to handle texture wrap in U (x) direction
                        .addressModeV = vk::SamplerAddressMode::eRepeat, // How to handle texture wrap in V (y) direction
                        .addressModeW = vk::SamplerAddressMode::eRepeat, // How to handle texture wrap in W (z) direction (form volumetric textures)
                        .mipLodBias = 0.0f, // Level of Details bias for mipmap level (https://ru.wikipedia.org/wiki/MIP-текстурирование)
#ifdef ENABLE_ANISOTROPY
                        .anisotropyEnable = VK_TRUE, // Enable anisotropic filtering (effect can be seen on large objects that are stretched far away)
#endif
                        .maxAnisotropy = 16, // The amount of samples being taken in anisotropic filtering
                        .minLod = 0.0f, // Minimum Level of Details to pick mip level
                        .maxLod = 0.0f, // Maximum Level of Details to pick mip level (0 because we don't use mipmaps in this example)
                        .borderColor = vk::BorderColor::eIntOpaqueBlack, // Has an effect only if addressMode = clamp to border
                        .unnormalizedCoordinates = VK_FALSE, // Normalized coordinates (0..1, 0..1), unnormalized coords (0..width, 0...height)
                    });
            }

            // Descriptor sets can't be created directly, they must be allocated
            // from a pool like command buffers.
            vk::DescriptorPool CreateSamplerDescriptorPool()
            {
                std::array<vk::DescriptorPoolSize, 1> poolSizes
                {
                    vk::DescriptorPoolSize
                    {
                        .type = vk::DescriptorType::eCombinedImageSampler, // descriptor type (combined Image descriptor + Sampler descriptor)
                        .descriptorCount = MAX_TEXTURES // the number of descriptors of the specified type which can be allocated in total from the pool
                    }
                };

                vk::DescriptorPoolCreateInfo poolInfo
                {
                    .maxSets = MAX_MODELS, // the maximum number of descriptor sets that may be allocated from the pool
                    .poolSizeCount = poolSizes.size(),
                    .pPoolSizes = poolSizes.data(),
                };

                // See https://www.reddit.com/r/vulkan/comments/8u9zqr/having_trouble_understanding_descriptor_pool/

                return m_VkGraphicsContext.LogicalDevice().createDescriptorPool(poolInfo);
            }

            vk::DescriptorSet CreateTextureSamplerDescriptorSet(
                const model_type& model)
            {
                // We need all the copies of the layout because the next function expects an array matching the number of sets.
                std::array<vk::DescriptorSetLayout, 1> layouts
                {
                    m_GraphicsPipeline.SamplerDescriptorSetLayout()
                };

                vk::DescriptorSetAllocateInfo allocInfo
                {
                    .descriptorPool = m_SamplerDescriptorPool,
                    .descriptorSetCount = layouts.size(), // One diffuse texture per model
                    .pSetLayouts = layouts.data()
                };

                auto descriptorSets = m_VkGraphicsContext.LogicalDevice().allocateDescriptorSets(allocInfo);

                // The descriptor set has been allocated now, but the descriptors within still need to be configured...
                vk::DescriptorImageInfo descriptorImageInfo
                {
                    .sampler = m_TextureSampler,
                    .imageView = model.material().DiffuseColor->Image().ImageView(),
                    .imageLayout = vk::ImageLayout::eShaderReadOnlyOptimal // Image layout when it is in use
                };

                // Info about what to write to a single descriptor (there can be many of them in a descriptor set)
                vk::WriteDescriptorSet descriptorWrite
                {
                    .dstSet = descriptorSets[0], // Descriptor set to update
                    .dstBinding = 0,             // Binding index inside shader
                    .dstArrayElement = 0,        // Descriptors can be arrays, so we also need to specify
                                                 // the first index in the array that we want to update.
                    .descriptorCount = 1,        // How many array elements you want to update
                    .descriptorType = vk::DescriptorType::eCombinedImageSampler,
                    .pImageInfo = &descriptorImageInfo
                };

                m_VkGraphicsContext.LogicalDevice().updateDescriptorSets(
                    1, // the number of descriptors to write to
                    &descriptorWrite, // array of WriteDescriptorSet
                    0, nullptr);      // array of CoypDescriptorSet

                return descriptorSets[0];
            }

        public:
            void AddModel(
                const model_type& model)
            {
                util::Requires::That(m_Models.size() < MAX_MODELS, FUNCTION_INFO);

                m_TextureSamplerDescriptorSets.push_back(
                    CreateTextureSamplerDescriptorSet(model));

                m_Models.push_back(&model);
            }

            void RemoveModel(size_t i)
            {
                m_VkGraphicsContext.LogicalDevice().freeDescriptorSets(
                    m_SamplerDescriptorPool,
                    m_TextureSamplerDescriptorSets[i]);

                m_TextureSamplerDescriptorSets.erase(
                    m_TextureSamplerDescriptorSets.begin() + i);

                m_Models.erase(m_Models.begin() + i);
            }

            // Record commands to the command buffers.
            // The commands bind resources (pipeline, uniforms, textures) and do drawing.
            // The usual practice is to prerecord a set of commands to the command buffer once
            // and then in the rendering loop submit them to the command queue on each iteration.
            virtual void RecordCommands(
                uint32_t imageIndex,
                const vk::CommandBuffer& commandBuffer,
                const vk::Framebuffer& framebuffer,
                const vk::RenderPass& renderPass,
                const vk::Extent2D& swapchainExtent,
                const engine::mat4& worldToScreen)
            {
#ifdef USE_DYNAMIC_STATES
                // Viewports define the transformation from the image to the framebuffer
                commandBuffer.setViewport(0,
                    vk::Viewport
                    {
                        .x = 0.0f,
                        .y = 0.0f,
                        .width = static_cast<float>(swapchainExtent.width),
                        .height = static_cast<float>(swapchainExtent.height),
                        .minDepth = 0.0f,
                        .maxDepth = 1.0f
                    });

                // Any pixels outside the scissor rectangles will be discarded by the rasterizer
                commandBuffer.setScissor(0,
                    vk::Rect2D
                    {
                        .offset = { 0, 0 },
                        .extent = swapchainExtent
                    });
#endif

                // Information about how to begin a render pass (only needed for graphical applications)
                // List of clear values
                std::array<vk::ClearValue, 2> clearValues
                {
                    vk::ClearColorValue(std::array<float, 4>{0.0f, 0.0f, 0.0f, 1.0f}), // black
                    vk::ClearDepthStencilValue{1.0f} // Depth Attachment Clear Value
                };
                vk::RenderPassBeginInfo renderPassBeginInfo
                {
                    .renderPass = renderPass, // Render Pass to begin
                    .framebuffer = framebuffer,
                    .renderArea =
                    {
                        .offset = {0, 0}, // Start point of render pass in pixels
                        .extent = swapchainExtent // Size of region to run render pass on (starting at offset)
                    },

                    // pClearValues is a pointer to an array of clearValueCount VkClearValue structures that contains
                    // clear values for each attachment, if the attachment uses a loadOp value of
                    // VK_ATTACHMENT_LOAD_OP_CLEAR or if the attachment has a depth/stencil format and
                    // uses a stencilLoadOp value of VK_ATTACHMENT_LOAD_OP_CLEAR. The array is indexed
                    // by attachment number. Only elements corresponding to cleared attachments are used.
                    // Other elements of pClearValues are ignored.
                    // https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkRenderPassBeginInfo.html
                    .clearValueCount = clearValues.size(),
                    .pClearValues = clearValues.data(),
                };

                // The 3rd parameter:
                // VK_SUBPASS_CONTENTS_INLINE
                //     The render pass commands will be embedded in the primary command buffer itself and no
                //     secondary command buffers will be executed.
                // VK_SUBPASS_CONTENTS_SECONDARY_COMMAND_BUFFERS
                //     The render pass commands will be executed from secondary command buffers that will be
                //     called from the primary command buffer.
                RunRenderPass runRenderPass(
                    commandBuffer,
                    renderPassBeginInfo,
                    vk::SubpassContents::eInline);
                {
                    // Binding pipeline here tells the GPU how to render the graphics primitives that are coming later.
                    // VK_PIPELINE_BIND_POINT_GRAPHICS tells the GPU that this is a graphics pipeline instead of a
                    // compute pipeline. Note that since this command is a command buffer command, it is possible
                    // for a program to define several graphics pipelines and switch between them in a single command
                    // buffer. https://vulkan.lunarg.com/doc/view/1.2.170.0/linux/tutorial/html/15-draw_cube.html
                    commandBuffer.bindPipeline(
                        vk::PipelineBindPoint::eGraphics,
                        m_GraphicsPipeline.Pipeline());

                    for (size_t iModel = 0; iModel < m_Models.size(); iModel++)
                    {
                        const auto& model = *m_Models[iModel];

                        const uint32_t VERTEX_BINDING_COUNT = 1;
                        const uint32_t VERTEX_BINDING_POINT = 0;
                        // Buffers to bind
                        std::array<vk::Buffer, VERTEX_BINDING_COUNT> vertexBuffers
                        {
                            model.Mesh().VertexBuffer().Buffer()
                        };
                        // Offsets into buffers being bound
                        std::array<vk::DeviceSize, VERTEX_BINDING_COUNT> offsets
                        {
                            0
                        };
                        commandBuffer.bindVertexBuffers(
                            VERTEX_BINDING_POINT, // first binding point 
                            VERTEX_BINDING_COUNT, // binding count
                            vertexBuffers.data(),
                            offsets.data());

                        commandBuffer.bindIndexBuffer(
                            model.Mesh().IndexBuffer().Buffer(), // buffer
                            0, // offset
                            type_traits<index_type>::index_type); // index type

                        // Bind descriptor sets (there can be many of them).
                        // At any time, the application can bind a new descriptor set to the command buffer in any point that has
                        // an identical layout. The same descriptor set layouts can be used to create multiple pipelines.
                        // Therefore, if you have a set of objects that share a common set of resources, but additionally each
                        // require some unique resources, you can leave the common set bound and replace the unique
                        // resources as your application moves through the objects that need to be rendered. [Sellers]
                        std::array<vk::DescriptorSet, 1> descriptorSets
                        {
                            m_TextureSamplerDescriptorSets[iModel],
                            //m_UniformDescriptorSets[iModel][imageIndex]
                        };
                        commandBuffer.bindDescriptorSets(
                            vk::PipelineBindPoint::eGraphics, // Unlike vertex and index buffers, descriptor sets are not unique to graphics pipelines.
                                                              // Therefore we need to specify if we want to bind descriptor sets to the graphics or compute pipeline.
                                                              // There is a separate set of bind points for each pipeline type, so binding one does not disturb the others.
                            m_GraphicsPipeline.PipelineLayout(), // Pipeline layout object used to program the bindings
                            0, // First descriptor set
                            descriptorSets.size(), // The number of descriptor sets to bind
                            descriptorSets.data(),
                            0, nullptr); // Dynamic offsets for a dynamic uniform buffer

                        auto mvp = worldToScreen * model.Wpo().ModelToWorldMatrix();

                        // Push constants to given shader stage directly
                        commandBuffer.pushConstants(
                            m_GraphicsPipeline.PipelineLayout(),
                            vk::ShaderStageFlagBits::eVertex, // Shader stage to push constants to
                            0,                                // offset into the data
                            sizeof(mvp), // size of constant being pushed
                            &mvp);       // pValues

                        // Command to execute pipeline
                        commandBuffer.drawIndexed(
                            model.Mesh().IndexCount(), // index count
                            1,  // instance count
                            0,  // first index
                            0,  // vertex offset
                            0); // first instance
                    }
                }
            }

        #pragma endregion
    };

    class VkModelRendererPostprocessing : public VkModelRenderer
    {
        #pragma region Fields

        private:
            const GraphicsPipeline& m_GraphicsPipelinePostprocessing;
            const VulkanSwapchainPostprocessing* m_VulkanSwapchainPostprocessing;
            vk::DescriptorPool m_InputAttachmentsDescriptorPool;
            std::vector<vk::DescriptorSet> m_InputAttachmentsDescriptorSets;

        #pragma endregion

        #pragma region Constructors

        public:
            VkModelRendererPostprocessing(
                const VkGraphicsContext& vkGraphicsContext,
                const GraphicsPipeline& graphicsPipeline,
                const GraphicsPipeline& graphicsPipelinePostprocessing,
                const VulkanSwapchainPostprocessing& vulkanSwapchainPostprocessing) :
                VkModelRenderer(vkGraphicsContext, graphicsPipeline),
                m_GraphicsPipelinePostprocessing(graphicsPipelinePostprocessing),
                m_VulkanSwapchainPostprocessing(&vulkanSwapchainPostprocessing),
                m_InputAttachmentsDescriptorPool(CreateInputAttachmentsDescriptorPool()),
                m_InputAttachmentsDescriptorSets(CreateInputAttachmentsDescriptorSets())
            {}

        #pragma endregion

        #pragma region Destructor

        public:
            void Destroy() override
            {
                if (m_IsDestroyed)
                    return;

                m_VkGraphicsContext.LogicalDevice().destroyDescriptorPool(m_InputAttachmentsDescriptorPool);
                VkModelRenderer::Destroy();
            }

            virtual ~VkModelRendererPostprocessing() { Destroy(); }

        #pragma endregion

        #pragma region Methods

        private:
            // Create input attachments descriptor pool
            vk::DescriptorPool CreateInputAttachmentsDescriptorPool()
            {
                std::array<vk::DescriptorPoolSize, 2> poolSizes
                {
                    // Color input attachment
                    vk::DescriptorPoolSize
                    {
                        .type = vk::DescriptorType::eInputAttachment, // descriptor type (combined Image descriptor + Sampler descriptor)
                        .descriptorCount = m_VulkanSwapchainPostprocessing->ColorBuffers().size() // the number of descriptors of the specified type which can be allocated in total from the pool
                    },
                    // Depth input attachment
                    vk::DescriptorPoolSize
                    {
                        .type = vk::DescriptorType::eInputAttachment, // descriptor type (combined Image descriptor + Sampler descriptor)
                        .descriptorCount = m_VulkanSwapchainPostprocessing->DepthBuffers().size() // the number of descriptors of the specified type which can be allocated in total from the pool
                    },
                };

                vk::DescriptorPoolCreateInfo poolInfo
                {
                    .maxSets = m_VulkanSwapchainPostprocessing->NumberOfImages(), // the maximum number of descriptor sets that may be allocated from the pool
                    .poolSizeCount = poolSizes.size(),
                    .pPoolSizes = poolSizes.data(),
                };

                return m_VkGraphicsContext.LogicalDevice().createDescriptorPool(poolInfo);
            }

            std::vector<vk::DescriptorSet> CreateInputAttachmentsDescriptorSets()
            {
                // We need all the copies of the layout because the next function expects
                // an array matching the number of sets.
                std::vector<vk::DescriptorSetLayout> layouts(
                    m_VulkanSwapchainPostprocessing->NumberOfImages(),
                    m_GraphicsPipelinePostprocessing.InputAttachmentsDescriptorSetLayout());

                vk::DescriptorSetAllocateInfo allocInfo
                {
                    .descriptorPool = m_InputAttachmentsDescriptorPool,
                    .descriptorSetCount = layouts.size(),
                    .pSetLayouts = layouts.data()
                };

                auto descriptorSets = m_VkGraphicsContext.LogicalDevice().allocateDescriptorSets(allocInfo);

                // The descriptor sets have been allocated now, but the descriptors within still need to be configured...
                for (size_t i = 0; i < descriptorSets.size(); i++)
                {
                    vk::DescriptorImageInfo colorImageInfo
                    {
                        .sampler = vk::Sampler{}, // Sampler doesn't work with input attachments
                        .imageView = m_VulkanSwapchainPostprocessing->ColorBuffers()[i].ImageView(),
                        .imageLayout = vk::ImageLayout::eShaderReadOnlyOptimal // Image layout when it is in use
                    };

                    vk::DescriptorImageInfo depthImageInfo
                    {
                        .sampler = vk::Sampler{}, // Sampler doesn't work with input attachments
                        .imageView = m_VulkanSwapchainPostprocessing->DepthBuffers()[i].ImageView(),
                        .imageLayout = vk::ImageLayout::eShaderReadOnlyOptimal // Image layout when it is in use
                    };

                    std::array<vk::WriteDescriptorSet, 2> writes
                    {
                        // color descriptor set
                        vk::WriteDescriptorSet
                        {
                            .dstSet = descriptorSets[i], // Descriptor set to update
                            .dstBinding = 0,             // Binding index inside shader
                            .dstArrayElement = 0,        // Descriptors can be arrays, so we also need to specify
                                                         // the first index in the array that we want to update.
                            .descriptorCount = 1,        // How many array elements you want to update
                            .descriptorType = vk::DescriptorType::eInputAttachment,
                            .pImageInfo = &colorImageInfo
                        },
                        // depth descriptor set
                        vk::WriteDescriptorSet
                        {
                            .dstSet = descriptorSets[i], // Descriptor set to update
                            .dstBinding = 1,             // Binding index inside shader
                            .dstArrayElement = 0,        // Descriptors can be arrays, so we also need to specify
                                                         // the first index in the array that we want to update.
                            .descriptorCount = 1,        // How many array elements you want to update
                            .descriptorType = vk::DescriptorType::eInputAttachment,
                            .pImageInfo = &depthImageInfo
                        }
                    };

                    m_VkGraphicsContext.LogicalDevice().updateDescriptorSets(
                        writes.size(), // the number of descriptors to write to
                        writes.data(), // array of WriteDescriptorSet
                        0, nullptr);   // array of CoypDescriptorSet
                }

                return descriptorSets;
            }

        public:
            void RecreateSwapChain(
                const VulkanSwapchainPostprocessing& newVulkanSwapchain)
            {
                m_VkGraphicsContext.LogicalDevice().destroyDescriptorPool(m_InputAttachmentsDescriptorPool);

                m_VulkanSwapchainPostprocessing = &newVulkanSwapchain;

                m_InputAttachmentsDescriptorPool = CreateInputAttachmentsDescriptorPool();
                m_InputAttachmentsDescriptorSets = CreateInputAttachmentsDescriptorSets();
            }

            void RecordCommands(
                uint32_t imageIndex,
                const vk::CommandBuffer& commandBuffer,
                const vk::Framebuffer& framebuffer,
                const vk::RenderPass& renderPass,
                const vk::Extent2D& swapchainExtent,
                const engine::mat4& worldToScreen) override
            {
#ifdef USE_DYNAMIC_STATES
                // Viewports define the transformation from the image to the framebuffer
                commandBuffer.setViewport(0,
                    vk::Viewport
                    {
                        .x = 0.0f,
                        .y = 0.0f,
                        .width = static_cast<float>(swapchainExtent.width),
                        .height = static_cast<float>(swapchainExtent.height),
                        .minDepth = 0.0f,
                        .maxDepth = 1.0f
                    });

                // Any pixels outside the scissor rectangles will be discarded by the rasterizer
                commandBuffer.setScissor(0,
                    vk::Rect2D
                    {
                        .offset = { 0, 0 },
                        .extent = swapchainExtent
                    });
#endif

                // Information about how to begin a render pass (only needed for graphical applications)
                // List of clear values matching the attachments in the render pass.
                std::array<vk::ClearValue, 3> clearValues
                {
                    vk::ClearColorValue(std::array<float, 4>{0.0f, 0.0f, 0.0f, 1.0f}), // Clear color for the swapchain image (black)
                    vk::ClearColorValue(std::array<float, 4>{0.0f, 0.0f, 0.0f, 1.0f}), // Clear color for the color buffer (black)
                    vk::ClearDepthStencilValue{1.0f} // Depth Attachment Clear Value
                };
                vk::RenderPassBeginInfo renderPassBeginInfo
                {
                    .renderPass = renderPass, // Render Pass to begin
                    .framebuffer = framebuffer,
                    .renderArea =
                    {
                        .offset = {0, 0}, // Start point of render pass in pixels
                        .extent = swapchainExtent // Size of region to run render pass on (starting at offset)
                    },

                    // pClearValues is a pointer to an array of clearValueCount VkClearValue structures that contains
                    // clear values for each attachment, if the attachment uses a loadOp value of
                    // VK_ATTACHMENT_LOAD_OP_CLEAR or if the attachment has a depth/stencil format and
                    // uses a stencilLoadOp value of VK_ATTACHMENT_LOAD_OP_CLEAR. The array is indexed
                    // by attachment number. Only elements corresponding to cleared attachments are used.
                    // Other elements of pClearValues are ignored.
                    // https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkRenderPassBeginInfo.html
                    .clearValueCount = clearValues.size(),
                    .pClearValues = clearValues.data()
                };

                // The 3rd parameter:
                // VK_SUBPASS_CONTENTS_INLINE
                //     The render pass commands will be embedded in the primary command buffer itself and no
                //     secondary command buffers will be executed.
                // VK_SUBPASS_CONTENTS_SECONDARY_COMMAND_BUFFERS
                //     The render pass commands will be executed from secondary command buffers that will be
                //     called from the primary command buffer.
                RunRenderPass runRenderPass(
                    commandBuffer,
                    renderPassBeginInfo,
                    vk::SubpassContents::eInline);
                {
                    // Binding pipeline here tells the GPU how to render the graphics primitives that are coming later.
                    // VK_PIPELINE_BIND_POINT_GRAPHICS tells the GPU that this is a graphics pipeline instead of a
                    // compute pipeline. Note that since this command is a command buffer command, it is possible
                    // for a program to define several graphics pipelines and switch between them in a single command
                    // buffer. https://vulkan.lunarg.com/doc/view/1.2.170.0/linux/tutorial/html/15-draw_cube.html
                    commandBuffer.bindPipeline(
                        vk::PipelineBindPoint::eGraphics,
                        m_GraphicsPipeline.Pipeline());

                    for (size_t iModel = 0; iModel < m_Models.size(); iModel++)
                    {
                        const auto& model = *m_Models[iModel];

                        const uint32_t VERTEX_BINDING_COUNT = 1;
                        const uint32_t VERTEX_BINDING_POINT = 0;
                        // Buffers to bind
                        std::array<vk::Buffer, VERTEX_BINDING_COUNT> vertexBuffers
                        {
                            model.Mesh().VertexBuffer().Buffer()
                        };
                        // Offsets into buffers being bound
                        std::array<vk::DeviceSize, VERTEX_BINDING_COUNT> offsets
                        {
                            0
                        };
                        commandBuffer.bindVertexBuffers(
                            VERTEX_BINDING_POINT, // first binding point 
                            VERTEX_BINDING_COUNT, // binding count
                            vertexBuffers.data(),
                            offsets.data());

                        commandBuffer.bindIndexBuffer(
                            model.Mesh().IndexBuffer().Buffer(), // buffer
                            0, // offset
                            type_traits<index_type>::index_type); // index type

                        // Bind descriptor sets (there can be many of them).
                        // At any time, the application can bind a new descriptor set to the command buffer in any point that has
                        // an identical layout. The same descriptor set layouts can be used to create multiple pipelines.
                        // Therefore, if you have a set of objects that share a common set of resources, but additionally each
                        // require some unique resources, you can leave the common set bound and replace the unique
                        // resources as your application moves through the objects that need to be rendered. [Sellers]
                        std::array<vk::DescriptorSet, 1> descriptorSets
                        {
                            m_TextureSamplerDescriptorSets[iModel],
                            //m_UniformDescriptorSets[iModel][imageIndex]
                        };
                        commandBuffer.bindDescriptorSets(
                            vk::PipelineBindPoint::eGraphics, // Unlike vertex and index buffers, descriptor sets are not unique to graphics pipelines.
                                                              // Therefore we need to specify if we want to bind descriptor sets to the graphics or compute pipeline.
                                                              // There is a separate set of bind points for each pipeline type, so binding one does not disturb the others.
                            m_GraphicsPipeline.PipelineLayout(), // Pipeline layout object used to program the bindings
                            0, // First descriptor set
                            descriptorSets.size(), // The number of descriptor sets to bind
                            descriptorSets.data(),
                            0, nullptr); // Dynamic offsets for a dynamic uniform buffer

                        auto mvp = worldToScreen * model.Wpo().ModelToWorldMatrix();

                        // Push constants to given shader stage directly
                        commandBuffer.pushConstants(
                            m_GraphicsPipeline.PipelineLayout(),
                            vk::ShaderStageFlagBits::eVertex, // Shader stage to push constants to
                            0,                                // offset into the data
                            sizeof(mvp), // size of constant being pushed
                            &mvp);       // pValues

                        // Command to execute pipeline
                        commandBuffer.drawIndexed(
                            model.Mesh().IndexCount(), // index count
                            1,  // instance count
                            0,  // first index
                            0,  // vertex offset
                            0); // first instance
                    }

                    // Start 2nd subpass
                    commandBuffer.nextSubpass(vk::SubpassContents::eInline);

                    commandBuffer.bindPipeline(
                        vk::PipelineBindPoint::eGraphics,
                        m_GraphicsPipelinePostprocessing.Pipeline());

                    commandBuffer.bindDescriptorSets(
                        vk::PipelineBindPoint::eGraphics,
                        m_GraphicsPipelinePostprocessing.PipelineLayout(), // Pipeline layout object used to program the bindings
                        0, // First descriptor set
                        1, // The number of descriptor sets to bind
                        &m_InputAttachmentsDescriptorSets[imageIndex],
                        0, nullptr); // Dynamic offsets for a dynamic uniform buffer

                    // We are drawing just a single triangle
                    commandBuffer.draw(
                        /*vertexCount*/ 3,
                        /*instanceCount*/ 1,
                        /*firstVertex*/ 0,
                        /*firstInstance*/ 0);
                }
            }

        #pragma endregion
    };
}