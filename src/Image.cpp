/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

// STL
#include <stdexcept>

// Utility
#include "Requires.h"

// STB IMAGE
#define STB_IMAGE_IMPLEMENTATION
#include "stb/stb_image.h"

#include "Image.h"

namespace util
{
    Image::Image(
        const std::string& filename,
        unsigned int requiredChannels) :
        m_Data(nullptr),
        m_Width(0),
        m_Height(0),
        m_Channels(4),
        m_IsDestroyed(false)
    {
        util::Requires::That(requiredChannels <= 4, FUNCTION_INFO);

        int channels;
        m_Data = ::stbi_load(filename.c_str(), &m_Width, &m_Height, &channels, STBI_rgb_alpha);
        if (!m_Data)
            throw std::runtime_error("Image::Image() -> Cannot read file: " + filename);
    }

    void Image::Destroy()
    {
        if (m_IsDestroyed)
            return;

        m_IsDestroyed = true;

        ::stbi_image_free(m_Data);
    }
}
