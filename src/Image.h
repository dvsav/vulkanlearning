/*
Copyright (c) 2020 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

// STL
#include <string>

namespace util
{
    class Image
    {
    private:
        void* m_Data;
        int m_Width;
        int m_Height;
        int m_Channels;

        bool m_IsDestroyed;

    public:
        Image(
            const std::string& filename,
            unsigned int requiredChannels = 4);

        Image(Image&& rvalue) noexcept :
            m_Data(rvalue.m_Data),
            m_Width(rvalue.m_Width),
            m_Height(rvalue.m_Height),
            m_Channels(rvalue.m_Channels),
            m_IsDestroyed(rvalue.m_IsDestroyed)
        {
            rvalue.m_IsDestroyed = true;
        }

        void Destroy();

        ~Image() noexcept { Destroy(); }

    private:
        Image(const Image&) = delete;
        Image& operator=(const Image&) = delete;

    public:
        const void* data() const { return m_Data; }
        size_t width() const { return m_Width; }
        size_t height() const { return m_Height; }
        size_t channels() const { return m_Channels; }
        size_t size() const { return m_Width * m_Height * m_Channels; }
    };
}