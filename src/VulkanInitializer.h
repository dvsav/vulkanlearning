#pragma once

#include "ConditionalCompilation.h"

#include "VkHeaders.h"

namespace vulkan
{
    /// <summary>
    /// This class initializes Vulkan.
    /// See: Pawel Lapinski - API without Secrets: Introduction to Vulkan* Part 1: The Beginning
    /// https://software.intel.com/content/www/us/en/develop/articles/api-without-secrets-introduction-to-vulkan-part-1.html
    /// for details about loading Vulkan library and its entry points.
    /// </summary>
    class VulkanInitializer
    {
    public:
        VulkanInitializer()
        {
            // Vulkan API library is loaded dynamically in the constructor of the 
            // DynamicLoader class and a handle to the library is holded in it.
            static vk::DynamicLoader dl;

            static bool uninitialized = true;

            if (uninitialized)
            {
                // vkGetInstanceProcAddr is the only function that is guarateed to be available
                // by calling an OS-specific GetProcAddress() function in Windows or dlsym() in Linux.
                PFN_vkGetInstanceProcAddr vkGetInstanceProcAddr = dl.getProcAddress<PFN_vkGetInstanceProcAddr>("vkGetInstanceProcAddr");

                // This is to initialize defaultDispatchLoaderDynamic - a global object that
                // loads Vulkan API functions and holds the pointers to those functions.
                VULKAN_HPP_DEFAULT_DISPATCHER.init(vkGetInstanceProcAddr);

                uninitialized = false;
            }
        }
    };
}