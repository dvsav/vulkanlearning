#pragma once

#include "ConditionalCompilation.h"

// Utility
#include "VkUtility.h"
#include "utility.h"

// VkEngine
#include "VkContext.h"

namespace vulkan
{
    // Binds together three Vulkan notions: "image", "memory" and "image view".
    class VkImage2d final
    {
        #pragma region Fields

        private:
            const vk::Device& m_LogicalDevice;
            uint32_t m_Width;
            uint32_t m_Height;
            vk::Format m_ImageFormat;
            vk::Image m_Image;
            vk::DeviceMemory m_ImageMemory;
            vk::ImageView m_ImageView;

            bool m_IsDestroyed;

        #pragma endregion

        #pragma region Constructors

        public:
            VkImage2d(
                const vk::PhysicalDevice& physicalDevice,
                const vk::Device& logicalDevice,
                uint32_t width,
                uint32_t height,
                vk::Format format,
                vk::ImageTiling tiling,
                vk::ImageUsageFlags useFlags,
                vk::MemoryPropertyFlags memoryPropFlags,
                vk::ImageAspectFlags aspectFlags) :
                m_LogicalDevice(logicalDevice),
                m_Width(width),
                m_Height(height),
                m_ImageFormat(format),
                m_IsDestroyed(false)
            {
                // CREATE IMAGE STRUCTRE
                m_Image = logicalDevice.createImage(
                    vk::ImageCreateInfo
                    {
                        .imageType = vk::ImageType::e2D,             // Type of image (1D, 2D, 3D)
                        .format = format,
                        .extent = vk::Extent3D {width, height, 1u},  // width, height, depth
                        .mipLevels = 1,                              // Number of mipmap levels
                        .arrayLayers = 1,                            // Number of layers in image array
                        .samples = vk::SampleCountFlagBits::e1,
                        .tiling = tiling,                            // How image data should be arranged in memory
                        .usage = useFlags,
                        .initialLayout = vk::ImageLayout::eUndefined // Layout of image data on creation
                    });

                // ALLOCATE MEMORY
                auto memoryRequirements = logicalDevice.getImageMemoryRequirements(m_Image);
                m_ImageMemory = logicalDevice.allocateMemory(
                    vk::MemoryAllocateInfo
                    {
                        .allocationSize = memoryRequirements.size,
                        .memoryTypeIndex = FindMemoryTypeIndex(
                            physicalDevice,
                            memoryRequirements,
                            memoryPropFlags)
                    });

                // BIND IMAGE AND MEMORY
                logicalDevice.bindImageMemory(
                    m_Image,
                    m_ImageMemory,
                    0); // offset to memory

                // CREATE IMAGE VIEW
                m_ImageView = CreateImageView(
                    logicalDevice,
                    m_Image,
                    format,
                    aspectFlags);
            }

            VkImage2d(VkImage2d&& rvalue) noexcept :
                m_LogicalDevice(rvalue.m_LogicalDevice),
                m_Width(rvalue.m_Width),
                m_Height(rvalue.m_Height),
                m_ImageFormat(rvalue.m_ImageFormat),
                m_Image(rvalue.m_Image),
                m_ImageMemory(rvalue.m_ImageMemory),
                m_ImageView(rvalue.m_ImageView),
                m_IsDestroyed(rvalue.m_IsDestroyed)
            {
                rvalue.m_IsDestroyed = true;
            }

            VkImage2d& operator=(VkImage2d&& rvalue) noexcept
            {
                util::Requires::That(m_LogicalDevice == rvalue.m_LogicalDevice, FUNCTION_INFO);
                m_Width = rvalue.m_Width;
                m_Height = rvalue.m_Height;
                m_ImageFormat = rvalue.m_ImageFormat;
                m_Image = rvalue.m_Image;
                m_ImageMemory = rvalue.m_ImageMemory;
                m_ImageView = rvalue.m_ImageView;
                m_IsDestroyed = rvalue.m_IsDestroyed;
                rvalue.m_IsDestroyed = true;
                return *this;
            }

        #pragma endregion

        #pragma region Destructor

        public:
            void Destroy()
            {
                if (m_IsDestroyed)
                    return;

                m_IsDestroyed = true;

                m_LogicalDevice.destroyImageView(m_ImageView);
                m_LogicalDevice.destroyImage(m_Image);
                m_LogicalDevice.freeMemory(m_ImageMemory);
            }

            ~VkImage2d()
            {
                Destroy();
            }

        #pragma endregion

        #pragma region Deleted Functions

        private:
            VkImage2d(const VkImage2d&) = delete;
            VkImage2d& operator=(const VkImage2d&) = delete;

        #pragma endregion

        #pragma region Properties

        public:
            uint32_t width() const { return m_Width; }
            uint32_t height() const { return m_Height; }
            vk::Format ImageFormat() const { return m_ImageFormat; }
            const vk::Image& Image() const { return m_Image; }
            const vk::DeviceMemory& ImageMemory() const { return m_ImageMemory; }
            const vk::ImageView& ImageView() const { return m_ImageView; }

        #pragma endregion
    };

    /// <summary>
    /// Copies data from one memory type to another using
    /// temporary created command buffer.
    /// </summary>
    inline void CopyBufferToImage(
        const VkContext& vkContext,
        const vk::Buffer& source,
        const vk::Image& destination,
        size_t srcOffset,
        vk::Offset3D dstOffset,
        uint32_t width,
        uint32_t height)
    {
        vk::BufferImageCopy imageRegion
        {
            .bufferOffset = srcOffset,
            .bufferRowLength = 0,
            .bufferImageHeight = 0,
            .imageSubresource = vk::ImageSubresourceLayers
            {
                .aspectMask = vk::ImageAspectFlagBits::eColor, // Which aspect of image to copy
                .mipLevel = 0,       // Mipmap level to copy
                .baseArrayLayer = 0, // Starting array layer (if array)
                .layerCount = 1      // Number of layers to copy (if array)
            },
            .imageOffset = dstOffset, // x, y, z
            .imageExtent = { width, height, 1 }
        };

        // Data is copied from one memory type to another
        // through the execution of a copy command that must be
        // placed in a command buffer.
        // https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkMemoryBufferImageCopy.html

        RunTemporaryCommandBuffer(
            vkContext.LogicalDevice,
            vkContext.GraphicsQueue,
            vkContext.GraphicsCommandPool)
            .getCommandBuffer()
            .copyBufferToImage(
                source,
                destination,
                // The layout of the image subresources of dstImage specified in pRegions at the time this command is executed.
                // This means that the image MUST have this layout so as the copy operation to succeed.
                // To put it another way, command buffer "expects" the image to have this layout.
                vk::ImageLayout::eTransferDstOptimal,
                imageRegion);
    }

    // Change image layout.
    inline void TransitionImageLayout(
        const VkContext& vkContext,
        const vk::Image& image,
        vk::ImageLayout oldLayout,
        vk::ImageLayout newLayout)
    {
        // Memory barrier describes what stages of a pipeline depend on other stages finishing first.
        // Beteween the two pipeline stages we also can perform transitions between layouts of images.
        // So the transition, if we've defined it, takes place after the first pipeline stage and before
        // the second pipeline stage.

        vk::ImageMemoryBarrier imageMemoryBarrier
        {
            .oldLayout = oldLayout,
            .newLayout = newLayout,
            .srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED, // Queue family to transition from
            .dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED, // Queue family to transition to
            .image = image, // Image being accessed and modified as part of barrier
            .subresourceRange = vk::ImageSubresourceRange
            {
                .aspectMask = vk::ImageAspectFlagBits::eColor, // The aspect of image being altered
                .baseMipLevel = 0,   // 1st mipmap level to start alteration on
                .levelCount = 1,     // Number of mipmap levels to alter
                .baseArrayLayer = 0, // 1st layer to start alterations on (if array)
                .layerCount = 1      // Number of layers to alter (if array)
            }
        };

        vk::PipelineStageFlags src_stage{};
        vk::PipelineStageFlags dst_stage{};

        // If transitioning from new (undefined) image to image ready to receive data...
        if (oldLayout == vk::ImageLayout::eUndefined && newLayout == vk::ImageLayout::eTransferDstOptimal)
        {
            imageMemoryBarrier.srcAccessMask = {}; // Transition is allowed to start after any point at the 1st pipeline stage
            imageMemoryBarrier.dstAccessMask = vk::AccessFlagBits::eTransferWrite; // Transition must occur before we attempt to transfer-write (copy data to the image) at the 2nd pipeline stage

            src_stage = vk::PipelineStageFlagBits::eTopOfPipe;
            dst_stage = vk::PipelineStageFlagBits::eTransfer;
        }
        // If transitioning from transfer destination to shader readable...
        else if (oldLayout == vk::ImageLayout::eTransferDstOptimal && newLayout == vk::ImageLayout::eShaderReadOnlyOptimal)
        {
            imageMemoryBarrier.srcAccessMask = vk::AccessFlagBits::eTransferWrite; // The 1st stage has written data into image
            imageMemoryBarrier.dstAccessMask = vk::AccessFlagBits::eShaderRead;    // The 2nd stage reads texels from image

            src_stage = vk::PipelineStageFlagBits::eTransfer;
            dst_stage = vk::PipelineStageFlagBits::eFragmentShader;
        }

        RunTemporaryCommandBuffer(
            vkContext.LogicalDevice,
            vkContext.GraphicsQueue,
            vkContext.GraphicsCommandPool)
            .getCommandBuffer()
            .pipelineBarrier(
                /*srcStageMask*/ src_stage, // 1st pipeline stage
                /*dstStageMask*/ dst_stage, // 2nd pipeline stage
                /*dependencyFlags*/ vk::DependencyFlags{},
                /*memoryBarriers*/ nullptr,
                /*bufferMemoryBarriers*/ nullptr,
                /*imageMemoryBarriers*/ imageMemoryBarrier);
    }
}