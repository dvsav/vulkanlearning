#pragma once

#include "ConditionalCompilation.h"

// Utility
#include "VkImage2d.h"

namespace vulkan
{
    // A depth attachment is based on an image, just like the color attachment. The difference is that the swap chain
    // will not automatically create depth images for us. We only need a single depth image, because only one draw
    // operation is running at once. The depth image will again require the trifecta of resources: image, memory and image view.
    // https://vulkan-tutorial.com/Depth_buffering
    inline VkImage2d CreateDepthBuffer(
        const vk::PhysicalDevice& physicalDevice,
        const vk::Device& logicalDevice,
        const vk::Extent2D& imageExtent,
        vk::ImageUsageFlags usage = vk::ImageUsageFlagBits{})
    {
        return VkImage2d(
            physicalDevice,
            logicalDevice,
            imageExtent.width,
            imageExtent.height,
            /*format*/ FindDepthFormat(physicalDevice),
            /*tiling*/ vk::ImageTiling::eOptimal,
            /*useFlags*/ vk::ImageUsageFlagBits::eDepthStencilAttachment | usage,
            /*propFlags*/ vk::MemoryPropertyFlagBits::eDeviceLocal,
            /*aspectFlags*/ vk::ImageAspectFlagBits::eDepth);
    }

    // Create color buffer (for example, for deferred rendering).
    inline VkImage2d CreateColorBuffer(
        const vk::PhysicalDevice& physicalDevice,
        const vk::Device& logicalDevice,
        const vk::Extent2D& swapChainExtent,
        vk::ImageUsageFlags usage = vk::ImageUsageFlagBits{})
    {
        return VkImage2d(
            physicalDevice,
            logicalDevice,
            swapChainExtent.width,
            swapChainExtent.height,
            /*format*/ FindColorBufferFormat(physicalDevice),
            /*tiling*/ vk::ImageTiling::eOptimal,
            /*useFlags*/ vk::ImageUsageFlagBits::eColorAttachment | usage,
            /*propFlags*/ vk::MemoryPropertyFlagBits::eDeviceLocal,
            /*aspectFlags*/ vk::ImageAspectFlagBits::eColor);
    }
}