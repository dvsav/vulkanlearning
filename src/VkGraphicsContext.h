/*
Copyright (c) 2020 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

// STL
#include <cstring>
#include <vector>
#include <array>
#include <iostream>
#include <algorithm>
#include <limits>
#include <optional>
#include <string>

// Utility
#include "utility.h"
#include "VkUtility.h"

// Engine
#include "ProgramBase.h"

// VkEngine
#include "VulkanInitializer.h"

// References
// https://vulkan-tutorial.com
// https://vulkan.lunarg.com/doc/view/1.2.170.0/linux/tutorial/html/index.html
// https://software.intel.com/content/www/us/en/develop/articles/api-without-secrets-introduction-to-vulkan-preface.html
// https://gpuopen.com/learn/understanding-vulkan-objects/
// https://vkguide.dev
// https://zeux.io/2020/02/27/writing-an-efficient-vulkan-renderer/
// https://developer.samsung.com/galaxy-gamedev/resources.html

namespace vulkan
{
    class VkGraphicsContext final
    {
        #pragma region Fields

        private:
            // the extensions that the physical device should support
            static std::vector<const char*> DEVICE_EXTENSIONS;

            VulkanInitializer VULKAN_INITIALIZER;

        private:
            // The Vulkan API uses the vkInstance object to store all per-application state.
            // The application must create a Vulkan instance before performing any other Vulkan operations.
            // https://vulkan.lunarg.com/doc/view/1.2.170.0/linux/tutorial/html/01-init_instance.html
            vk::Instance m_VkInstance;
#ifdef ENABLE_DEBUG_LAYERS
            vk::DebugUtilsMessengerEXT m_DebugMessenger;
#endif
            vk::SurfaceKHR m_Surface;  // abstract interface to the platfrom-specific window subsystem
            uint32_t m_GraphicsQueueFamilyIndex;
            uint32_t m_PresentationQueueFamilyIndex;
            vk::PhysicalDevice m_PhysicalDevice; // the GPU
            vk::Device m_LogicalDevice; // abstract device used as an interface to the physical device

            // When we want to process any data (that is, draw a 3D scene from vertex data and vertex attributes)
            // we call Vulkan functions that are passed to the driver. These functions are not passed directly,
            // as sending each request separately down through a communication bus is inefficient. It is better
            // to aggregate them and pass in groups. In OpenGL this was done automatically by the driver and was
            // hidden from the user. OpenGL API calls were queued in a buffer and if this buffer was full (or if
            // we requested to flush it) whole buffer was passed to hardware for processing. In Vulkan this
            // mechanism is directly visible to the user and, more importantly, the user must specifically create
            // and manage buffers for commands. These are called (conveniently) command buffers.
            // Command buffers (as whole objects) are passed to the hardware for execution through queues. However,
            // these buffers may contain different types of operations, such as graphics commands (used for generating
            // and displaying images like in typical 3D games) or compute commands (used for processing data).
            // Specific types of commands may be processed by dedicated hardware, and that�s why queues are also
            // divided into different types. In Vulkan these queue types are called families.
            // https://software.intel.com/content/www/us/en/develop/articles/api-without-secrets-introduction-to-vulkan-part-1.html
            vk::Queue m_GraphicsQueue;  // the queue of graphics commands that we are going to send to the logical device
            vk::Queue m_PresentationQueue;

            bool m_IsDestroyed;

        #pragma endregion

        #pragma region Constructors

        public:
            VkGraphicsContext(HWND windowHandle) :
                VULKAN_INITIALIZER(),
                m_VkInstance(CreateInstance()),
#ifdef ENABLE_DEBUG_LAYERS
                m_DebugMessenger(m_VkInstance.createDebugUtilsMessengerEXT(DebugMessengerCreateInfo())),
#endif
                m_Surface(m_VkInstance.createWin32SurfaceKHR(
                    vk::Win32SurfaceCreateInfoKHR
                    {
                        .hinstance = ::GetModuleHandle(nullptr),
                        .hwnd = windowHandle
                    })),

                m_PhysicalDevice(PickAppropriatePhysicalDevice(m_GraphicsQueueFamilyIndex, m_PresentationQueueFamilyIndex)),
                m_LogicalDevice(CreateLogicalDevice()),
                m_GraphicsQueue(m_LogicalDevice.getQueue(m_GraphicsQueueFamilyIndex, 0)),         // retrieve queue from logical device (queue index from within a given family must be smaller than the total number of queues we've requested from the given family)
                m_PresentationQueue(m_LogicalDevice.getQueue(m_PresentationQueueFamilyIndex, 0)), // retrieve queue from logical device (queue index from within a given family must be smaller than the total number of queues we've requested from the given family)

                m_IsDestroyed(false)
            {}

        #pragma endregion

        #pragma region Deleted Functions

        private:
            VkGraphicsContext(const VkGraphicsContext&) = delete;
            VkGraphicsContext& operator=(const VkGraphicsContext&) = delete;

        #pragma endregion

        #pragma region Destructor

        public:
            void Destroy()
            {
                if (m_IsDestroyed)
                    return;

                m_IsDestroyed = true;

                // Wait on the host for the completion of outstanding queue 
                // operations for all queues on a given logical device.
                m_LogicalDevice.waitIdle();

                m_VkInstance.destroySurfaceKHR(m_Surface);
                m_LogicalDevice.destroy();

#ifdef ENABLE_DEBUG_LAYERS
                m_VkInstance.destroyDebugUtilsMessengerEXT(m_DebugMessenger);
#endif

                m_VkInstance.destroy();
            }

            ~VkGraphicsContext() { Destroy(); }

        #pragma endregion

        #pragma region Properties

        public:
            const vk::Instance& Instance() const { return m_VkInstance; }
            const vk::SurfaceKHR& Surface() const { return m_Surface; }
            uint32_t GraphicsQueueFamilyIndex() const { return m_GraphicsQueueFamilyIndex; }
            uint32_t PresentationQueueFamilyIndex() const { return m_PresentationQueueFamilyIndex; }
            const vk::PhysicalDevice& PhysicalDevice() const { return m_PhysicalDevice; }
            const vk::Device& LogicalDevice() const { return m_LogicalDevice; }
            const vk::Queue& GraphicsQueue() const { return m_GraphicsQueue; }
            const vk::Queue& PresentationQueue() const { return m_PresentationQueue; }

        #pragma endregion

        #pragma region Methods

        private:
            // Creates a Vulkan instance.
            // Specifies enabled layers and extensions
            // as well as the debug message callback function.
            vk::Instance CreateInstance()
            {
                // Information about the aplication.
                // Most of data here is just for the developer's information.
                vk::ApplicationInfo appInfo
                {
                    .pApplicationName = "VulkanLearning",           // custom name of the application
                    .applicationVersion = VK_MAKE_VERSION(1, 0, 0), // custom version of the application
                    .pEngineName = "OpenRenderingEngine",           // custom engine name
                    .engineVersion = VK_MAKE_VERSION(1, 0, 0),      // custom engine version
                    .apiVersion = VK_API_VERSION_1_2                // minimal required Vulkan version
                };

                // A list of Vulkan extensions to be used.
                // Vulkan extensions are simply additional features that Vulkan implementations may provide
                // if they so choose to. They add new functions, structs, and valid enumerators to the API,
                // and they can change some of the behavior of existing functions. There are instance level
                // extensions and device level extensions. They must be enabled separately.
                // You have to detect whether the implementation supports these extensions before creating
                // an instance / device. And you have to explicitly ask for such extensions when you create
                // the instance / device (in OpenGL all extensions are available in created context while
                // in Vulkan they are not available unless you enabled them explicitly).
                // Source: https://computergraphics.stackexchange.com/questions/8170/what-is-a-vulkan-extension
                std::vector<const char*> extensions
                {
                    #ifdef ENABLE_DEBUG_LAYERS
                        // Provides debug message callbacks
                        // https://www.lunarg.com/wp-content/uploads/2018/05/Vulkan-Debug-Utils_05_18_v1.pdf
                        VK_EXT_DEBUG_UTILS_EXTENSION_NAME,
                    #endif

                    // A WSI (Window System Integration) extension needed to be able to draw graphics on the screen.
                    // It describes a �surface� object, which is a logical representation of an application's window.
                    // This extension allows us to check different parameters (that is,  capabilities, supported formats,
                    // size) of a surface and to query whether the given physical device supports a swap chain (more
                    // precisely, whether the given queue family supports presenting an image to a given surface).
                    // https://software.intel.com/content/www/us/en/develop/articles/api-without-secrets-introduction-to-vulkan-part-2.html
                    VK_KHR_SURFACE_EXTENSION_NAME,

                    // This is a platform-specific extension required for the interoperation with MS Windows window subsystem
                    // see https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VK_KHR_win32_surface.html
                    // Although the VkSurfaceKHR object and its usage is platform agnostic, its creation isn't because it depends
                    // on window system details. For example, it needs the HWND and HMODULE handles on Windows. Therefore there
                    // is a platform-specific addition to the extension, which on Windows is called VK_KHR_win32_surface
                    // see https://vulkan-tutorial.com/en/Drawing_a_triangle/Presentation/Window_surface
                    // This extension allows us to create a surface that represents the application's window in a given OS (Windows).
                    VK_KHR_WIN32_SURFACE_EXTENSION_NAME
                };

                // Check that the required extensions are supported
                for (auto ext : extensions)
                {
                    if (!IsInstanceExtensionSupported(ext))
                        throw VkException(FUNCTION_INFO);
                }

                // Vulkan supports intercepting or hooking API entry points via a layer framework.
                // A layer can intercept all or any subset of Vulkan API entry points. Multiple
                // layers can be chained together to cascade their functionality in the appearance
                // of a single, larger layer.
                // Source: https://vulkan.lunarg.com/doc/view/1.2.131.1/windows/layer_configuration.html
                std::vector<const char*> layers
                {
                    #ifdef ENABLE_DEBUG_LAYERS
                        "VK_LAYER_KHRONOS_validation"
                    #endif
                };

                // Check that the required layers are supported
                for (auto layer : layers)
                {
                    if (!IsLayerSupported(layer))
                        throw VkException(FUNCTION_INFO);
                }

                // In Vulkan every XXX that can be created requires an XXXCreateInfo
                vk::InstanceCreateInfo instInfo
                {
                    .pApplicationInfo = &appInfo,

                    .enabledLayerCount = layers.size(),
                    .ppEnabledLayerNames = layers.data(),

                    .enabledExtensionCount = extensions.size(),
                    .ppEnabledExtensionNames = extensions.data()
                };

                auto instance = vk::createInstance(instInfo);

                // Load instance-level functions' entry points
                VULKAN_HPP_DEFAULT_DISPATCHER.init(instance);

                return instance;
            }

            static VKAPI_ATTR VkBool32 VKAPI_CALL DebugCallback(
                VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
                VkDebugUtilsMessageTypeFlagsEXT messageType,
                const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
                void* pUserData)
            {
                // VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT - Diagnostic message
                // VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT    - Informational message like the creation of a resource
                // VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT - Message about behavior that is not necessarily an error, but very likely a bug in your application
                // VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT   - Message about behavior that is invalid and may cause crashes
                // Source: https://vulkan-tutorial.com/en/Drawing_a_triangle/Setup/Validation_layers
                // if the message is severe enough...
                if (messageSeverity >= VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT)
                {
                    std::stringstream is(pCallbackData->pMessage);
                    while (is)
                    {
                        char c = is.get();
                        if (c == ';' || c == ']')
                        {
                            std::cerr << c;
                            std::cerr << std::endl;
                        }
                        else if (c == '|')
                            std::cerr << std::endl;
                        else
                            std::cerr << c;
                    }
                    std::cerr << std::endl << std::endl;
                }

                return VK_FALSE;
            }

            vk::DebugUtilsMessengerCreateInfoEXT DebugMessengerCreateInfo()
            {
                return vk::DebugUtilsMessengerCreateInfoEXT
                {
                    .messageSeverity = vk::DebugUtilsMessageSeverityFlagBitsEXT::eVerbose
                                     | vk::DebugUtilsMessageSeverityFlagBitsEXT::eWarning
                                     | vk::DebugUtilsMessageSeverityFlagBitsEXT::eError,

                    .messageType = vk::DebugUtilsMessageTypeFlagBitsEXT::eGeneral
                                 | vk::DebugUtilsMessageTypeFlagBitsEXT::eValidation
                                 | vk::DebugUtilsMessageTypeFlagBitsEXT::ePerformance,

                    .pfnUserCallback = DebugCallback,
                    .pUserData = this
                };
            }

            // Looks for a queue that supports graphics commands.
            // In case of success returns the queue family index which the graphics queue belongs to.
            static std::optional<uint32_t> getGraphicsQueueFamilyIndex(
                const vk::PhysicalDevice& pdev)
            {
                uint32_t i = 0;
                for (const auto& queueFamily : pdev.getQueueFamilyProperties())
                {
                    // Queue family is something that contains several queues of commands running in parallel
                    if (queueFamily.queueCount > 0
                        && queueFamily.queueFlags & vk::QueueFlagBits::eGraphics) // The queue must support graphics operations (like drawing things into a framebuffer).
                                                                                  // A queue that supports graphics operations also supports transfer operations by default.
                    {
                        return i;
                    }
                    i++;
                }
                return std::optional<uint32_t>{};
            }

            // Looks for a queue that supports presentation commands.
            // In case of success returns the queue family index which the graphics queue belongs to.
            // Remark: Not every physical device present in the system supports the window system integration.
            // Only those devices can draw graphics to a window that support a queue (so called presentation queue)
            // compatible with the surface passed to this function as a parameter.
            static std::optional<uint32_t> getPresentationQueueFamilyIndex(
                const vk::PhysicalDevice& pdev,
                const vk::SurfaceKHR& surf)
            {
                uint32_t i = 0;
                for (const auto& queueFamily : pdev.getQueueFamilyProperties())
                {
                    if (queueFamily.queueCount > 0
                        && pdev.getSurfaceSupportKHR(i, surf)) // the queue family must support presenting images to the given surface
                    {
                        return i;
                    }
                    i++;
                }
                return std::optional<uint32_t>{};
            }

            // Selects a physical device (GPU) that fits our demands.
            // https://vulkan-tutorial.com/Drawing_a_triangle/Setup/Physical_devices_and_queue_families
            vk::PhysicalDevice PickAppropriatePhysicalDevice(
                uint32_t& graphicsFamilyIndex,
                uint32_t& presentationFamilyIndex)
            {
                vulkan::PrintPhysicalDevices(m_VkInstance);

                for (const auto& pdev : m_VkInstance.enumeratePhysicalDevices())
                {
                    // Check if all of the required device extensions are supported.
                    if (std::any_of(
                            DEVICE_EXTENSIONS.cbegin(),
                            DEVICE_EXTENSIONS.cend(),
                            [&pdev](const char* ext_name) { return !vulkan::IsDeviceExtensionSupported(pdev, ext_name); }))
                    {
                        continue;
                    }

                    auto graphicsQueueFamilyIndex = getGraphicsQueueFamilyIndex(pdev);
                    if (!graphicsQueueFamilyIndex.has_value())
                        continue;

                    auto presentationQueueFamilyIndex = getPresentationQueueFamilyIndex(pdev, m_Surface);
                    if (!presentationQueueFamilyIndex.has_value())
                        continue;

                    // Features are additional hardware capabilities that are similar to extensions.
                    // They may not necessarily be supported by the driver and by default are not enabled.
                    // Features contain items such as geometry and tessellation shaders multiple viewports,
                    // logical operations, or additional texture compression formats. If a given physical
                    // device supports any feature we can enable it during logical device creation.
                    // Features are not enabled by default in Vulkan.
                    // https://software.intel.com/content/www/us/en/develop/articles/api-without-secrets-introduction-to-vulkan-part-1.html
                    auto features = pdev.getFeatures();
#ifdef ENABLE_GEOMETRY_SHADER
                    if (!features.geometryShader)
                        continue;
#endif
#ifdef ENABLE_ANISOTROPY
                    if (!features.samplerAnisotropy)
                        continue;
#endif

                    auto properties = pdev.getProperties();
                    if (properties.deviceType != vk::PhysicalDeviceType::eDiscreteGpu &&
                        properties.deviceType != vk::PhysicalDeviceType::eIntegratedGpu)
                        continue;

                    if (pdev.getSurfacePresentModesKHR(m_Surface).empty() ||
                        pdev.getSurfaceFormatsKHR(m_Surface).empty())
                        continue;

#ifdef PRINT_INFORMATION
                    graphicsFamilyIndex = graphicsQueueFamilyIndex.value();
                    presentationFamilyIndex = presentationQueueFamilyIndex.value();
                    std::cout << "graphicsFamilyIndex = " << graphicsFamilyIndex << std::endl;
                    std::cout << "presentationFamilyIndex = " << presentationFamilyIndex << std::endl;
                    vulkan::PrintQueueFamilies(pdev);
                    vulkan::PrintMemoryInfo(pdev);
#endif

                    return pdev;
                }

                throw VkException(FUNCTION_INFO);
            }

            // A logical device is the application's interface with the physical device.
            // It encapsulates command queues used to send commands to physical device.
            // A logical device represents a physical device and all the features and
            // extensions we enabled for it.
            vk::Device CreateLogicalDevice()
            {
                // Queues are created automatically along with the device.
                // Each element in the following vector describes a different queue family
                // (we cannot create additional queues or use queues from families we didn�t request).
                std::vector<vk::DeviceQueueCreateInfo> queuesCreateInfo;
                // Create graphics queue
                {
                    // if several queues are running in parallel Vulkan uses this
                    // information to prioritize queues (lowest=0.0 highest=1.0)
                    float queuePriorities = 1.0f;
                    vk::DeviceQueueCreateInfo queueCreateInfo
                    {
                        .queueFamilyIndex = m_GraphicsQueueFamilyIndex, // the index to the family to create the queue from
                        .queueCount = 1,                                // number of queues to create
                        .pQueuePriorities = &queuePriorities
                    };
                    queuesCreateInfo.push_back(queueCreateInfo);
                }
                // Create presentation queue (taking into account that it may appear to be the same as the graphics queue)
                if (m_GraphicsQueueFamilyIndex != m_PresentationQueueFamilyIndex)
                {
                    // if several queues are running in parallel Vulkan uses this
                    // information to prioritize queues (lowest=0.0 highest=1.0)
                    float queuePriorities = 1.0f;
                    vk::DeviceQueueCreateInfo queueCreateInfo
                    {
                        .queueFamilyIndex = m_PresentationQueueFamilyIndex, // the index to the family to create the queue from
                        .queueCount = 1,                                    // number of queues to create
                        .pQueuePriorities = &queuePriorities
                    };
                    queuesCreateInfo.push_back(queueCreateInfo);
                }

                // These are the features that we want to enable and that we checked that they
                // are supported by our physical device (see a call to vk::PhysicalDevice::getFeatures() in PickAppropriatePhysicalDevice)
                vk::PhysicalDeviceFeatures deviceFeatures{};
#ifdef ENABLE_GEOMETRY_SHADER
                deviceFeatures.setGeometryShader(VK_TRUE);
#endif
#ifdef ENABLE_ANISOTROPY
                deviceFeatures.setSamplerAnisotropy(VK_TRUE);
#endif

                // logical device creation info
                vk::DeviceCreateInfo devCreateInfo
                {
                    .queueCreateInfoCount = queuesCreateInfo.size(),
                    .pQueueCreateInfos = queuesCreateInfo.data(),
                    //.enabledLayerCount = ...,   // ignored by up-to-date implementations
                    //.ppEnabledLayerNames = ..., // ignored by up-to-date implementations
                    .enabledExtensionCount = DEVICE_EXTENSIONS.size(),
                    .ppEnabledExtensionNames = DEVICE_EXTENSIONS.data(),
                    .pEnabledFeatures = &deviceFeatures // physical device features logical device will use
                };

                auto device = m_PhysicalDevice.createDevice(devCreateInfo);

                // Loads device-level function's entry points.
                // A good thing to do if you use just one logical device.
                // When we acquire device-level procedures using this function we in fact acquire
                // addresses of a simple "jump" functions. These functions take the handle of a
                // logical device and jump to a proper implementation (function implemented for a
                // specific device). The overhead of this jump can be avoided. The recommended
                // behavior is to load procedures for each device separately using another function
                // (vkGetDeviceProcAddr). See https://software.intel.com/content/www/us/en/develop/articles/api-without-secrets-introduction-to-vulkan-part-1.html
                VULKAN_HPP_DEFAULT_DISPATCHER.init(device);

                return device;
            }

        #pragma endregion
    };
}