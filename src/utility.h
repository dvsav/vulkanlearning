/*
Copyright (c) 2020 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

#include <iostream>
#include <limits>
#include <type_traits>

namespace util
{
    // util::value_type<float>::t = float
    // util::value_type< std::vector<float> >::t = float
    template <typename T, typename = void>
    struct value_type
    {
        using t = T;
    };
    template <typename T>
    struct value_type< T, std::void_t<decltype(sizeof(typename T::value_type))> >
    {
        using t = typename T::value_type;
    };

    // Extracts and discards all symbols from standard input stream until encounters newline symbol.
    // Extracts and discards the newline symbol and after that returns.
    inline void skip_to_newline()
    {
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    }

    /*
    Returns:
    if value > max return max
    if value < min return min
    if min <= value <= max return value
    */
    template<typename T>
    inline T constraint(T value, T min, T max)
    {
        if (value > max)
            return max;
        else if (value < min)
            return min;
        else
            return value;
    }

    template<typename T, typename...> struct is_any_of : std::false_type {};

    template<typename T, typename Head, typename... Tail>
    struct is_any_of<T, Head, Tail...> : std::conditional_t<
        std::is_same<T, Head>::value,
        std::true_type,
        is_any_of<T, Tail...> >
    {};

    #define M_PI 3.14159265358979323846f
    template<typename T>
    inline constexpr T to_radians(T degrees) { return degrees * M_PI / 180.0f; }
}
