/*
Copyright (c) 2020 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

// STL
#include <cstring>
#include <vector>
#include <array>
#include <iostream>
#include <algorithm>
#include <limits>
#include <optional>
#include <string>

// Utility
#include "File.h"
#include "utility.h"
#include "VkUtility.h"

// Engine
#include "ProgramBase.h"
#include "Vertex.h"
#include "WorldPositionAndOrientation.h"
#include "Model.h"
#include "Camera.h"

// VkEngine
#include "VkGraphicsContext.h"
#include "VulkanSwapchain.h"
#include "VkGraphicsPipeline.h"
#include "VkRenderLoop.h"
#include "VkModelRenderer.h"
#include "VkContext.h"
#include "VkMesh.h"
#include "UniformBuffer.h"
#include "DynamicUniformBuffer.h"
#include "VkTexture.h"
#include "VkImport.h"
#include "VkModel.h"

using namespace vulkan;
using namespace std::placeholders;

// References
// https://vulkan-tutorial.com
// https://vulkan.lunarg.com/doc/view/1.2.170.0/linux/tutorial/html/index.html
// https://software.intel.com/content/www/us/en/develop/articles/api-without-secrets-introduction-to-vulkan-preface.html
// https://gpuopen.com/learn/understanding-vulkan-objects/
// https://vkguide.dev
// https://zeux.io/2020/02/27/writing-an-efficient-vulkan-renderer/
// https://developer.samsung.com/galaxy-gamedev/resources.html

namespace engine
{
    class Program : public ProgramBase
    {
        #pragma region Typedefs

        private:
            using vertex_type = VertexPT;
            using index_type = uint16_t;
            using mesh_type = VkMesh<vertex_type, index_type>;

        #pragma endregion

        #pragma region Fields

        private:
            VkGraphicsContext m_VkGraphicsContext;
            volatile bool m_WindowIsResized;
            VulkanSwapchain m_VulkanSwapchain;
            VkRenderLoop m_VkRenderLoop;
            GraphicsPipeline m_GraphicsPipeline;
            VkModelRenderer m_VkModelRenderer;

            VkContext m_VkContext;
            VkModel<vertex_type, index_type> m_Model1;
            VkModel<vertex_type, index_type> m_Model2;
            VkModel<vertex_type, index_type> m_Model3;

        #pragma endregion

        #pragma region Constructors

        public:
            Program() :
                m_VkGraphicsContext(getWindow().getWindowHandle()),

                m_WindowIsResized(false),

                m_VulkanSwapchain(
                    m_VkGraphicsContext,
                    getWindow().getClientWidth(),
                    getWindow().getClientHeight()),

                m_VkRenderLoop(
                    m_VkGraphicsContext,
                    m_VulkanSwapchain),

                m_GraphicsPipeline(GraphicsPipeline::Create<vertex_type>(
                    /*logicalDevice*/ m_VkGraphicsContext.LogicalDevice(),
                    /*renderPass*/ m_VkRenderLoop.Swapchain().RenderPass(),
                    /*swapchainExtent*/ m_VkRenderLoop.Swapchain().SwapChainExtent(),
                    /*vertexShaderFile*/ "assets/shaders/textures/vert.spv",
                    /*fragmentShaderFile*/ "assets/shaders/textures/frag.spv")),

                m_VkModelRenderer(
                    m_VkGraphicsContext,
                    m_GraphicsPipeline),

                m_VkContext
                {
                    .PhysicalDevice = m_VkGraphicsContext.PhysicalDevice(),
                    .LogicalDevice = m_VkGraphicsContext.LogicalDevice(),
                    .GraphicsCommandPool = m_VkRenderLoop.GraphicsCommandPool(),
                    .GraphicsQueue = m_VkGraphicsContext.GraphicsQueue()
                },

                m_Model1(std::make_shared<mesh_type>(
                    /*vkContext*/ m_VkContext,
                    /*primitiveTopology*/ PrimitiveTopologyEnum::TriangleList,
                    /*vertices*/ std::vector<vertex_type>
                    {
                        vertex_type(vec3{  0.0f, -0.4f, 1.0f }, vec2{ 1.0f, 0.0f }),
                        vertex_type(vec3{  0.4f,  0.4f, 1.0f }, vec2{ 1.0f, 1.0f }),
                        vertex_type(vec3{ -0.4f,  0.4f, 1.0f }, vec2{ 0.0f, 1.0f }),
                        vertex_type(vec3{ -0.8f, -0.4f, 1.0f }, vec2{ 0.0f, 0.0f })
                    },
                    /*indices*/ std::vector<index_type>
                    {
                        0, 1, 2, // 1st triangle
                        0, 2, 3  // 2nd triangle
                    }),
                    /*material*/ Material
                    {
                        .DiffuseColor = std::make_shared<Texture>(
                            /*vkContext*/ m_VkContext,
                            /*image*/ util::Image("assets/textures/giraffe.jpg"))
                    }),

                m_Model2(std::make_shared<mesh_type>(
                    /*vkContext*/ m_VkContext,
                    /*primitiveTopology*/ PrimitiveTopologyEnum::TriangleList,
                    /*vertices*/ std::vector<vertex_type>
                    {
                        vertex_type(vec3{  0.0f, -0.4f, 0.0f }, vec2{ 1.0f, 0.0f }),
                        vertex_type(vec3{  0.4f,  0.4f, 0.0f }, vec2{ 1.0f, 1.0f }),
                        vertex_type(vec3{ -0.4f,  0.4f, 0.0f }, vec2{ 0.0f, 1.0f }),
                        vertex_type(vec3{ -0.8f, -0.4f, 0.0f }, vec2{ 0.0f, 0.0f })
                    },
                    /*indices*/ std::vector<index_type>
                    {
                        0, 1, 2, // 1st triangle
                        0, 2, 3  // 2nd triangle
                    }),
                    /*material*/ m_Model1.material()),

                m_Model3(
                    /*mesh*/ std::make_shared<mesh_type>(
                        vkImportMesh<vertex_type, index_type>(
                            m_VkContext,
                            "assets/private/Jug/tl2qbdffa_LOD0.fbx",
                            engine::vec3(1.0f))),
                    /*material*/ Material
                    {
                        .DiffuseColor = std::make_shared<Texture>(
                            m_VkContext,
                            util::Image("assets/private/Jug/tl2qbdffa_2K_Albedo.jpg"))
                    })
            {
                m_VkModelRenderer.AddModel(m_Model1);
                m_VkModelRenderer.AddModel(m_Model2);
                m_VkModelRenderer.AddModel(m_Model3);

                m_Model2.Wpo().MoveRightLeft(1.0f);
                getCamera().PositionAndOrientation().MoveForwardBackwardWorld(-5.0f);
            }

        #pragma endregion

        #pragma region Destructor

        public:
            ~Program()
            {
                // Wait on the host for the completion of outstanding queue 
                // operations for all queues on a given logical device.
                m_VkGraphicsContext.LogicalDevice().waitIdle();

                m_Model1.Destroy();
                m_Model2.Destroy();
                m_Model3.Destroy();

                m_VkModelRenderer.Destroy();
                m_GraphicsPipeline.Destroy();
                m_VkRenderLoop.Destroy();
                m_VulkanSwapchain.Destroy();
                m_VkGraphicsContext.Destroy();
            }

        #pragma endregion

        #pragma region Methods

        private:
            void RecreateSwapChain()
            {
                // Wait on the host for the completion of outstanding queue 
                // operations for all queues on a given logical device because
                // we shouldn't touch resources that may still be in use.
                m_VkGraphicsContext.LogicalDevice().waitIdle();

                // Cleanup swapchain and all the stuff that depends on it.
                // The uniform descriptor pool should be destroyed when the swap chain is recreated because it depends on the number of images.
                //m_VkGraphicsContext.LogicalDevice().destroyDescriptorPool(m_UniformDescriptorPool);

#ifndef USE_DYNAMIC_STATES
                // Viewport and scissor rectangle size is specified during graphics pipeline creation, so the pipeline also needs to be rebuilt.
                // It is possible to avoid this by using dynamic state for the viewports and scissor rectangles.
                m_GraphicsPipeline.Destroy();
#endif

                // Obviously, the first thing we'll have to do is recreate the swap chain itself.
                m_VulkanSwapchain.Destroy();
                m_VulkanSwapchain = VulkanSwapchain(
                    m_VkGraphicsContext,
                    getWindow().getClientWidth(),
                    getWindow().getClientHeight());

                m_VkRenderLoop.RecreateSwapChain(m_VulkanSwapchain);

#ifndef USE_DYNAMIC_STATES
                // Viewport and scissor rectangle size is specified during graphics pipeline creation, so the pipeline also needs to be rebuilt.
                // It is possible to avoid this by using dynamic state for the viewports and scissor rectangles.
                m_GraphicsPipeline = GraphicsPipeline::Create<vertex_type>(
                    /*logicalDevice*/ m_VkGraphicsContext.LogicalDevice(),
                    /*renderPass*/ m_VkRenderLoop.Swapchain().RenderPass(),
                    /*swapchainExtent*/ m_VkRenderLoop.Swapchain().SwapChainExtent(),
                    /*vertexShaderFile*/ "assets/shaders/textures/vert.spv",
                    /*fragmentShaderFile*/ "assets/shaders/textures/frag.spv");
#endif

                // The uniform descriptor pool should be recreated when the swap chain is recreated because it depends on the number of images.
                //m_UniformDescriptorPool = CreateUniformDescriptorPool();

                // Because we've recreated uniform descriptor pool, we need to recreate descriptor sets.
                //m_UniformDescriptorSets = CreateUniformDescriptorSets();
            }

            void OnSizeChangedOverride(
                int width, int height) override
            {
                m_WindowIsResized = true;
            }

        private:
            // Record commands to the command buffers.
            // The commands bind resources (pipeline, uniforms, textures) and do drawing.
            // The usual practice is to prerecord a set of commands to the command buffer once
            // and then in the rendering loop submit them to the command queue on each iteration.
            const vk::CommandBuffer& RecordCommands(
                uint32_t imageIndex)
            {
                const vk::CommandBuffer& commandBuffer = m_VkRenderLoop.GraphicsCommandBuffers()[imageIndex];
                // Start recording commands to command buffer
                commandBuffer.begin(vk::CommandBufferBeginInfo
                {
                    // Buffer can be resubmitted when it has already been submitted and is awaiting execution in a queue
                    //.flags = vk::CommandBufferUsageFlagBits::eSimultaneousUse
                });
                m_VkModelRenderer.RecordCommands(
                    /*imageIndex*/ imageIndex,
                    /*commandBuffer*/ commandBuffer,
                    /*framebuffer*/ m_VkRenderLoop.Swapchain().Framebuffers()[imageIndex],
                    /*renderPass*/ m_VkRenderLoop.Swapchain().RenderPass(),
                    /*swapchainExtent*/ m_VkRenderLoop.Swapchain().SwapChainExtent(),
                    /*worldToScreen*/ getCamera().getWorldToScreenMatrix());
                // Stop recording to command buffer
                commandBuffer.end();
                return commandBuffer;
            }

        protected:
            void UpdateTime() override
            {
                const double SPEED = 0.1;
                m_Model1.Wpo().RollWorld(static_cast<float>(SPEED * TimeSinceLastFrame()));
                m_Model2.Wpo().RollWorld(static_cast<float>(-SPEED * TimeSinceLastFrame()));
            }

            void setupCamera(const Camera& cam) override {}

            void OnRender() override
            {
                static auto recordCommands = std::bind(&Program::RecordCommands, this, _1);

                if (m_WindowIsResized || m_VkRenderLoop.Render(recordCommands))
                {
                    RecreateSwapChain();
                    m_WindowIsResized = false;
                }
            }

        #pragma endregion
    };
}