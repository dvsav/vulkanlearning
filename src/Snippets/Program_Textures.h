/*
Copyright (c) 2020 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

// STL
#include <cstring>
#include <vector>
#include <array>
#include <iostream>
#include <algorithm>
#include <limits>
#include <optional>

// Utility
#include "File.h"
#include "utility.h"
#include "VkUtility.h"

// Engine
#include "ProgramBase.h"
#include "Vertex.h"
#include "WorldPositionAndOrientation.h"
#include "Model.h"
#include "Camera.h"

// VkEngine
#include "VulkanInitializer.h"
#include "VkTypeTraits.h"
#include "VkContext.h"
#include "VkMesh.h"
#include "UniformBuffer.h"
#include "DynamicUniformBuffer.h"
#include "BufferAttachment.h"
#include "VkTexture.h"

using namespace vulkan;

// References
// https://vulkan-tutorial.com
// https://vulkan.lunarg.com/doc/view/1.2.170.0/linux/tutorial/html/index.html
// https://software.intel.com/content/www/us/en/develop/articles/api-without-secrets-introduction-to-vulkan-preface.html
// https://gpuopen.com/learn/understanding-vulkan-objects/
// https://vkguide.dev
// https://zeux.io/2020/02/27/writing-an-efficient-vulkan-renderer/
// https://developer.samsung.com/galaxy-gamedev/resources.html

namespace engine
{
    class Program : public ProgramBase
    {
        #pragma region Typedefs

        private:
            using vertex_type = VertexPT;
            using index_type = uint16_t;
            using mesh_type = VkMesh<vertex_type, index_type>;

        #pragma endregion

        #pragma region Nested Types

        public:
            struct SwapchainImage
            {
                vk::Image Image;
                vk::ImageView ImageView;
            };

        #pragma endregion

        #pragma region Fields

        private:
            // how many textures we can load in our program
            static constexpr size_t MAX_TEXTURES = 10;

            // how many images we can have submitted at once
            static constexpr size_t MAX_FRAME_DRAWS = 2;
            // the extensions that the physical device should support
            static std::vector<const char*> DEVICE_EXTENSIONS;

            VulkanInitializer VULKAN_INITIALIZER;

        private:
            // The Vulkan API uses the vkInstance object to store all per-application state.
            // The application must create a Vulkan instance before performing any other Vulkan operations.
            // https://vulkan.lunarg.com/doc/view/1.2.170.0/linux/tutorial/html/01-init_instance.html
            vk::Instance m_VkInstance;
#ifdef ENABLE_DEBUG_LAYERS
            vk::DebugUtilsMessengerEXT m_DebugMessenger;
#endif
            vk::SurfaceKHR m_Surface;  // abstract interface to the platfrom-specific window subsystem
            uint32_t m_GraphicsQueueFamilyIndex;
            uint32_t m_PresentationQueueFamilyIndex;
            vk::PhysicalDevice m_PhysicalDevice; // the GPU
            vk::Device m_LogicalDevice; // abstract device used as an interface to the physical device

            // When we want to process any data (that is, draw a 3D scene from vertex data and vertex attributes)
            // we call Vulkan functions that are passed to the driver. These functions are not passed directly,
            // as sending each request separately down through a communication bus is inefficient. It is better
            // to aggregate them and pass in groups. In OpenGL this was done automatically by the driver and was
            // hidden from the user. OpenGL API calls were queued in a buffer and if this buffer was full (or if
            // we requested to flush it) whole buffer was passed to hardware for processing. In Vulkan this
            // mechanism is directly visible to the user and, more importantly, the user must specifically create
            // and manage buffers for commands. These are called (conveniently) command buffers.
            // Command buffers (as whole objects) are passed to the hardware for execution through queues. However,
            // these buffers may contain different types of operations, such as graphics commands (used for generating
            // and displaying images like in typical 3D games) or compute commands (used for processing data).
            // Specific types of commands may be processed by dedicated hardware, and that�s why queues are also
            // divided into different types. In Vulkan these queue types are called families.
            // https://software.intel.com/content/www/us/en/develop/articles/api-without-secrets-introduction-to-vulkan-part-1.html
            vk::Queue m_GraphicsQueue;  // the queue of graphics commands that we are going to send to the logical device
            vk::Queue m_PresentationQueue;

            volatile bool m_WindowIsResized;

            // If we want to display something we can create a set of buffers to which we can render. These buffers
            // along with their properties are called a swap chain. A swap chain can contain  many images. To display
            // any of them we don't "swap" them � as the name suggests � but we present them, which means that we
            // give them back to a presentation engine. (Remark: There will be situations where we will have to destroy this
            // swap chain and recreate it. In the middle of a working application).
            // https://software.intel.com/content/www/us/en/develop/articles/api-without-secrets-introduction-to-vulkan-part-2.html
            vk::Format m_SwapChainImageFormat;
            vk::Extent2D m_SwapChainExtent;
            vk::SwapchainKHR m_Swapchain;
            std::vector<SwapchainImage> m_SwapchainImages;

            VkImage2d m_DepthBuffer;

            vk::RenderPass m_RenderPass;
            vk::DescriptorSetLayout m_SamplerDescriptorSetLayout;
            vk::PushConstantRange m_PushConstantRange;
            vk::PipelineLayout m_PipelineLayout;
            vk::Pipeline m_GraphicsPipeline;

            int m_CurrentFrame;
            std::vector<vk::Framebuffer> m_Framebuffers; // this vector will be the same size as m_SwapchainImages
            vk::CommandPool m_GraphicsCommandPool;
            std::vector<vk::CommandBuffer> m_GraphicsCommandBuffers;

            std::array<vk::Semaphore, MAX_FRAME_DRAWS> m_SemaphoreImageAvailable;
            std::array<vk::Semaphore, MAX_FRAME_DRAWS> m_SemaphoreRenderFinished;
            std::array<vk::Fence, MAX_FRAME_DRAWS> m_DrawFences;

            VkContext m_VkContext;
            Model<mesh_type> m_Model1;
            Model<mesh_type> m_Model2;
            std::vector< const Model<mesh_type>* > m_Models;
            Texture m_Texture;

            vk::Sampler m_TextureSampler;
            vk::DescriptorPool m_SamplerDescriptorPool;
            std::vector<vk::DescriptorSet> m_TextureSamplerDescriptorSets;

        #pragma endregion

        #pragma region Constructors

        public:
            Program() :
                VULKAN_INITIALIZER(),
                m_VkInstance(CreateInstance()),
#ifdef ENABLE_DEBUG_LAYERS
                m_DebugMessenger(m_VkInstance.createDebugUtilsMessengerEXT(DebugMessengerCreateInfo())),
#endif
                m_Surface(m_VkInstance.createWin32SurfaceKHR(
                    vk::Win32SurfaceCreateInfoKHR
                    {
                        .hinstance = ::GetModuleHandle(nullptr),
                        .hwnd = getWindow().getWindowHandle()
                    })),

                m_PhysicalDevice(PickAppropriatePhysicalDevice(m_GraphicsQueueFamilyIndex, m_PresentationQueueFamilyIndex)),
                m_LogicalDevice(CreateLogicalDevice()),
                m_GraphicsQueue(m_LogicalDevice.getQueue(m_GraphicsQueueFamilyIndex, 0)),         // retrieve queue from logical device (queue index from within a given family must be smaller than the total number of queues we've requested from the given family)
                m_PresentationQueue(m_LogicalDevice.getQueue(m_PresentationQueueFamilyIndex, 0)), // retrieve queue from logical device (queue index from within a given family must be smaller than the total number of queues we've requested from the given family)

                m_WindowIsResized(false),

                m_SwapChainImageFormat(),
                m_SwapChainExtent(),
                m_Swapchain(CreateSwapchain(getWindow().getClientWidth(), getWindow().getClientHeight())),
                m_SwapchainImages(CreateImageViews()),

                m_DepthBuffer(CreateDepthBuffer(m_PhysicalDevice, m_LogicalDevice, m_SwapChainExtent)),
                
                m_RenderPass(CreateRenderPass()),
                m_SamplerDescriptorSetLayout(CreateSamplerDescriptorSetLayout()),
                m_PushConstantRange(CreatePushConstantRange()),
                m_PipelineLayout(CreatePipelineLayout()),
                m_GraphicsPipeline(CreateGraphicsPipeline()),

                m_CurrentFrame(0),
                m_Framebuffers(CreateFramebuffers()),

                m_GraphicsCommandPool(CreateGraphicsCommandPool()),
                m_GraphicsCommandBuffers(CreateGraphicsCommandBuffers()),
                m_SemaphoreImageAvailable
                {
                    m_LogicalDevice.createSemaphore(vk::SemaphoreCreateInfo()),
                    m_LogicalDevice.createSemaphore(vk::SemaphoreCreateInfo())
                },
                m_SemaphoreRenderFinished
                {
                    m_LogicalDevice.createSemaphore(vk::SemaphoreCreateInfo()),
                    m_LogicalDevice.createSemaphore(vk::SemaphoreCreateInfo())
                },
                m_DrawFences
                {
                    m_LogicalDevice.createFence(vk::FenceCreateInfo { .flags = vk::FenceCreateFlagBits::eSignaled }),
                    m_LogicalDevice.createFence(vk::FenceCreateInfo { .flags = vk::FenceCreateFlagBits::eSignaled })
                },

                m_VkContext
                {
                    .PhysicalDevice = m_PhysicalDevice,
                    .LogicalDevice = m_LogicalDevice,
                    .GraphicsCommandPool = m_GraphicsCommandPool,
                    .GraphicsQueue = m_GraphicsQueue
                },
                m_Model1(std::make_shared<mesh_type>(
                    /*vkContext*/ m_VkContext,
                    /*primitiveTopology*/ PrimitiveTopologyEnum::TriangleList,
                    /*vertices*/ std::vector<vertex_type>
                    {
                        vertex_type(vec3{  0.0f, -0.4f, 1.0f }, vec2{ 1.0f, 0.0f }),
                        vertex_type(vec3{  0.4f,  0.4f, 1.0f }, vec2{ 1.0f, 1.0f }),
                        vertex_type(vec3{ -0.4f,  0.4f, 1.0f }, vec2{ 0.0f, 1.0f }),
                        vertex_type(vec3{ -0.8f, -0.4f, 1.0f }, vec2{ 0.0f, 0.0f })
                    },
                    /*indices*/ std::vector<index_type>
                    {
                        0, 1, 2, // 1st triangle
                        0, 2, 3  // 2nd triangle
                    })),

                m_Model2(std::make_shared<mesh_type>(
                    /*vkContext*/ m_VkContext,
                    /*primitiveTopology*/ PrimitiveTopologyEnum::TriangleList,
                    /*vertices*/ std::vector<vertex_type>
                    {
                        vertex_type(vec3{  0.0f, -0.4f, 0.0f }, vec2{ 1.0f, 0.0f }),
                        vertex_type(vec3{  0.4f,  0.4f, 0.0f }, vec2{ 1.0f, 1.0f }),
                        vertex_type(vec3{ -0.4f,  0.4f, 0.0f }, vec2{ 0.0f, 1.0f }),
                        vertex_type(vec3{ -0.8f, -0.4f, 0.0f }, vec2{ 0.0f, 0.0f })
                    },
                    /*indices*/ std::vector<index_type>
                    {
                        0, 1, 2, // 1st triangle
                        0, 2, 3  // 2nd triangle
                    })),

                m_Models { &m_Model1, &m_Model2 },
                m_Texture(
                    /*vkContext*/ m_VkContext,
                    /*image*/ util::Image("assets/textures/giraffe.jpg")),

                m_TextureSampler(CreateTextureSampler()),
                m_SamplerDescriptorPool(CreateSamplerDescriptorPool()),
                m_TextureSamplerDescriptorSets(CreateTextureSamplerDescriptorSets())
            {
                m_Model2.Wpo().MoveRightLeft(1.0f);
                getCamera().PositionAndOrientation().MoveForwardBackwardWorld(-5.0f);
            }

        #pragma endregion

        #pragma region Destructor

        public:
            ~Program()
            {
                // Wait on the host for the completion of outstanding queue 
                // operations for all queues on a given logical device.
                m_LogicalDevice.waitIdle();

                m_LogicalDevice.destroyDescriptorPool(m_SamplerDescriptorPool);
                m_LogicalDevice.destroySampler(m_TextureSampler);

                m_Texture.Destroy();
                m_Model1.Destroy();
                m_Model2.Destroy();

                for (size_t i = 0; i < MAX_FRAME_DRAWS; i++)
                {
                    m_LogicalDevice.destroyFence(m_DrawFences[i]);
                    m_LogicalDevice.destroySemaphore(m_SemaphoreRenderFinished[i]);
                    m_LogicalDevice.destroySemaphore(m_SemaphoreImageAvailable[i]);
                }

                m_LogicalDevice.destroyCommandPool(m_GraphicsCommandPool);

                for (auto& framebuffer : m_Framebuffers)
                    m_LogicalDevice.destroyFramebuffer(framebuffer);

                m_LogicalDevice.destroyPipeline(m_GraphicsPipeline);
                m_LogicalDevice.destroyPipelineLayout(m_PipelineLayout);
                m_LogicalDevice.destroyDescriptorSetLayout(m_SamplerDescriptorSetLayout);
                m_LogicalDevice.destroyRenderPass(m_RenderPass);

                m_DepthBuffer.Destroy();

                for (auto& image : m_SwapchainImages)
                    m_LogicalDevice.destroyImageView(image.ImageView);
                m_SwapchainImages.clear();

                m_LogicalDevice.destroySwapchainKHR(m_Swapchain);
                m_VkInstance.destroySurfaceKHR(m_Surface);
                m_LogicalDevice.destroy();

#ifdef ENABLE_DEBUG_LAYERS
                m_VkInstance.destroyDebugUtilsMessengerEXT(m_DebugMessenger);
#endif

                m_VkInstance.destroy();
            }

        #pragma endregion

        #pragma region Methods

        private:
            void CleanupSwapChain()
            {
                // The descriptor pool should be destroyed when the swap chain is recreated because it depends on the number of images.
                m_LogicalDevice.destroyDescriptorPool(m_SamplerDescriptorPool);

                // Command buffers directly depend on the swap chain images.
                m_LogicalDevice.freeCommandBuffers(
                    m_GraphicsCommandPool, m_GraphicsCommandBuffers);

                // Framebuffers directly depend on the swap chain images.
                for (auto& framebuffer : m_Framebuffers)
                    m_LogicalDevice.destroyFramebuffer(framebuffer);

                // Viewport and scissor rectangle size is specified during graphics pipeline creation, so the pipeline also needs to be rebuilt.
                // It is possible to avoid this by using dynamic state for the viewports and scissor rectangles.
                m_LogicalDevice.destroyPipeline(m_GraphicsPipeline);

                // The render pass needs to be destroyed because it depends on the format of the swap chain images.
                // It is rare for the swap chain image format to change during an operation like a window resize, but it should still be handled.
                m_LogicalDevice.destroyRenderPass(m_RenderPass);

                // Depth buffer depends on the swap chain extent.
                m_DepthBuffer.Destroy();

                // The image views need to be destroyed because they are based directly on the swap chain images.
                for (auto& image : m_SwapchainImages)
                    m_LogicalDevice.destroyImageView(image.ImageView);
                m_SwapchainImages.clear();

                // Swapchain itself
                m_LogicalDevice.destroySwapchainKHR(m_Swapchain);
            }

            void RecreateSwapChain()
            {
                // Wait on the host for the completion of outstanding queue 
                // operations for all queues on a given logical device because
                // we shouldn't touch resources that may still be in use.
                m_LogicalDevice.waitIdle();

                // Cleanup swapchain and all the stuff that depends on it.
                CleanupSwapChain();

                // Obviously, the first thing we'll have to do is recreate the swap chain itself.
                m_Swapchain = CreateSwapchain(
                    getWindow().getClientWidth(),
                    getWindow().getClientHeight());

                // The image views need to be recreated because they are based directly on the swap chain images.
                m_SwapchainImages = CreateImageViews();

                // Depth buffer depends on the swap chain extent.
                m_DepthBuffer = CreateDepthBuffer(m_PhysicalDevice, m_LogicalDevice, m_SwapChainExtent),

                // The render pass needs to be recreated because it depends on the format of the swap chain images.
                // It is rare for the swap chain image format to change during an operation like a window resize, but it should still be handled.
                m_RenderPass = CreateRenderPass();

                // Viewport and scissor rectangle size is specified during graphics pipeline creation, so the pipeline also needs to be rebuilt.
                // It is possible to avoid this by using dynamic state for the viewports and scissor rectangles.
                m_GraphicsPipeline = CreateGraphicsPipeline();

                // Framebuffers directly depend on the swap chain images.
                m_Framebuffers = CreateFramebuffers();

                // Command buffers directly depend on the swap chain images.
                m_GraphicsCommandBuffers = CreateGraphicsCommandBuffers();

                // The descriptor pool should be recreated when the swap chain is recreated because it depends on the number of images.
                m_SamplerDescriptorPool = CreateSamplerDescriptorPool();

                // Because we've recreated descriptor pool, we need to recreate descriptor sets.
                m_TextureSamplerDescriptorSets = CreateTextureSamplerDescriptorSets();
            }

            void OnSizeChangedOverride(int width, int height) override
            {
                m_WindowIsResized = true;
            }

        private:
            // Creates a Vulkan instance.
            // Specifies enabled layers and extensions
            // as well as the debug message callback function.
            vk::Instance CreateInstance()
            {
                // Information about the aplication.
                // Most of data here is just for the developer's information.
                vk::ApplicationInfo appInfo
                {
                    .pApplicationName = "VulkanLearning",           // custom name of the application
                    .applicationVersion = VK_MAKE_VERSION(1, 0, 0), // custom version of the application
                    .pEngineName = "OpenRenderingEngine",           // custom engine name
                    .engineVersion = VK_MAKE_VERSION(1, 0, 0),      // custom engine version
                    .apiVersion = VK_API_VERSION_1_2                // minimal required Vulkan version
                };

                // A list of Vulkan extensions to be used.
                // Vulkan extensions are simply additional features that Vulkan implementations may provide
                // if they so choose to. They add new functions, structs, and valid enumerators to the API,
                // and they can change some of the behavior of existing functions. There are instance level
                // extensions and device level extensions. They must be enabled separately.
                // You have to detect whether the implementation supports these extensions before creating
                // an instance / device. And you have to explicitly ask for such extensions when you create
                // the instance / device (in OpenGL all extensions are available in created context while
                // in Vulkan they are not available unless you enabled them explicitly).
                // Source: https://computergraphics.stackexchange.com/questions/8170/what-is-a-vulkan-extension
                std::vector<const char*> extensions
                {
                    #ifdef ENABLE_DEBUG_LAYERS
                        VK_EXT_DEBUG_UTILS_EXTENSION_NAME,
                    #endif

                    // A WSI (Window System Integration) extension needed to be able to draw graphics on the screen.
                    // It describes a �surface� object, which is a logical representation of an application's window.
                    // This extension allows us to check different parameters (that is,  capabilities, supported formats,
                    // size) of a surface and to query whether the given physical device supports a swap chain (more
                    // precisely, whether the given queue family supports presenting an image to a given surface).
                    // https://software.intel.com/content/www/us/en/develop/articles/api-without-secrets-introduction-to-vulkan-part-2.html
                    VK_KHR_SURFACE_EXTENSION_NAME,

                    // This is a platform-specific extension required for the interoperation with MS Windows window subsystem
                    // see https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VK_KHR_win32_surface.html
                    // Although the VkSurfaceKHR object and its usage is platform agnostic, its creation isn't because it depends
                    // on window system details. For example, it needs the HWND and HMODULE handles on Windows. Therefore there
                    // is a platform-specific addition to the extension, which on Windows is called VK_KHR_win32_surface
                    // see https://vulkan-tutorial.com/en/Drawing_a_triangle/Presentation/Window_surface
                    // This extension allows us to create a surface that represents the application's window in a given OS (Windows).
                    VK_KHR_WIN32_SURFACE_EXTENSION_NAME
                };

                // Check that the required extensions are supported
                for (auto ext : extensions)
                {
                    if (!IsInstanceExtensionSupported(ext))
                        throw VkException(FUNCTION_INFO);
                }

                // Vulkan supports intercepting or hooking API entry points via a layer framework.
                // A layer can intercept all or any subset of Vulkan API entry points. Multiple
                // layers can be chained together to cascade their functionality in the appearance
                // of a single, larger layer.
                // Source: https://vulkan.lunarg.com/doc/view/1.2.131.1/windows/layer_configuration.html
                std::vector<const char*> layers
                {
                    #ifdef ENABLE_DEBUG_LAYERS
                        "VK_LAYER_KHRONOS_validation"
                    #endif
                };

                // Check that the required layers are supported
                for (auto layer : layers)
                {
                    if (!IsLayerSupported(layer))
                        throw VkException(FUNCTION_INFO);
                }

                #ifdef ENABLE_DEBUG_LAYERS
                auto dbgMesgrCreateInfo = DebugMessengerCreateInfo();
                #endif

                // In Vulkan every XXX that can be created requires an XXXCreateInfo
                vk::InstanceCreateInfo instInfo
                {
                    #ifdef ENABLE_DEBUG_LAYERS
                    .pNext = &dbgMesgrCreateInfo,
                    #endif

                    .pApplicationInfo = &appInfo,

                    .enabledLayerCount = layers.size(),
                    .ppEnabledLayerNames = layers.data(),

                    .enabledExtensionCount = extensions.size(),
                    .ppEnabledExtensionNames = extensions.data()
                };

                auto instance = vk::createInstance(instInfo);

                // Load instance-level functions' entry points
                VULKAN_HPP_DEFAULT_DISPATCHER.init(instance);

                return instance;
            }

            static VKAPI_ATTR VkBool32 VKAPI_CALL DebugCallback(
                VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
                VkDebugUtilsMessageTypeFlagsEXT messageType,
                const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
                void* pUserData)
            {
                // VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT - Diagnostic message
                // VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT    - Informational message like the creation of a resource
                // VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT - Message about behavior that is not necessarily an error, but very likely a bug in your application
                // VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT   - Message about behavior that is invalid and may cause crashes
                // Source: https://vulkan-tutorial.com/en/Drawing_a_triangle/Setup/Validation_layers
                // if the message is severe enough...
                if (messageSeverity >= VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT)
                    std::cerr << pCallbackData->pMessage << std::endl << std::endl;

                return VK_FALSE;
            }

            vk::DebugUtilsMessengerCreateInfoEXT DebugMessengerCreateInfo()
            {
                return vk::DebugUtilsMessengerCreateInfoEXT
                {
                    .messageSeverity = vk::DebugUtilsMessageSeverityFlagBitsEXT::eVerbose | vk::DebugUtilsMessageSeverityFlagBitsEXT::eWarning | vk::DebugUtilsMessageSeverityFlagBitsEXT::eError,
                    .messageType = vk::DebugUtilsMessageTypeFlagBitsEXT::eGeneral | vk::DebugUtilsMessageTypeFlagBitsEXT::eValidation | vk::DebugUtilsMessageTypeFlagBitsEXT::ePerformance,
                    .pfnUserCallback = DebugCallback,
                    .pUserData = this
                };
            }

            // Looks for a queue that supports graphics commands.
            // In case of success returns the queue family index which the graphics queue belongs to.
            static std::optional<uint32_t> getGraphicsQueueFamilyIndex(
                const vk::PhysicalDevice& pdev)
            {
                uint32_t i = 0;
                for (const auto& queueFamily : pdev.getQueueFamilyProperties())
                {
                    // Queue family is something that contains several queues of commands running in parallel
                    if (queueFamily.queueCount > 0
                        && queueFamily.queueFlags & vk::QueueFlagBits::eGraphics) // The queue must support graphics operations (like drawing things into a framebuffer).
                                                                                  // A queue that supports graphics operations also supports transfer operations by default.
                    {
                        return i;
                    }
                    i++;
                }
                return std::optional<uint32_t>{};
            }

            // Looks for a queue that supports presentation commands.
            // In case of success returns the queue family index which the graphics queue belongs to.
            // Remark: Not every physical device present in the system supports the window system integration.
            // Only those devices can draw graphics to a window that support a queue (so called presentation queue)
            // compatible with the surface passed to this function as a parameter.
            static std::optional<uint32_t> getPresentationQueueFamilyIndex(
                const vk::PhysicalDevice& pdev,
                const vk::SurfaceKHR& surf)
            {
                uint32_t i = 0;
                for (const auto& queueFamily : pdev.getQueueFamilyProperties())
                {
                    if (queueFamily.queueCount > 0
                        && pdev.getSurfaceSupportKHR(i, surf)) // the queue family must support presenting images to the given surface
                    {
                        return i;
                    }
                    i++;
                }
                return std::optional<uint32_t>{};
            }

            // Selects a physical device (GPU) that fits our demands.
            // https://vulkan-tutorial.com/Drawing_a_triangle/Setup/Physical_devices_and_queue_families
            vk::PhysicalDevice PickAppropriatePhysicalDevice(
                uint32_t& graphicsFamilyIndex,
                uint32_t& presentationFamilyIndex)
            {
                vulkan::PrintPhysicalDevices(m_VkInstance);

                for (const auto& pdev : m_VkInstance.enumeratePhysicalDevices())
                {
                    // Check if all of the required device extensions are supported.
                    if (std::any_of(
                            DEVICE_EXTENSIONS.cbegin(),
                            DEVICE_EXTENSIONS.cend(),
                            [&pdev](const char* ext_name) { return !vulkan::IsDeviceExtensionSupported(pdev, ext_name); }))
                    {
                        continue;
                    }

                    auto graphicsQueueFamilyIndex = getGraphicsQueueFamilyIndex(pdev);
                    auto presentationQueueFamilyIndex = getPresentationQueueFamilyIndex(pdev, m_Surface);

                    // Features are additional hardware capabilities that are similar to extensions.
                    // They may not necessarily be supported by the driver and by default are not enabled.
                    // Features contain items such as geometry and tessellation shaders multiple viewports,
                    // logical operations, or additional texture compression formats. If a given physical
                    // device supports any feature we can enable it during logical device creation.
                    // Features are not enabled by default in Vulkan.
                    // https://software.intel.com/content/www/us/en/develop/articles/api-without-secrets-introduction-to-vulkan-part-1.html
                    auto features = pdev.getFeatures();

                    auto properties = pdev.getProperties();

                    if ((properties.deviceType == vk::PhysicalDeviceType::eDiscreteGpu || properties.deviceType == vk::PhysicalDeviceType::eIntegratedGpu)
                        && graphicsQueueFamilyIndex.has_value()
                        && presentationQueueFamilyIndex.has_value()
                        && !pdev.getSurfacePresentModesKHR(m_Surface).empty()
                        && !pdev.getSurfaceFormatsKHR(m_Surface).empty()
#ifdef ENABLE_GEOMETRY_SHADER
                        && features.geometryShader
#endif
#ifdef ENABLE_ANISOTROPY
                        && features.samplerAnisotropy
#endif
                        )
                    {
                        graphicsFamilyIndex = graphicsQueueFamilyIndex.value();
                        presentationFamilyIndex = presentationQueueFamilyIndex.value();
                        std::cout << "graphicsFamilyIndex = " << graphicsFamilyIndex << std::endl;
                        std::cout << "presentationFamilyIndex = " << presentationFamilyIndex << std::endl;
                        vulkan::PrintQueueFamilies(pdev);
                        return pdev;
                    }
                }

                throw VkException(FUNCTION_INFO);
            }

            // A logical device is the application's interface with the physical device.
            // It encapsulates command queues used to send commands to physical device.
            // A logical device represents a physical device and all the features and
            // extensions we enabled for it.
            vk::Device CreateLogicalDevice()
            {
                // Queues are created automatically along with the device.
                // Each element in the following vector describes a different queue family
                // (we cannot create additional queues or use queues from families we didn�t request).
                std::vector<vk::DeviceQueueCreateInfo> queuesCreateInfo;
                // Create graphics queue
                {
                    // if several queues are running in parallel Vulkan uses this
                    // information to prioritize queues (lowest=0.0 highest=1.0)
                    float queuePriorities = 1.0f;
                    vk::DeviceQueueCreateInfo queueCreateInfo
                    {
                        .queueFamilyIndex = m_GraphicsQueueFamilyIndex, // the index to the family to create the queue from
                        .queueCount = 1,                                // number of queues to create
                        .pQueuePriorities = &queuePriorities
                    };
                    queuesCreateInfo.push_back(queueCreateInfo);
                }
                // Create presentation queue (taking into account that it may appear to be the same as the graphics queue)
                if (m_GraphicsQueueFamilyIndex != m_PresentationQueueFamilyIndex)
                {
                    // if several queues are running in parallel Vulkan uses this
                    // information to prioritize queues (lowest=0.0 highest=1.0)
                    float queuePriorities = 1.0f;
                    vk::DeviceQueueCreateInfo queueCreateInfo
                    {
                        .queueFamilyIndex = m_PresentationQueueFamilyIndex, // the index to the family to create the queue from
                        .queueCount = 1,                                    // number of queues to create
                        .pQueuePriorities = &queuePriorities
                    };
                    queuesCreateInfo.push_back(queueCreateInfo);
                }

                // These are the features that we want to enable and that we checked that they
                // are supported by our physical device (see a call to vk::PhysicalDevice::getFeatures() in PickAppropriatePhysicalDevice)
                vk::PhysicalDeviceFeatures deviceFeatures{};
                deviceFeatures.setGeometryShader(VK_TRUE);
                deviceFeatures.setSamplerAnisotropy(VK_TRUE);

                // logical device creation info
                vk::DeviceCreateInfo devCreateInfo
                {
                    .queueCreateInfoCount = queuesCreateInfo.size(),
                    .pQueueCreateInfos = queuesCreateInfo.data(),
                    //.enabledLayerCount = ...,   // ignored by up-to-date implementations
                    //.ppEnabledLayerNames = ..., // ignored by up-to-date implementations
                    .enabledExtensionCount = DEVICE_EXTENSIONS.size(),
                    .ppEnabledExtensionNames = DEVICE_EXTENSIONS.data(),
                    .pEnabledFeatures = &deviceFeatures // physical device features logical device will use
                };

                auto device = m_PhysicalDevice.createDevice(devCreateInfo);

                // Loads device-level function's entry points.
                // A good thing to do if you use just one logical device.
                // When we acquire device-level procedures using this function we in fact acquire
                // addresses of a simple "jump" functions. These functions take the handle of a
                // logical device and jump to a proper implementation (function implemented for a
                // specific device). The overhead of this jump can be avoided. The recommended
                // behavior is to load procedures for each device separately using another function
                // (vkGetDeviceProcAddr). See https://software.intel.com/content/www/us/en/develop/articles/api-without-secrets-introduction-to-vulkan-part-1.html
                VULKAN_HPP_DEFAULT_DISPATCHER.init(device);

                return device;
            }

            // Vulkan does not have the concept of a "default framebuffer", hence it requires
            // an infrastructure that will own the buffers we will render to before we visualize
            // them on the screen. This infrastructure is known as the swap chain and must be
            // created explicitly in Vulkan. The swap chain is essentially a queue of images that
            // are waiting to be presented to the screen... The general purpose of the swap chain
            // is to synchronize the presentation of images with the refresh rate of the screen.
            // Source: https://vulkan-tutorial.com/en/Drawing_a_triangle/Presentation/Swap_chain
            vk::SwapchainKHR CreateSwapchain(
                uint32_t width,
                uint32_t height)
            {
                // Note: Swapchain must be compatible with our window surface.

                // 1. Choose the best surface format
                auto surfaceFormats = m_PhysicalDevice.getSurfaceFormatsKHR(m_Surface);
                if (surfaceFormats.empty())
                    throw VkException(FUNCTION_INFO);
                vk::SurfaceFormatKHR surfaceFormat = surfaceFormats[0];
                if (surfaceFormats.size() == 1 && surfaceFormats[0].format == vk::Format::eUndefined)
                {   // This means that all formats are available
                    surfaceFormat =
                    {
                        // Format specifies the bit layout of a color value in memory
                        // (eR8G8B8A8Unorm: 32-bit unsigned normalized format that has
                        // an 8-bit R component in byte 0, an 8-bit G component in byte 1,
                        // an 8-bit B component in byte 2, and an 8-bit A component in byte 3).
                        // Unsigned normalized format maps [0...255] integer to [0.0 ... 1.0] float.
                        vk::Format::eR8G8B8A8Unorm,

                        // Color space specifies the mapping between the RGB component values
                        // and the human-perceived color (SRGB color space is commonly used in computer displays)
                        vk::ColorSpaceKHR::eSrgbNonlinear
                    };
                }
                else
                {   // Not all formats are available
                    auto searchFmt = std::find_if(
                        surfaceFormats.cbegin(),
                        surfaceFormats.cend(),
                        [](const vk::SurfaceFormatKHR& fmt)
                        {
                            return (fmt.format == vk::Format::eR8G8B8A8Unorm
                                || fmt.format == vk::Format::eB8G8R8A8Unorm)
                                && fmt.colorSpace == vk::ColorSpaceKHR::eSrgbNonlinear;
                        });
                    if (searchFmt != surfaceFormats.cend())
                        surfaceFormat = *searchFmt;
                }

                // 2. Choose the best presentation mode
                // Double buffering was introduced to prevent the visibility of drawing operations: one image
                // was displayed and the second was used to render into. During presentation, the contents
                // of the second image were copied into the first image (earlier) or (later) the images were
                // swapped (remember SwapBuffers() function used in OpenGL applications?) which means that
                // their pointers were exchanged.
                // Tearing was another issue with displaying images, so the ability to wait for the
                // vertical blank signal was introduced if we wanted to avoid it. But waiting introduced
                // another problem: input lag. So double buffering was changed into triple buffering in
                // which we were drawing into two back buffers interchangeably and during v-sync the most
                // recent one was used for presentation.
                // https://software.intel.com/content/www/us/en/develop/articles/api-without-secrets-introduction-to-vulkan-part-2.html
                auto presentModes = m_PhysicalDevice.getSurfacePresentModesKHR(m_Surface);
                if (presentModes.empty())
                    throw VkException(FUNCTION_INFO);
                vk::PresentModeKHR presentMode = vk::PresentModeKHR::eFifo; // FIFO presentation mode (no tearing, possible input lag) is the only one that is guaranteed to be available
                auto searchPmd = std::find(
                    presentModes.cbegin(),
                    presentModes.cend(),
                    vk::PresentModeKHR::eMailbox); // Another good option is MAILBOX: no tearing and input lag is minimized. Makes sense only if there are at least 3 images in the swap chain.
                if (searchPmd != presentModes.cend())
                    presentMode = *searchPmd;

                // Acquired capabilities contain important information about ranges (limits) that
                // are supported by the swap chain, that is, minimal and maximal number of images,
                // minimal and maximal dimensions of images, or supported transforms (some platforms
                // may require transformations applied to images before these images may be presented).
                // https://software.intel.com/content/www/us/en/develop/articles/api-without-secrets-introduction-to-vulkan-part-2.html
                auto surfaceCapabilities = m_PhysicalDevice.getSurfaceCapabilitiesKHR(m_Surface);

                // 3. Choose the best swap chain image resolution
                // If currentExtent.(width|height) == uint32_t::max this means that window
                // subsystem allows us to set any width and height for our swapchain within
                // [minImageExtent...maxImageExtent] boundaries
                vk::Extent2D swapExtent = 
                    surfaceCapabilities.currentExtent.width != (std::numeric_limits<uint32_t>::max)() ?
                    surfaceCapabilities.currentExtent : vk::Extent2D{ width, height };
                // Surface defines max and min size, so make sure within boundaries by clamping value
                swapExtent.width = util::constraint(swapExtent.width, surfaceCapabilities.minImageExtent.width, surfaceCapabilities.maxImageExtent.width);
                swapExtent.height = util::constraint(swapExtent.width, surfaceCapabilities.minImageExtent.height, surfaceCapabilities.maxImageExtent.height);

                // 4. Create swap chain createinfo
                vk::SwapchainCreateInfoKHR createInfo
                {
                    .surface = m_Surface,

                    // An application may request more images. If it wants to use multiple images at
                    // once it may do so, for example, when encoding a video stream where every fourth
                    // image is a key frame and the application needs it to prepare the remaining three
                    // frames. Such usage will determine the number of images that will be automatically
                    // created in a swap chain: how many images the application requires at once for
                    // processing and how many images the presentation engine requires to function properly.
                    // https://software.intel.com/content/www/us/en/develop/articles/api-without-secrets-introduction-to-vulkan-part-2.html
                    .minImageCount = surfaceCapabilities.maxImageCount > 0 ? // maxImageCount = 0 means that there's no maximum
                        (std::min)(surfaceCapabilities.minImageCount + 1, surfaceCapabilities.maxImageCount) :
                        surfaceCapabilities.minImageCount + 1,

                    .imageFormat = surfaceFormat.format,
                    .imageColorSpace = surfaceFormat.colorSpace,
                    .imageExtent = swapExtent,
                    .imageArrayLayers = 1, // specifies the amount of layers each image consists of. This is always 1 unless you are developing a stereoscopic 3D application.

                    // Usage flags define how a given image may be used in Vulkan. If we want an image to be
                    // sampled (used inside shaders) it must be created with "sampled" usage. If the image
                    // should be used as a depth render target, it must be created with "depth and stencil" usage.
                    // An image without proper usage "enabled" cannot be used for a given purpose or the results
                    // of such operations will be undefined.
                    // For a swap chain we want to render(in most cases) into the image(use it as a render target),
                    // so we must specify �color attachment� usage with VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT enum.
                    // In Vulkan this usage is always available for swap chains, so we can always set it without
                    // any additional checking.But for any other usage we must ensure it is supported � we can do
                    // this through a "supportedUsageFlags" member of surface capabilities structure.
                    // https://software.intel.com/content/www/us/en/develop/articles/api-without-secrets-introduction-to-vulkan-part-2.html
                    .imageUsage = vk::ImageUsageFlagBits::eColorAttachment | vk::ImageUsageFlagBits::eTransferDst, // just color, no depth; "transfer destination" usage is required for image clear operation

                    .preTransform = surfaceCapabilities.currentTransform, // we can specify that a certain transform should be applied to images in the swap chain if it is supported
                    .compositeAlpha = vk::CompositeAlphaFlagBitsKHR::eOpaque, // no alpha blending (specifies if the alpha channel should be used for blending with other windows in the window system)
                    .presentMode = presentMode,
                    .clipped = VK_TRUE // If set to VK_TRUE then that means that we don't care about the color of pixels that are obscured, for example because another window is in front of them.
                };

                // 5. If graphics and presentation families are different, then swapchain must let images be shared between queue families
                uint32_t queuFamilyIndices[]
                {
                    m_GraphicsQueueFamilyIndex,
                    m_PresentationQueueFamilyIndex
                };
                if (m_GraphicsQueueFamilyIndex != m_PresentationQueueFamilyIndex)
                {
                    // If we want to reference images from many different queue families at a time we can do so.
                    // In this case we must provide "concurrent" sharing mode. But this (probably) requires us to
                    // manage image data coherency by ourselves, that is, we must synchronize different queues in
                    // such a way that data in the images is proper and no hazards occur - some queues are reading
                    // data from images, but other queues haven't finished writing to them yet.
                    // We may not specify these queue families and just tell Vulkan that only one queue family
                    // (queues from one family) will be referencing image at a time. This doesn't mean other queues
                    // can't reference these images. It just means they can't do it all at once, at the same time.
                    // So if we want to reference images from one family and then from another we must specifically
                    // tell Vulkan: "My image was used inside this queue family, but from now on another family,
                    // this one, will be referencing it." Such a transition is done using image memory barrier.
                    // https://software.intel.com/content/www/us/en/develop/articles/api-without-secrets-introduction-to-vulkan-part-2.html

                    // Images can be used across multiple queue families without explicit ownership transfers.
                    createInfo.imageSharingMode = vk::SharingMode::eConcurrent;
                    // Concurrent mode requires you to specify in advance between which queue families ownership will be shared.
                    createInfo.queueFamilyIndexCount = sizeof(queuFamilyIndices) / sizeof(queuFamilyIndices[0]);
                    createInfo.pQueueFamilyIndices = queuFamilyIndices;
                }
                else
                {
                    // An image is owned by one queue family at a time and ownership must be explicitly transferred
                    // before using it in another queue family. This option offers the best performance.
                    createInfo.imageSharingMode = vk::SharingMode::eExclusive;
                    createInfo.queueFamilyIndexCount = 0;
                    createInfo.pQueueFamilyIndices = nullptr;
                }

                // 6. With Vulkan it's possible that your swap chain becomes invalid or unoptimized while your application
                // is running, for example because the window was resized. In that case the swap chain actually needs to be
                // recreated from scratch and a reference to the old one must be specified in this field.
                createInfo.oldSwapchain = vk::SwapchainKHR(); // default vk::SwapchainKHR() plays a role of nullptr

                // 7. Create swapchain
                m_SwapChainImageFormat = createInfo.imageFormat;
                m_SwapChainExtent = swapExtent;
                return m_LogicalDevice.createSwapchainKHR(createInfo);
            }

            std::vector<SwapchainImage> CreateImageViews()
            {
                std::vector<SwapchainImage> imageViews;
                for (auto image : m_LogicalDevice.getSwapchainImagesKHR(m_Swapchain))
                {
                    imageViews.push_back(
                        {
                            image,
                            CreateImageView(m_LogicalDevice, image, m_SwapChainImageFormat, vk::ImageAspectFlagBits::eColor)
                        });
                }
                return imageViews;
            }

            // Before we can finish creating the pipeline, we need to tell Vulkan about the framebuffer attachments
            // that will be used while rendering. We need to specify how many color and depth buffers there will be,
            // how many samples to use for each of them and how their contents should be handled throughout the
            // rendering operations. All of this information is wrapped in a render pass object.
            // https://vulkan-tutorial.com/en/Drawing_a_triangle/Graphics_pipeline_basics/Render_passes
            // In Vulkan, a render pass represents (or describes) a set of framebuffer attachments (images) required
            // for drawing operations and a collection of subpasses that drawing operations will be ordered into.
            // Each of these drawing operations may read from some input attachments and render data into some other
            // (color, depth, stencil) attachments. A render pass also describes the dependencies between these
            // attachments: in one subpass we perform rendering into the texture, but in another this texture will
            // be used as a source of data (that is, it will be sampled from).
            // https://software.intel.com/content/www/us/en/develop/articles/api-without-secrets-introduction-to-vulkan-part-3.html
            // A render pass describes the scope of a rendering operation by specifying the collection of attachments,
            // subpasses, and dependencies used during the rendering operation. A render pass consists of at least one
            // subpass. The communication of this information to the driver allows the driver to know what to expect
            // when rendering begins and to set up the hardware optimally for the rendering operation.
            // https://vulkan.lunarg.com/doc/view/1.2.170.0/linux/tutorial/html/10-init_render_pass.html
            vk::RenderPass CreateRenderPass()
            {
                // --- ATTACHMENTS ---

                // Color attachment of the render pass
                vk::AttachmentDescription colorAttachment
                {
                    .format = m_SwapChainImageFormat,         // Format to use for attachment (here we are rendering directly into a swap chain so we need to take its format)
                    .samples = vk::SampleCountFlagBits::e1,   // Number of samples to write for multisampling (we are not using any multisampling here so we just use one sample)
                    .loadOp = vk::AttachmentLoadOp::eClear,   // What to do with the attached framebuffer before rendering
                    .storeOp = vk::AttachmentStoreOp::eStore, // What to do with the attached framebuffer after rendering

                    // When an attachment has a depth format (and potentially also a stencil component) load and store ops refer
                    // only to the depth component. If a stencil is present, stencil values are treated the way stencil load and
                    // store ops describe. For color attachments, stencil ops are not relevant.
                    //.stencilLoadOp = vk::AttachmentLoadOp::eDontCare,   // What to do with the stencil buffer before rendering
                    //.stencilStoreOp = vk::AttachmentStoreOp::eDontCare, // What to do with the stencil buffer after rendering

                    // Framebuffer data will be stored as an image, but images can be given different data layouts
                    // to give optimal use for certain operations.
                    // Layout is an internal memory arrangement of an image. Image data may be organized in such a way
                    // that neighboring "image pixels" are also neighbors in memory, which can increase cache hits
                    // (faster memory reading) when image is used as a source of data (that is, during texture sampling).
                    // But caching is not necessary when the image is used as a target for drawing operations, and the
                    // memory for that image may be organized in a totally different way. Image may have linear layout
                    // (which gives the CPU ability to read or populate image's memory contents) or optimal layout
                    // (which is optimized for performance but is also hardware/vendor dependent). So some hardware may
                    // have special memory organization for some types of operations; other hardware may be operations-
                    // agnostic. Some of the memory layouts may be better suited for some intended image "usages."
                    // Or from the other side, some usages may require specific memory layouts. There is also a general
                    // layout that is compatible with all types of operations. But from the performance point of view,
                    // it is always best to set the layout appropriate for an intended image usage and it is application's
                    // responsibility to inform the driver about transitions.
                    .initialLayout = vk::ImageLayout::eUndefined,  // INITIAL layout informs the hardware about the layout the application "provides" the given attachment with.
                    .finalLayout = vk::ImageLayout::ePresentSrcKHR // The final layout is the layout the given attachment will be transitioned into (automatically) AT THE END of a render pass.
                };

                // Depth attachment of the render pass
                vk::AttachmentDescription depthAttachment
                {
                    .format = m_DepthBuffer.ImageFormat(),       // Format to use for attachment (here we are rendering directly into a swap chain so we need to take its format)
                    .samples = vk::SampleCountFlagBits::e1,      // Number of samples to write for multisampling (we are not using any multisampling here so we just use one sample)
                    .loadOp = vk::AttachmentLoadOp::eClear,      // What to do with the attached framebuffer before rendering
                    .storeOp = vk::AttachmentStoreOp::eDontCare, // What to do with the attached framebuffer after rendering

                    // When an attachment has a depth format (and potentially also a stencil component) load and store ops refer
                    // only to the depth component. If a stencil is present, stencil values are treated the way stencil load and
                    // store ops describe.
                    .stencilLoadOp = vk::AttachmentLoadOp::eDontCare,
                    .stencilStoreOp = vk::AttachmentStoreOp::eDontCare,

                    .initialLayout = vk::ImageLayout::eUndefined,                   // INITIAL layout informs the hardware about the layout the application "provides" the given attachment with.
                    .finalLayout = vk::ImageLayout::eDepthStencilAttachmentOptimal  // The final layout is the layout the given attachment will be transitioned into (automatically) AT THE END of a render pass.
                };

                // --- REFERENCES ---
                // Every subpass references one or more of the attachments.
                // These references are themselves AttachmentReference structs.
                // There is a separation between a whole render pass and its subpasses because each subpass may use
                // multiple attachments in a different way, that is, in one subpass we are rendering into one color
                // attachment but in the next subpass we are reading from this attachment. In this way, we can prepare
                // a list of all attachments used in the whole render pass, and at the same time we can specify how
                // each attachment will be used in each subpass. And as each subpass may use a given attachment in
                // its own way, we must also specify each image�s layout for each subpass.
                // So before we can specify a description of all subpasses(an array with elements of type
                // VkSubpassDescription) we must create references for each attachment used in each subpass.
                // And this is what the color_attachment_references variable was created for.

                vk::AttachmentReference colorAttachmentRef
                {
                    // specifies which attachment to reference by its index in the attachment descriptions array
                    .attachment = 0,

                    // Requested (required) layout the attachment will use DURING a given subpass.
                    // The hardware will perform an automatic transition into a provided layout just
                    // before a given subpass.
                    .layout = vk::ImageLayout::eColorAttachmentOptimal
                };

                vk::AttachmentReference depthAttachmentRef
                {
                    // specifies which attachment to reference by its index in the attachment descriptions array
                    .attachment = 1,

                    // Requested (required) layout the attachment will use DURING a given subpass.
                    // The hardware will perform an automatic transition into a provided layout just
                    // before a given subpass.
                    .layout = vk::ImageLayout::eDepthStencilAttachmentOptimal
                };

                // --- SUBPASSES ---

                // Information about a particular subpass the render pass is using.
                // A subpass consists of drawing operations that use (more or less) the same attachments.
                // Each of these drawing operations may read from some input attachments and render data
                // into some other (color, depth, stencil) attachments.
                // Each subpass uses its own unique pipeline (the one currently bound to the pipelineBindPoint).
                vk::SubpassDescription subpass
                {
                    // Type of pipeline in which this subpass will be used (graphics
                    // or compute). Our example, of course, uses a graphics pipeline.
                    .pipelineBindPoint = vk::PipelineBindPoint::eGraphics,

                    // Array describing (pointing to) attachments which will be used
                    // as color render targets (that image will be rendered into).
                    // The index of the attachment in this array is directly referenced from the
                    // fragment shader with the layout(location = 0) out vec4 outColor directive.
                    .colorAttachmentCount = 1,
                    .pColorAttachments = &colorAttachmentRef,

                    // Unlike color attachments, a subpass can only use a single depth (+stencil) attachment.
                    .pDepthStencilAttachment = &depthAttachmentRef
                };

                // Need to determine when layout transitions occur using subpass dependencies.
                // An execution dependency is a guarantee that for two sets of operations, the 1st set must happen-before the 2nd set.
                // A memory dependency is an execution dependency which includes availability and visibility operations such that:
                //     The first set of operations happens - before the availability operation.
                //     The availability operation happens - before the visibility operation.
                //     The visibility operation happens - before the second set of operations.
                // If a synchronization command includes a source stage mask, its first synchronization scope only includes execution
                // of the pipeline stages specified in that mask, and its first access scope only includes memory access performed by
                // pipeline stages specified in that mask. If a synchronization command includes a destination stage mask, its second
                // synchronization scope only includes execution of the pipeline stages specified in that mask, and its second access
                // scope only includes memory access performed by pipeline stages specified in that mask.
                // https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSubpassDependency.html
                // https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#synchronization-dependencies-scopes
                // When multiple subpasses are in use, the driver needs to be told the relationship between them. A subpass can depend
                // on operations which were submitted outside the current render pass, or be the source on which later rendering depends.
                // Most commonly, the need is to ensure that the fragment shader from an earlier subpass has completed rendering (to the
                // current tile, on a tiler) before the next subpass starts to try to read that data.
                // https://developer.samsung.com/galaxy-gamedev/resources/articles/renderpasses.html
                std::array<vk::SubpassDependency, 2> subpassDependencies
                {
                    // Transition vk::ImageLayout::eUndefined --> vk::ImageLayout::eColorAttachmentOptimal
                    vk::SubpassDependency
                    {
                        .srcSubpass = VK_SUBPASS_EXTERNAL, // The subpass index of the 1st subpass in the dependency. Subpass index (VK_SUBPASS_EXTERNAL = special value meaning outside of renderpass).
                        .dstSubpass = 0, // The subpass index of the 2nd subpass in the dependency

                        // The synchronization scope of the 1st set of commands. What pipeline stage must have completed for the dependency
                        .srcStageMask = vk::PipelineStageFlagBits::eBottomOfPipe,
                        // The synchronization scope of the 2nd set of commands. What pipeline stage is waiting on the dependency
                        .dstStageMask = vk::PipelineStageFlagBits::eColorAttachmentOutput | vk::PipelineStageFlagBits::eEarlyFragmentTests,

                        // Memory access scope of the 1st set of commands (The presentation engine only reads from the image)
                        .srcAccessMask = vk::AccessFlagBits::eMemoryRead,
                        // Memory access scope of the 2nd set of commands
                        .dstAccessMask = vk::AccessFlagBits::eColorAttachmentWrite | vk::AccessFlagBits::eDepthStencilAttachmentWrite
                    },
                    // Transition vk::ImageLayout::eColorAttachmentOptimal --> vk::ImageLayout::ePresentSrcKHR
                    vk::SubpassDependency
                    {
                        .srcSubpass = 0,  // The subpass index of the 1st subpass in the dependency
                        .dstSubpass = VK_SUBPASS_EXTERNAL, // The subpass index of the 2nd subpass in the dependency. Subpass index (VK_SUBPASS_EXTERNAL = special value meaning outside of renderp

                         // The synchronization scope of the 1st set of commands. What pipeline stage must have completed for the dependency
                        .srcStageMask = vk::PipelineStageFlagBits::eColorAttachmentOutput | vk::PipelineStageFlagBits::eEarlyFragmentTests,
                        // The synchronization scope of the 2nd set of commands. What pipeline stage is waiting on the dependency
                        .dstStageMask = vk::PipelineStageFlagBits::eBottomOfPipe,

                        // Memory access scope of the 1st set of commands
                        .srcAccessMask = vk::AccessFlagBits::eColorAttachmentWrite | vk::AccessFlagBits::eDepthStencilAttachmentWrite,
                        // Memory access scope of the 2nd set of commands (The presentation engine only reads from the image)
                        .dstAccessMask = vk::AccessFlagBits::eMemoryRead
                    },
                };

                // --- RENDER PASS ---

                std::array<vk::AttachmentDescription, 2> attachments
                {
                    colorAttachment, // 0 (position is important, because AttachmentReferences refer to it)
                    depthAttachment  // 1 (position is important, because AttachmentReferences refer to it)
                };

                vk::RenderPassCreateInfo renderPassCreateInfo
                {
                    .attachmentCount = attachments.size(), // Number of all different attachments (elements in pAttachments array) used during whole render pass
                    .pAttachments = attachments.data(),    // Array specifying all attachments used in a render pass
                    .subpassCount = 1,      // Number of subpasses a render pass consists of
                    .pSubpasses = &subpass, // Array with descriptions of all subpasses
                    .dependencyCount = subpassDependencies.size(),
                    .pDependencies = subpassDependencies.data()
                };

                return m_LogicalDevice.createRenderPass(renderPassCreateInfo);
            }

            // Push constant is a value of a limited size (usually not greater than 128 bytes)
            // that is passed as part of commads when recording them into command buffer.
            // There can be only one push constant per shader stage.
            vk::PushConstantRange CreatePushConstantRange()
            {
                return vk::PushConstantRange
                {
                    .stageFlags = vk::ShaderStageFlagBits::eVertex, // Shader stage push constant will go to
                    .offset = 0, // Offset into given data to pass to push constant
                    .size = sizeof(engine::mat4) // Size of data being passed
                };
            }

            // Descriptor set layout is a structure describing several (but not necessarily all)
            // uniform objects and texture samplers existing inside the shader program. Each descriptor
            // set is associated with a certain descriptor set layout. It's possible to have several
            // descriptor sets bound to the pipeline that have different descriptor set layouts (those
            // layouts are specifiedin the PipelineLayoutCreateInfo (see CreatePipelineLayout).
            // Two sets with the same layout are considered to be compatible and interchangeable [Sellers].
            vk::DescriptorSetLayout CreateSamplerDescriptorSetLayout()
            {
                // Each uniform buffer or texture sampler has exactly one binding
                std::array<vk::DescriptorSetLayoutBinding, 1> layoutBindings
                {
                    // Texture sampler
                    vk::DescriptorSetLayoutBinding
                    {
                        .binding = 0, // Binding index in the shader
                        .descriptorType = vk::DescriptorType::eCombinedImageSampler,
                        .descriptorCount = 1, // It is possible for the shader variable to represent an array of uniform buffer objects, and descriptorCount specifies the number of values in the array
                        .stageFlags = vk::ShaderStageFlagBits::eFragment, // In which shader stages the descriptor is going to be referenced
                    }
                };

                // Descriptor set layout describes multiple uniform buffer objects and texture samplers
                // that exist inside the shader program.
                vk::DescriptorSetLayoutCreateInfo layoutInfo
                {
                    .bindingCount = layoutBindings.size(),
                    .pBindings = layoutBindings.data()
                };

                return m_LogicalDevice.createDescriptorSetLayout(layoutInfo);
            }

            // -- PIPELINE LAYOUT --
            // Uniform values & texture samplers need to be specified during pipeline
            // creation by creating a PipelineLayout object because there may be multiple DescriptorSets
            // bound and Vulkan wants to know in advance how many and what types of them it should expect.
            // Access to descriptor sets from a pipeline is accomplished through a pipeline layout.
            // Zero or more descriptor set layouts and zero or more push constant ranges are combined
            // to form a pipeline layout object describing the complete set of resources that can be
            // accessed by a pipeline. The pipeline layout represents a sequence of descriptor sets
            // with each having a specific layout. This sequence of layouts is used to determine the
            // interface between shader stages and shader resources. Each pipeline is created using
            // a pipeline layout.
            vk::PipelineLayout CreatePipelineLayout()
            {
                std::array<vk::DescriptorSetLayout, 1> descriptorSetLayouts
                {
                    m_SamplerDescriptorSetLayout,
                    // m_UniformDescriptorSetLayout
                };

                // It is actually possible to bind multiple descriptor sets simultaneously. You need to
                // specify a descriptor layout for each descriptor set when creating the pipeline layout.
                // Shaders can then reference specific descriptor sets like this:
                //     layout(set = 0, binding = 0) uniform UniformBufferObject { ... }
                // You can use this feature to put descriptors that vary per-object and descriptors
                // that are shared into separate descriptor sets. In that case you avoid rebinding most
                // of the descriptors across draw calls which is potentially more efficient.
                vk::PipelineLayoutCreateInfo pipelineLayoutCreateInfo
                {
                    .setLayoutCount = descriptorSetLayouts.size(), // the number of different descriptor set layouts inside the shader program
                    .pSetLayouts = descriptorSetLayouts.data(), // pointer to an array of descriptor set layouts

                    .pushConstantRangeCount = 1,
                    .pPushConstantRanges = &m_PushConstantRange
                };
                return m_LogicalDevice.createPipelineLayout(pipelineLayoutCreateInfo);
            }

            // Pipeline is the big one, as it composes most of the objects listed before. It represents the configuration
            // of the whole pipeline and has a lot of parameters. One of them is PipelineLayout � it defines the layout of
            // descriptors and push constants. There are two types of Pipelines � ComputePipeline and GraphicsPipeline.
            // ComputePipeline is the simpler one, because all it supports is compute-only programs (sometimes called
            // compute shaders). GraphicsPipeline is much more complex, because it encompasses all the parameters like
            // vertex, fragment, geometry, compute and tessellation where applicable, plus things like vertex attributes,
            // primitive topology, backface culling, and blending mode, to name just a few. All those parameters that used
            // to be separate settings in much older graphics APIs (DirectX 9, OpenGL), were later grouped into a smaller
            // number of state objects as the APIs progressed (DirectX 10 and 11) and must now be baked into a single big,
            // immutable object with today�s modern APIs like Vulkan.  For each different set of parameters needed during
            // rendering you must create a new Pipeline. You can then set it as the current active Pipeline in a
            // CommandBuffer by calling the function vkCmdBindPipeline.
            // https://gpuopen.com/learn/understanding-vulkan-objects/
            vk::Pipeline CreateGraphicsPipeline()
            {
                // Read in SPIR-V code of shaders
                auto vertexShaderCode = util::file::ReadAllBin("assets/shaders/textures/vert.spv");
                auto fragmentShaderCode = util::file::ReadAllBin("assets/shaders/textures/frag.spv");

                // Build shader modules
                auto vertexShader = CreateShaderModule(vertexShaderCode);
                auto fragmentShader = CreateShaderModule(fragmentShaderCode);

                // Describe shader stages
                vk::PipelineShaderStageCreateInfo shaderStageCreateInfos[]
                {
                    vk::PipelineShaderStageCreateInfo
                    {
                        .stage = vk::ShaderStageFlagBits::eVertex,
                        .module = vertexShader,
                        .pName = "main",               // shader entry point
                        .pSpecializationInfo = nullptr // allows you to specify values for shader constants
                    },
                    vk::PipelineShaderStageCreateInfo
                    {
                        .stage = vk::ShaderStageFlagBits::eFragment,
                        .module = fragmentShader,
                        .pName = "main",               // shader entry point
                        .pSpecializationInfo = nullptr // allows you to specify values for shader constants
                    }
                };

                // SETUP FIXED-FUNCTION PART OF PIPELINE
                // https://vulkan-tutorial.com/en/Drawing_a_triangle/Graphics_pipeline_basics/Fixed_functions

                // -- VERTEX INPUT --
                
                // How the data for a single vertex (position, color, texture coords,
                // normals, etc) is as a whole
                vk::VertexInputBindingDescription vertexBindingDescription
                {
                    .binding = 0, // The binding point to which a vertex buffer will be bound
                    .stride = sizeof(vertex_type), // Size of a single vertex object
                    .inputRate = vk::VertexInputRate::eVertex // No instanced drawing
                };

                // How the data fo an attribute is defined within a vertex
                std::array<vk::VertexInputAttributeDescription, 2> vertexAttribDescriptions
                {
                    vk::VertexInputAttributeDescription
                    {
                        .location = 0, // Location in shader where data will be read from
                        .binding = 0,  // Which binding the data is at
                        .format = type_traits<decltype(vertex_type::Position)>::format, // Format the data will take (also helps define size of data)
                        .offset = offsetof(vertex_type, vertex_type::Position) // Where this attribute is defined in the data for a single vertex
                    },
                    vk::VertexInputAttributeDescription
                    {
                        .location = 1, // Location in shader where data will be read from
                        .binding = 0,  // Which binding the data is at
                        .format = type_traits<decltype(vertex_type::TextureCoords)>::format, // Format the data will take (also helps define size of data)
                        .offset = offsetof(vertex_type, vertex_type::TextureCoords) // Where this attribute is defined in the data for a single vertex
                    },
                };

                vk::PipelineVertexInputStateCreateInfo vInputStateCreateInfo
                {
                    // spacing between data and whether the data is per-vertex or per-instance
                    .vertexBindingDescriptionCount = 1,
                    .pVertexBindingDescriptions = &vertexBindingDescription,
                    
                    // type of the attributes passed to the vertex shader, which binding
                    // to load them from and at which offset
                    .vertexAttributeDescriptionCount = vertexAttribDescriptions.size(),
                    .pVertexAttributeDescriptions = vertexAttribDescriptions.data(),
                };

                // -- INPUT ASSEMBLY --
                // Describes two things: what kind of geometry will be drawn
                // from the vertices and if primitive restart should be enabled
                vk::PipelineInputAssemblyStateCreateInfo inputAssemblyCreateInfo
                {
                    .topology = vk::PrimitiveTopology::eTriangleList, // type of primitives to be assembled from vertices
                    .primitiveRestartEnable = VK_FALSE                // relevant for strips and fans (to start new strip or fan)
                };

                // -- VIEWPORT & SCISSOR --
                // Viewports define the transformation from the image to the framebuffer
                vk::Viewport viewport
                {
                    .x = 0.0f,
                    .y = 0.0f,
                    .width = static_cast<float>(m_SwapChainExtent.width),
                    .height = static_cast<float>(m_SwapChainExtent.height),
                    .minDepth = 0.0f,
                    .maxDepth = 1.0f
                };

                // Any pixels outside the scissor rectangles will be discarded by the rasterizer
                vk::Rect2D scissor
                {
                    .offset = { 0, 0 },
                    .extent = m_SwapChainExtent
                };

                // Viewport and scissor rectangle need to be combined into a viewport state.
                // It is possible to use multiple viewports and scissor rectangles on some graphics cards.
                vk::PipelineViewportStateCreateInfo viewportStateCreateInfo
                {
                    .viewportCount = 1,
                    .pViewports = &viewport,
                    .scissorCount = 1,
                    .pScissors = &scissor
                };

                // -- DYNAMIC STATE --
                // A limited amount of the state that we've specified in the previous structs can actually
                // be changed without recreating the pipeline. Examples are the size of the viewport, line
                // width and blend constants. If you want to do that, then you'll have to fill in a
                // vk::DynamicState. This will cause the configuration of these values to be ignored and
                // you will be required to specify the data at drawing time.
                // But anyway you still need to recreate the swapchain when the window is resized.
                std::array<vk::DynamicState, 2> dynamicStates
                {
                    // Dynamic Viewport: Can resize in command buffer
                    // with vkCmdSetViewport(commandBuffer, 0, 1, &viewport);
                    vk::DynamicState::eViewport,
                    // Dynamic Scissor: Can resize in command buffer
                    // with vkCmdSetScissor(commandBuffer, 0, 1, &scissor);
                    vk::DynamicState::eScissor
                };

                vk::PipelineDynamicStateCreateInfo dynamicState
                {
                    .dynamicStateCount = dynamicStates.size(),
                    .pDynamicStates = dynamicStates.data()
                };

                // -- RASTERIZER --
                // Many features here if enabled require enabling certain device features (see PhysicalDeviceFeatures).
                vk::PipelineRasterizationStateCreateInfo rasterizerCreateInfo
                {
                    .depthClampEnable = VK_FALSE,                  // when set to VK_TRUE: if fragment depth > max_depth then set depth = max_depth
                    .rasterizerDiscardEnable = VK_FALSE,           // if set to VK_TRUE, then geometry never passes through the rasterizer stage (useful when you don't need to draw anything on the screen)
                    .polygonMode = vk::PolygonMode::eFill,         // fill, lines (wireframe), points
                    .cullMode = vk::CullModeFlagBits::eBack,       // don't draw back faces
                    .frontFace = vk::FrontFace::eCounterClockwise, // which face is considered front
                    .depthBiasEnable = VK_FALSE,                   // whether to add depth bias to fragments (good for stopping shadow acne)
                    .lineWidth = 1.0f,                             // describes the thickness of lines in terms of number of fragments
                };

                // -- MULTISAMPLING --
                // It works by combining the fragment shader results of multiple polygons that rasterize to the same pixel.
                // This mainly occurs along edges, which is also where the most noticeable aliasing artifacts occur.
                vk::PipelineMultisampleStateCreateInfo multisamplingCreateInfo
                {
                    .rasterizationSamples = vk::SampleCountFlagBits::e1, // number of samples to use per fragment
                    .sampleShadingEnable = VK_FALSE                      // enable multisample shading or not
                };

                // -- BLENDING --

                // Blend attachment state contains the configuration per attached framebuffer.
                vk::PipelineColorBlendAttachmentState colorBlendingState
                {
                    .blendEnable = VK_TRUE, // enable blending

                    // Blending uses equation: (srcCoorBlendFactor * newColor) colorBlendOp (dstColorBlendFactor * oldColor)
                    .srcColorBlendFactor = vk::BlendFactor::eSrcAlpha,
                    .dstColorBlendFactor = vk::BlendFactor::eOneMinusSrcAlpha,
                    .colorBlendOp = vk::BlendOp::eAdd,

                    .srcAlphaBlendFactor = vk::BlendFactor::eOne,  // replace old alpha with the new one
                    .dstAlphaBlendFactor = vk::BlendFactor::eZero, // get rid of the old alpha
                    .alphaBlendOp = vk::BlendOp::eAdd,
                    
                    .colorWriteMask = vk::ColorComponentFlagBits::eR | vk::ColorComponentFlagBits::eG // which colors to apply blending operations to
                                    | vk::ColorComponentFlagBits::eB | vk::ColorComponentFlagBits::eA,
                };

                // The PipelineColorBlendStateCreateInfo structure references the array of structures for all of the framebuffers.
                vk::PipelineColorBlendStateCreateInfo colorBlendCreateInfo
                {
                    .logicOpEnable = VK_FALSE, // alternative to calculations is to use logical operations
                    .attachmentCount = 1,
                    .pAttachments = &colorBlendingState
                };

                // -- DEPTH STENCIL TESTING --
                vk::PipelineDepthStencilStateCreateInfo depthStencil
                {
                    .depthTestEnable = VK_TRUE,  // Enable checking depth to determine fragment write
                    .depthWriteEnable = VK_TRUE, // Enable writing to depth buffer (to replace old values)
                    .depthCompareOp = vk::CompareOp::eLess, // If the new depth value is LESS it replaces the old one
                    .depthBoundsTestEnable = VK_FALSE, // Depth bounds test: does the depth test fall into the bounds [minDepth...maxDepth]
                    .stencilTestEnable = VK_FALSE,
                    .minDepthBounds = 0.0f, // Optional
                    .maxDepthBounds = 1.0f, // Optional
                };

                // -- GRAPHICS PIPELINE CREATION --
                vk::GraphicsPipelineCreateInfo pipelineInfo
                {
                    .stageCount = 2, // Number of shader stages
                    .pStages = shaderStageCreateInfos, // List of shader stages
                    .pVertexInputState = &vInputStateCreateInfo,     // All the fixed function pipeline states
                    .pInputAssemblyState = &inputAssemblyCreateInfo,
                    .pViewportState = &viewportStateCreateInfo,
                    .pRasterizationState = &rasterizerCreateInfo,
                    .pMultisampleState = &multisamplingCreateInfo,
                    .pDepthStencilState = &depthStencil,
                    .pColorBlendState = &colorBlendCreateInfo,
                    .pDynamicState = nullptr, // Optional
                    .layout = m_PipelineLayout, // Pipeline layout pipeline should use
                    .renderPass = m_RenderPass, // Pipeline can be used with any render pass compatible with the provided one
                    .subpass = 0, // Subpass of render pass to use with pipeline (a pipeline can only be attached to one particular subpass)
                                  // Different subpass requires different pipeline.
                    
                    // Pipeline Derivatives: Can create multiple pipelines that derive from one another for optimization
                    .basePipelineHandle = VK_NULL_HANDLE, // Existing pipeline to derive from...
                    .basePipelineIndex = -1, // ... or index of pipeline being created to derive from (in case creating multiple at once)
                };
                vk::Pipeline pipeline = m_LogicalDevice.createGraphicsPipeline(
                    vk::PipelineCache(), pipelineInfo).value;

                // Destroy shader modules because they are no longer needed
                m_LogicalDevice.destroyShaderModule(vertexShader);
                m_LogicalDevice.destroyShaderModule(fragmentShader);

                return pipeline;
            }

            vk::ShaderModule CreateShaderModule(
                const std::vector<unsigned char>& compiledCode)
            {
                vk::ShaderModuleCreateInfo createInfo
                {
                    .codeSize = compiledCode.size(),
                    .pCode = reinterpret_cast<const uint32_t*>(compiledCode.data())
                };

                return m_LogicalDevice.createShaderModule(createInfo);
            }

            // Framebuffers represent a collection of memory attachments that are used by a render pass instance.
            // Examples of these memory attachments include the color image buffers and depth buffer.
            // A framebuffer provides the attachments that a render pass needs while rendering.
            // For example there might be two images in a swapchain (color buffers) and just a single depth buffer,
            // so there will be two framebuffer objects each referencing one of the color buffers (image view object)
            // and that single depth buffer.
            // https://vulkan.lunarg.com/doc/view/1.2.170.0/linux/tutorial/html/12-init_frame_buffers.html
            std::vector<vk::Framebuffer> CreateFramebuffers()
            {
                std::vector<vk::Framebuffer> framebuffers;
                framebuffers.reserve(m_SwapchainImages.size());
                for (size_t i = 0; i < m_SwapchainImages.size(); i++)
                {
                    std::array<vk::ImageView, 2> attachments
                    {
                        m_SwapchainImages[i].ImageView, // 0 (position is important - it shoukd match that of the render pass)
                        m_DepthBuffer.ImageView()       // 1 (position is important - it shoukd match that of the render pass)
                    };

                    vk::FramebufferCreateInfo framebufferCreateInfo
                    {
                        // Render Pass with which the framebuffer needs to be compatible. You can only use a framebuffer
                        // with the render passes that it is compatible with, which roughly means that they use the same
                        // number and type of attachments.
                        // https://vulkan-tutorial.com/Drawing_a_triangle/Drawing/Framebuffers
                        .renderPass = m_RenderPass,

                        .attachmentCount = static_cast<uint32_t>(attachments.size()),
                        .pAttachments = attachments.data(), // List of attachments (1:1 with Render Pass)
                        .width = m_SwapChainExtent.width,   // Framebuffer width
                        .height = m_SwapChainExtent.height, // Framebuffer height
                        .layers = 1                         // Framebuffer layers
                    };

                    framebuffers.push_back(
                        m_LogicalDevice.createFramebuffer(framebufferCreateInfo));
                }
                return framebuffers;
            }

            vk::CommandPool CreateGraphicsCommandPool()
            {
                vk::CommandPoolCreateInfo commandPoolCreateInfo
                {
                    // This flag allows command buffer, created from the pool, to be rerecorded (reset) which is not allowed by default.
                    .flags = vk::CommandPoolCreateFlagBits::eResetCommandBuffer,
                    // If there is more than one hardware queue in the GPU hardware, as described by
                    // the physical device queue families, then the driver might need to allocate command
                    // buffer pools with different memory allocation attributes, specific to each GPU
                    // hardware queue. These details are handled for you by the driver as long as it knows
                    // the queue family containing the queue that the command buffer will use.
                    // https://vulkan.lunarg.com/doc/view/1.2.170.0/linux/tutorial/html/04-init_command_buffer.html
                    .queueFamilyIndex = m_GraphicsQueueFamilyIndex // Queue Family type that buffers from this command pool will use
                };
                return m_LogicalDevice.createCommandPool(commandPoolCreateInfo);
            }

            // Because one of the drawing commands involves binding the right VkFramebuffer, we'll actually have
            // to record a command buffer for every image in the swap chain.
            // https://vulkan-tutorial.com/Drawing_a_triangle/Drawing/Command_buffers
            std::vector<vk::CommandBuffer> CreateGraphicsCommandBuffers()
            {
                vk::CommandBufferAllocateInfo cbAllocInfo
                {
                    .commandPool = m_GraphicsCommandPool,
                    .level = vk::CommandBufferLevel::ePrimary, // Primary buffer is the one that you submit directly to queue and can't be called by other command buffers
                    .commandBufferCount = static_cast<uint32_t>(m_SwapchainImages.size())
                };
                return m_LogicalDevice.allocateCommandBuffers(cbAllocInfo);
            }

            // Sampler describes how a texture is sampled inside shader.
            // Determines mipmap filtering, clamping etc.
            vk::Sampler CreateTextureSampler()
            {
                return m_LogicalDevice.createSampler(
                    vk::SamplerCreateInfo
                    {
                        .magFilter = vk::Filter::eLinear, // How to render when texture pixel occupies many screen pixels
                        .minFilter = vk::Filter::eLinear, // How to render when texture pixel occupies less than one screen pixel
                        .mipmapMode = vk::SamplerMipmapMode::eLinear,    // Mipmap interpolation mode
                        .addressModeU = vk::SamplerAddressMode::eRepeat, // How to handle texture wrap in U (x) direction
                        .addressModeV = vk::SamplerAddressMode::eRepeat, // How to handle texture wrap in V (y) direction
                        .addressModeW = vk::SamplerAddressMode::eRepeat, // How to handle texture wrap in W (z) direction (form volumetric textures)
                        .mipLodBias = 0.0f, // Level of Details bias for mipmap level (https://ru.wikipedia.org/wiki/MIP-���������������)
#ifdef ENABLE_ANISOTROPY
                        .anisotropyEnable = VK_TRUE, // Enable anisotropic filtering (effect can be seen on large objects that are stretched far away)
#endif
                        .maxAnisotropy = 16,         // The amount of samples being taken in anisotropic filtering
                        .minLod = 0.0f,     // Minimum Level of Details to pick mip level
                        .maxLod = 0.0f,     // Maximum Level of Details to pick mip level (0 because we don't use mipmaps in this example)
                        .borderColor = vk::BorderColor::eIntOpaqueBlack, // Has an effect only if addressMode = clamp to border
                        .unnormalizedCoordinates = VK_FALSE, // Normalized coordinates (0..1, 0..1), unnormalized coords (0..width, 0...height)
                    });
            }

            // Descriptor sets can't be created directly, they must be allocated
            // from a pool like command buffers.
            vk::DescriptorPool CreateSamplerDescriptorPool()
            {
                std::array<vk::DescriptorPoolSize, 1> poolSizes
                {
                    vk::DescriptorPoolSize
                    {
                        .type = vk::DescriptorType::eCombinedImageSampler, // descriptor type (combined Image descriptor + Sampler descriptor)
                        .descriptorCount = MAX_TEXTURES // the number of descriptors of the specified type which can be allocated in total from the pool
                    }
                };

                vk::DescriptorPoolCreateInfo poolInfo
                {
                    .maxSets = m_SwapchainImages.size(), // the maximum number of descriptor sets that may be allocated from the pool
                    .poolSizeCount = poolSizes.size(),
                    .pPoolSizes = poolSizes.data(),
                };

                // See https://www.reddit.com/r/vulkan/comments/8u9zqr/having_trouble_understanding_descriptor_pool/

                return m_LogicalDevice.createDescriptorPool(poolInfo);
            }

            // Descriptor set is a structure that accumulates references to uniforms
            // and texture samplers and that is bound to the pipeline during command recording
            // (see RecordCommands).
            std::vector<vk::DescriptorSet> CreateTextureSamplerDescriptorSets()
            {
                // We need all the copies of the layout because the next function expects an array matching the number of sets.
                std::vector<vk::DescriptorSetLayout> layouts(m_Models.size(), m_SamplerDescriptorSetLayout);

                vk::DescriptorSetAllocateInfo allocInfo
                {
                    .descriptorPool = m_SamplerDescriptorPool,
                    .descriptorSetCount = layouts.size(), // One diffuse texture per model
                    .pSetLayouts = layouts.data()
                };

                auto descriptorSets = m_LogicalDevice.allocateDescriptorSets(allocInfo);

                // The descriptor sets have been allocated now, but the descriptors within still need to be configured...
                for (size_t i = 0; i < descriptorSets.size(); i++)
                {
                    vk::DescriptorImageInfo descriptorImageInfo
                    {
                        .sampler = m_TextureSampler,
                        .imageView = m_Texture.Image().ImageView(),
                        .imageLayout = vk::ImageLayout::eShaderReadOnlyOptimal // Image layout when it is in use
                    };

                    // Info about what to write to a single descriptor (there can be many of them in a descriptor set)
                    vk::WriteDescriptorSet descriptorWrite
                    {
                        .dstSet = descriptorSets[i], // Descriptor set to update
                        .dstBinding = 0,             // Binding index inside shader
                        .dstArrayElement = 0,        // Descriptors can be arrays, so we also need to specify
                                                     // the first index in the array that we want to update.
                        .descriptorCount = 1,        // How many array elements you want to update
                        .descriptorType = vk::DescriptorType::eCombinedImageSampler,
                        .pImageInfo = &descriptorImageInfo
                    };

                    m_LogicalDevice.updateDescriptorSets(
                        1, // the number of descriptors to write to
                        &descriptorWrite, // array of WriteDescriptorSet
                        0, nullptr);      // array of CoypDescriptorSet
                }

                return descriptorSets;
            }

            // Record commands to the command buffers.
            // The commands bind resources (pipeline, uniforms, textures) and do drawing.
            // The usual practice is to prerecord a set of commands to the command buffer once
            // and then in the rendering loop submit them to the command queue on each iteration.
            void RecordCommands(uint32_t imageIndex)
            {
                // Information about how to begin each command buffer
                vk::CommandBufferBeginInfo commandBufferBeginInfo
                {
                    // Buffer can be resubmitted when it has already been submitted and is awaiting execution in a queue
                    //.flags = vk::CommandBufferUsageFlagBits::eSimultaneousUse
                };

                // Information about how to begin a render pass (only needed for graphical applications)
                // List of clear values
                std::array<vk::ClearValue, 2> clearValues
                {
                    vk::ClearColorValue(std::array<float, 4>{0.0f, 0.0f, 0.0f, 1.0f}), // black
                    vk::ClearDepthStencilValue{1.0f} // Depth Attachment Clear Value
                };
                vk::RenderPassBeginInfo renderPassBeginInfo
                {
                    .renderPass = m_RenderPass, // Render Pass to begin
                    .renderArea =
                    {
                        .offset = {0, 0},           // Start point of render pass in pixels
                        .extent = m_SwapChainExtent // Size of region to run render pass on (starting at offset)
                    },

                    // pClearValues is a pointer to an array of clearValueCount VkClearValue structures that contains
                    // clear values for each attachment, if the attachment uses a loadOp value of
                    // VK_ATTACHMENT_LOAD_OP_CLEAR or if the attachment has a depth/stencil format and
                    // uses a stencilLoadOp value of VK_ATTACHMENT_LOAD_OP_CLEAR. The array is indexed
                    // by attachment number. Only elements corresponding to cleared attachments are used.
                    // Other elements of pClearValues are ignored.
                    // https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkRenderPassBeginInfo.html
                    .clearValueCount = clearValues.size(),
                    .pClearValues = clearValues.data(),
                };

                auto& commandBuffer = m_GraphicsCommandBuffers[imageIndex];

                // Start recording commands to command buffer
                commandBuffer.begin(commandBufferBeginInfo);
                {
                    renderPassBeginInfo.framebuffer = m_Framebuffers[imageIndex];

                    // The second parameter:
                    // VK_SUBPASS_CONTENTS_INLINE
                    //     The render pass commands will be embedded in the primary command buffer itself and no
                    //     secondary command buffers will be executed.
                    // VK_SUBPASS_CONTENTS_SECONDARY_COMMAND_BUFFERS
                    //     The render pass commands will be executed from secondary command buffers.
                    commandBuffer.beginRenderPass(renderPassBeginInfo, vk::SubpassContents::eInline);
                    {
                        // Binding pipeline here tells the GPU how to render the graphics primitives that are coming later.
                        // VK_PIPELINE_BIND_POINT_GRAPHICS tells the GPU that this is a graphics pipeline instead of a
                        // compute pipeline. Note that since this command is a command buffer command, it is possible
                        // for a program to define several graphics pipelinesand switch between them in a single command
                        // buffer. https://vulkan.lunarg.com/doc/view/1.2.170.0/linux/tutorial/html/15-draw_cube.html
                        commandBuffer.bindPipeline(
                            vk::PipelineBindPoint::eGraphics,
                            m_GraphicsPipeline);

                        for (size_t j = 0; j < m_Models.size(); j++)
                        {
                            const auto& model = *m_Models[j];

                            const size_t BINDING_COUNT = 1;
                            // Buffers to bind
                            std::array<vk::Buffer, BINDING_COUNT> vertexBuffers
                            {
                                model.Mesh().VertexBuffer().Buffer()
                            };
                            // Offsets into buffers being bound
                            std::array<vk::DeviceSize, BINDING_COUNT> offsets
                            {
                                0
                            };
                            commandBuffer.bindVertexBuffers(
                                0,             // first binding point 
                                BINDING_COUNT, // binding count
                                vertexBuffers.data(),
                                offsets.data());

                            commandBuffer.bindIndexBuffer(
                                model.Mesh().IndexBuffer().Buffer(), // buffer
                                0, // offset
                                type_traits<index_type>::index_type); // index type

                            // Bind descriptor sets (there can be many of them).
                            // At any time, the application can bind a new descriptor set to the command buffer in any point that has
                            // an identical layout. The same descriptor set layouts can be used to create multiple pipelines.
                            // Therefore, if you have a set of objects that share a common set of resources, but additionally each
                            // require some unique resources, you can leave the common set bound and replace the unique
                            // resources as your application moves through the objects that need to be rendered. [Sellers]
                            commandBuffer.bindDescriptorSets(
                                vk::PipelineBindPoint::eGraphics, // Unlike vertex and index buffers, descriptor sets are not unique to graphics pipelines.
                                                                  // Therefore we need to specify if we want to bind descriptor sets to the graphics or compute pipeline.
                                                                  // There is a separate set of bind points for each pipeline type, so binding one does not disturb the others.
                                m_PipelineLayout, // Pipeline layout object used to program the bindings
                                0, // First descriptor set
                                1, // The number of descriptor sets to bind
                                &m_TextureSamplerDescriptorSets[j],
                                0, nullptr); // Dynamic offsets for a dynamic uniform buffer

                            // Push constants to given shader stage directly
                            auto mvp = getCamera().getWorldToScreenMatrix() * model.Wpo().ModelToWorldMatrix();
                            commandBuffer.pushConstants(
                                m_PipelineLayout,
                                vk::ShaderStageFlagBits::eVertex, // Shader stage to push constants to
                                0,                                // offset into the data
                                sizeof(engine::mat4), // size of constant being pushed
                                &mvp);                // pValues

                            // Command to execute pipeline
                            commandBuffer.drawIndexed(
                                model.Mesh().IndexCount(), // index count
                                1,  // instance count
                                0,  // first index
                                0,  // vertex offset
                                0); // first instance
                        }
                    }
                    commandBuffer.endRenderPass();
                }
                // Stop recording to command buffer
                commandBuffer.end();
            }

        protected:
            void UpdateTime() override
            {
                const double SPEED = 0.1;
                m_Model1.Wpo().RollWorld(SPEED * TimeSinceLastFrame());
                m_Model2.Wpo().RollWorld(-SPEED * TimeSinceLastFrame());
            }

            void setupCamera(const Camera& cam) override
            {
            }

            void OnRender() override
            {
                // Host program waits until fence is signaled ("opened")
                // i.e. until all submitted command buffers have completed execution.
                m_LogicalDevice.waitForFences(
                    1, &m_DrawFences[m_CurrentFrame],
                    VK_TRUE, // whether to wait for all of our fences
                    std::numeric_limits<uint64_t>::max() // timeout
                );

                // 1. Get next available image to draw to and set a semaphore that
                //    will be signaled when the image becomes available.
                uint32_t imageIndex;
                auto result = m_LogicalDevice.acquireNextImageKHR(
                    m_Swapchain,
                    std::numeric_limits<uint64_t>::max(), // timeout
                    m_SemaphoreImageAvailable[m_CurrentFrame], // semaphore to signal when the image becomes available (semaphore synchronizes GPU<->GPU actions)
                    vk::Fence(), // a fence to signal (fence synchronizes GPU<->CPU actions)
                    &imageIndex);

                // Swapchain is outdated due to the window resize.
                if (result == vk::Result::eErrorOutOfDateKHR || m_WindowIsResized)
                {
                    m_WindowIsResized = false;
                    RecreateSwapChain();
                    return;
                }
                else if (result != vk::Result::eSuccess && result != vk::Result::eSuboptimalKHR)
                    throw std::runtime_error("Failed to acquire swap chain image!");

                // Reset ("close") fence
                m_LogicalDevice.resetFences(1, &m_DrawFences[m_CurrentFrame]);

                RecordCommands(imageIndex);

                // 2. Submit command buffer to queue for execution, making sure it waits for the image
                //    to be signaled as available before drawing and signals it has finished rendering.
                std::array<vk::PipelineStageFlags, 1> waitStages
                {
                    vk::PipelineStageFlags(vk::PipelineStageFlagBits::eColorAttachmentOutput)
                };
                vk::SubmitInfo submitInfo
                {
                    .waitSemaphoreCount = 1,
                    .pWaitSemaphores = &m_SemaphoreImageAvailable[m_CurrentFrame],
                    .pWaitDstStageMask = waitStages.data(), // array of pipeline stages at which each corresponding semaphore wait will occur
                    
                    .commandBufferCount = 1,
                    .pCommandBuffers = &m_GraphicsCommandBuffers[imageIndex], // command buffers to submit

                    .signalSemaphoreCount = 1,
                    .pSignalSemaphores = &m_SemaphoreRenderFinished[m_CurrentFrame] // semaphores to signal when command buffer finishes
                };
                m_GraphicsQueue.submit(
                    1, &submitInfo,
                    m_DrawFences[m_CurrentFrame] // a fence to be signaled once all submitted command buffers have completed execution
                );

                // 3. Present image to screen when it has signaled finished rendering.
                vk::PresentInfoKHR presentInfo
                {
                    .waitSemaphoreCount = 1,
                    .pWaitSemaphores = &m_SemaphoreRenderFinished[m_CurrentFrame],

                    .swapchainCount = 1,
                    .pSwapchains = &m_Swapchain, // swapchains to present images to

                    .pImageIndices = &imageIndex
                };
                result = m_PresentationQueue.presentKHR(&presentInfo);
                if (result == vk::Result::eErrorOutOfDateKHR || result == vk::Result::eSuboptimalKHR)
                {
                    m_WindowIsResized = false;
                    RecreateSwapChain();
                }
                else if (result != vk::Result::eSuccess)
                    throw std::runtime_error("Failed to present swap chain image!");

                m_CurrentFrame = (m_CurrentFrame + 1) % MAX_FRAME_DRAWS;
            }

        #pragma endregion
    };
}