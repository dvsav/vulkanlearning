#pragma once

#include "ConditionalCompilation.h"

// STL
#include <string>
#include <vector>

// Utility
#include "File.h"
#include "VkUtility.h"

// VkEngine
#include "VkTypeTraits.h"

// Engine
#include "Vertex.h"

namespace vulkan
{
    inline vk::ShaderModule CreateShaderModule(
        const vk::Device& logicalDevice,
        const std::vector<unsigned char>& compiledCode)
    {
        return logicalDevice.createShaderModule(
            vk::ShaderModuleCreateInfo
            {
                .codeSize = compiledCode.size(),
                .pCode = reinterpret_cast<const uint32_t*>(compiledCode.data())
            });
    }

    template<typename vertex_type>
    inline std::vector<vk::VertexInputAttributeDescription> VertexAttributes(
        uint32_t binding,
        uint32_t start_location = 0U);

    template<>
    inline std::vector<vk::VertexInputAttributeDescription> VertexAttributes<engine::VertexPT>(
        uint32_t binding,
        uint32_t start_location)
    {
        using vertex_type = engine::VertexPT;

        // How the data fo an attribute is defined within a vertex
        return std::vector<vk::VertexInputAttributeDescription>
        {
            vk::VertexInputAttributeDescription
            {
                .location = start_location, // Location in shader where data will be read from
                .binding = binding,  // Which binding the data is at
                .format = type_traits<decltype(vertex_type::Position)>::format, // Format the data will take (also helps define size of data)
                .offset = offsetof(vertex_type, vertex_type::Position) // Where this attribute is defined in the data for a single vertex
            },
            vk::VertexInputAttributeDescription
            {
                .location = start_location+1, // Location in shader where data will be read from
                .binding = binding,  // Which binding the data is at
                .format = type_traits<decltype(vertex_type::TextureCoords)>::format, // Format the data will take (also helps define size of data)
                .offset = offsetof(vertex_type, vertex_type::TextureCoords) // Where this attribute is defined in the data for a single vertex
            }
        };
    }

    template<>
    inline std::vector<vk::VertexInputAttributeDescription> VertexAttributes<engine::VertexPC>(
        uint32_t binding,
        uint32_t start_location)
    {
        using vertex_type = engine::VertexPC;

        // How the data fo an attribute is defined within a vertex
        return std::vector<vk::VertexInputAttributeDescription>
        {
            vk::VertexInputAttributeDescription
            {
                .location = start_location, // Location in shader where data will be read from
                .binding = binding, // Which binding the data is at
                .format = type_traits<decltype(vertex_type::Position)>::format, // Format the data will take (also helps define size of data)
                .offset = offsetof(vertex_type, vertex_type::Position) // Where this attribute is defined in the data for a single vertex
            },
            vk::VertexInputAttributeDescription
            {
                .location = start_location+1, // Location in shader where data will be read from
                .binding = binding, // Which binding the data is at
                .format = type_traits<decltype(vertex_type::Color)>::format, // Format the data will take (also helps define size of data)
                .offset = offsetof(vertex_type, vertex_type::Color) // Where this attribute is defined in the data for a single vertex
            }
        };
    }

    // Pipeline is the big one, as it composes most of the objects listed before. It represents the configuration
    // of the whole pipeline and has a lot of parameters. One of them is PipelineLayout � it defines the layout of
    // descriptors and push constants. There are two types of Pipelines � ComputePipeline and GraphicsPipeline.
    // ComputePipeline is the simpler one, because all it supports is compute-only programs (sometimes called
    // compute shaders). GraphicsPipeline is much more complex, because it encompasses all the parameters like
    // vertex, fragment, geometry, compute and tessellation where applicable, plus things like vertex attributes,
    // primitive topology, backface culling, and blending mode, to name just a few. All those parameters that used
    // to be separate settings in much older graphics APIs (DirectX 9, OpenGL), were later grouped into a smaller
    // number of state objects as the APIs progressed (DirectX 10 and 11) and must now be baked into a single big,
    // immutable object with today�s modern APIs like Vulkan.  For each different set of parameters needed during
    // rendering you must create a new Pipeline. You can then set it as the current active Pipeline in a
    // CommandBuffer by calling the function vkCmdBindPipeline.
    // https://gpuopen.com/learn/understanding-vulkan-objects/
    template<typename vertex_type>
    inline vk::Pipeline CreateGraphicsPipeline(
        const vk::Device& logicalDevice,
        const vk::PipelineLayout& pipelineLayout,
        const vk::RenderPass& renderPass,
        const vk::Extent2D& swapchainExtent,
        const std::string& vertexShaderFile,
        const std::string& fragmentShaderFile)
    {
        // Read in SPIR-V code of shaders
        auto vertexShaderCode = util::file::ReadAllBin(vertexShaderFile);
        auto fragmentShaderCode = util::file::ReadAllBin(fragmentShaderFile);

        // Build shader modules
        auto vertexShader = CreateShaderModule(logicalDevice, vertexShaderCode);
        auto fragmentShader = CreateShaderModule(logicalDevice, fragmentShaderCode);

        // Describe shader stages
        std::array<vk::PipelineShaderStageCreateInfo, 2> shaderStageCreateInfos
        {
            vk::PipelineShaderStageCreateInfo
            {
                .stage = vk::ShaderStageFlagBits::eVertex,
                .module = vertexShader,
                .pName = "main",               // shader entry point
                .pSpecializationInfo = nullptr // allows you to specify values for shader constants
            },
            vk::PipelineShaderStageCreateInfo
            {
                .stage = vk::ShaderStageFlagBits::eFragment,
                .module = fragmentShader,
                .pName = "main",               // shader entry point
                .pSpecializationInfo = nullptr // allows you to specify values for shader constants
            }
        };

        // SETUP FIXED-FUNCTION PART OF PIPELINE
        // https://vulkan-tutorial.com/en/Drawing_a_triangle/Graphics_pipeline_basics/Fixed_functions

        // -- VERTEX INPUT --

        uint32_t vertex_binding = 0;

        // How the data for a single vertex (position, color, texture coords,
        // normals, etc) is as a whole
        std::array<vk::VertexInputBindingDescription, 1> vertexBindingDescriptions
        {
            vk::VertexInputBindingDescription
            {
                .binding = vertex_binding, // The binding point to which a vertex buffer will be bound
                .stride = sizeof(vertex_type), // Size of a single vertex object
                .inputRate = vk::VertexInputRate::eVertex // No instanced drawing
            }
        };

        // How the data of an attribute is defined within a vertex
        auto vertexAttribDescriptions = VertexAttributes<vertex_type>(vertex_binding);

        vk::PipelineVertexInputStateCreateInfo vInputStateCreateInfo
        {
            // spacing between data and whether the data is per-vertex or per-instance
            .vertexBindingDescriptionCount = vertexBindingDescriptions.size(),
            .pVertexBindingDescriptions = vertexBindingDescriptions.data(),

            // type of the attributes passed to the vertex shader, which binding
            // to load them from and at which offset
            .vertexAttributeDescriptionCount = vertexAttribDescriptions.size(),
            .pVertexAttributeDescriptions = vertexAttribDescriptions.data(),
        };

        // -- INPUT ASSEMBLY --
        // Describes two things: what kind of geometry will be drawn
        // from the vertices and if primitive restart should be enabled
        vk::PipelineInputAssemblyStateCreateInfo inputAssemblyCreateInfo
        {
            .topology = vk::PrimitiveTopology::eTriangleList, // type of primitives to be assembled from vertices
            .primitiveRestartEnable = VK_FALSE                // relevant for strips and fans (to start new strip or fan)
        };

        // -- VIEWPORT & SCISSOR --
        // Viewports define the transformation from the image to the framebuffer
        vk::Viewport viewport
        {
            .x = 0.0f,
            .y = 0.0f,
            .width = static_cast<float>(swapchainExtent.width),
            .height = static_cast<float>(swapchainExtent.height),
            .minDepth = 0.0f,
            .maxDepth = 1.0f
        };

        // Any pixels outside the scissor rectangles will be discarded by the rasterizer
        vk::Rect2D scissor
        {
            .offset = { 0, 0 },
            .extent = swapchainExtent
        };

        // Viewport and scissor rectangle need to be combined into a viewport state.
        // It is possible to use multiple viewports and scissor rectangles on some graphics cards.
        vk::PipelineViewportStateCreateInfo viewportStateCreateInfo
        {
            .viewportCount = 1,
            .pViewports = &viewport,
            .scissorCount = 1,
            .pScissors = &scissor
        };

        // -- DYNAMIC STATE --
#ifdef USE_DYNAMIC_STATES
        // A limited amount of the state that we've specified in the previous structs can actually
        // be changed without recreating the pipeline. Examples are the size of the viewport, line
        // width and blend constants. If you want to do that, then you'll have to fill in a
        // vk::DynamicState. This will cause the configuration of these values to be ignored and
        // you will be required to specify the data at drawing time.
        // But anyway you still need to recreate the swapchain when the window is resized.
        std::array<vk::DynamicState, 2> dynamicStates
        {
            // Dynamic Viewport: Can resize in command buffer
            // with vkCmdSetViewport(commandBuffer, 0, 1, &viewport);
            vk::DynamicState::eViewport,
            // Dynamic Scissor: Can resize in command buffer
            // with vkCmdSetScissor(commandBuffer, 0, 1, &scissor);
            vk::DynamicState::eScissor
        };

        vk::PipelineDynamicStateCreateInfo dynamicState
        {
            .dynamicStateCount = dynamicStates.size(),
            .pDynamicStates = dynamicStates.data()
        };
#endif

        // -- RASTERIZER --
        // Many features here if enabled require enabling certain device features (see PhysicalDeviceFeatures).
        vk::PipelineRasterizationStateCreateInfo rasterizerCreateInfo
        {
            .depthClampEnable = VK_FALSE,                  // when set to VK_TRUE: if fragment depth > max_depth then set depth = max_depth
            .rasterizerDiscardEnable = VK_FALSE,           // if set to VK_TRUE, then geometry never passes through the rasterizer stage (useful when you don't need to draw anything on the screen)
            .polygonMode = vk::PolygonMode::eFill,         // fill, lines (wireframe), points
            .cullMode = vk::CullModeFlagBits::eBack,       // don't draw back faces
            .frontFace = vk::FrontFace::eCounterClockwise, // which face is considered front
            .depthBiasEnable = VK_FALSE,                   // whether to add depth bias to fragments (good for stopping shadow acne)
            .lineWidth = 1.0f,                             // describes the thickness of lines in terms of number of fragments
        };

        // -- MULTISAMPLING --
        // It works by combining the fragment shader results of multiple polygons that rasterize to the same pixel.
        // This mainly occurs along edges, which is also where the most noticeable aliasing artifacts occur.
        vk::PipelineMultisampleStateCreateInfo multisamplingCreateInfo
        {
            .rasterizationSamples = vk::SampleCountFlagBits::e1, // number of samples to use per fragment
            .sampleShadingEnable = VK_FALSE                      // enable multisample shading or not
        };

        // -- BLENDING --

        // Blend attachment state contains the configuration for each attached color buffer.
        vk::PipelineColorBlendAttachmentState colorBlendingState
        {
            .blendEnable = VK_TRUE, // enable blending

            // Blending uses equation: (srcCoorBlendFactor * newColor) colorBlendOp (dstColorBlendFactor * oldColor)
            .srcColorBlendFactor = vk::BlendFactor::eSrcAlpha,
            .dstColorBlendFactor = vk::BlendFactor::eOneMinusSrcAlpha,
            .colorBlendOp = vk::BlendOp::eAdd,

            .srcAlphaBlendFactor = vk::BlendFactor::eOne,  // replace old alpha with the new one
            .dstAlphaBlendFactor = vk::BlendFactor::eZero, // get rid of the old alpha
            .alphaBlendOp = vk::BlendOp::eAdd,

            .colorWriteMask = vk::ColorComponentFlagBits::eR | vk::ColorComponentFlagBits::eG // which colors to apply blending operations to
                            | vk::ColorComponentFlagBits::eB | vk::ColorComponentFlagBits::eA,
        };

        // The PipelineColorBlendStateCreateInfo structure references the array of structures for all color attachments.
        vk::PipelineColorBlendStateCreateInfo colorBlendCreateInfo
        {
            .logicOpEnable = VK_FALSE, // alternative to calculations is to use logical operations
            // Each element of the pAttachments array is a VkPipelineColorBlendAttachmentState structure
            // specifying per-target blending state for each individual color attachment
            // of the subpass in which this pipeline is used.
            .attachmentCount = 1,
            .pAttachments = &colorBlendingState
        };

        // -- DEPTH STENCIL TESTING --
        vk::PipelineDepthStencilStateCreateInfo depthStencil
        {
            .depthTestEnable = VK_TRUE,  // Enable checking depth to determine fragment write
            .depthWriteEnable = VK_TRUE, // Enable writing to depth buffer (to replace old values)
            .depthCompareOp = vk::CompareOp::eLess, // If the new depth value is LESS it replaces the old one
            .depthBoundsTestEnable = VK_FALSE, // Depth bounds test: does the depth test fall into the bounds [minDepth...maxDepth]
            .stencilTestEnable = VK_FALSE,
            .minDepthBounds = 0.0f, // Optional
            .maxDepthBounds = 1.0f, // Optional
        };

        // -- GRAPHICS PIPELINE CREATION --
        vk::GraphicsPipelineCreateInfo pipelineInfo
        {
            .stageCount = shaderStageCreateInfos.size(), // Number of shader stages
            .pStages = shaderStageCreateInfos.data(), // List of shader stages
            .pVertexInputState = &vInputStateCreateInfo, // All the fixed function pipeline states
            .pInputAssemblyState = &inputAssemblyCreateInfo,
            .pViewportState = &viewportStateCreateInfo,
            .pRasterizationState = &rasterizerCreateInfo,
            .pMultisampleState = &multisamplingCreateInfo,
            .pDepthStencilState = &depthStencil,
            .pColorBlendState = &colorBlendCreateInfo,
#ifdef USE_DYNAMIC_STATES
            .pDynamicState = &dynamicState, // Optional
#endif
            .layout = pipelineLayout, // Pipeline layout pipeline should use
            .renderPass = renderPass, // Pipeline can be used with any render pass compatible with the provided one
            .subpass = 0, // Subpass of render pass to use with pipeline (a pipeline can only be attached to one particular subpass)
                          // Different subpass requires different pipeline.

            // Pipeline Derivatives: Can create multiple pipelines that derive from one another for optimization
            .basePipelineHandle = VK_NULL_HANDLE, // Existing pipeline to derive from...
            .basePipelineIndex = -1, // ... or index of pipeline being created to derive from (in case creating multiple at once)
        };
        vk::Pipeline pipeline = logicalDevice.createGraphicsPipeline(
            vk::PipelineCache(), pipelineInfo).value;

        // Destroy shader modules because they are no longer needed
        logicalDevice.destroyShaderModule(vertexShader);
        logicalDevice.destroyShaderModule(fragmentShader);

        return pipeline;
    }

    inline vk::Pipeline CreateGraphicsPipelinePostprocessing(
        const vk::Device& logicalDevice,
        const vk::PipelineLayout& pipelineLayout,
        const vk::RenderPass& renderPass,
        const vk::Extent2D& swapchainExtent,
        const std::string& vertexShaderFile,
        const std::string& fragmentShaderFile)
    {
        // Read in SPIR-V code of shaders
        auto vertexShaderCode = util::file::ReadAllBin(vertexShaderFile);
        auto fragmentShaderCode = util::file::ReadAllBin(fragmentShaderFile);

        // Build shader modules
        auto vertexShader = CreateShaderModule(logicalDevice, vertexShaderCode);
        auto fragmentShader = CreateShaderModule(logicalDevice, fragmentShaderCode);

        // Describe shader stages
        std::array<vk::PipelineShaderStageCreateInfo, 2> shaderStageCreateInfos
        {
            vk::PipelineShaderStageCreateInfo
            {
                .stage = vk::ShaderStageFlagBits::eVertex,
                .module = vertexShader,
                .pName = "main",               // shader entry point
                .pSpecializationInfo = nullptr // allows you to specify values for shader constants
            },
            vk::PipelineShaderStageCreateInfo
            {
                .stage = vk::ShaderStageFlagBits::eFragment,
                .module = fragmentShader,
                .pName = "main",               // shader entry point
                .pSpecializationInfo = nullptr // allows you to specify values for shader constants
            }
        };

        // -- VERTEX INPUT --
        // No vertex input
        vk::PipelineVertexInputStateCreateInfo vInputStateCreateInfo
        {
            .vertexBindingDescriptionCount = 0,
            .pVertexBindingDescriptions = nullptr,
            .vertexAttributeDescriptionCount = 0,
            .pVertexAttributeDescriptions = nullptr,
        };

        // -- INPUT ASSEMBLY --
        // Describes two things: what kind of geometry will be drawn
        // from the vertices and if primitive restart should be enabled
        vk::PipelineInputAssemblyStateCreateInfo inputAssemblyCreateInfo
        {
            .topology = vk::PrimitiveTopology::eTriangleList, // type of primitives to be assembled from vertices
            .primitiveRestartEnable = VK_FALSE                // relevant for strips and fans (to start new strip or fan)
        };

        // -- VIEWPORT & SCISSOR --
        // Viewports define the transformation from the image to the framebuffer
        vk::Viewport viewport
        {
            .x = 0.0f,
            .y = 0.0f,
            .width = static_cast<float>(swapchainExtent.width),
            .height = static_cast<float>(swapchainExtent.height),
            .minDepth = 0.0f,
            .maxDepth = 1.0f
        };

        // Any pixels outside the scissor rectangles will be discarded by the rasterizer
        vk::Rect2D scissor
        {
            .offset = { 0, 0 },
            .extent = swapchainExtent
        };

        // Viewport and scissor rectangle need to be combined into a viewport state.
        // It is possible to use multiple viewports and scissor rectangles on some graphics cards.
        vk::PipelineViewportStateCreateInfo viewportStateCreateInfo
        {
            .viewportCount = 1,
            .pViewports = &viewport,
            .scissorCount = 1,
            .pScissors = &scissor
        };

        // -- DYNAMIC STATE --
#ifdef USE_DYNAMIC_STATES
        // A limited amount of the state that we've specified in the previous structs can actually
        // be changed without recreating the pipeline. Examples are the size of the viewport, line
        // width and blend constants. If you want to do that, then you'll have to fill in a
        // vk::DynamicState. This will cause the configuration of these values to be ignored and
        // you will be required to specify the data at drawing time.
        // But anyway you still need to recreate the swapchain when the window is resized.
        std::array<vk::DynamicState, 2> dynamicStates
        {
            // Dynamic Viewport: Can resize in command buffer
            // with vkCmdSetViewport(commandBuffer, 0, 1, &viewport);
            vk::DynamicState::eViewport,
            // Dynamic Scissor: Can resize in command buffer
            // with vkCmdSetScissor(commandBuffer, 0, 1, &scissor);
            vk::DynamicState::eScissor
        };

        vk::PipelineDynamicStateCreateInfo dynamicState
        {
            .dynamicStateCount = dynamicStates.size(),
            .pDynamicStates = dynamicStates.data()
        };
#endif

        // -- RASTERIZER --
        // Many features here if enabled require enabling certain device features (see PhysicalDeviceFeatures).
        vk::PipelineRasterizationStateCreateInfo rasterizerCreateInfo
        {
            .depthClampEnable = VK_FALSE,                  // when set to VK_TRUE: if fragment depth > max_depth then set depth = max_depth
            .rasterizerDiscardEnable = VK_FALSE,           // if set to VK_TRUE, then geometry never passes through the rasterizer stage (useful when you don't need to draw anything on the screen)
            .polygonMode = vk::PolygonMode::eFill,         // fill, lines (wireframe), points
            .cullMode = vk::CullModeFlagBits::eBack,       // don't draw back faces
            .frontFace = vk::FrontFace::eCounterClockwise, // which face is considered front
            .depthBiasEnable = VK_FALSE,                   // whether to add depth bias to fragments (good for stopping shadow acne)
            .lineWidth = 1.0f,                             // describes the thickness of lines in terms of number of fragments
        };

        // -- MULTISAMPLING --
        // It works by combining the fragment shader results of multiple polygons that rasterize to the same pixel.
        // This mainly occurs along edges, which is also where the most noticeable aliasing artifacts occur.
        vk::PipelineMultisampleStateCreateInfo multisamplingCreateInfo
        {
            .rasterizationSamples = vk::SampleCountFlagBits::e1, // number of samples to use per fragment
            .sampleShadingEnable = VK_FALSE                      // enable multisample shading or not
        };

        // -- BLENDING --

        // Blend attachment state contains the configuration per attached framebuffer.
        vk::PipelineColorBlendAttachmentState colorBlendingState
        {
            .blendEnable = VK_TRUE, // enable blending

            // Blending uses equation: (srcCoorBlendFactor * newColor) colorBlendOp (dstColorBlendFactor * oldColor)
            .srcColorBlendFactor = vk::BlendFactor::eSrcAlpha,
            .dstColorBlendFactor = vk::BlendFactor::eOneMinusSrcAlpha,
            .colorBlendOp = vk::BlendOp::eAdd,

            .srcAlphaBlendFactor = vk::BlendFactor::eOne,  // replace old alpha with the new one
            .dstAlphaBlendFactor = vk::BlendFactor::eZero, // get rid of the old alpha
            .alphaBlendOp = vk::BlendOp::eAdd,

            .colorWriteMask = vk::ColorComponentFlagBits::eR | vk::ColorComponentFlagBits::eG // which colors to apply blending operations to
                            | vk::ColorComponentFlagBits::eB | vk::ColorComponentFlagBits::eA,
        };

        // The PipelineColorBlendStateCreateInfo structure references the array of structures for all of the framebuffers.
        vk::PipelineColorBlendStateCreateInfo colorBlendCreateInfo
        {
            .logicOpEnable = VK_FALSE, // alternative to calculations is to use logical operations
            // Each element of the pAttachments array is a VkPipelineColorBlendAttachmentState structure
            // specifying per-target blending state for each individual color attachment
            // of the subpass in which this pipeline is used.
            .attachmentCount = 1,
            .pAttachments = &colorBlendingState
        };

        // -- DEPTH STENCIL TESTING --
        // No writing to depth buffer
        vk::PipelineDepthStencilStateCreateInfo depthStencil
        {
            .depthWriteEnable = VK_FALSE
        };

        // -- GRAPHICS PIPELINE CREATION --
        vk::GraphicsPipelineCreateInfo pipelineInfo
        {
            .stageCount = shaderStageCreateInfos.size(), // Number of shader stages
            .pStages = shaderStageCreateInfos.data(), // List of shader stages
            .pVertexInputState = &vInputStateCreateInfo,     // All the fixed function pipeline states
            .pInputAssemblyState = &inputAssemblyCreateInfo,
            .pViewportState = &viewportStateCreateInfo,
            .pRasterizationState = &rasterizerCreateInfo,
            .pMultisampleState = &multisamplingCreateInfo,
            .pDepthStencilState = &depthStencil,
            .pColorBlendState = &colorBlendCreateInfo,
#ifdef USE_DYNAMIC_STATES
            .pDynamicState = &dynamicState, // Optional
#endif
            .layout = pipelineLayout, // Pipeline layout pipeline should use
            .renderPass = renderPass, // Pipeline can be used with any render pass compatible with the provided one
            .subpass = 1, // Subpass of render pass to use with pipeline (a pipeline can only be attached to one particular subpass)
                          // Different subpass requires different pipeline.

            // Pipeline Derivatives: Can create multiple pipelines that derive from one another for optimization
            .basePipelineHandle = VK_NULL_HANDLE, // Existing pipeline to derive from...
            .basePipelineIndex = -1, // ... or index of pipeline being created to derive from (in case creating multiple at once)
        };
        vk::Pipeline pipeline = logicalDevice.createGraphicsPipeline(
            vk::PipelineCache(), pipelineInfo).value;

        // Destroy shader modules because they are no longer needed
        logicalDevice.destroyShaderModule(vertexShader);
        logicalDevice.destroyShaderModule(fragmentShader);

        return pipeline;
    }

    class GraphicsPipeline
    {
        #pragma region Fields

        private:
            const vk::Device& m_LogicalDevice;

            vk::DescriptorSetLayout m_SamplerDescriptorSetLayout;
            vk::DescriptorSetLayout m_InputAttachmentsDescriptorSetLayout;
            vk::PushConstantRange m_PushConstantRange;
            vk::PipelineLayout m_PipelineLayout;
            vk::Pipeline m_GraphicsPipeline;

            bool m_IsDestroyed;

        #pragma endregion

        #pragma region Constructors

        public:
            template<typename vertex_type>
            static GraphicsPipeline Create(
                const vk::Device& logicalDevice,
                const vk::RenderPass& renderPass,
                const vk::Extent2D& swapchainExtent,
                const std::string& vertexShaderFile,
                const std::string& fragmentShaderFile)
            {
                vk::DescriptorSetLayout samplerDescriptorSetLayout
                    = CreateSamplerDescriptorSetLayout(logicalDevice);

                vk::PushConstantRange pushConstantRange
                    = CreatePushConstantRange();

                vk::PipelineLayout pipelineLayout
                    = CreatePipelineLayout(
                        logicalDevice,
                        std::vector<vk::DescriptorSetLayout>
                        {
                            samplerDescriptorSetLayout
                        },
                        pushConstantRange);

                return GraphicsPipeline(
                    logicalDevice,
                    samplerDescriptorSetLayout,
                    /*inputAttachmentsDescriptorSetLayout*/ vk::DescriptorSetLayout{},
                    pushConstantRange,
                    pipelineLayout,
                    CreateGraphicsPipeline<vertex_type>(
                        /*logicalDevice*/ logicalDevice,
                        /*pipelineLayout*/ pipelineLayout,
                        /*renderPass*/ renderPass,
                        /*swapchainExtent*/ swapchainExtent,
                        /*vertexShaderFile*/ vertexShaderFile,
                        /*fragmentShaderFile*/ fragmentShaderFile)
                );
            }

            static GraphicsPipeline CreatePostprocessing(
                const vk::Device& logicalDevice,
                const vk::RenderPass& renderPass,
                const vk::Extent2D& swapchainExtent,
                const std::string& vertexShaderFile,
                const std::string& fragmentShaderFile)
            {
                vk::DescriptorSetLayout inputAttachmentsDescriptorSetLayout
                    = CreateInputAttachmentsDescriptorSetLayout(logicalDevice);
               
                vk::PipelineLayout pipelineLayout = logicalDevice.createPipelineLayout(
                    vk::PipelineLayoutCreateInfo
                    {
                        .setLayoutCount = 1, // the number of different descriptor set layouts inside the shader program
                        .pSetLayouts = &inputAttachmentsDescriptorSetLayout // pointer to an array of descriptor set layouts
                    });

                return GraphicsPipeline(
                    logicalDevice,
                    /*samplerDescriptorSetLayout*/ vk::DescriptorSetLayout{},
                    inputAttachmentsDescriptorSetLayout,
                    vk::PushConstantRange{},
                    pipelineLayout,
                    CreateGraphicsPipelinePostprocessing(
                        /*logicalDevice*/ logicalDevice,
                        /*pipelineLayout*/ pipelineLayout,
                        /*renderPass*/ renderPass,
                        /*swapchainExtent*/ swapchainExtent,
                        /*vertexShaderFile*/ vertexShaderFile,
                        /*fragmentShaderFile*/ fragmentShaderFile)
                );
            }

            GraphicsPipeline(GraphicsPipeline&& rvalue) noexcept :
                m_LogicalDevice(rvalue.m_LogicalDevice),
                m_SamplerDescriptorSetLayout(rvalue.m_SamplerDescriptorSetLayout),
                m_InputAttachmentsDescriptorSetLayout(rvalue.m_InputAttachmentsDescriptorSetLayout),
                m_PushConstantRange(rvalue.m_PushConstantRange),
                m_PipelineLayout(rvalue.m_PipelineLayout),
                m_GraphicsPipeline(rvalue.m_GraphicsPipeline),
                m_IsDestroyed(false)
            {
                rvalue.m_IsDestroyed = true;
            }

            GraphicsPipeline& operator=(GraphicsPipeline&& rvalue) noexcept
            {
                util::Requires::That(&m_LogicalDevice == &rvalue.m_LogicalDevice, FUNCTION_INFO);
                m_SamplerDescriptorSetLayout = rvalue.m_SamplerDescriptorSetLayout;
                m_InputAttachmentsDescriptorSetLayout = rvalue.m_InputAttachmentsDescriptorSetLayout;
                m_PushConstantRange = rvalue.m_PushConstantRange;
                m_PipelineLayout = std::move(rvalue.m_PipelineLayout);
                m_GraphicsPipeline = std::move(rvalue.m_GraphicsPipeline);
                m_IsDestroyed = rvalue.m_IsDestroyed;
                rvalue.m_IsDestroyed = true;
                return *this;
            }

        private:
            GraphicsPipeline(
                const vk::Device& logicalDevice,
                const vk::DescriptorSetLayout& samplerDescriptorSetLayout,
                const vk::DescriptorSetLayout& inputAttachmentsDescriptorSetLayout,
                const vk::PushConstantRange& pushConstantRange,
                const vk::PipelineLayout& pipelineLayout,
                const vk::Pipeline& graphicsPipeline) :
                m_LogicalDevice(logicalDevice),
                m_SamplerDescriptorSetLayout(samplerDescriptorSetLayout),
                m_InputAttachmentsDescriptorSetLayout(inputAttachmentsDescriptorSetLayout),
                m_PushConstantRange(pushConstantRange),
                m_PipelineLayout(pipelineLayout),
                m_GraphicsPipeline(graphicsPipeline),
                m_IsDestroyed(false)
            {}

        #pragma endregion

        #pragma region Destructor

        public:
            void Destroy()
            {
                if (m_IsDestroyed)
                    return;
                m_IsDestroyed = true;

                m_LogicalDevice.destroyPipeline(m_GraphicsPipeline);
                m_LogicalDevice.destroyPipelineLayout(m_PipelineLayout);
                m_LogicalDevice.destroyDescriptorSetLayout(m_SamplerDescriptorSetLayout);
                m_LogicalDevice.destroyDescriptorSetLayout(m_InputAttachmentsDescriptorSetLayout);
            }

            ~GraphicsPipeline() { Destroy(); }

        #pragma endregion

        #pragma region Deleted Functions

        private:
            GraphicsPipeline(const GraphicsPipeline&) = delete;
            GraphicsPipeline& operator=(const GraphicsPipeline&) = delete;

        #pragma endregion

        #pragma region Properties

        public:
            const vk::DescriptorSetLayout& SamplerDescriptorSetLayout() const { return m_SamplerDescriptorSetLayout; }
            const vk::DescriptorSetLayout& InputAttachmentsDescriptorSetLayout() const { return m_InputAttachmentsDescriptorSetLayout; }
            const vk::PipelineLayout& PipelineLayout() const { return m_PipelineLayout; }
            const vk::Pipeline& Pipeline() const { return m_GraphicsPipeline; }

        #pragma endregion

        #pragma region Methods

        private:
            // Push constant is a value of a limited size (usually not greater than 128 bytes)
            // that is passed as part of commads when recording them into command buffer.
            // There can be only one push constant per shader stage.
            static vk::PushConstantRange CreatePushConstantRange()
            {
                return vk::PushConstantRange
                {
                    .stageFlags = vk::ShaderStageFlagBits::eVertex, // Shader stage push constant will go to
                    .offset = 0, // Offset into given data to pass to push constant
                    .size = sizeof(engine::mat4) // Size of data being passed
                };
            }

            // Descriptor set layout is a structure describing several (but not necessarily all)
            // uniform objects and texture samplers existing inside the shader program. Each descriptor
            // set is associated with a certain descriptor set layout. It's possible to have several
            // descriptor sets bound to the pipeline that have different descriptor set layouts (those
            // layouts are specifiedin the PipelineLayoutCreateInfo (see CreatePipelineLayout).
            // Two sets with the same layout are considered to be compatible and interchangeable [Sellers].
            static vk::DescriptorSetLayout CreateSamplerDescriptorSetLayout(
                const vk::Device& logicalDevice)
            {
                // Each uniform buffer or texture sampler has exactly one binding
                std::array<vk::DescriptorSetLayoutBinding, 1> layoutBindings
                {
                    // Texture sampler
                    vk::DescriptorSetLayoutBinding
                    {
                        .binding = 0, // Binding index in the shader
                        .descriptorType = vk::DescriptorType::eCombinedImageSampler,
                        .descriptorCount = 1, // It is possible for the shader variable to represent an array of uniform buffer objects, and descriptorCount specifies the number of values in the array
                        .stageFlags = vk::ShaderStageFlagBits::eFragment, // In which shader stages the descriptor is going to be referenced
                    }
                };

                // Descriptor set layout describes multiple uniform buffer objects and texture samplers
                // that exist inside the shader program.
                vk::DescriptorSetLayoutCreateInfo layoutInfo
                {
                    .bindingCount = layoutBindings.size(),
                    .pBindings = layoutBindings.data()
                };

                return logicalDevice.createDescriptorSetLayout(layoutInfo);
            }

            // Create input attachment image descriptor set layout
            static vk::DescriptorSetLayout CreateInputAttachmentsDescriptorSetLayout(
                const vk::Device& logicalDevice)
            {
                std::array<vk::DescriptorSetLayoutBinding, 2> layoutBindings
                {
                    // Color input binding
                    vk::DescriptorSetLayoutBinding
                    {
                        .binding = 0, // Binding index in the shader
                        .descriptorType = vk::DescriptorType::eInputAttachment,
                        .descriptorCount = 1, // It is possible for the shader variable to represent an array of uniform buffer objects, and descriptorCount specifies the number of values in the array
                        .stageFlags = vk::ShaderStageFlagBits::eFragment, // In which shader stages the descriptor is going to be referenced
                    },
                    // Depth input binding
                    vk::DescriptorSetLayoutBinding
                    {
                        .binding = 1, // Binding index in the shader
                        .descriptorType = vk::DescriptorType::eInputAttachment,
                        .descriptorCount = 1, // It is possible for the shader variable to represent an array of uniform buffer objects, and descriptorCount specifies the number of values in the array
                        .stageFlags = vk::ShaderStageFlagBits::eFragment, // In which shader stages the descriptor is going to be referenced
                    }
                };

                return logicalDevice.createDescriptorSetLayout(
                    vk::DescriptorSetLayoutCreateInfo
                    {
                        .bindingCount = layoutBindings.size(),
                        .pBindings = layoutBindings.data()
                    });
            }

            // -- PIPELINE LAYOUT --
            // Uniform values & texture samplers need to be specified during pipeline
            // creation by creating a PipelineLayout object because there may be multiple DescriptorSets
            // bound and Vulkan wants to know in advance how many and what types of them it should expect.
            // Access to descriptor sets from a pipeline is accomplished through a pipeline layout.
            // Zero or more descriptor set layouts and zero or more push constant ranges are combined
            // to form a pipeline layout object describing the complete set of resources that can be
            // accessed by a pipeline. The pipeline layout represents a sequence of descriptor sets
            // with each having a specific layout. This sequence of layouts is used to determine the
            // interface between shader stages and shader resources. Each pipeline is created using
            // a pipeline layout.
            static vk::PipelineLayout CreatePipelineLayout(
                const vk::Device& logicalDevice,
                const std::vector<vk::DescriptorSetLayout>& descriptorSetLayouts,
                const vk::PushConstantRange& pushConstantRange)
            {
                // It is actually possible to bind multiple descriptor sets simultaneously. You need to
                // specify a descriptor layout for each descriptor set when creating the pipeline layout.
                // Shaders can then reference specific descriptor sets like this:
                //     layout(set = 0, binding = 0) uniform UniformBufferObject { ... }
                // You can use this feature to put descriptors that vary per-object and descriptors
                // that are shared into separate descriptor sets. In that case you avoid rebinding most
                // of the descriptors across draw calls which is potentially more efficient.
                vk::PipelineLayoutCreateInfo pipelineLayoutCreateInfo
                {
                    .setLayoutCount = descriptorSetLayouts.size(), // the number of different descriptor set layouts inside the shader program
                    .pSetLayouts = descriptorSetLayouts.data(), // pointer to an array of descriptor set layouts

                    .pushConstantRangeCount = 1,
                    .pPushConstantRanges = &pushConstantRange
                };
                return logicalDevice.createPipelineLayout(pipelineLayoutCreateInfo);
            }

        #pragma endregion
    };
}