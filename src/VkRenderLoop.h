/*
Copyright (c) 2020 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

// STL
#include <array>
#include <limits>
#include <string>
#include <functional>

// Utility
#include "utility.h"
#include "VkUtility.h"

// VkEngine
#include "VkGraphicsContext.h"
#include "VulkanSwapchain.h"
#include "VkGraphicsPipeline.h"

namespace vulkan
{
    class VkRenderLoop
    {
        #pragma region Fields

        private:
            // how many images we can have submitted at once
            static constexpr size_t MAX_FRAME_DRAWS = 2;

        private:
            const VkGraphicsContext& m_VkGraphicsContext;
            const VulkanSwapchainBase* m_VulkanSwapchain;
            vk::CommandPool m_GraphicsCommandPool;
            std::vector<vk::CommandBuffer> m_GraphicsCommandBuffers;
            std::array<vk::Semaphore, MAX_FRAME_DRAWS> m_SemaphoreImageAvailable;
            std::array<vk::Semaphore, MAX_FRAME_DRAWS> m_SemaphoreRenderFinished;
            std::array<vk::Fence, MAX_FRAME_DRAWS> m_DrawFences;
            int m_CurrentFrame;
            bool m_IsDestroyed;

        #pragma endregion

        #pragma region Constructors

        public:
            VkRenderLoop(
                const VkGraphicsContext& vkGraphicsContext,
                const VulkanSwapchainBase& vulkanSwapchain) :
                m_VkGraphicsContext(vkGraphicsContext),
                m_VulkanSwapchain(&vulkanSwapchain),
                m_GraphicsCommandPool(CreateGraphicsCommandPool()),
                m_GraphicsCommandBuffers(CreateGraphicsCommandBuffers(m_VulkanSwapchain->NumberOfImages())),
                m_SemaphoreImageAvailable
                {
                    m_VkGraphicsContext.LogicalDevice().createSemaphore(vk::SemaphoreCreateInfo()),
                    m_VkGraphicsContext.LogicalDevice().createSemaphore(vk::SemaphoreCreateInfo())
                },
                m_SemaphoreRenderFinished
                {
                    m_VkGraphicsContext.LogicalDevice().createSemaphore(vk::SemaphoreCreateInfo()),
                    m_VkGraphicsContext.LogicalDevice().createSemaphore(vk::SemaphoreCreateInfo())
                },
                m_DrawFences
                {
                    m_VkGraphicsContext.LogicalDevice().createFence(vk::FenceCreateInfo { .flags = vk::FenceCreateFlagBits::eSignaled }),
                    m_VkGraphicsContext.LogicalDevice().createFence(vk::FenceCreateInfo { .flags = vk::FenceCreateFlagBits::eSignaled })
                },
                m_CurrentFrame(0),
                m_IsDestroyed(false)
            {}

            VkRenderLoop(VkRenderLoop&& rvalue) noexcept :
                m_VkGraphicsContext(rvalue.m_VkGraphicsContext),
                m_VulkanSwapchain(rvalue.m_VulkanSwapchain),
                m_GraphicsCommandPool(rvalue.m_GraphicsCommandPool),
                m_GraphicsCommandBuffers(std::move(rvalue.m_GraphicsCommandBuffers)),
                m_SemaphoreImageAvailable(std::move(rvalue.m_SemaphoreImageAvailable)),
                m_SemaphoreRenderFinished(std::move(rvalue.m_SemaphoreRenderFinished)),
                m_DrawFences(std::move(rvalue.m_DrawFences)),
                m_CurrentFrame(rvalue.m_CurrentFrame),
                m_IsDestroyed(false)
            {
                rvalue.m_IsDestroyed = true;
            }

        #pragma endregion

        #pragma region Destructor

        public:
            void Destroy()
            {
                if (m_IsDestroyed)
                    return;

                m_IsDestroyed = true;

                for (size_t i = 0; i < MAX_FRAME_DRAWS; i++)
                {
                    m_VkGraphicsContext.LogicalDevice().destroyFence(m_DrawFences[i]);
                    m_VkGraphicsContext.LogicalDevice().destroySemaphore(m_SemaphoreRenderFinished[i]);
                    m_VkGraphicsContext.LogicalDevice().destroySemaphore(m_SemaphoreImageAvailable[i]);
                }

                m_VkGraphicsContext.LogicalDevice().destroyCommandPool(m_GraphicsCommandPool);
            }

            ~VkRenderLoop() { Destroy(); }

        #pragma endregion

        #pragma region Deleted Functions

        private:
            VkRenderLoop(const VkRenderLoop&) = delete;
            VkRenderLoop& operator=(const VkRenderLoop&) = delete;

        #pragma endregion

        #pragma region Properties

        public:
            const VulkanSwapchainBase& Swapchain() const { return *m_VulkanSwapchain; }
            const vk::CommandPool& GraphicsCommandPool() const { return m_GraphicsCommandPool; }
            const std::vector<vk::CommandBuffer>& GraphicsCommandBuffers() const { return m_GraphicsCommandBuffers; }

        #pragma endregion

        #pragma region Methods

        private:
            vk::CommandPool CreateGraphicsCommandPool()
            {
                vk::CommandPoolCreateInfo commandPoolCreateInfo
                {
                    // This flag allows command buffer, created from the pool, to be rerecorded (reset) which is not allowed by default.
                    .flags = vk::CommandPoolCreateFlagBits::eResetCommandBuffer,
                    // If there is more than one hardware queue in the GPU hardware, as described by
                    // the physical device queue families, then the driver might need to allocate command
                    // buffer pools with different memory allocation attributes, specific to each GPU
                    // hardware queue. These details are handled for you by the driver as long as it knows
                    // the queue family containing the queue that the command buffer will use.
                    // https://vulkan.lunarg.com/doc/view/1.2.170.0/linux/tutorial/html/04-init_command_buffer.html
                    .queueFamilyIndex = m_VkGraphicsContext.GraphicsQueueFamilyIndex() // Queue Family type that buffers from this command pool will use
                };
                return m_VkGraphicsContext.LogicalDevice().createCommandPool(commandPoolCreateInfo);
            }

            // Because one of the drawing commands involves binding the right VkFramebuffer, we'll actually have
            // to record a command buffer for every image in the swap chain.
            // https://vulkan-tutorial.com/Drawing_a_triangle/Drawing/Command_buffers
            std::vector<vk::CommandBuffer> CreateGraphicsCommandBuffers(
                uint32_t numberOfSwapchainImages)
            {
                vk::CommandBufferAllocateInfo cbAllocInfo
                {
                    .commandPool = m_GraphicsCommandPool,
                    .level = vk::CommandBufferLevel::ePrimary, // Primary buffer is the one that you submit directly to queue and can't be called by other command buffers
                    .commandBufferCount = numberOfSwapchainImages
                };
                return m_VkGraphicsContext.LogicalDevice().allocateCommandBuffers(cbAllocInfo);
            }

        public:
            void RecreateSwapChain(
                const VulkanSwapchainBase& newVulkanSwapchain)
            {
                m_VulkanSwapchain = &newVulkanSwapchain;

                m_VkGraphicsContext.LogicalDevice().freeCommandBuffers(
                    m_GraphicsCommandPool, m_GraphicsCommandBuffers);

                m_GraphicsCommandBuffers = CreateGraphicsCommandBuffers(
                    m_VulkanSwapchain->NumberOfImages());
            }

            /// <summary>
            /// Acquires an image from the specified swapchain, performs rendering into it and
            /// presents it to the screen.
            /// </summary>
            /// <param name="swapchain">Swapchain to acquire image from.</param>
            /// <param name="prepareCommandBuffer">Function pointer that returns the command buffer
            /// that performs the rendering.</param>
            /// <returns>true if swapchain needs to be recreated</returns>
            bool Render(
                std::function<const vk::CommandBuffer&(uint32_t)> prepareCommandBuffer)
            {
                // Host program waits until fence is signaled ("opened")
                // i.e. until all submitted command buffers have completed execution.
                m_VkGraphicsContext.LogicalDevice().waitForFences(
                    1, &m_DrawFences[m_CurrentFrame],
                    VK_TRUE, // whether to wait for all of our fences
                    std::numeric_limits<uint64_t>::max() // timeout
                );
                // Reset ("close") fence
                m_VkGraphicsContext.LogicalDevice().resetFences(
                    1, &m_DrawFences[m_CurrentFrame]);

                // 1. Get next available image to draw to and set a semaphore that
                //    will be signaled when the image becomes available.
                uint32_t imageIndex;
                auto result = m_VkGraphicsContext.LogicalDevice().acquireNextImageKHR(
                    m_VulkanSwapchain->Swapchain(),
                    std::numeric_limits<uint64_t>::max(), // timeout
                    m_SemaphoreImageAvailable[m_CurrentFrame], // semaphore to signal when the image becomes available (semaphore synchronizes GPU<->GPU actions)
                    // The presentation engine may not have finished reading from the image at the time it is acquired,
                    // so the application must use semaphore and/or fence to ensure that the image layout and contents
                    // are not modified until the presentation engine reads have completed.
                    vk::Fence(), // a fence to signal (fence synchronizes GPU<->CPU actions)
                    &imageIndex);

                // A surface has changed in such a way that it is no longer compatible with the swapchain, and further
                // presentation requests using the swapchain will fail. Applications must query the new surface properties
                // and recreate their swapchain if they wish to continue presenting to the surface.
                // https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#vkAcquireNextImageKHR
                if (result == vk::Result::eErrorOutOfDateKHR // The surface is no longer compatible with the swapchain and the swapchain cannot be used to present to the surface
                    || result == vk::Result::eSuboptimalKHR) // A swapchain no longer matches the surface properties exactly, but can still be used to present to the surface successfully
                    return true;
                else if (result != vk::Result::eSuccess)
                    throw std::runtime_error("Failed to acquire swap chain image!");

                // 2. Prepare the command buffer (record commands if necessary)
                const vk::CommandBuffer& commandBuffer = prepareCommandBuffer(imageIndex);

                // 3. Submit command buffer to queue for execution, making sure it waits for the image
                //    to be signaled as available before drawing and signals it has finished rendering.
                std::array<vk::PipelineStageFlags, 1> waitStages
                {
                    vk::PipelineStageFlags(vk::PipelineStageFlagBits::eColorAttachmentOutput)
                };
                m_VkGraphicsContext.GraphicsQueue().submit(
                    vk::SubmitInfo
                    {
                        .waitSemaphoreCount = 1,
                        .pWaitSemaphores = &m_SemaphoreImageAvailable[m_CurrentFrame], // semaphores to wait before the command buffers begin execution
                        .pWaitDstStageMask = waitStages.data(), // array of pipeline stages at which each corresponding semaphore wait will occur

                        .commandBufferCount = 1,
                        .pCommandBuffers = &commandBuffer, // command buffers to submit

                        .signalSemaphoreCount = 1,
                        .pSignalSemaphores = &m_SemaphoreRenderFinished[m_CurrentFrame] // semaphores to signal when command buffer finishes
                    },
                    m_DrawFences[m_CurrentFrame] // a fence to be signaled once all submitted command buffers have completed execution
                );

                // 4. Present image to screen when it has signaled finished rendering.
                vk::PresentInfoKHR presentInfo
                {
                    .waitSemaphoreCount = 1,
                    .pWaitSemaphores = &m_SemaphoreRenderFinished[m_CurrentFrame], // semaphores to wait for before issuing the present request

                    .swapchainCount = 1,
                    .pSwapchains = &m_VulkanSwapchain->Swapchain(), // swapchains to present images to

                    .pImageIndices = &imageIndex
                };
                result = m_VkGraphicsContext.PresentationQueue().presentKHR(&presentInfo);
                if (result == vk::Result::eErrorOutOfDateKHR // The surface is no longer compatible with the swapchain and the swapchain cannot be used to present to the surface
                    || result == vk::Result::eSuboptimalKHR) // A swapchain no longer matches the surface properties exactly, but can still be used to present to the surface successfully
                    return true;
                else if (result != vk::Result::eSuccess)
                    throw std::runtime_error("Failed to present swap chain image!");

                m_CurrentFrame = (m_CurrentFrame + 1) % MAX_FRAME_DRAWS;

                return false;
            }

        #pragma endregion
    };
}