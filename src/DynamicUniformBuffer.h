#pragma once

#include "ConditionalCompilation.h"

// C-lib
#include <cmath>

// VkEngine
#include "UniformBuffer.h"

namespace vulkan
{
    // Dynamic uniform buffer is usually used for instanced drawing
    // where you have multiple models to draw that differ by their
    // uniform data (MVP matrices being the most common example).
    // In such case you place all uniforms of all models to a single
    // block of memory which is called "dynamic uniform buffer".
    // This block of memory must meet certain alignment requirements.
    //                            ***
    // Allocating multiple things into the same buffer is generally
    // a good idea in Vulkan. There are some very nice techniques that
    // can be done with that, like dynamic descriptors, that allow to
    // reuse the same descriptor set over and over again but every
    // time with a different offset into the buffer.
    // https://vkguide.dev/docs/chapter-4/descriptors_code_more/
    template <typename TData>
    class DynamicUniformBuffer final
    {
        #pragma region Typedefs

        public:
            using value_type = TData;

        #pragma endregion

        #pragma region Fields

        private:
            size_t m_Stride;
            size_t m_Count;
            void* m_Data;

            UniformBuffer m_UniformBuffer;
            bool m_NeedToFlush;

            bool m_IsDestroyed;

        #pragma endregion

        #pragma region Constructors

        public:
            DynamicUniformBuffer(
                const VkContext& vkContext,
                size_t objectsNumber) :
                m_Stride(CalcStride(vkContext.PhysicalDevice.getProperties().limits.minUniformBufferOffsetAlignment)),
                m_Count(objectsNumber),
                m_Data(::_aligned_malloc(objectsNumber * m_Stride, m_Stride)),
                m_UniformBuffer(vkContext, objectsNumber * m_Stride),
                m_NeedToFlush(false),
                m_IsDestroyed(false)
            {}

            DynamicUniformBuffer(DynamicUniformBuffer&& rvalue) noexcept :
                m_Stride(rvalue.m_Stride),
                m_Count(rvalue.m_Count),
                m_Data(rvalue.m_Data),
                m_UniformBuffer(std::move(rvalue.m_UniformBuffer)),
                m_NeedToFlush(rvalue.m_NeedToFlush),
                m_IsDestroyed(rvalue.m_IsDestroyed)
            {
                rvalue.m_IsDestroyed = true;
            }

        #pragma endregion

        #pragma region Deleted Functions

        private:
            DynamicUniformBuffer(const DynamicUniformBuffer&) = delete;
            DynamicUniformBuffer& operator=(const DynamicUniformBuffer&) = delete;

        #pragma endregion

        #pragma region Destructor

        public:
            void Destroy()
            {
                if (m_IsDestroyed)
                    return;

                m_IsDestroyed = true;

                ::_aligned_free(m_Data);
            }

            ~DynamicUniformBuffer()
            {
                Destroy();
            }

        #pragma endregion

        #pragma region Properties

        public:
            size_t Stride() const { return m_Stride; }
            size_t Count() const { return m_Count; }
            size_t Size() const { return m_UniformBuffer.Size(); }

            const vk::Buffer& Buffer() const { return m_UniformBuffer.Buffer(); }

            constexpr vk::DescriptorType DescriptorType() const { return vk::DescriptorType::eUniformBufferDynamic; }

            value_type& operator[](size_t i)
            {
                m_NeedToFlush = true;
                return *reinterpret_cast<value_type*>((size_t)m_Data + i * m_Stride);
            }

            const value_type& operator[](size_t i) const
            {
                return *reinterpret_cast<value_type*>((size_t)m_Data + i * m_Stride);
            }

        #pragma endregion

        #pragma region Methods

        private:
            static size_t CalcStride(
                size_t minUniformBufferOffsetAlignment)
            {
                return static_cast<size_t>(std::ceil(sizeof(value_type) / (float)minUniformBufferOffsetAlignment)) * minUniformBufferOffsetAlignment;
            }

        public:
            void Flush()
            {
                if (m_NeedToFlush)
                {
                    m_UniformBuffer.set(
                        /*offset*/ 0,
                        /*size*/ Size(),
                        /*srcData*/ m_Data);

                    m_NeedToFlush = false;
                }
            }

        #pragma endregion
    };
}