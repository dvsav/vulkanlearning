/*
Copyright (c) 2020 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#include "ConditionalCompilation.h"

#include "WinUtility.h"
#include <string>
#include <windows.h>

namespace win
{
    std::string GetLastErrorMessage()
    {
        DWORD dwLastError = ::GetLastError();
        const int sz = 256;
        char   lpBuffer[sz];

        ::FormatMessage(
            FORMAT_MESSAGE_FROM_SYSTEM, // It's a system error
            NULL,                       // No string to be formatted needed
            dwLastError,                // Hey Windows: Please explain this error!
            NULL,                       // Do it in the standard language
            lpBuffer,                   // Put the message here
            sz - 1,                     // Number of bytes to store the message
            NULL);

        return std::string(lpBuffer);
    };
}