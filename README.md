# VulkanLearning

VulkanLearning is an amateur Vulkan based API written in C++ for educational purposes.
For more information see [the project author's blog](https://dvsav.ru) if you can read in Russian.
The object model of this API looks like this:
![alt VulkanLearning object model](https://dvsav.ru/wp-content/uploads/2021/10/VulkanObjectModel6.jpg)

## Dependencies

[stb](https://github.com/nothings/stb)  
[Assimp](https://www.assimp.org)  
[FastDelegate](http://www.codeproject.com/cpp/FastDelegate.asp)
[JSON for Modern C++](https://github.com/nlohmann/json)

## Prerequisites

To build VulkanLearning you will need the following software:  
[Windows 7 or newer](https://www.microsoft.com/windows/) (required)  
[Microsoft Visual Studio 2019](https://visualstudio.microsoft.com/) (required)  
[git](https://git-scm.com/) (optional)  

## Getting Started

Launch git command line interface. In the command prompt go to the folder to which you want to copy the VulkanLearning source code.  
Type: `git clone https://bitbucket.org/dvsav/vulkanlearning.git` to download the source code on your computer.  

Launch Visual Studio 2019, open [VulkanLearning.sln](build/vs2019/VulkanLearning.sln) and build the project from there.  

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
